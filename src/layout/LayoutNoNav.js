import React from 'react';
import {
  Button,
  Container,
  Column,
  Columns,
  Icon,
  Section
} from 'shaper-react';

const LayoutNoNav = ({ breadcrumb, addShortcut, ...props }) => {
  return (
    <>
      <Section
        className="py-0 is-flex is-align-items-center has-elevation-1 is-hidden-touch"
        style={{ minHeight: '60px' }}
      >
        <Container className="is-flex-1">
          <Columns>
            <Column className="is-flex is-align-items-center">
              {breadcrumb}
            </Column>
            {addShortcut && (
              <Column narrow className="px-4">
                <Button small flat>
                  <Icon>quicklinks</Icon>
                  <span>Add to shortcuts</span>
                </Button>
              </Column>
            )}
            <Column narrow style={{ width: '290px' }} />
          </Columns>
        </Container>
      </Section>
      {props.children}
    </>
  );
};

export default LayoutNoNav;
