import React from 'react';
import PortfolioNav from '../components/SubNavs/PortfolioNav';
import {
  Button,
  Container,
  Column,
  Columns,
  Icon,
  Section
} from 'shaper-react';

const LayoutPortfolio = ({ breadcrumb, ...props }) => {
  return (
    <div id={`portfolio`}>
      <PortfolioNav />
      <Section className="py-4 is-hidden-touch">
        <Container>
          <Columns gapless>
            <Column className="mb-0 ">{breadcrumb}</Column>
            <Column className="mb-0 has-text-right">
              <Button small flat className="mb-0">
                <Icon>quicklinks</Icon>
                <span>Add to shortcuts</span>
              </Button>
            </Column>
          </Columns>
        </Container>
      </Section>
      {props.children}
    </div>
  );
};

export default LayoutPortfolio;
