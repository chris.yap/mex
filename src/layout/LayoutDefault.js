import React from 'react';
import { Breakpoints } from 'shaper-react';
import { getState } from '../context/StateProvider';
import SiteNavs from '../components/SiteNavs';
import Overlay from '../components/Overlay';

const LayoutDefault = ({ activeBreakpoints, breadcrumb, subnav, ...props }) => {
  const [{ orderPad, orderSummary }, dispatch] = getState();
  return (
    <div
      id={`layout`}
      style={{
        paddingTop: activeBreakpoints.below.md ? '50px' : '128px',
        minHeight: 'calc(100vh - 180px)'
      }}>
      <SiteNavs breadcrumb={breadcrumb} subnav={subnav} />

      <Overlay
        isOpened={orderPad.isOpened || orderSummary.isOpened}
        isTransparent
        isClear
        zIndex="100"
        onClick={e =>
          dispatch({
            type: orderPad.isOpened
              ? 'toggleOrderPad'
              : orderSummary.isOpened
              ? 'toggleOrderSummary'
              : null
          })
        }
      />
      <main className="site-content">{props.children}</main>
    </div>
  );
};

export default Breakpoints(LayoutDefault);
