import React from 'react';
import { StateProvider } from './context/StateProvider';
// Reducers
import overlayReducer from './context/reducers/overlayReducer';
import loginReducer from './context/reducers/loginReducer';
import floatingNavReducer from './context/reducers/floatingNavReducer';
import mobileNavReducer from './context/reducers/mobileNavReducer';
import searchReducer from './context/reducers/searchReducer';
import orderPadReducer from './context/reducers/orderPadReducer';
import orderSummaryReducer from './context/reducers/orderSummaryReducer';
import conditionalOrderReducer from './context/reducers/conditionalOrderReducer';
import alertReducer from './context/reducers/alertReducer';
import otpReducer from './context/reducers/otpReducer';
import cashTransferReducer from './context/reducers/cashTransferReducer';
import { BrowserRouter } from 'react-router-dom';

import Main from './Main';

const App = () => {
  const initialState = {
    login: {
      isLogin: true,
      resetPassword: false,
      passwordSequence: 1
    },
    orderPad: {
      isOpened: false,
      overlay: false,
      errorHandling: true,
      buySell: 0,
      help: 0,
      summary: false,
      moreInfo: false,
      searchBox: true,

      coOverlay: false,
      conditionalOrder: false,
      placeConditionalOrder: false,
      conditionalOrderConfirmation: false,

      type: 'buy',
      orderType: 'limit',
      duration: 'until cancelled',
      amountType: 'qty',
      limitPrice: null,
      aud: null
    },
    orderSummary: {
      isOpened: false,
      orderConfirmation: false,
      orderSubmit: false
    },
    conditionalOrder: {
      conditionalOrders: [],
      coExpand: false,
      condition: 'stopentry',
      triggerType: 'fixed',
      trigger: { value: '$', label: '$' },
      ifLastPrice: 'smallerEqual',
      trailLastPrice: 'above'
    },
    floatingNav: {
      hover: false,
      hide: false
    },
    mobileNav: {
      isOpened: false,
      openSub: null
    },
    searchBar: {
      isOpened: false,
      selectedStock: {},
      searchValue: '',
      searchSelection: ''
    },
    alert: {
      flyIn: false
    },
    overlay: {
      isActive: false
    },
    cash: {
      expandedAccount: null,
      accountOpenedTab: 0,
      isOpened: false,
      step: 0,
      transferTarget: '',
      selectedAccount: '',
      transferType: null,
      defaultFromAccount: null,
      defaultToAccount: null,
      newTransfer: true,
      changeLimit: false,
      modalView: null,
      submissionError: false
    },
    otp: {
      isOpened: false
    }
  };

  const mainReducer = (
    {
      alert,
      cash,
      conditionalOrder,
      floatingNav,
      login,
      mobileNav,
      orderPad,
      orderSummary,
      otp,
      overlay,
      searchBar
    },
    action
  ) => ({
    conditionalOrder: conditionalOrderReducer(conditionalOrder, action),
    floatingNav: floatingNavReducer(floatingNav, action),
    login: loginReducer(login, action),
    mobileNav: mobileNavReducer(mobileNav, action),
    orderPad: orderPadReducer(orderPad, action),
    orderSummary: orderSummaryReducer(orderSummary, action),
    overlay: overlayReducer(overlay, action),
    searchBar: searchReducer(searchBar, action),
    alert: alertReducer(alert, action),
    otp: otpReducer(otp, action),
    cash: cashTransferReducer(cash, action)
  });

  return (
    <StateProvider initialState={initialState} reducer={mainReducer}>
      <BrowserRouter>
        <Main />
      </BrowserRouter>
    </StateProvider>
  );
};

export default App;
