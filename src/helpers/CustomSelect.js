import React from 'react';
import { getState } from '../context/StateProvider';
import { Button, Columns, Column, Subtitle, Title } from 'shaper-react';

const AccountSingleValue = ({ data, ...props }) => (
  <div className="px-1">
    <Title size="5" className="mb-0 has-text-ellipsis">
      {data.label}
    </Title>
    <Subtitle size="7" className="my-0 has-text-uppercase">
      {data.value}
    </Subtitle>
  </div>
);

const AccountOption = ({
  data,
  innerRef,
  innerProps,
  isDisabled,
  isFocused,
  isSelected
}) =>
  !isDisabled ? (
    <div
      ref={innerRef}
      {...innerProps}
      className={`nab-select__option px-4 py-2 mb-0 ${
        isFocused ? 'nab-select__option--is-focused' : ''
      } ${isSelected ? 'nab-select__option--is-selected' : ''}`}>
      <Title size="5" className="mb-0 has-text-ellipsis">
        {data.label}
      </Title>
      <Subtitle size="7" className="my-0 has-text-uppercase">
        {data.value}
      </Subtitle>
    </div>
  ) : null;

const SecurityOption = ({
  data,
  innerRef,
  innerProps,
  commonProps,
  isDisabled,
  isFocused,
  isSelected
}) => {
  return !isDisabled ? (
    <div
      ref={innerRef}
      {...innerProps}
      className={`nab-select__option px-4 py-2 mb-0 ${
        isFocused ? 'nab-select__option--is-focused' : ''
      } ${isSelected ? 'nab-select__option--is-selected' : ''}`}>
      <Title size="5" className="mb-0 has-text-ellipsis">
        {data.label}
      </Title>
      <Subtitle size="7" className="my-0 has-text-uppercase">
        {data.code} | {data.exchange}
      </Subtitle>
    </div>
  ) : (
    <div
      ref={innerRef}
      {...innerProps}
      className={`nab-select__option px-4 py-2 mb-0`}
      style={{ opacity: '.5', pointerEvents: 'none' }}>
      <Title
        size="6"
        className="has-text-centered has-text-danger has-text-ellipsis mb-0">
        {data.label}
      </Title>
    </div>
  );
};

const SecurityOptionTrade = ({
  data,
  innerRef,
  innerProps,
  isDisabled,
  isFocused,
  isSelected
}) => {
  const [dispatch] = getState();
  return (
    <React.Fragment>
      {!isDisabled ? (
        <div
          ref={innerRef}
          {...innerProps}
          className={`nab-select__option px-4 py-2 mb-0 ${
            isFocused ? 'nab-select__option--is-focused' : ''
          } ${isSelected ? 'nab-select__option--is-selected' : ''}`}>
          <Columns mobile>
            <Column className="is-overflow-hidden">
              <Title size="5" className="mb-0 has-text-ellipsis">
                {data.label}
              </Title>
              <Subtitle size="7" className="my-0 has-text-uppercase">
                {data.code} | {data.exchange}
              </Subtitle>
            </Column>
            <Column narrow className="is-flex is-align-items-center px-2">
              <Button
                small
                primary
                className="mr-4"
                onClick={() => {
                  dispatch({ type: 'orderPadSelectOption', payload: data });
                  dispatch({ type: 'toggleOrderPad' });
                }}>
                Trade
              </Button>
            </Column>
          </Columns>
        </div>
      ) : null}
    </React.Fragment>
  );
};

export {
  AccountOption,
  AccountSingleValue,
  SecurityOption,
  SecurityOptionTrade
};
