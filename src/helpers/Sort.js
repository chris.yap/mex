const sortTable = (field, ascending, primer) => {
	var key = primer
		? function(x) {
				return primer(x[field]);
		  }
		: function(x) {
				return x[field];
		  };

	ascending = !ascending ? 1 : -1;

	return function(a, b) {
		return (a = key(a)), (b = key(b)), ascending * ((a > b) - (b > a));
	};
};

export default sortTable;
