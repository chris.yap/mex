const Breadcrumbs = {
  portfolio: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Portfolio', link: '/portfolio', active: true }
    ],
    personal: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Portfolio', link: '/portfolio' },
        { name: 'Personal', link: '/portfolio/personal', active: true }
      ]
    },
    openOrders: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Portfolio', link: '/portfolio' },
        { name: 'Open orders', link: '/portfolio/open-orders', active: true }
      ]
    },
    orderHistory: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Portfolio', link: '/portfolio' },
        {
          name: 'Order history',
          link: '/portfolio/order-history',
          active: true
        }
      ]
    },
    orderDetail: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Portfolio', link: '/portfolio' },
        { name: 'Order history', link: '/portfolio/order-history' },
        { name: 'Order detail', link: '/portfolio/order-detail', active: true }
      ]
    }
  },
  markets: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Markets', link: '/markets-insights', active: true }
    ],
    news: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Markets', link: '/markets-insights' },
        { name: 'News', link: '/markets-insights/news', active: true }
      ]
    },
    asxAnnouncements: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Markets', link: '/markets-insights' },
        {
          name: 'ASX announcements',
          link: '/markets-insights/announcements',
          active: true
        }
      ]
    }
  },
  watchlist: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Watchlists', link: '/watchlists', active: true }
    ]
  },
  alerts: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Alerts', link: '/alerts', active: true }
    ]
  },
  account: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Account', link: '/account', active: true }
    ]
  },
  support: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Support', link: '/support', active: true }
    ],
    forms: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Support', link: '/support' },
        { name: 'Forms', link: '/support/forms', active: true }
      ]
    },
    faq: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Support', link: '/support' },
        { name: 'F.A.Q.', link: '/support/faq', active: true }
      ]
    },
    howto: {
      items: [
        { name: 'Home', link: '/' },
        { name: 'Support', link: '/support' },
        { name: "How to's", link: '/support/how-to', active: true }
      ]
    }
  },
  investmentOptions: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Investment options', link: '/investment-options', active: true }
    ]
  },
  payeeList: {
    items: [
      { name: 'Home', link: '/' },
      { name: 'Payee list', link: '/payee-list', active: true }
    ]
  }
};

export { Breadcrumbs };
