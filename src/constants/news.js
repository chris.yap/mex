const News = [
	{
		title: 'BUZZ - Australian miners slip as iron ore rally loses steam, broader index falls too',
		date: ' 11 04 2019 02:51pm AEST',
	},
	{
		title: 'UPDATE 1 - Nearly 2,000 Zambian villagers can sue Vedanta in England - Supreme Court',
		date: ' 10 04 2019 08:29pm AEST',
	},
	{
		title: "BRIEF - Rio Tinto Says Resolution's Final Environmental Permit Still Expected By Summer 2021",
		date: ' 09 04 2019 11:58pm AEST',
	},
	{
		title:
			'Rio Tinto PLC says resolution copper project in Arizona set to file draft environmental study by this summer',
		date: ' 09 04 2019 11:57pm AEST',
	},
	{
		title: "UPDATE 1 - Australia's heavy-sweet crudes surge in preparation for IMO 2020",
		date: ' 09 04 2019 05:57pm AEST',
	},
	{
		title: 'UPDATE 2 - European shares dip, banks and Boeing suppliers in focus',
		date: ' 09 04 2019 02:50am AEST',
	},
	{
		title: "Australia's heavy-sweet crudes surge in preparation for IMO 2020",
		date: ' 08 04 2019 07:29pm AEST',
	},
	{
		title: "Australia's heavy-sweet crudes surge in preparation for IMO 2020",
		date: ' 08 04 2019 07:18pm AEST',
	},
	{
		title: 'European shares fall as tech, auto stocks weigh',
		date: ' 08 04 2019 05:56pm AEST',
	},
	{
		title: 'UPDATE 1 - UK Stocks-Factors to watch on April 8',
		date: ' 08 04 2019 05:12pm AEST',
	},
	{
		title: 'UK Stocks - Factors to watch on April 08',
		date: ' 08 04 2019 03:43pm AEST',
	},
	{
		title: 'PRESS DIGEST - New York Times business news - April 8',
		date: ' 08 04 2019 03:06pm AEST',
	},
	{
		title: 'BHP Group to cut more than 700 jobs - report',
		date: ' 06 04 2019 04:14pm AEDT',
	},
	{
		title: 'BHP Group to cut more than 700 jobs - report',
		date: ' 06 04 2019 03:56pm AEDT',
	},
	{
		title: 'MEDIA-BHP poised to cut jobs as streamlining plan progresses - Bloomberg',
		date: ' 06 04 2019 02:48pm AEDT',
	},
	{
		title: 'CRU-CESCO-BHP Americas boss says miners must make copper greener',
		date: ' 06 04 2019 04:45am AEDT',
	},
	{
		title: 'UPDATE 1 - Ethical investors ask miners to publish tailings dam details',
		date: ' 06 04 2019 04:00am AEDT',
	},
	{
		title: 'Ethical investors ask miners to publish tailings dam details',
		date: ' 05 04 2019 10:13pm AEDT',
	},
	{
		title: "Australia's Port Hedland iron ore shipments to China slip 8 percent in March",
		date: ' 05 04 2019 07:17pm AEDT',
	},
	{
		title: "Australia's Port Hedland iron ore shipments to China slip 8 pct in March",
		date: ' 05 04 2019 06:32pm AEDT',
	},
];

export { News };
