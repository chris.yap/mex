const Announcements = [
	{
		title: "Initial Director's Interest Notice",
		date: ' 03 04 2019 04:18pm AEDT',
		type: 'Security Holder Details',
	},
	{
		title: "Initial Director's Interest Notice",
		date: ' 03 04 2019 04:17pm AEDT',
		type: 'Security Holder Details',
		sensitive: true,
	},
	{
		title: 'Tropical Cyclone Veronica - WAIO Update',
		date: ' 02 04 2019 08:26am AEDT',
		type: 'Progress Report',
	},
	{
		title: 'Update - Dividend/Distribution - BHP',
		date: ' 29 03 2019 08:22am AEDT',
		type: 'Dividend Announcement',
	},
	{
		title: 'Board appointments',
		date: ' 19 03 2019 04:35pm AEDT',
		type: 'Company Administration',
		sensitive: true,
	},
	{
		title: 'Potential targets identified at Torrens East',
		date: ' 14 03 2019 10:02am AEDT',
		type: 'Progress Report',
	},
	{
		title: 'Update - Dividend/Distribution - BHP',
		date: ' 12 03 2019 08:30am AEDT',
		type: 'Dividend Announcement',
	},
	{
		title: 'Notice of Dividend Currency Exchange Rates',
		date: ' 12 03 2019 08:30am AEDT',
		type: 'Dividend Announcement',
	},
	{
		title: 'Executive Leadership Team',
		date: ' 28 02 2019 08:46am AEDT',
		type: 'Company Administration',
		sensitive: true,
	},
	{
		title: 'BHP Group Plc Notification of Major Interest in Shares',
		date: ' 22 02 2019 08:18am AEDT',
		type: 'Security Holder Details',
		sensitive: true,
	},
	{
		title: 'Dividend/Distribution - BHP',
		date: ' 19 02 2019 04:28pm AEDT',
		type: 'Dividend Announcement',
	},
	{
		title: 'BHP Interim Results Presentation',
		date: ' 19 02 2019 04:22pm AEDT',
		type: 'Periodic Reports',
	},
	{
		title: 'Half Yearly Report and Accounts',
		date: ' 19 02 2019 04:21pm AEDT',
		type: 'Periodic Reports, Dividend Announcement',
	},
	{
		title: 'Conventional petroleum update',
		date: ' 13 02 2019 04:18pm AEDT',
		type: 'Progress Report',
	},
	{
		title: 'Samarco debt negotiations update',
		date: ' 29 01 2019 09:41am AEDT',
		type: 'Progress Report',
	},
	{
		title: 'Company Secretary changes',
		date: ' 22 01 2019 06:30pm AEDT',
		type: 'Company Administration',
	},
	{
		title: 'Quarterly Activities Report',
		date: ' 22 01 2019 08:33am AEDT',
		type: 'Quarterly Activities Report',
		sensitive: true,
	},
	{
		title: 'Update - Dividend/Distribution - BHP',
		date: ' 14 01 2019 08:22am AEDT',
		type: 'Dividend Announcement',
	},
	{
		title: 'Notice of Dividend Currency Exchange Rates',
		date: ' 14 01 2019 08:21am AEDT',
		type: 'Dividend Announcement',
	},
];

export { Announcements };
