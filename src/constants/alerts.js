const AlertItems = [
  {
    code: 'Z1P',
    name: 'Zip Co Limited',
    exchange: 'ASX',
    dateUnformatted: '20190501',
    date: '01-05-2019',
    desc: 'Price drops below $3.40 for Z1P:ASX at 12.40PM AEST',
    type: 'Company',
    delivery: [{ icon: 'contact', label: 'Email' }],
    unread: true
  },
  {
    code: 'Z1P',
    name: 'Zip Co Limited',
    exchange: 'ASX',
    dateUnformatted: '20190430',
    date: '30-04-2019',
    desc: 'Price rise above $3.50',
    type: 'Company',
    delivery: [
      { icon: 'contact', label: 'Email' },
      { icon: 'sms', label: 'SMS' }
    ]
  },
  {
    code: 'BHP',
    name: 'BHP Group Ltd',
    exchange: 'ASX',
    dateUnformatted: '20190501',
    date: '01-05-2019',
    desc: 'Price drops below $34.40',
    type: 'Company',
    delivery: [{ icon: 'contact', label: 'Email' }],
    unread: true
  },
  {
    code: 'BHP',
    name: 'BHP Group Ltd',
    exchange: 'ASX',
    dateUnformatted: '20190430',
    date: '30-04-2019',
    desc: 'Price drops below $35.50 for BHP:ASX at 11.46AM AEST',
    type: 'Company',
    delivery: [
      { icon: 'contact', label: 'Email' },
      { icon: 'sms', label: 'SMS' }
    ]
  }
];

const AlertSettings = [
  {
    code: 'Z1P',
    name: 'Zip Co Limited',
    exchange: 'ASX',
    type: 'Company',
    conditions: [
      {
        label: 'Price reaches a new <strong>52-week high</strong>',
        condition: 'high',
        active: true
      },
      {
        label: 'Price reaches a new <strong>52-week low</strong>',
        condition: 'low',
        active: true
      },
      {
        label: 'Price <strong>drops below</strong>',
        condition: 'drops',
        value: ['3.50', '3.40'],
        active: true
      },
      {
        label: 'Price <strong>rises above</strong>',
        condition: 'rises',
        value: ['3.60'],
        active: true
      },
      {
        label: 'ASX <strong>price sensitive</strong> announcements',
        condition: 'asx',
        active: true
      }
    ],
    delivery: ['email', 'sms']
  },
  {
    code: 'BHP',
    name: 'BHP Group Ltd',
    exchange: 'ASX',
    conditions: [
      {
        label: 'Price reaches a new <strong>52-week high</strong>',
        condition: 'high',
        active: false
      },
      {
        label: 'Price reaches a new <strong>52-week low</strong>',
        condition: 'low',
        active: false
      },
      {
        label: 'Price <strong>drops below</strong>',
        condition: 'drops',
        value: ['35.00'],
        active: true
      },
      {
        label: 'Price <strong>rises above</strong>',
        condition: 'rises',
        value: ['3.60'],
        active: false
      },
      {
        label: 'ASX <strong>price sensitive</strong> announcements',
        condition: 'asx',
        active: false
      }
    ],
    type: 'Company',
    delivery: ['email']
  }
];

export { AlertItems, AlertSettings };
