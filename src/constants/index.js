const MainNav = [
  { label: 'Portfolio', to: '/portfolio' },
  { label: 'Markets & insights', to: '/markets-insights' },
  { label: 'Investment options', to: '/investment-options' },
  { label: 'Support', to: '/support' }
];

const ShortcutItems = [{ label: 'Cash transfers' }];

const PortfolioItems = [
  { label: 'Open orders', to: '/portfolio/open-orders' },
  { label: 'Order history', to: '/portfolio/order-history' },
  { label: 'Place order', onClick: 'toggleOrderPad' }
  // { label: 'Statements & reports', to: '/portfolio/statements-reports' }
];

const MarketsInsightsItems = [
  { label: 'Markets', to: '/markets-insights' },
  { label: 'News', to: '/markets-insights/news' },
  { label: 'ASX Announcements', to: '/markets-insights/announcements' }
  // { label: 'Research & reports', to: '/markets-insights/research-reports' },
];

const InvestmentOptionsItems = [
  { label: 'Investment types', to: '/investment-options/types' },
  { label: 'Products', to: '/investment-options/products' }
];

const SupportItems = [
  { label: 'Forms', to: '/support/forms' },
  { label: 'F.A.Q.', to: '/support/faq' },
  { label: "How to's", to: '/support/how-to' }
];

const SettingsItems = [
  { label: 'Change password', to: '/account' },
  {
    label: 'Communication preferences',
    to: '/settings/communication-preferences'
  },
  { label: 'Alerts & notifications', to: '/settings/alerts-notifications' }
];

const CashItems = [{ label: 'Payee list', to: '/payee-list' }];

const FooterItems = [
  { label: 'Important notice', to: '/#' },
  { label: 'nabtrade Financial Services Guide', to: '/#' },
  { label: 'Terms of use', to: '/#' },
  { label: 'NAB privacy policy', to: '/#' },
  { label: 'nab.com.au', to: '/#' },
  { label: 'Security alerts', to: '/#' },
  { label: 'View full site', to: '/#' }
];

const SocialItems = [
  { label: 'twitter', url: 'https://twitter.com/nabtrade' },
  {
    label: 'youtube',
    url: 'https://www.youtube.com/user/nabtrade'
  },
  {
    label: 'linkedin',
    url: 'https://www.linkedin.com/company/nabtrade'
  }
];

export {
  ShortcutItems,
  PortfolioItems,
  MarketsInsightsItems,
  InvestmentOptionsItems,
  SupportItems,
  SettingsItems,
  MainNav,
  FooterItems,
  SocialItems,
  CashItems
};
