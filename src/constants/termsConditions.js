const termsConditionsItems = [
  {
    title: 'Contact Us',
    url: 'https://www.nabtrade.com.au/investor/support',
    type: 'link'
  },
  {
    title: 'Terms of Use',
    url: 'https://www.nabtrade.com.au/terms-of-use',
    type: 'link'
  },
  {
    title: 'NAB Privacy Policy',
    url: 'https://www.nab.com.au/common/privacy-policy',
    type: 'link'
  },
  {
    title: 'Financial Services Guide',
    url:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Financial_Services_Guide.pdf',
    type: 'pdf'
  },
  {
    title: 'Important Notice',
    url:
      'https://www.nabtrade.com.au/investor/terms-and-conditions/Important-information',
    type: 'link'
  },
  {
    title: 'General Advice Warning',
    url: 'https://www.nabtrade.com.au/terms-of-use#section2',
    type: 'link'
  },
  {
    title: 'nabtrade Client Agreement',
    url:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Client_Agreement.pdf',
    type: 'pdf'
  },
  {
    title: 'Best Execution Policy',
    url:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Best_Execution_Policy.pdf',
    type: 'pdf'
  },
  {
    title: 'NAB Cookie Statement',
    url: 'https://www.nab.com.au/about-us/using-this-site/cookies',
    type: 'link'
  },
  {
    title: 'Get a nabtrade account now',
    url: 'https://dev1.nabtradedev.com.au/onboarding',
    type: 'nabtrade'
  }
];

export { termsConditionsItems };
