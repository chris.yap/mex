const Portfolios = [
  {
    name: 'Personal Portfolio',
    hin: '123456789',
    link: '/portfolio/personal',
    alert: 4,
    funds: '100,000.00',
    value: '90,000.00',
    change: '11.32',
    gainloss: '1.24'
  },
  {
    name: 'Company Portfolio',
    hin: '123456789',
    link: '/portfolio/company',
    alert: 5,
    funds: '80,000.00',
    value: '150,000.00',
    change: '-2.45',
    gainloss: '-0.24'
  },
  {
    name: 'SMSF Portfolio',
    hin: '123456789',
    link: '/portfolio/smsf',
    alert: 2,
    funds: '250,000.00',
    value: '300,000.00',
    change: '11.32',
    gainloss: '-0.24'
  },
  {
    name: 'NAB accounts',
    // link: '/nab-accounts',
    logo: 'nabtrade',
    funds: '50,000.00',
    total: '55,000.00'
  }
];

const PersonalAccounts = [
  {
    name: 'Investments',
    desc: 'Australian and international',
    alerts: 5,
    cash: '10,000.00',
    value: '100,000.00',
    change: '13.00',
    gainloss: '-1.5'
  },
  {
    name: 'Cash accounts',
    alerts: 3,
    cash: '220,000.00',
    balance: '370,000.00'
  }
  // {
  //   name: 'Margin loan investments',
  //   security: '100,000.00',
  //   cash: '100,000.00',
  //   today: '10.00',
  //   total: '-0.5'
  // },
  // {
  //   name: 'Margin loan',
  //   alerts: 2,
  //   funds: '100,000.00',
  //   security: '100,000.00',
  //   cash: '100,000.00'
  // },
  // {
  //   name: 'Other assets',
  //   alerts: 2,
  //   funds: '100,000.00',
  //   security: '100,000.00',
  //   cash: '100,000.00'
  // },
  // {
  //   name: 'Other liabilities',
  //   funds: '100,000.00',
  //   security: '100,000.00',
  //   cash: '100,000.00'
  // }
];

const CashTransactions = [
  {
    date: '1524169152',
    type: 'Debit',
    desc: 'luctus sit amet,',
    debit: '0,242.58',
    credit: '4,838.54',
    balance: '7,829.47'
  },
  {
    date: '1526344172',
    type: 'Debit',
    desc: 'arcu. Aliquam ultrices iaculis',
    debit: '2,232.93',
    credit: '9,028.80',
    balance: '0,291.57'
  },
  {
    date: '1572862696',
    type: 'Credit',
    desc: 'molestie tortor nibh sit',
    debit: '6,943.25',
    credit: '4,708.43',
    balance: '1,318.90'
  },
  {
    date: '1555234073',
    type: 'Credit',
    desc: 'enim.',
    debit: '6,410.36',
    credit: '9,872.66',
    balance: '6,922.65'
  },
  {
    date: '1518623272',
    type: 'Credit',
    desc: 'sed',
    debit: '3,210.27',
    credit: '3,401.53',
    balance: '5,364.56'
  },
  {
    date: '1570691519',
    type: 'Credit',
    desc: 'ut, molestie',
    debit: '7,898.36',
    credit: '3,502.74',
    balance: '9,142.54'
  },
  {
    date: '1515392266',
    type: 'Debit',
    desc: 'tincidunt. Donec vitae erat',
    debit: '9,674.97',
    credit: '2,690.16',
    balance: '0,173.98'
  },
  {
    date: '1542218313',
    type: 'Debit',
    desc: 'penatibus',
    debit: '6,877.57',
    credit: '5,512.10',
    balance: '4,508.32'
  },
  {
    date: '1556797812',
    type: 'Credit',
    desc: 'tempor',
    debit: '4,968.55',
    credit: '9,524.88',
    balance: '2,925.74'
  },
  {
    date: '1534238012',
    type: 'Credit',
    desc: 'Proin',
    debit: '4,522.61',
    credit: '0,028.96',
    balance: '3,573.09'
  },
  {
    date: '1544172464',
    type: 'Debit',
    desc: 'Cras sed',
    debit: '7,581.27',
    credit: '0,738.89',
    balance: '7,923.71'
  },
  {
    date: '1547295220',
    type: 'Debit',
    desc: 'dolor',
    debit: '1,278.75',
    credit: '0,762.03',
    balance: '9,537.64'
  },
  {
    date: '1572285371',
    type: 'Credit',
    desc: 'adipiscing elit. Curabitur',
    debit: '6,723.10',
    credit: '6,702.50',
    balance: '5,841.52'
  },
  {
    date: '1545320840',
    type: 'Credit',
    desc: 'nisl. Nulla eu neque',
    debit: '9,739.72',
    credit: '3,906.22',
    balance: '1,912.45'
  },
  {
    date: '1551624916',
    type: 'Debit',
    desc: 'Mauris vestibulum, neque sed',
    debit: '0,747.79',
    credit: '3,385.22',
    balance: '5,439.79'
  },
  {
    date: '1541820878',
    type: 'Debit',
    desc: 'Duis a mi',
    debit: '1,426.69',
    credit: '5,494.54',
    balance: '0,387.00'
  },
  {
    date: '1551146626',
    type: 'Debit',
    desc: 'ridiculus mus. Donec dignissim',
    debit: '7,383.68',
    credit: '976.64',
    balance: '4,954.99'
  },
  {
    date: '1521600135',
    type: 'Debit',
    desc: 'Cras lorem lorem, luctus',
    debit: '5,107.72',
    credit: '8,724.65',
    balance: '1,039.90'
  },
  {
    date: '1563096371',
    type: 'Credit',
    desc: 'non',
    debit: '8,264.06',
    credit: '0,819.09',
    balance: '0,046.01'
  },
  {
    date: '1554700546',
    type: 'Debit',
    desc: 'aliquam arcu.',
    debit: '2,542.11',
    credit: '0,233.37',
    balance: '5,824.38'
  }
];

const CashAccountDetails = [
  {
    name: 'NT cash',
    type: 'cash',
    portfolio: 'Personal Portfolio',
    link: '/portfolio/personal/cash-account',
    bsb: '064-234',
    accNum: '1000 5678',
    interest: '',
    balance: '210,000.00',
    available: '100,000.00',
    earned: ''
  },
  {
    name: 'High interest',
    type: 'hia',
    portfolio: 'SMSF Portfolio',
    bsb: '',
    accNum: 'NT1234556789-003',
    interest: '2.15',
    balance: '90,000.00',
    available: '50,000.00',
    earned: '3,000.00'
  }
  // {
  //   name: 'Cash managed',
  //   type: 'cma',
  //   portfolio: 'Personal Portfolio',
  //   bsb: '062-345',
  //   accNum: '1234 5678',
  //   interest: '',
  //   balance: '70,000.00',
  //   available: '70,000.00'
  // }
];

const CashPayeeListDetails = [
  {
    name: 'Bill Cosgrove',
    bsb: '064-254',
    accNum: '1000 5123'
  },
  {
    name: 'Bill Green',
    bsb: '064-284',
    accNum: '1000 5456'
  },
  {
    name: 'Matthew Jones',
    bsb: '064-134',
    accNum: '1000 5678'
  },
  {
    name: 'Corine Mcphee',
    bsb: '064-134',
    accNum: '1000 5678'
  },
  {
    name: 'Bill Johnson',
    bsb: '064-134',
    accNum: '1000 5678'
  },
  {
    name: 'Amy Johnson',
    bsb: '064-134',
    accNum: '1000 5678'
  },
  {
    name: 'Anna Walters',
    bsb: '064-134',
    accNum: '1000 5678'
  }
];

const AccountProfile = [
  {
    //code: 'API-2381'
    //code: 'API-2380'
    code: 'API-200'
  }
];
export {
  CashTransactions,
  Portfolios,
  PersonalAccounts,
  AccountProfile,
  CashAccountDetails,
  CashPayeeListDetails
};
