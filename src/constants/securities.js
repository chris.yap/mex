const Securities = [
  {
    value: 'bhp.asx',
    name: 'BHP Billiton Group Ltd',
    code: 'BHP',
    alert: 2,
    label: 'BHP Group Ltd',
    exchange: 'ASX',
    industry: 'Metals and Mining, Petroleum',
    last: '38.14',
    bid: '35.91',
    ask: '34.86',
    open: '35.43',
    change: '-0.33',
    changePercent: '-1.58',
    range: '33.96 - 33.08',
    vol: '5,198,068',
    adv: 'Within',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    },
    ms: {
      recommendation: 'Overvalued',
      date: '23-05-2019'
    }
  },
  {
    value: 'cba.asx',
    name: 'Commonwealth Bank of Aust.',
    code: 'CBA',
    label: 'Commonwealth Bank of Aust.',
    exchange: 'ASX',
    industry: 'Financials',
    last: '34.92',
    bid: '38.39',
    ask: '38.65',
    open: '35.43',
    change: '1.46',
    changePercent: '3.20',
    range: '34.12 - 33.55',
    vol: '4,111,485',
    adv: 'Within',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    },
    ms: {
      recommendation: 'Overvalued',
      date: '23-05-2019'
    }
  },
  {
    value: 'aapl.nasdaq',
    name: 'Apple Inc',
    code: 'APPL',
    label: 'Apple Inc',
    exchange: 'NASDAQ',
    industry: 'Technology',
    last: '38.60',
    bid: '35.79',
    ask: '37.41',
    open: '35.43',
    change: '3.52',
    changePercent: '3.41',
    range: '33.25 - 33.94',
    vol: '6,632,608',
    adv: 'Within',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    }
  },
  {
    value: 'VVLU.ASX',
    name: 'Vanguard Global Value Equity Active ETF',
    code: 'VVLU',
    label: 'Vanguard Global Value Equity Active ETF',
    exchange: 'ASX',
    industry: 'Equity World Large Value',
    type: 'etf',
    last: '49.07',
    bid: '-',
    ask: '49.00',
    change: '-1.51',
    changePercent: '-3.25'
  },
  {
    value: 'SCH11:ASX',
    name: 'Schroder Real Return CPI Plus 5% Fund - Wholesale Class',
    code: 'SCH11',
    label: 'Schroder Real Return CPI Plus 5% Fund - Wholesale Class',
    exchange: 'ASX',
    type: 'mfund'
  },
  {
    value: 'BHPJD9:ASX',
    name: 'BHP SEP9 1726 C',
    code: 'BHPJD9',
    label: 'BHP SEP9 1726 C',
    exchange: 'ASX',
    type: 'eto'
  },
  {
    value: 'googl.nasdaq',
    name: 'Alphabet Inc',
    code: 'Googl',
    label: 'Alphabet Inc',
    exchange: 'NASDAQ',
    industry: 'Technology',
    last: '37.01',
    bid: '38.61',
    ask: '38.24',
    open: '35.43',
    change: '-1.51',
    changePercent: '-4.25',
    range: '33.28 - 33.02',
    vol: '5,954,278',
    adv: 'Above',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    },
    ms: {
      recommendation: 'Overvalued',
      date: '23-05-2019'
    }
  },
  {
    value: 'Tsla.nasdaq',
    name: 'Tesla Inc',
    code: 'TSLA',
    label: 'Tesla Inc',
    exchange: 'NASDAQ',
    industry: 'Automotive, Energy storage',
    last: '39.30',
    bid: '38.96',
    ask: '38.20',
    open: '35.43',
    change: '7.03',
    changePercent: '2.54',
    range: '34.90 - 33.84',
    vol: '6,681,647',
    adv: 'Below',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    },
    ms: {
      recommendation: 'Overvalued',
      date: '23-05-2019'
    }
  },
  {
    value: 'nab.asx',
    alert: 2,
    name: 'National Aust. Bank',
    code: 'NAB',
    label: 'National Aust. Bank',
    exchange: 'ASX',
    industry: 'Financials',
    last: '37.31',
    bid: '35.46',
    ask: '36.11',
    open: '35.43',
    change: '0.85',
    changePercent: '0.71',
    range: '34.45 - 33.86',
    vol: '6,241,714',
    adv: 'Within',
    tr: {
      recommendation: 'Positive',
      date: '23-05-2019'
    },
    tcst: {
      recommendation: 'Limited Rise',
      date: '23-05-2019'
    },
    tcmt: {
      recommendation: 'Bullish',
      date: '23-05-2019'
    },
    ms: {
      recommendation: 'Overvalued',
      date: '23-05-2019'
    }
  }
];

const RecentSearches = [
  {
    value: 'bhp.asx',
    name: 'BHP Billiton Group Ltd',
    code: 'BHP',
    label: 'BHP Group Ltd',
    exchange: 'ASX',
    industry: 'Metals and Mining, Petroleum'
  },
  {
    value: 'cba.asx',
    name: 'Commonwealth Bank of Aust.',
    code: 'CBA',
    label: 'Commonwealth Bank of Aust.',
    exchange: 'ASX',
    industry: 'Financials'
  },
  {
    value: 'aapl.nasdaq',
    name: 'Apple Inc',
    code: 'APPL',
    label: 'Apple Inc',
    exchange: 'NASDAQ',
    industry: 'Technology'
  },
  {
    value: 'googl.nasdaq',
    name: 'Alphabet Inc',
    code: 'Googl',
    label: 'Alphabet Inc',
    exchange: 'NASDAQ',
    industry: 'Technology'
  },
  {
    value: 'Tsla.nasdaq',
    name: 'Tesla Inc',
    code: 'TSLA',
    label: 'Tesla Inc',
    exchange: 'NASDAQ',
    industry: 'Automotive, Energy storage'
  },
  {
    value: 'nab.asx',
    name: 'National Aust. Bank',
    code: 'NAB',
    label: 'National Aust. Bank',
    exchange: 'ASX',
    industry: 'Financials'
  }
  // {
  // 	value: '',
  // 	name: '',
  // 	code: '',
  // 	label: '',
  // 	exchange: '',
  // 	industry: '',
  // },
];

const Portfolio = [
  {
    value: 'bhp.asx',
    name: 'BHP Billiton Group Ltd',
    code: 'BHP',
    label: 'BHP Group Ltd',
    exchange: 'ASX',
    industry: 'Metals and Mining, Petroleum'
  },
  {
    value: 'googl.nasdaq',
    name: 'Alphabet Inc',
    code: 'Googl',
    label: 'Alphabet Inc',
    exchange: 'NASDAQ',
    industry: 'Technology'
  },
  {
    value: 'Tsla.nasdaq',
    name: 'Tesla Inc',
    code: 'TSLA',
    label: 'Tesla Inc',
    exchange: 'NASDAQ',
    industry: 'Automotive, Energy storage'
  },
  {
    value: 'nab.asx',
    name: 'National Aust. Bank',
    code: 'NAB',
    label: 'National Aust. Bank',
    exchange: 'ASX',
    industry: 'Financials'
  }
];

export { Portfolio, RecentSearches, Securities };
