const DomesticAcc = [
  {
    value: 'NT123455678-001',
    label: 'Domestic 1'
  },
  {
    value: 'NT123455678-002',
    label: 'Domestic 2'
  }
  // {
  // 	value: 'NT123455678-003',
  // 	label: 'Domestic 3',
  // },
  // {
  // 	value: 'NT123455678-004',
  // 	label: 'Domestic 4',
  // },
];

const InternationalAcc = [
  {
    value: 'NT123455678-003',
    label: 'International'
  },
  {
    value: 'NT123455678-004',
    label: 'International'
  }
];

export { DomesticAcc, InternationalAcc };
