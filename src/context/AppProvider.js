import React from "react";

const AppContext = React.createContext();

class AppProvider extends React.Component {
  state = {
    login: true,
    orderpad: false,
    overlay: false,
    mobileNav: false,
    selectedStock: {
      value: "bhp.asx",
      name: "BHP Billiton Group Ltd",
      code: "BHP",
      alert: 2,
      label: "BHP Group Ltd",
      exchange: "ASX",
      industry: "Metals and Mining, Petroleum",
      last: "38.14",
      bid: "35.91",
      ask: "34.86",
      open: "35.43",
      change: "-0.33",
      changePercent: "-1.58",
      range: "33.96 - 33.08",
      vol: "5,198,068",
      adv: "Within",
      tr: {
        recommendation: "Positive",
        date: "23-05-2019"
      },
      tcst: {
        recommendation: "Limited Rise",
        date: "23-05-2019"
      },
      tcmt: {
        recommendation: "Bullish",
        date: "23-05-2019"
      },
      ms: {
        recommendation: "Overvalued",
        date: "23-05-2019"
      }
    },

    toggleLogin: () => {
      this.setState({ login: !this.state.login });
    },
    toggleOrderpad: () => {
      this.setState({ orderpad: !this.state.orderpad });
    },
    toggleOverlay: () => {
      this.setState({ overlay: !this.state.overlay });
    },
    toggleMobileNav: val => {
      this.setState({ mobileNav: val ? val : !this.state.mobileNav });
    },
    selectStock: val => {
      this.setState({ selectedStock: val });
    }
  };
  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export { AppContext, AppProvider };
