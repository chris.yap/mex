import React from 'react';

let OrderPadContext = React.createContext();

let initialState = {
	help: 0,
	summary: false,
	moreInfo: false,
	searchBox: true,
	conditionalOrder: false,
	placeConditionalOrder: false,
	conditionalOrderConfirmation: false,

	type: 'buy',
	orderType: 'limit',
	duration: 'until cancelled',
	amountType: 'qty',
};

let reducer = (state, action) => {
	switch (action.type) {
		case 'toggleLogin':
			return { ...state, login: action.payload ? action.payload : !state.login };
	}
};

function OrderPadProvider(props) {
	const [state, dispatch] = React.useReducer(reducer, initialState);
	let value = { state, dispatch };
	return <OrderPadContext.Provider value={value}>{props.children}</OrderPadContext.Provider>;
}

let OrderPadContextConsumer = OrderPadContext.Consumer;

export { OrderPadContext, OrderPadProvider, OrderPadContextConsumer };
