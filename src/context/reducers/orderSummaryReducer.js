const orderSummaryReducer = (orderSummary, action) => {
	switch (action.type) {
		case 'toggleOrderSummary':
			return {
				...orderSummary,
				isOpened: action.payload ? action.payload : !orderSummary.isOpened,
			};
		case 'handleOpenConfirmation':
			return {
				...orderSummary,
				orderConfirmation: true,
			};
		case 'handleCloseConfirmation':
			return {
				...orderSummary,
				orderConfirmation: false,
			};
		case 'toggleSubmittingOrder':
			return {
				...orderSummary,
				orderSubmit: !orderSummary.orderSubmit,
			};
		default:
			return orderSummary;
	}
};

export default orderSummaryReducer;
