const otpReducer = (otp, action) => {
  switch (action.type) {
    case 'toggleOTP':
      return {
        ...otp,
        isOpened: action.payload ? action.payload : !otp.isOpened
      };
    default:
      return otp;
  }
};

export default otpReducer;
