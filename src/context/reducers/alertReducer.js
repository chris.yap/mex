const alertReducer = (alert, action) => {
	switch (action.type) {
		case 'toggleFlyInAlert':
			return {
				...alert,
				flyIn: action.payload ? action.payload : !alert.flyIn,
			};
		default:
			return alert;
	}
};

export default alertReducer;
