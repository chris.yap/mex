const overlayReducer = (overlay, action) => {
	switch (action.type) {
		case 'toggleOverlay':
			return {
				...overlay,
				isActive: action.payload ? action.payload : !overlay.isActive,
			};
		default:
			return overlay;
	}
};

export default overlayReducer;
