const cashTransferReducer = (cash, action) => {
  switch (action.type) {
    case 'toggleCashCard':
      return {
        ...cash,
        cardIsOpened: action.payload ? action.payload : !cash.cardIsOpened
      };
    case 'expandCashAccount':
      let value = Number(action.payload);
      return {
        ...cash,
        expandedAccount:
          cash.expandedAccount !== null && value === cash.expandedAccount
            ? null
            : value
      };
    case 'updateAccountOpenedTab':
      return {
        ...cash,
        accountOpenedTab: action.payload
      };
    case 'toggleChangeLimit':
      return {
        ...cash,
        changeLimit: action.payload ? action.payload : !cash.changeLimit
      };
    case 'toggleCashTransferModel':
      return {
        ...cash,
        isOpened: action.payload ? action.payload : !cash.isOpened
      };
    case 'handleStep':
      return {
        ...cash,
        step: action.payload
      };
    case 'updateTransferTarget':
      return {
        ...cash,
        transferTarget: action.payload
      };
    case 'updateSelectedAccount':
      return {
        ...cash,
        selectedAccount: action.payload
      };
    case 'updateTransferType':
      return {
        ...cash,
        transferType: action.payload
      };
    case 'getDefaultFromAccount':
      return {
        ...cash,
        defaultFromAccount: action.payload
      };
    case 'getDefaultToAccount':
      return {
        ...cash,
        defaultToAccount: action.payload
      };
    case 'updateModalView':
      return {
        ...cash,
        modalView: action.payload
      };
    case 'updateSubmissionError':
      return {
        ...cash,
        submissionError: action.payload
      };
    default:
      return cash;
  }
};

export default cashTransferReducer;
