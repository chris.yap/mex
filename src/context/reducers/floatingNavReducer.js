const floatingNavReducer = (floatingNav, action) => {
	switch (action.type) {
		case 'toggleFloatingNavHover':
			return {
				...floatingNav,
				hover: action.payload ? action.payload : !floatingNav.isLogin,
			};
		default:
			return floatingNav;
	}
};

export default floatingNavReducer;
