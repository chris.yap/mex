const searchReducer = (searchBar, action) => {
	switch (action.type) {
		case 'toggleSearchBar':
			return {
				...searchBar,
				isOpened: !searchBar.isOpened,
			};
		case 'selectStock':
			return {
				...searchBar,
				selectedStock: action.payload,
			};
		case 'handleSearchInput':
			let Xtion = action.payload.action.action;
			return {
				...searchBar,
				searchValue:
					Xtion === 'menu-close' || Xtion === 'input-blur' || Xtion === 'set-value'
						? searchBar.searchValue
						: action.payload.value,
			};
		default:
			return searchBar;
	}
};

export default searchReducer;
