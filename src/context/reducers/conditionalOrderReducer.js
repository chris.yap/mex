const conditionalOrderReducer = (conditionalOrder, action) => {
	switch (action.type) {
		case 'updateConditionalOrders':
			return {
				...conditionalOrder,
				conditionalOrders: action.payload,
			};
		case 'updateCoExpand':
			return {
				...conditionalOrder,
				coExpand: action.payload,
			};
		case 'handleTriggerType':
			return {
				...conditionalOrder,
				triggerType: action.payload,
			};
		case 'handleTrigger':
			return {
				...conditionalOrder,
				trigger: action.payload,
			};
		case 'updateCondition':
			return {
				...conditionalOrder,
				condition: action.payload,
			};
		case 'handleIfLastPrice':
			return {
				...conditionalOrder,
				ifLastPrice: action.payload,
			};
		case 'handleTrailLastPrice':
			return {
				...conditionalOrder,
				trailLastPrice: action.payload,
			};
		default:
			return conditionalOrder;
	}
};

export default conditionalOrderReducer;
