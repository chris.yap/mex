const mobileNavReducer = (mobileNav, action) => {
	switch (action.type) {
		case 'toggleMobileNav':
			return {
				...mobileNav,
				isOpened: action.payload ? action.payload : !mobileNav.isOpened,
			};
		case 'toggleSubNav':
			return {
				...mobileNav,
				openSub: action.payload === mobileNav.openSub ? null : action.payload,
			};
		default:
			return mobileNav;
	}
};

export default mobileNavReducer;
