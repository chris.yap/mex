const loginReducer = (login, action) => {
  switch (action.type) {
    case 'toggleLogin':
      return {
        ...login,
        isLogin: action.payload ? action.payload : !login.isLogin
      };
    case 'toggleResetPassword':
      return {
        ...login,
        resetPassword: action.payload ? action.payload : !login.resetPassword
      };
    case 'setPasswordSequence':
      return {
        ...login,
        passwordSequence: action.payload
      };
    case 'resetPasswordSequence':
      return {
        ...login,
        passwordSequence: !action.payload && 1
      };
    default:
      return login;
  }
};

export default loginReducer;
