const orderPadReducer = (orderPad, action) => {
  switch (action.type) {
    case 'toggleOrderPad':
      return {
        ...orderPad,
        isOpened: action.payload ? action.payload : !orderPad.isOpened
      };
    case 'closeOrderPad':
      return {
        ...orderPad,
        isOpened: false
      };
    case 'toggleOrderPadSearchBox':
      return {
        ...orderPad,
        searchBox: action.payload ? action.payload : !orderPad.searchBox
      };
    case 'toggleOrderPadMoreInfo':
      return {
        ...orderPad,
        moreInfo: action.payload ? action.payload : !orderPad.moreInfo
      };
    case 'setConditionalOrder':
      return {
        ...orderPad,
        conditionalOrder: action.payload
      };
    case 'getHelp':
      return {
        ...orderPad,
        help: action.payload ? action.payload : 0
      };
    case 'closeHelp':
      return {
        ...orderPad,
        help: 0
      };
    case 'selectBuySell':
      return {
        ...orderPad,
        buySell: action.payload
      };
    case 'setOrderType':
      return {
        ...orderPad,
        orderType: action.payload
      };
    case 'setAmountType':
      return {
        ...orderPad,
        amountType: action.payload
      };
    case 'orderPadSelectOption':
      return {
        ...orderPad,
        selectedOption: action.payload
      };
    case 'toggleMoreInfo':
      return {
        ...orderPad,
        moreInfo: !orderPad.moreInfo
      };
    case 'toggleOrderPadOverlay':
      return {
        ...orderPad,
        overlay: !orderPad.overlay
      };
    case 'toggleOrderPadCoOverlay':
      return {
        ...orderPad,
        coOverlay: !orderPad.coOverlay
      };
    case 'closeMoreInfo':
      return {
        ...orderPad,
        moreInfo: false
      };
    case 'handleAmountType':
      return {
        ...orderPad,
        amountType: action.payload
      };
    case 'updateLimitPrice':
      return {
        ...orderPad,
        limitPrice: action.payload ? action.payload : null
      };
    case 'updateAud':
      return {
        ...orderPad,
        aud: action.payload ? action.payload : null
      };
    case 'setDuration':
      return {
        ...orderPad,
        duration: action.payload ? action.payload : null
      };
    case 'reviewConditionerOrder':
      return {
        ...orderPad,
        placeConditionalOrder: true
      };
    case 'updateConditionalOrderConfirmation':
      return {
        ...orderPad,
        conditionalOrderConfirmation: action.payload
      };
    case 'updatePlaceConditionalOrder':
      return {
        ...orderPad,
        placeConditionalOrder: action.payload
      };
    default:
      return orderPad;
  }
};

export default orderPadReducer;
