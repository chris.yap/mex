import React, { useRef, useState, useEffect } from 'react';
import { getState } from './context/StateProvider';
import SiteFooter from './components/SiteFooter';
import SiteSearch from './components/Search';
// import {
//   disableBodyScroll,
//   enableBodyScroll
//   // clearAllBodyScrollLocks
// } from 'body-scroll-lock';
import { CashAccountDetails } from './constants/portfolio';
import cn from 'classnames';
import Overlay from './components/Overlay';
import {
  App,
  Alert,
  Breakpoints,
  Card,
  CardContent,
  CheckboxGroup,
  Checkbox,
  Title,
  WhichBrowser
} from 'shaper-react';
import { BrowserRouter as Router } from 'react-router-dom';
import MobileNav from './components/MobileNav';
import OrderPad from './components/OrderPad/OrderPad';
import OneTimePin from './components/OneTimePin';
import CashTransferModal from './pages/Portfolio/CashAccounts/CashTransferModal';
import Login from './pages/Login/Login';
import Routes from './pages/Routes';

const Main = ({ activeBreakpoints, activeBrowser, ...props }) => {
  const targetRef = useRef();
  const [error, setError] = useState([]);
  const [
    { alert, cash, login, orderPad, otp, overlay, searchBar },
    dispatch
  ] = getState();
  let SiteSearchField = null;
  let focusSearch = () => {
    SiteSearchField.focus();
  };
  useEffect(() => {
    if (searchBar.isOpened) {
      focusSearch();
    }
    if (orderPad.isOpened) {
      //disableBodyScroll(targetRef);
      document.body.style.overflow = 'hidden';
      document.body.style.position = activeBrowser.isIos ? 'fixed' : '';
    } else {
      //enableBodyScroll(targetRef);
      document.body.style.overflow = '';
      document.body.style.position = '';
    }
  });
  return (
    <Router>
      <App>
        {!login.isLogin ? (
          <Login login={() => dispatch({ type: 'toggleLogin' })} />
        ) : (
          <>
            <OrderPad
              error={error}
              width={activeBreakpoints.above.xs ? 375 : '100%'}
            />

            <div
              ref={targetRef}
              className={cn('app app-mex has-bg-blue-grey lighten-5')}>
              <Routes />

              <MobileNav />

              {/* { Search} */}
              <SiteSearch
                innerRef={ref => {
                  SiteSearchField = ref;
                }}
              />

              <SiteFooter />
            </div>
          </>
        )}

        <Overlay isOpened={overlay.isActive} spinner />

        <OneTimePin isOpened={otp.isOpened} />

        <Alert
          fixed
          info
          isOpened={alert.flyIn}
          onClose={() => dispatch({ type: 'toggleFlyInAlert' })}
          style={{ zIndex: '9999' }}>
          For demo purpose only
        </Alert>

        <CashTransferModal
          isOpened={cash.isOpened}
          transferTarget={cash.transferTarget}
          defaultFromAccount={cash.defaultFromAccount}
          defaultToAccount={cash.defaultToAccount}
          availableAccounts={CashAccountDetails}
          step={cash.step}
        />

        {orderPad.isOpened && orderPad.errorHandling && (
          <Card
            style={{
              bottom: '1em',
              left: '1em',
              maxWidth: '280px',
              position: 'fixed',
              zIndex: 1001
            }}>
            <CardContent className="py-2 px-4">
              <Title size="6" className="mb-1">
                Orderpad error handling
              </Title>
              <CheckboxGroup
                stacked
                className="mb-0"
                name="error"
                value={error}
                onChange={e => setError(e)}>
                <Checkbox value="search" label="Search Predictive Security" />
                <Checkbox value="balance" label="Retrieve Balance" />
                <Checkbox value="holdings" label="Retrieve Holdings" />
                <Checkbox value="stock" label="Get Stock" />
                <Checkbox value="validate" label="Validate Order" />
                <Checkbox value="create" label="Create Order" />
                <Checkbox value="nonKyc" label="Non KYC" />
              </CheckboxGroup>
            </CardContent>
          </Card>
        )}
      </App>
    </Router>
  );
};

export default WhichBrowser(Breakpoints(Main));
