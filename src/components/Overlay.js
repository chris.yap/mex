import React from 'react';
import LoaderAnimation from './LoaderAnimation';
import { Overlay as Over } from 'shaper-react';

const Overlay = ({ isOpened, spinner, ...props }) => {
  const loader = spinner && (
    <LoaderAnimation
      color={props.dark ? 'rgba(255,255,255,.5)' : 'rgba(76,98,108,.5)'}
      stroke={5}
      size="100px"
    />
  );
  return <Over isOpened={isOpened} spinner={loader} {...props} />;
};

export default Overlay;
