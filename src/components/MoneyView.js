import React from 'react';
import PropTypes from 'prop-types';

const MoneyView = ({ currency, symbol, top, value, ...props }) => {
  const newValue = value && value.replace('$', '');
  return (
    <React.Fragment>
      <sup className={`pos-r has-top${top}`}>
        {currency}
        {symbol}
      </sup>
      {newValue}
    </React.Fragment>
  );
};

export default MoneyView;

MoneyView.propTypes = {
  symbol: PropTypes.string,
  currency: PropTypes.string,
  top: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

MoneyView.defaultProps = {
  symbol: '$',
  top: 3
};
