import React from 'react';
import { Container, Hero, Title } from 'shaper-react';

const HeroView = ({ children, title, ...props }) => (
  <Hero bg="black" py={4} {...props}>
    <Container>
      {title ? (
        <Title className="font-nabimpact has-text-white mt-1 mb-0">
          {title}
        </Title>
      ) : (
        children
      )}
    </Container>
  </Hero>
);

export default HeroView;
