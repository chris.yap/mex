import React from 'react';
import {
  MultiField,
  Radio,
  RadioGroup,
  Section,
  Select,
  Textfield
} from 'shaper-react';
import { AccountSingleValue, AccountOption } from '../../helpers/CustomSelect';
import { accounts, amountTypeOptions, orderTypeOptions } from './constants';
import SellConditionalOrders from './SellConditionalOrders';

export default class Sell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderType: 'limit',
      amountType: 'qty',
      duration: 'until cancelled'
    };
    this.handleOrderType = this.handleOrderType.bind(this);
    this.handleDuration = this.handleDuration.bind(this);
    this.handleAmountType = this.handleAmountType.bind(this);
  }
  handleOrderType(event) {
    this.setState({ orderType: event });
  }
  handleDuration(event) {
    this.setState({ duration: event });
  }
  handleAmountType(event) {
    this.setState({ amountType: event });
  }
  render() {
    const { error, selectedOption } = this.props;
    return (
      <Section className="px-4 pt-4 pb-10">
        <Select
          label="Account to trade from"
          components={{
            Option: AccountOption,
            SingleValue: AccountSingleValue
          }}
          defaultMenuIsOpen={false}
          defaultValue={accounts[0]}
          height={60}
          options={accounts}
        />

        <SellConditionalOrders />

        <MultiField
          required
          label={`
          ${
            error.includes('holdings') && this.state.amountType === 'qty'
              ? 'Units'
              : !error.includes('holdings') && this.state.amountType === 'qty'
              ? 'Units (0 available)'
              : error.includes('holdings')
              ? 'Value'
              : 'Value ($0.00 available)'
          }`}
          leftClassName="is-flex-2"
          rightClassName="is-flex-3"
          className="is-hidden-touch"
          leftSlot={
            <Select
              isSearchable={false}
              options={amountTypeOptions}
              defaultValue={amountTypeOptions[0]}
              onChange={this.handleAmountType}
            />
          }
          rightSlot={
            <React.Fragment>
              {this.state.amountType === 'qty' ? (
                <Textfield placeholder="Enter a number of units" />
              ) : (
                <Textfield placeholder="Enter a value" />
              )}
            </React.Fragment>
          }
        />

        <MultiField
          required
          label={
            this.state.orderType === 'limit'
              ? 'Limit price in XXX'
              : 'Order at market price'
          }
          leftClassName="is-flex-2"
          rightClassName="is-flex-3"
          className="is-hidden-touch"
          leftSlot={
            <Select
              isSearchable={false}
              options={orderTypeOptions}
              defaultValue={orderTypeOptions[0]}
              onChange={this.handleOrderType}
            />
          }
          rightSlot={
            <Textfield
              isDisabled={this.state.orderType === 'market'}
              placeholder={
                this.state.orderType === 'market'
                  ? 'Not applicable'
                  : 'Enter a value'
              }
            />
          }
        />

        <RadioGroup
          buttons
          fullwidth
          name="orderType"
          label="Order type"
          selectedValue={this.state.orderType}
          onChange={this.handleOrderType}
          className="is-hidden-desktop"
        >
          <Radio button value="limit" label="Limit" />
          <Radio button value="market" label="Market" />
        </RadioGroup>

        {this.state.orderType === 'limit' && (
          <Textfield
            label="Limit price in AUD"
            required
            placeholder="Enter a value"
            className="is-hidden-desktop"
          />
        )}

        <RadioGroup
          buttons
          fullwidth
          name="amountType"
          label="Amount type"
          selectedValue={this.state.amountType}
          onChange={this.handleAmountType}
          className="is-hidden-desktop"
        >
          <Radio button value="qty" label="Quantity" />
          <Radio button value="value" label="Value" />
        </RadioGroup>

        {this.state.amountType === 'qty' ? (
          <Textfield
            label="Units"
            required
            placeholder="Enter number of units"
            className="is-hidden-desktop"
          />
        ) : (
          <Textfield
            label="Value"
            required
            placeholder="Enter value"
            className="is-hidden-desktop"
          />
        )}

        {selectedOption.type === 'eto' && (
          <Textfield
            label="Contracts (0 available)"
            placeholder="Enter number of contracts"
          />
        )}

        <RadioGroup
          name="duration"
          label="Duration"
          buttons
          fullwidth
          selectedValue={this.state.duration}
          onChange={this.handleDuration}
        >
          <Radio
            spreadEvenly
            button
            value="until cancelled"
            label="Until cancelled"
            className="mb-0"
          />
          <Radio
            spreadEvenly
            button
            value="today"
            label="Today"
            className="mb-0"
          />
          <Radio
            spreadEvenly
            button
            value="set date"
            label="Set date"
            className="mb-0"
          />
        </RadioGroup>

        {this.state.duration === 'set date' && (
          <Textfield label="Date" prependBoxedIcon="date" />
        )}
      </Section>
    );
  }
}
