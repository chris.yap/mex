import React from 'react';
import {
  Button,
  Divider,
  Div,
  Icon,
  Columns,
  Column,
  Select,
  Title
} from 'shaper-react';
import { getState } from '../../context/StateProvider';
import { Securities } from '../../constants/securities';
import { SecurityOption } from '../../helpers/CustomSelect';

const OrderPadSearch = ({ error }) => {
  const [{ orderPad }, dispatch] = getState();
  let handleChange = (value, event) => {
    if (event && event.action === 'select-option') {
      dispatch({
        type: 'orderPadSelectOption',
        payload: value ? value : null
      });
      dispatch({ type: 'toggleOrderPadSearchBox', payload: '0' });
    } else {
      dispatch({ type: 'closeMoreInfo' });
    }
  };
  let showError = () => {
    return [
      {
        value: 'error',
        label: 'Error retrieving info',
        isDisabled: true
      }
    ];
  };
  return (
    <>
      <Columns
        mobile
        gapless
        className="is-flex is-align-items-center has-bg-white pl-4 pr-2 my-0"
        style={{ height: '4.25rem' }}>
        <Column>
          <Div>
            {orderPad.searchBox ? (
              <Select
                defaultMenuIsOpen={false}
                components={{ Option: SecurityOption }}
                onChange={handleChange}
                options={error.includes('search') ? showError() : Securities}
                value={orderPad.selectedOption || ''}
                placeholder="Search for a security ..."
                isClearable
                mr={2}
              />
            ) : (
              <React.Fragment>
                <Button
                  secondary
                  inverted
                  square
                  className="mr-4"
                  onClick={() => dispatch({ type: 'toggleOrderPadSearchBox' })}>
                  <Icon medium>search</Icon>
                </Button>
                <Title size="5" className="mb-0">
                  Place order
                </Title>
              </React.Fragment>
            )}
          </Div>
        </Column>
        <Column narrow>
          <Button
            flat
            round
            small
            className="is-justify-self-flex-end my-0"
            onClick={() => {
              dispatch({ type: 'toggleOrderPad' });
              dispatch({ type: 'closeMoreInfo' });
            }}>
            <Icon>close</Icon>
          </Button>
        </Column>
      </Columns>
      <Divider />
    </>
  );
};

export default OrderPadSearch;
