import React, { useState } from 'react';
import Div100vh from 'react-div-100vh';
import { getState } from '../../context/StateProvider';
import RealtimeQuoteMsg from '../RealtimeQuoteMsg';
import { mfundData, mfundHeaders, mfundRow } from './constants';
import {
  Alert,
  Button,
  Card,
  Column,
  Columns,
  Content,
  Div,
  Divider,
  Drawer,
  Footer,
  Icon,
  List,
  ListItem,
  Section,
  Subtitle,
  Table,
  Tag,
  Title
} from 'shaper-react';

const MFundOrderSummary = ({
  closeOrderPad,
  elevation,
  logo,
  selectedOption,
  toggleDrawer,
  width,
  ...props
}) => {
  const [{ orderPad, orderSummary }, dispatch] = getState();
  const [alert, toggleAlert] = useState(false);
  const setAlert = value => {
    toggleAlert(value);
  };
  const [step, setStep] = useState(0);
  const handleStep = value => {
    setStep(value);
  };
  return (
    <React.Fragment>
      <Drawer
        className="pb-10"
        contentClassName={`is-overflow-visible`}
        elevation={!orderSummary.isOpened ? 0 : elevation}
        isOpened={orderSummary.isOpened}
        logo={logo}
        overlay={false}
        right
        variant="temporary"
        width={width}
        style={{ zIndex: 1002 }}>
        <Div100vh className="is-flex is-flex-column">
          <div
            className={`is-flex-1-1-auto ${
              step ? 'is-overflow-hidden' : 'is-overflow-auto'
            }`}>
            <Title
              size="5"
              className="has-text-weight-bold is-flex is-align-items-center has-bg-white pl-4 pr-2 my-0"
              style={{ height: '4.25rem' }}>
              <Button inverted square className="mr-4" onClick={toggleDrawer}>
                <Icon>arrow-left</Icon>
              </Button>
              Order summary {alert}
            </Title>

            <Divider />

            <div className="is-flex-column has-bg-blue-grey lighten-5">
              <Section className="pa-4">
                <Title
                  size="3"
                  className="font-nabimpact has-text-ellipsis has-text-centered">
                  {selectedOption ? selectedOption.label : null}
                </Title>
                <Subtitle
                  size="7"
                  className="has-text-centered has-text-ellipsis">
                  {selectedOption ? selectedOption.code : null} |{' '}
                  {selectedOption ? selectedOption.exchange : null}
                </Subtitle>
                <Table
                  isNarrow
                  hasStripes
                  headers={mfundHeaders}
                  data={mfundData}
                  tableRow={mfundRow}
                  mb={4}
                />
                <p className="has-text-blue-grey has-text-centered">
                  <RealtimeQuoteMsg local />
                </p>

                <Card flat className="mb-2">
                  <List>
                    <ListItem>
                      <Columns mobile className="is-flex-1">
                        <Column>Order type</Column>
                        <Column className="has-text-right has-text-ellipsis">
                          <Tag
                            success={orderPad.buySell === 0}
                            danger={orderPad.buySell === 1}>
                            {orderPad.buySell === 0 ? 'Buy ' : 'Sell '}
                            (Application)
                          </Tag>
                        </Column>
                      </Columns>
                    </ListItem>
                    {Summary.map((summ, sIndex) => (
                      <ListItem key={sIndex}>
                        <Columns mobile className="is-flex-1">
                          <Column>{summ.name}</Column>
                          <Column className="has-text-weight-bold has-text-right">
                            {summ.value}
                          </Column>
                        </Columns>
                      </ListItem>
                    ))}
                  </List>
                </Card>

                <Content small className="mb-10 pb-10">
                  <p>
                    Your trade confirmation will record brokerage charges and
                    WealthHub Securities does not accept any responsibility for
                    unit price movements.
                  </p>
                  <p>
                    You must ensure your cash funded trading account is selected
                    and sufficient funds are available to cover the total
                    consideration of the order. Ensure you have provided your
                    TFN/ABN.
                  </p>
                  <p>
                    Managed funds units are settled through the mFund Settlement
                    Service and are not traded on an open market or exchange.
                    You cannot sell or buy these units from other investors on
                    the market. You may not be able to convert your investment
                    to cash as quickly as you can for shares.
                  </p>
                </Content>
              </Section>
            </div>

            <Footer
              bg="white"
              fixed
              py={2}
              mt={0}
              borderTop="1px solid rgba(0,0,0,.15)"
              boxShadow={orderSummary.orderConfirmation && 24}>
              {step === 2 && (
                <Content small className="has-text-blue-grey text--darken-3">
                  <p className="mt-1 mb-3">
                    Your application for <strong>XXX.XXX</strong> units on{' '}
                    <strong>NTXXXXXXXX-XXX</strong> has been submitted
                  </p>
                  <p className="mb-3">Next steps:</p>
                  <ol className="mt-0">
                    <li>
                      You will receive an order notification email by the end of
                      the next business day.
                    </li>
                    <li>
                      Your order to the Fund Manager will be submitted for
                      processing by the end of the next business day. Funds are
                      debited from your cash account for the amount of the
                      order.
                    </li>
                    <li>
                      Yout indicative units and settlement date can be viewed in
                      the confirmations tab on the nabtrade desktop site.
                    </li>
                    <li>
                      On the unit allotment date, your units will be allocated
                      to your HIN and you will receive an email confirmation.
                    </li>
                  </ol>
                </Content>
              )}
              <Alert
                info={step === 1}
                success={step === 2}
                inverted
                isOpened={alert}
                mt={2}
                mb={4}>
                {step === 1 ? (
                  <React.Fragment>
                    <p className="mb-4">
                      You are about to place the following order:{' '}
                    </p>
                    <p className="mb-0">
                      BUY <strong>ACA01.ASX</strong> with an investment amount
                      of <strong>$10,000.00</strong>. <br />
                      Please confirm by clicking the submit button.
                    </p>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <p className="mb-2">
                      <strong>Your order #XXXXXXXX has been created.</strong>
                    </p>
                    <p className="mb-0">
                      If you have questions, please contact nabtrade at{' '}
                      <a href="tel:131380">13 13 80</a> or{' '}
                      <a href="mailto:enquiries@nabtrade.com.au">
                        enquiries@nabtrade.com.au
                      </a>
                      .
                    </p>
                  </React.Fragment>
                )}
              </Alert>
              <Div display="flex">
                {step === 0 ? (
                  <React.Fragment>
                    <Button
                      primary
                      fullwidth
                      onClick={() => {
                        setAlert(true);
                        handleStep(step + 1);
                      }}>
                      Place order
                    </Button>
                    <Button
                      flat
                      onClick={() => dispatch({ type: 'toggleOrderSummary' })}>
                      Amend order
                    </Button>
                  </React.Fragment>
                ) : step === 1 ? (
                  <React.Fragment>
                    <Button
                      primary
                      fullwidth
                      onClick={() => {
                        setAlert(true);
                        handleStep(step + 1);
                      }}>
                      Submit order
                    </Button>
                    <Button
                      flat
                      onClick={() => {
                        handleStep(step - 1);
                        setAlert(false);
                      }}>
                      Cancel
                    </Button>
                  </React.Fragment>
                ) : (
                  <Button
                    secondary
                    fullwidth
                    onClick={() => {
                      setAlert(false);
                      handleStep(0);
                      dispatch({ type: 'toggleOrderSummary' });
                      dispatch({ type: 'toggleOrderPad' });
                      dispatch({ type: 'orderPadSelectOption' });
                    }}>
                    Close
                  </Button>
                )}
              </Div>
            </Footer>
          </div>
        </Div100vh>
      </Drawer>
    </React.Fragment>
  );
};

export default MFundOrderSummary;

const Summary = [
  { name: 'Code', value: 'ACA01.ASX' },
  { name: 'Fund manager', value: 'AMP Capital Investors Limited' },
  { name: 'Fund', value: 'AMP Capital Australian Equity Income Fund' },
  { name: 'Indicative price', value: '$0.99215 per unit' },
  { name: 'Last price date', value: '21-05-2019' },
  { name: 'Account', value: 'NT4640361-002' },
  { name: 'Distribution preference', value: 'CASH' },
  { name: 'Investment amount', value: '$10,000.00' },
  { name: 'Estimated brokerage', value: '$19.95' },
  { name: 'Includes GST', value: '$1.81' },
  { name: <strong>Total</strong>, value: '$10,019.95' }
];
