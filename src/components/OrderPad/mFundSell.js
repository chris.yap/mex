import React from 'react';
import {
  Button,
  Column,
  Columns,
  Label,
  Section,
  Select,
  Textfield
} from 'shaper-react';
import { AccountSingleValue, AccountOption } from '../../helpers/CustomSelect';
import { accounts } from './constants';

class MFundSell extends React.Component {
  state = {
    units: '',
    error: false
  };
  handleChange = e => {
    const newVal = e.target.value;
    this.setState({
      units: e.target.value,
      error: !newVal || newVal > 200 ? true : false
    });
  };
  render = () => {
    const { error, units } = this.state;
    return (
      <Section className="px-4 pt-4 pb-10">
        <Select
          label="Account to trade from"
          components={{
            Option: AccountOption,
            SingleValue: AccountSingleValue
          }}
          defaultMenuIsOpen={false}
          defaultValue={accounts[0]}
          height={60}
          options={accounts}
        />
        <Columns gapless mobile className="mb-2">
          <Column>
            {/* <Textfield label="Available units" value={200 - units} readonly /> */}
            <Label className="nab-label mb-0">Available units</Label>
            <span className={200 - units < 0 && 'has-text-danger'}>
              {200 - units}
            </span>
          </Column>
          <Column narrow>
            <Button
              secondary={units < 200}
              inverted
              className="mx-0"
              onClick={() => this.setState({ units: 200 })}
              disabled={units === 200}
            >
              Redeem all
            </Button>
          </Column>
        </Columns>

        <Columns gapless mobile className="mb-2">
          <Column>
            <Textfield
              label="Number of units"
              placeholder="Enter number of units"
              errorMessage={
                units > 200
                  ? 'Not enough available units'
                  : 'Units cannot be zero'
              }
              error={error}
              value={units}
              onChange={this.handleChange}
            />
          </Column>
          <Column narrow>
            <Button
              secondary={units}
              inverted
              className="mt-6 mr-0 pos-r has-top-1"
              onClick={() => this.setState({ units: '' })}
              disabled={!units}
            >
              Clear
            </Button>
          </Column>
        </Columns>
      </Section>
    );
  };
}

export default MFundSell;
