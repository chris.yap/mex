import React from 'react';
import MoneyView from '../../components/MoneyView';
import { TableRow as Row, TableCell as Cell } from 'shaper-react';

const headers = [
  { name: 'Last', value: 'last', align: 'centered' },
  { name: 'Bid', value: 'bid', align: 'centered' },
  { name: 'Ask', value: 'ask', align: 'centered' }
];

const mfundHeaders = [
  { name: 'Application', value: 'application', align: 'centered' },
  { name: 'Redemption', value: 'redemption', align: 'centered' }
];

const data = [{ last: '34.35', bid: '34.34', ask: '34.36' }];
const mfundData = [{ application: '1.11522', redemption: '1.1476' }];

const TableRow = ({ row }) => (
  <Row>
    <Cell centered>
      <sup style={{ position: 'relative', top: '4px' }}>$</sup>
      {row.last}
    </Cell>
    <Cell centered>
      <sup style={{ position: 'relative', top: '4px' }}>$</sup>
      {row.bid}
    </Cell>
    <Cell centered>
      <sup style={{ position: 'relative', top: '4px' }}>$</sup>
      {row.ask}
    </Cell>
  </Row>
);

const amountTypeOptions = [
  { value: 'qty', label: 'Quantity' },
  { value: 'value', label: 'Value' }
];

const orderTypeOptions = [
  { value: 'limit', label: 'Limit' },
  { value: 'market', label: 'Market' }
];

const accounts = [
  {
    value: 'NT123455678-001',
    label: 'Mr John Smith'
  },
  {
    value: 'NT123455678-002',
    label: 'Mr John Smith'
  }
];

const headersDepthBuy = [
  { name: 'Buyers', value: 'buyers' },
  { name: 'Vol', value: 'vol', align: 'right' },
  { name: 'Bid', value: 'bid', align: 'right' }
];

const tableRowDepthBuy = ({ row }) => (
  <Row>
    <Cell className="">{row.buyers}</Cell>
    <Cell className="has-text-right">{row.vol}</Cell>
    <Cell className="has-text-right">{row.bid}</Cell>
  </Row>
);

const headersDepthSell = [
  { name: 'Offer', value: 'offer' },
  { name: 'Vol', value: 'vol', align: 'right' },
  { name: 'Sellers', value: 'sellers', align: 'right' }
];

const tableRowDepthSell = ({ row }) => (
  <Row>
    <Cell className="">{row.offer}</Cell>
    <Cell className="has-text-right">{row.vol}</Cell>
    <Cell className="has-text-right">{row.sellers}</Cell>
  </Row>
);

const headersCourseOfSales = [
  { name: 'Time', value: 'time', align: 'left' },
  { name: 'Price', value: 'price', align: 'right' },
  { name: 'Qty', value: 'qty', align: 'right' }
];

const tableRowCourseOfSales = ({ row }) => (
  <Row>
    <Cell>{row.time}</Cell>
    <Cell className="has-text-right">{row.price}</Cell>
    <Cell className="has-text-right">{row.qty}</Cell>
  </Row>
);

const mfundRow = ({ row }) => (
  <Row>
    <Cell className="has-text-centered">
      <MoneyView value={row.application} />
    </Cell>
    <Cell className="has-text-centered">
      <MoneyView value={row.redemption} />
    </Cell>
  </Row>
);

export {
  accounts,
  amountTypeOptions,
  data,
  headers,
  orderTypeOptions,
  TableRow,
  headersDepthBuy,
  tableRowDepthBuy,
  headersDepthSell,
  tableRowDepthSell,
  headersCourseOfSales,
  tableRowCourseOfSales,
  mfundHeaders,
  mfundData,
  mfundRow
};
