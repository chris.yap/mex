import React from 'react';
import LoaderAnimation from '../LoaderAnimation';

import {
  Card,
  Column,
  Columns,
  Divider,
  Drawer,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Section,
  Table,
  Title
} from 'shaper-react';
import {
  headersDepthBuy,
  tableRowDepthBuy,
  headersDepthSell,
  tableRowDepthSell,
  headersCourseOfSales,
  tableRowCourseOfSales
} from './constants';

export default class SecurityDataPanel extends React.Component {
  constructor(props) {
    super();
    this.state = {
      windowWidth: window.innerWidth,
      loaded: false
    };
    this.handleResize = this.handleResize.bind(this);
  }
  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }
  componentDidUpdate(prevProps) {
    if (this.props.isOpened !== prevProps.isOpened) {
      if (this.props.isOpened === true) {
        setTimeout(() => {
          this.setState({ loaded: this.props.isOpened });
        }, 3000);
      } else {
        setTimeout(() => {
          this.setState({ loaded: this.props.isOpened });
        }, 500);
      }
    }
  }
  handleResize(e) {
    this.setState({ windowWidth: window.innerWidth });
  }
  render() {
    const { elevation, isOpened, selectedOption, width } = this.props;
    return (
      <React.Fragment>
        <Drawer
          id="supportOrderpad"
          className="has-bg-secondary lighten-5"
          elevation={!isOpened ? 0 : elevation}
          isOpened={isOpened}
          overlay={false}
          right
          width={460 + width}>
          <Columns gapless>
            <Column>
              <div
                className="has-bg-blue-grey lighten-4 is-flex is-align-items-center px-4"
                style={{ height: '4.25rem' }}>
                <Title size="4" className="mb-0 is-flex-1">
                  {selectedOption && selectedOption.name}
                </Title>
                <span className="has-text-size-1 is-pulled-right">
                  As of 10:02:02am
                </span>
              </div>

              <Divider />

              <Section className="pa-4">
                {this.state.loaded ? (
                  <React.Fragment>
                    <Title size="5" className="mb-4">
                      Quote
                    </Title>

                    <Card flat className="mb-4">
                      <List>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Last</LevelItem>
                            <LevelItem right>
                              <strong>33.110</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Change</LevelItem>
                            <LevelItem right>
                              <strong className="has-text-danger">
                                -0.090
                                <Icon>caret-down</Icon>
                                -0.27
                                <sup
                                  style={{ position: 'relative', top: '2px' }}>
                                  %
                                </sup>
                              </strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Bid</LevelItem>
                            <LevelItem right>
                              <strong>33.110</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Offer</LevelItem>
                            <LevelItem right>
                              <strong>33.120</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>High</LevelItem>
                            <LevelItem right>
                              <strong>33.135</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Low</LevelItem>
                            <LevelItem right>
                              <strong>33.040</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                        <ListItem className="is-block">
                          <Level style={{ width: '100%' }}>
                            <LevelItem left>Volume</LevelItem>
                            <LevelItem right>
                              <strong>66,325</strong>
                            </LevelItem>
                          </Level>
                        </ListItem>
                      </List>
                    </Card>

                    <Title size="5" className="mb-1">
                      Market depth
                    </Title>
                    <Columns gapless>
                      <Column>
                        <Table
                          hasStripes
                          headers={headersDepthBuy}
                          data={DataDepthBuy}
                          tableRow={tableRowDepthBuy}
                          mb={0}
                        />
                      </Column>
                      <Column>
                        <Table
                          hasStripes
                          headers={headersDepthSell}
                          data={DataDepthSell}
                          tableRow={tableRowDepthSell}
                          mb={0}
                        />
                      </Column>
                    </Columns>

                    <Title size="5" className="mb-1">
                      Course of sales
                    </Title>
                    <Columns>
                      <Column>
                        <Table
                          hasStripes
                          headers={headersCourseOfSales}
                          data={DataCourseOfSalesBuy}
                          tableRow={tableRowCourseOfSales}
                        />
                      </Column>
                      <Column>
                        <Table
                          hasStripes
                          headers={headersCourseOfSales}
                          data={DataCourseOfSalesSell}
                          tableRow={tableRowCourseOfSales}
                        />
                      </Column>
                    </Columns>
                  </React.Fragment>
                ) : (
                  <div className="py-10">
                    {/* <GridLoader css={override} color={'#909ea4'} loading={!this.state.loaded} className="my-10" /> */}
                    <LoaderAnimation
                      color="rgba(0,0,0,.2)"
                      className="my-10"
                      size="80px"
                    />
                  </div>
                )}
              </Section>
            </Column>
            <Column narrow style={{ width: width }} />
          </Columns>
        </Drawer>
      </React.Fragment>
    );
  }
}

const DataDepthBuy = [
  { buyers: 2, vol: 131, bid: '33.110' },
  { buyers: 4, vol: 755, bid: '33.100' },
  { buyers: 3, vol: 245, bid: '33.090' },
  { buyers: 3, vol: 813, bid: '33.080' },
  { buyers: 5, vol: 899, bid: '33.070' }
  // { buyers: , vol: , bid: '' },
];

const DataDepthSell = [
  { offer: '33.120', vol: 46, sellers: 2 },
  { offer: '33.130', vol: 212, sellers: 2 },
  { offer: '33.140', vol: 791, sellers: 3 },
  { offer: '33.150', vol: 833, sellers: 5 },
  { offer: '33.160', vol: '1,203', sellers: 5 }
  // { offer: , vol: , sellers: '' },
];

const DataCourseOfSalesBuy = [
  { time: '10:02:35', price: '33.110', qty: 95 },
  { time: '10:02:35', price: '33.110', qty: 275 },
  { time: '10:02:33', price: '33.105', qty: 52 }
];

const DataCourseOfSalesSell = [
  { time: '10:02:35', price: '33.105', qty: 35 },
  { time: '10:02:35', price: '33.110', qty: 22 },
  { time: '10:02:33', price: '33.110', qty: 352 }
];
