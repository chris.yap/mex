import React from 'react';
import {
  Label,
  Radio,
  RadioGroup,
  Section,
  Select,
  Textfield
} from 'shaper-react';
import { AccountSingleValue, AccountOption } from '../../helpers/CustomSelect';
import { accounts } from './constants';

class MFundBuy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distribution: 'cash'
    };
  }
  handleDistribution = value => {
    this.setState({
      distribution: value
    });
  };
  render = () => {
    return (
      <Section className="px-4 pt-4 pb-10">
        <Select
          label="Account to trade from"
          components={{
            Option: AccountOption,
            SingleValue: AccountSingleValue
          }}
          defaultMenuIsOpen={false}
          defaultValue={accounts[0]}
          height={60}
          options={accounts}
        />
        <Label className="nab-label mb-0">
          Minimum initial investment amount
        </Label>
        <p>$20,000.00</p>
        <Textfield
          label="Investment amount"
          placeholder="Enter value"
          hint=""
        />
        <RadioGroup
          name="distribution"
          buttons
          fullwidth
          label="Distribution preference"
          selectedValue={this.state.distribution}
          onChange={this.handleDistribution}
        >
          <Radio button value="cash" label="Cash" />
          <Radio button value="reinvestment" label="Reinvestment" />
        </RadioGroup>
      </Section>
    );
  };
}

export default MFundBuy;
