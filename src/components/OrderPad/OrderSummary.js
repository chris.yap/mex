import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import Div100vh from 'react-div-100vh';
import { getState } from '../../context/StateProvider';
import {
  Alert,
  Button,
  Card,
  Content,
  Div,
  Divider,
  Drawer,
  Footer,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Section,
  Subtitle,
  Table,
  Tag,
  Title
} from 'shaper-react';
import { headers, TableRow, data } from './constants';
import Overlay from '../../components/Overlay';
import RealtimeQuoteMsg from '../RealtimeQuoteMsg';

const OrderSummary = ({
  closeOrderPad,
  elevation,
  error,
  logo,
  selectedOption,
  toggleDrawer,
  width,
  ...props
}) => {
  const [{ orderPad, orderSummary }, dispatch] = getState();
  const [message, setMessage] = useState(false);

  return (
    <Drawer
      id="order-summary"
      className="pb-10"
      contentClassName={`is-overflow-visible`}
      elevation={!orderSummary.isOpened ? 0 : elevation}
      isOpened={orderSummary.isOpened}
      logo={logo}
      overlay={false}
      right
      variant="temporary"
      width={width}
      style={{ zIndex: 1001 }}>
      <Div100vh className="is-flex is-flex-column">
        <div className="is-flex-1-1-auto is-overflow-auto">
          <Title
            size="5"
            className="has-text-weight-bold is-flex is-align-items-center has-bg-white pl-4 pr-2 my-0"
            style={{ height: '4.25rem' }}>
            <Button inverted square className="mr-4" onClick={toggleDrawer}>
              <Icon>arrow-left</Icon>
            </Button>
            Order summary
          </Title>

          <Divider />

          <div className="is-flex-column has-bg-blue-grey lighten-5">
            <Section className="pa-4">
              <Title
                size="3"
                className="font-nabimpact has-text-ellipsis has-text-centered">
                {selectedOption ? selectedOption.label : null}
              </Title>
              <Subtitle
                size="7"
                className="has-text-centered has-text-ellipsis">
                {selectedOption ? selectedOption.code : null} |{' '}
                {selectedOption ? selectedOption.exchange : null}
              </Subtitle>
              <Table
                isNarrow
                hasStripes
                headers={headers}
                data={data}
                tableRow={TableRow}
                mb={4}
              />
              <RealtimeQuoteMsg
                local
                className="has-text-blue-grey has-text-centered"
              />

              <Card flat className="mb-2">
                <List>
                  <ListItem>
                    <Level mobile className="is-flex-1">
                      <LevelItem left>Order type</LevelItem>
                      <LevelItem right>
                        <strong>
                          <Tag
                            success={orderPad.buySell === 0}
                            danger={orderPad.buySell === 1}>
                            {orderPad.buySell === 0 ? 'Buy' : 'Sell'}
                          </Tag>
                        </strong>
                      </LevelItem>
                    </Level>
                  </ListItem>
                  {summary.map((summ, sIndex) => (
                    <ListItem key={sIndex}>
                      <Level mobile style={{ width: '100%' }}>
                        <LevelItem left>{summ.name}</LevelItem>
                        <LevelItem right>
                          <strong>{summ.value}</strong>
                        </LevelItem>
                      </Level>
                    </ListItem>
                  ))}
                </List>
              </Card>

              <Card flat className="mb-4">
                <List>
                  {summary2.map((summ, sIndex) => (
                    <ListItem key={sIndex}>
                      <Level mobile style={{ width: '100%' }}>
                        <LevelItem left>{summ.name}</LevelItem>
                        <LevelItem right>
                          <strong>{summ.value}</strong>
                        </LevelItem>
                      </Level>
                    </ListItem>
                  ))}
                </List>
              </Card>

              <Content small>
                <p>
                  Trade confirmations will record brokerage charges and
                  WealthHub Securities does not accept any responsibility for
                  market movements.
                </p>
                <p>
                  You must ensure sufficient funds are available to cover the
                  total consideration of the order.
                </p>
                <p>
                  For full disclosure of fees associated with international
                  orders please <a href="/#">click here</a>.
                </p>
              </Content>
            </Section>
          </div>

          <Overlay absolute isOpened={orderPad.overlay} spinner />
        </div>

        <Footer
          fixed
          bg="white"
          py={2}
          mt={0}
          borderTop="1px solid rgba(0,0,0,.15)"
          boxShadow={orderSummary.orderConfirmation && 24}>
          {orderSummary.orderConfirmation && !orderSummary.orderSubmit ? (
            <React.Fragment>
              {message ? (
                <Alert isOpened={true} inverted warning mt={2} mb={4}>
                  Error submitting order. Please try again.
                </Alert>
              ) : (
                <Alert info inverted isOpened={true} mt={2} mb={4}>
                  <p className="mb-1">
                    You are about to place the following order:{' '}
                  </p>
                  <p className="has-text-size2">
                    BUY X units of XXX.XXX at market price.
                  </p>
                  <p className="mb-0">
                    Please confirm by clicking on the submit button.
                  </p>
                </Alert>
              )}
              <Div display="flex">
                <Button
                  block
                  loading={!message}
                  primary
                  className="ml-0 px-10"
                  onClick={
                    error.includes('validate') || error.includes('create')
                      ? () => {
                          setTimeout(() => {
                            setMessage(true);
                          }, 2000);
                        }
                      : () => {
                          dispatch({ type: 'toggleOrderPadOverlay' });
                          setTimeout(() => {
                            dispatch({ type: 'toggleOrderPadOverlay' });
                            dispatch({ type: 'toggleSubmittingOrder' });
                          }, 3000);
                        }
                  }>
                  Submit
                </Button>
                <Button
                  secondary
                  flat
                  onClick={() => dispatch({ type: 'handleCloseConfirmation' })}>
                  Cancel
                </Button>
              </Div>
            </React.Fragment>
          ) : orderSummary.orderConfirmation && orderSummary.orderSubmit ? (
            <React.Fragment>
              <Alert success inverted isOpened={true} my={2}>
                <strong>
                  Your order #XXXXXXXX has been created and currently being
                  authorised. Please go your open orders to view your status.
                </strong>
              </Alert>
              <Div display="flex">
                <NavLink to="/portfolio/open-orders">
                  <Button
                    block
                    secondary
                    className="ml-0 mr-1"
                    onClick={() => {
                      dispatch({ type: 'toggleOrderPad' });
                      dispatch({
                        type: 'orderPadSelectOption',
                        payload: null
                      });
                      dispatch({ type: 'toggleOrderSummary' });
                      dispatch({ type: 'handleOpenConfirmation' });
                      dispatch({ type: 'toggleSubmittingOrder' });
                    }}>
                    Go to Open orders
                  </Button>
                </NavLink>
                <Button
                  primary
                  className="ml-1 mr-0"
                  onClick={() => {
                    dispatch({ type: 'toggleOrderPad', payload: false });
                    dispatch({
                      type: 'orderPadSelectOption',
                      payload: null
                    });
                    dispatch({
                      type: 'toggleOrderSummary',
                      payload: false
                    });
                    dispatch({
                      type: 'handleCloseConfirmation',
                      payload: false
                    });
                    dispatch({
                      type: 'toggleSubmittingOrder',
                      payload: false
                    });
                  }}>
                  Close
                </Button>
              </Div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Div display="flex">
                <Button
                  block
                  disabled={error.includes('nonKyc')}
                  className="is-flex-2"
                  primary
                  onClick={() => dispatch({ type: 'handleOpenConfirmation' })}>
                  Place order
                </Button>
                <Button className="is-flex-1" flat onClick={toggleDrawer}>
                  Amend order
                </Button>
              </Div>
            </React.Fragment>
          )}
        </Footer>
      </Div100vh>
    </Drawer>
  );
};

export default OrderSummary;

const summary = [
  {
    name: 'Volume',
    value: '1000'
  },
  {
    name: 'Order price',
    value: '$72.41'
  },
  {
    name: 'Duration',
    value: 'Today'
  }
];

const summary2 = [
  {
    name: 'Brokerage',
    value: '$79.65'
  },
  {
    name: 'Includes GST',
    value: '$7.24'
  },
  {
    name: 'Other fees',
    value: '$0.00'
  },
  {
    name: <strong>Total</strong>,
    value: '$72,225.96 AUD'
  }
];
