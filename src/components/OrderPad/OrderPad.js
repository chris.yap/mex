import React from 'react';
import cn from 'classnames';
// import Div100vh from 'react-div-100vh';
import { getState } from '../../context/StateProvider';
import OrderPadPortfolioRelated from './OrderPadPortfolioRelated';
import {
  Alert,
  Breakpoints,
  Box,
  Button,
  Column,
  Columns,
  Drawer,
  Icon,
  Level,
  LevelItem,
  Modal,
  Section,
  Subtitle,
  Table,
  Tabs,
  Tab,
  Title
} from 'shaper-react';
import Buy from './Buy';
import Sell from './Sell';
import MFundBuy from './mFundBuy';
import MFundSell from './mFundSell';
import {
  headers,
  TableRow,
  data,
  mfundData,
  mfundHeaders,
  mfundRow
} from './constants';
import OrderPadSearch from './OrderPadSearch';
import OrderSummary from './OrderSummary';
import MFundOrderSummary from './mFundOrderSummary';
import SecurityDataPanel from './SecurityDataPanel';
import OrderPadFooter from './OrderPadFooter';
import MFundFooter from './mFundFooter';
import Overlay from '../../components/Overlay';
import EtoListInfo from './EtoListInfo';
import RealtimeQuoteMsg from '../RealtimeQuoteMsg';

const OrderPad = ({
  activeBreakpoints,
  className,
  elevation,
  error,
  logo,
  onClose,
  placement,
  variant,
  width,
  ...props
}) => {
  const [{ orderPad }, dispatch] = getState();

  return (
    <React.Fragment>
      <SecurityDataPanel
        isOpened={orderPad.moreInfo}
        selectedOption={orderPad.selectedOption}
        width={width}
      />
      <Drawer
        id="orderpad"
        className={cn('pb-10', className)}
        contentClassName={`pb-10`}
        elevation={!orderPad.isOpened ? 0 : elevation}
        isOpened={orderPad.isOpened}
        logo={logo}
        overlay={false}
        right
        variant="temporary"
        width={width}
        style={{ zIndex: 1001 }}
        toggleDrawer={() => dispatch({ type: 'closeOrderPad' })}
        {...props}>
        {/* <div className="is-flex is-flex-column">
          <div className="is-flex-1-1-auto"> */}
        <OrderPadSearch error={error} />
        <div
          className="is-flex-column has-bg-blue-grey lighten-5"
          style={{ minHeight: 'calc(100vh - 131px)' }}>
          <Section className="pa-4">
            {!orderPad.selectedOption ? (
              <OrderPadPortfolioRelated />
            ) : (
              <React.Fragment>
                <Alert isOpened={error.includes('nonKyc')} inverted warning>
                  <Title size="5">Profile not verified</Title>
                  <p>We cannot find your profile or it is inactive.</p>
                  <p>
                    If you have joined nabtrade and not completed your identity
                    verification, please visit the full site at{' '}
                    <a
                      href="https://www.nabtrade.com.au"
                      target="_blank"
                      rel="noopener noreferrer">
                      nabtrade.com.au
                    </a>{' '}
                    to do this. You can complete the pending verification action
                    from the Outstanding actions panel at the top of any of the
                    Admin menu pages.
                  </p>
                  <p className="mb-0">
                    If you are a returning customer, we may need to reactivate
                    your profile. Visit{' '}
                    <a
                      href="https://www.nabtrade.com.au/investor/support"
                      target="_blank"
                      rel="noopener noreferrer">
                      Support
                    </a>{' '}
                    for more information.{' '}
                  </p>
                </Alert>
                <Title
                  size="3"
                  className="font-nabimpact has-text-ellipsis has-text-centered">
                  {orderPad.selectedOption.label}
                </Title>
                <Subtitle
                  size="7"
                  className="has-text-centered has-text-ellipsis">
                  {orderPad.selectedOption.code} |{' '}
                  {orderPad.selectedOption.exchange}
                </Subtitle>
                {error.includes('stock') ? (
                  <Alert isOpened={true} danger inverted mb={2}>
                    Error retrieving stock price. <br />
                    <Button small danger className="mx-0">
                      Try again
                    </Button>
                  </Alert>
                ) : (
                  <Columns gapless mobile className="mb-0">
                    {activeBreakpoints.above.md &&
                      orderPad.selectedOption.exchange === 'ASX' &&
                      !orderPad.selectedOption.type && (
                        <Column narrow>
                          <Button
                            square
                            large
                            secondary={orderPad.moreInfo}
                            className={cn('ml-0 mt-0 mr-2')}
                            onClick={() =>
                              dispatch({ type: 'toggleMoreInfo' })
                            }>
                            <Icon>directions</Icon>
                          </Button>
                        </Column>
                      )}
                    <Column>
                      {orderPad.selectedOption.type === 'mfund' ? (
                        <Table
                          hasNoScroll
                          isNarrow
                          hasStripes
                          headers={mfundHeaders}
                          data={mfundData}
                          tableRow={mfundRow}
                          mt={0}
                          mb={4}
                        />
                      ) : (
                        <Table
                          hasNoScroll
                          isNarrow
                          hasStripes
                          headers={headers}
                          data={data}
                          tableRow={TableRow}
                          mt={0}
                          mb={orderPad.selectedOption.type === 'eto' ? 0 : 4}
                        />
                      )}
                    </Column>
                  </Columns>
                )}
                {orderPad.selectedOption.type === 'eto' && <EtoListInfo />}
                <RealtimeQuoteMsg
                  local
                  className="has-text-blue-grey has-text-centered mb-0"
                />
              </React.Fragment>
            )}
          </Section>

          {orderPad.selectedOption && (
            <div className="is-flex-1 has-bg-white">
              <Tabs boxed fullwidth selectedTab={orderPad.buySell}>
                <Tab
                  label="Buy"
                  success={true}
                  tabClick={() =>
                    dispatch({ type: 'selectBuySell', payload: 0 })
                  }>
                  {orderPad.selectedOption.type !== 'mfund' ? (
                    <Buy
                      error={error}
                      selectedOption={orderPad.selectedOption}
                      conditionalOrder={orderPad.conditionalOrder}
                      help={orderPad.getHelp}
                    />
                  ) : (
                    <MFundBuy error={error} />
                  )}
                </Tab>
                <Tab
                  label="Sell"
                  danger={true}
                  tabClick={() =>
                    dispatch({ type: 'selectBuySell', payload: 1 })
                  }>
                  {orderPad.selectedOption.type !== 'mfund' ? (
                    <Sell
                      error={error}
                      selectedOption={orderPad.selectedOption}
                      conditionalOrder={orderPad.conditionalOrder}
                      help={orderPad.getHelp}
                    />
                  ) : (
                    <MFundSell error={error} />
                  )}
                </Tab>
              </Tabs>
            </div>
          )}
        </div>
        <Overlay absolute isOpened={orderPad.coOverlay} transparent />
        {orderPad.selectedOption &&
        orderPad.selectedOption.type &&
        orderPad.selectedOption.type === 'mfund' ? (
          <MFundFooter />
        ) : (
          <OrderPadFooter />
        )}
        <Overlay absolute isOpened={orderPad.overlay} spinner />
      </Drawer>

      {/* Order Summary */}
      {orderPad.selectedOption &&
      orderPad.selectedOption.type &&
      orderPad.selectedOption.type === 'mfund' ? (
        <MFundOrderSummary
          error={error}
          closeOrderPad={() => dispatch({ type: 'toggleOrderpad' })}
          isOpened={orderPad.summary}
          selectedOption={orderPad.selectedOption}
          toggleDrawer={() => dispatch({ type: 'toggleOrderSummary' })}
          width={width}
        />
      ) : (
        <OrderSummary
          error={error}
          closeOrderPad={() => dispatch({ type: 'toggleOrderpad' })}
          isOpened={orderPad.summary}
          selectedOption={orderPad.selectedOption}
          toggleDrawer={() => dispatch({ type: 'toggleOrderSummary' })}
          width={width}
        />
      )}

      {/* HELP */}
      <Modal
        isOpened={!!orderPad.help}
        onClose={() => dispatch({ type: 'closeHelp' })}
        header={
          orderPad.help === 1
            ? 'What is a stop order?'
            : "What is a 'fixed' trigger price?"
        }
        footer={
          <Level mobile className="is-flex-1">
            <LevelItem left>
              <Button secondary>
                <Icon>arrow-left</Icon>
                <span>Stop loss</span>
              </Button>
              <Button secondary>
                <span>Take profit</span>
                <Icon>arrow-right</Icon>
              </Button>
            </LevelItem>
            <LevelItem right>
              <Button
                flat
                danger
                onClick={() => dispatch({ type: 'closeHelp' })}>
                <Icon>close</Icon>
                <span>Close</span>
              </Button>
            </LevelItem>
          </Level>
        }>
        {orderPad.help === 1 ? (
          <React.Fragment>
            <p>
              A stop entry order is an order to buy a security when its price
              increases to or above a specified price.
            </p>
            <p>
              Stop entry orders enable investors to benefit where they believe
              there are gains to be made in a security once the price reaches a
              higher level.
            </p>
            <img src="/images/chart-stop-entry.png" alt="stop entry" />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <p>
              A ‘fixed’ trigger price will be set at a level above or below the
              current market price as either a price or percentage.
            </p>
            <p>
              The trigger price will remain at the same level until it is either
              triggered or the Investor amends or cancels the Instruction.
            </p>
            <img src="/images/chart-fixed.png" alt="fixed" />
            <Box />
          </React.Fragment>
        )}
      </Modal>
    </React.Fragment>
  );
};

export default Breakpoints(OrderPad);
