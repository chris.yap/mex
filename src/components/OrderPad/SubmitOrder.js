import React from 'react';
import { NavLink } from 'react-router-dom';
import { Alert, Button, Modal } from 'shaper-react';

export default class SubmitOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitOrder: false
    };
    this.toggleOrder = this.toggleOrder.bind(this);
  }
  toggleOrder() {
    if (this.state.submitOrder) {
      this.setState({ submitOrder: !this.state.submitOrder });
    } else {
      setTimeout(() => {
        this.setState({ submitOrder: !this.state.submitOrder });
      }, 2000);
    }
  }
  render() {
    const { isOpened, closeModal, closeOrderPad } = this.props;
    return (
      <Modal
        header="Submit order"
        isOpened={isOpened}
        onClose={closeModal}
        footer={
          <React.Fragment>
            {!this.state.submitOrder ? (
              <React.Fragment>
                <Button
                  loading
                  primary
                  className="ml-0 px-10"
                  onClick={this.toggleOrder}>
                  Submit
                </Button>
                <Button secondary flat onClick={closeModal}>
                  Cancel
                </Button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <NavLink
                  to="/portfolio/open-orders"
                  onClick={() => (
                    closeOrderPad(), closeModal(), this.toggleOrder()
                  )}
                  className="nab-button is-secondary ml-0">
                  Go to Open orders
                </NavLink>
                <Button
                  secondary
                  flat
                  onClick={() => (
                    closeOrderPad(), closeModal(), this.toggleOrder()
                  )}>
                  Close
                </Button>
              </React.Fragment>
            )}
          </React.Fragment>
        }>
        {!this.state.submitOrder ? (
          <React.Fragment>
            <p className="mb-0">You are about to place the following order: </p>
            <p className="has-text-size2">
              BUY X units of XXX.XXX at market price.
            </p>
            <p className="mb-0">
              Please confirm by clicking on the submit button.
            </p>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Alert success inverted isOpened={true} mb={0}>
              <strong>
                Your order #XXXXXXXX has been created and currently being
                authorised.
              </strong>
            </Alert>
            <p className="mb-0" />
          </React.Fragment>
        )}
      </Modal>
    );
  }
}
