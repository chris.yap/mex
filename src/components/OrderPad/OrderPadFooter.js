import React from 'react';
import { NavLink } from 'react-router-dom';
import { getState } from '../../context/StateProvider';
import { Alert, Breakpoints, Button, Div, Footer } from 'shaper-react';

const OrderPadFooter = ({ activeBreakpoints, ...props }) => {
  const [{ orderPad }, dispatch] = getState();
  let handleChange = selectedOption => {
    dispatch({
      type: 'orderPadSelectOption',
      payload: selectedOption ? selectedOption : null
    });
    dispatch({ type: 'toggleOrderPadSearchBox', payload: '0' });
    if (!selectedOption) {
      dispatch({ type: 'closeMoreInfo' });
    }
  };
  return (
    <Footer
      fixed
      bg="white"
      py={2}
      mt={0}
      overflow="auto"
      maxHeight="100vh"
      borderTop="1px solid rgba(0,0,0,.15)"
      style={{
        zIndex: orderPad.coOverlay ? 201 : 10
      }}>
      {orderPad.conditionalOrderConfirmation ? (
        <React.Fragment>
          <Alert success inverted isOpened={true} mt={2} mb={4}>
            <strong>
              Your order #XXXXXXXX has been created. Please go to your open
              orders to view your status.
            </strong>
          </Alert>
          <Div display="flex">
            <NavLink to="/portfolio/open-orders">
              <Button
                secondary
                block
                onClick={() => {
                  dispatch({
                    type: 'updateConditionalOrderConfirmation',
                    payload: false
                  });
                  dispatch({
                    type: 'updatePlaceConditionalOrder',
                    payload: false
                  });
                  dispatch({ type: 'setConditionalOrder', payload: false });
                }}>
                Go to Open orders
              </Button>
            </NavLink>
            <Button
              primary
              onClick={() => {
                dispatch({
                  type: 'updateConditionalOrderConfirmation',
                  payload: false
                });
                dispatch({
                  type: 'updatePlaceConditionalOrder',
                  payload: false
                });
                dispatch({ type: 'orderPadSelectOption', payload: null });
                dispatch({ type: 'setConditionalOrder', payload: false });
                dispatch({ type: 'toggleOrderPad' });
              }}>
              Close
            </Button>
          </Div>
        </React.Fragment>
      ) : orderPad.placeConditionalOrder ? (
        <React.Fragment>
          <Alert info inverted isOpened={true} mt={2} mb={4}>
            <p>You are about to place the following conditional order:</p>
            <p>
              If last proce of BHP Billiton Limited (BHP.ASX){' '}
              <strong>increases by 5%</strong> then{' '}
              <strong>BUY 20 units</strong> of BHP.ASX at price{' '}
              <strong>25</strong>.
            </p>
            <p className="mb-0">
              Please confirm by clicking the confirm order button.
            </p>
          </Alert>
          <Div display="flex">
            <Button
              primary
              block
              loading
              onClick={() => {
                dispatch({ type: 'toggleOrderPadCoOverlay' });
                dispatch({ type: 'toggleOrderPadOverlay' });
                setTimeout(() => {
                  dispatch({
                    type: 'updateConditionalOrderConfirmation',
                    payload: true
                  });
                  dispatch({
                    type: 'updatePlaceConditionalOrder',
                    payload: false
                  });
                  dispatch({ type: 'setConditionalOrder', payload: false });
                  dispatch({ type: 'orderPadSelectOption', payload: null });
                  dispatch({ type: 'toggleOrderPadOverlay' });
                }, 3000);
              }}>
              Confirm order
            </Button>
            <Button
              block
              flat
              onClick={() => {
                dispatch({
                  type: 'updatePlaceConditionalOrder',
                  payload: false
                });
                dispatch({ type: 'toggleOrderPadCoOverlay' });
              }}>
              Cancel
            </Button>
          </Div>
        </React.Fragment>
      ) : (
        <Div display="flex">
          {!orderPad.conditionalOrder ? (
            <Button
              fullwidth
              primary
              onClick={() => dispatch({ type: 'toggleOrderSummary' })}
              disabled={!orderPad.selectedOption}>
              Review order
            </Button>
          ) : (
            <Button
              primary
              onClick={() => {
                dispatch({ type: 'reviewConditionerOrder' });
                dispatch({ type: 'toggleOrderPadCoOverlay' });
              }}>
              Submit
            </Button>
          )}
          {!orderPad.selectedOption ? (
            <Button
              flat
              fullwidth
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              Cancel
            </Button>
          ) : (
            <Button flat onClick={() => handleChange(null)}>
              Reset
            </Button>
          )}
        </Div>
      )}
    </Footer>
  );
};

export default Breakpoints(OrderPadFooter);
