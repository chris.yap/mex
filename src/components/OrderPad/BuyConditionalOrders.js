import React from 'react';
import cn from 'classnames';
import { getState } from '../../context/StateProvider';
import {
  Checkbox,
  CheckboxGroup,
  Collapse,
  Divider,
  Label,
  MultiField,
  Radio,
  RadioGroup,
  Section,
  Select,
  Textfield,
  Title
} from 'shaper-react';

const BuyConditionalOrders = ({ ...props }) => {
  const [{ conditionalOrder }, dispatch] = getState();

  let conditionalOrdersChanged = value => {
    let coExpand = false;
    if (!conditionalOrder.conditionalOrders.includes('co')) {
      coExpand = true;
    }
    dispatch({ type: 'updateConditionalOrders', payload: value });
    dispatch({ type: 'updateCoExpand', payload: coExpand });
    dispatch({ type: 'setConditionalOrder', payload: coExpand });
  };
  let handleCondition = value => {
    let newTriggerType = conditionalOrder.triggerType;
    if (value === 'stopentry') {
      newTriggerType = 'fixed';
    }
    dispatch({ type: 'handleTriggerType', payload: newTriggerType });
    dispatch({ type: 'updateCondition', payload: value });
  };
  return (
    <React.Fragment>
      <Section className="pa-0">
        <Divider className="mb-2" />
        <CheckboxGroup
          right
          fullwidth
          isSwitch
          name="conditionalOrders"
          value={conditionalOrder.conditionalOrders}
          onChange={conditionalOrdersChanged}
          className="mb-0"
          mb={0}>
          <Checkbox
            value="co"
            label={<span className="nab-label">Conditional order</span>}
            className="mb-0"
          />
        </CheckboxGroup>
        <Collapse isOpened={conditionalOrder.coExpand}>
          <RadioGroup
            name="condition"
            label={
              <React.Fragment>
                Condition{' '}
                <a
                  href="/#"
                  onClick={() => dispatch({ type: 'getHelp', payload: 1 })}
                  className="ml-2 is-underlined is-capitalized has-text-primary">
                  What's this?
                </a>
              </React.Fragment>
            }
            buttons
            fullwidth
            required
            selectedValue={conditionalOrder.condition}
            onChange={handleCondition}
            className="mt-2">
            <Radio
              button
              value="stopentry"
              label="Stop entry"
              className="mb-0"
            />
            <Radio button value="advanced" label="Advanced" className="mb-0" />
          </RadioGroup>

          <RadioGroup
            name="trigger"
            label={
              <React.Fragment>
                Trigger{' '}
                <a
                  href="/#"
                  onClick={() => dispatch({ type: 'getHelp', payload: 2 })}
                  className="ml-2 is-underlined is-capitalized has-text-primary">
                  What's this?
                </a>
              </React.Fragment>
            }
            required
            buttons
            fullwidth
            selectedValue={
              conditionalOrder.condition === 'stopentry'
                ? 'fixed'
                : conditionalOrder.triggerType
            }
            onChange={() => dispatch({ type: 'handleTriggerType' })}
            className="mt-2">
            <Radio button value="fixed" label="Fixed" className="mb-0" />
            <Radio
              button
              value="trailing"
              label="Trailing"
              className="mb-0"
              disabled={
                conditionalOrder.condition === 'stopentry' ? true : false
              }
            />
          </RadioGroup>

          {conditionalOrder.condition === 'advanced' &&
            conditionalOrder.triggerType === 'fixed' && (
              <RadioGroup
                name="lastprice"
                label="If last price"
                required
                buttons
                fullwidth
                selectedValue={conditionalOrder.ifLastPrice}
                onChange={() => dispatch({ type: 'handleIfLastPrice' })}
                className="mt-2">
                <Radio button value="smallerEqual" label="≤" className="mb-0" />
                <Radio button value="smaller" label="<" className="mb-0" />
                <Radio button value="larger" label=">" className="mb-0" />
                <Radio button value="largerEqual" label="≥" className="mb-0" />
              </RadioGroup>
            )}

          {conditionalOrder.triggerType === 'fixed' && (
            <MultiField
              required
              label={
                conditionalOrder.trigger &&
                conditionalOrder.trigger.value === '$'
                  ? 'Trigger price'
                  : 'Trigger percentage'
              }
              leftClassName="is-flex-2"
              rightClassName="is-flex-3"
              leftSlot={
                <Select
                  isSearchable={false}
                  options={triggers}
                  defaultValue={triggers[0]}
                  onChange={() => dispatch({ type: 'handleTrigger' })}
                />
              }
              rightSlot={
                <React.Fragment>
                  {conditionalOrder.trigger &&
                  conditionalOrder.trigger.value === '$' ? (
                    <Textfield placeholder="Enter a trigger price" />
                  ) : (
                    <Textfield placeholder="Enter a trigger percentage" />
                  )}
                </React.Fragment>
              }
            />
          )}

          {conditionalOrder.condition === 'advanced' &&
            conditionalOrder.triggerType === 'trailing' && (
              <React.Fragment>
                <RadioGroup
                  name="traillastprice"
                  label="Trail last price"
                  required
                  buttons
                  fullwidth
                  selectedValue={conditionalOrder.trailLastPrice}
                  onChange={() => dispatch({ type: 'handleTrailLastPrice' })}
                  className="mt-2">
                  <Radio button value="above" label="Above" className="mb-0" />
                  <Radio button value="below" label="Below" className="mb-0" />
                </RadioGroup>

                <MultiField
                  leftClassName="is-flex-2"
                  rightClassName="is-flex-3"
                  leftSlot={
                    <Select
                      isSearchable={false}
                      options={triggers}
                      defaultValue={triggers[0]}
                      onChange={this.handleTriggers}
                    />
                  }
                  rightSlot={
                    <Textfield placeholder="Enter a trailing offset" />
                  }
                />
              </React.Fragment>
            )}

          {conditionalOrder.trigger &&
            conditionalOrder.trigger.value === '%' &&
            conditionalOrder.triggerType === 'fixed' && (
              <div className="nab-box pa-4">
                <Label className="nab-label mb-0">
                  Converted trigger price
                </Label>
                <Title size="5" className="mb-0">
                  $0
                </Title>
              </div>
            )}
        </Collapse>
        <Divider
          className={cn('mb-2', conditionalOrder.coExpand ? 'mt-4' : 'mt-2')}
        />
      </Section>
    </React.Fragment>
  );
};

export default BuyConditionalOrders;

const triggers = [
  {
    label: '$ Price',
    value: '$'
  },
  {
    label: '% Percentage',
    value: '%'
  }
];
