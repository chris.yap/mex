import React from 'react';
import { Portfolio, RecentSearches } from '../../constants/securities';
import { getState } from '../../context/StateProvider';
import {
  Card,
  Column,
  Columns,
  Icon,
  List,
  ListItem,
  Subtitle,
  Tab,
  Tabs,
  Title
} from 'shaper-react';

const OrderPadPortfolioRelated = () => {
  const [, dispatch] = getState();
  let handleChange = selectedOption => {
    dispatch({
      type: 'orderPadSelectOption',
      payload: selectedOption ? selectedOption : null
    });
    dispatch({ type: 'toggleOrderPadSearchBox', payload: '0' });
    if (!selectedOption) {
      dispatch({ type: 'closeMoreInfo' });
    }
  };
  return (
    <Tabs>
      <Tab label="Portfolio">
        <Card flat>
          <List>
            {Portfolio.map((security, sIndex) => (
              <ListItem key={sIndex} onClick={() => handleChange(security)}>
                <Columns mobile style={{ width: 'calc(100% + 1.5rem)' }}>
                  <Column className="is-overflow-hidden">
                    <Title size="5" className="mb-4 has-text-ellipsis">
                      {security.label}
                    </Title>
                    <Subtitle size="7" className="mb-0 has-text-uppercase">
                      {security.code} | {security.exchange}
                    </Subtitle>
                  </Column>
                  <Column narrow className="is-flex is-align-items-center px-2">
                    <Icon className="has-text-primary">arrow-right</Icon>
                  </Column>
                </Columns>
              </ListItem>
            ))}
          </List>
        </Card>
      </Tab>
      <Tab label="Recent searches">
        <Card flat>
          <List>
            {RecentSearches.map((security, sIndex) => (
              <ListItem key={sIndex} onClick={() => handleChange(security)}>
                <Columns mobile style={{ width: 'calc(100% + 1.5rem)' }}>
                  <Column className="is-overflow-hidden">
                    <Title size="5" className="mb-4 has-text-ellipsis">
                      {security.label}
                    </Title>
                    <Subtitle size="7" className="mb-0 has-text-uppercase">
                      {security.code} | {security.exchange}
                    </Subtitle>
                  </Column>
                  <Column narrow className="is-flex is-align-items-center px-2">
                    <Icon className="has-text-primary">arrow-right</Icon>
                  </Column>
                </Columns>
              </ListItem>
            ))}
          </List>
        </Card>
      </Tab>
    </Tabs>
  );
};

export default OrderPadPortfolioRelated;
