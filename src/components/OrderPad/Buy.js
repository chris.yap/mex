import React from 'react';
import cn from 'classnames';
import {
  Alert,
  DatePicker,
  MultiField,
  Radio,
  RadioGroup,
  Section,
  Select,
  Textfield
} from 'shaper-react';
import { AccountSingleValue, AccountOption } from '../../helpers/CustomSelect';
import { accounts, amountTypeOptions, orderTypeOptions } from './constants';
import BuyConditionalOrders from './BuyConditionalOrders';

export default class Buy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderType: 'limit',
      duration: 'until cancelled',
      amountType: 'qty'
    };
    this.handleOrderType = this.handleOrderType.bind(this);
    this.handleDuration = this.handleDuration.bind(this);
    this.handleAmountType = this.handleAmountType.bind(this);
  }
  componentDidMount() {
    if (this.props.selectedOption.exchange !== 'ASX') {
      this.setState({
        duration: 'today'
      });
    }
  }
  handleOrderType(event) {
    var value = event;
    if (typeof event === 'object') {
      console.log(event.value);
      value = event.value;
    }
    this.setState({ orderType: value });
  }
  handleDuration(event) {
    this.setState({ duration: event });
  }
  handleAmountType(event) {
    var value = event;
    if (typeof event === 'object') {
      console.log(event.value);
      value = event.value;
    }
    this.setState({ amountType: value });
  }
  updateLimitPrice = event => {
    let aud = parseFloat(event.target.value * 1.38).toFixed(2);
    this.setState({
      limitPrice: event.target.value,
      aud: aud
    });
  };
  render() {
    const { conditionalOrder, error, selectedOption, help } = this.props;
    return (
      <Section className="px-4 pt-4 pb-10">
        <Select
          label="Account to trade from"
          components={{
            Option: AccountOption,
            SingleValue: AccountSingleValue
          }}
          defaultMenuIsOpen={false}
          defaultValue={accounts[0]}
          height={60}
          options={accounts}
        />

        <BuyConditionalOrders conditionalOrder={conditionalOrder} help={help} />

        <MultiField
          required
          label={`${
            error.includes('balance') && this.state.amountType === 'qty'
              ? 'Units'
              : !error.includes('balance') && this.state.amountType === 'qty'
              ? 'Units (0 available)'
              : error.includes('balance')
              ? 'Value'
              : 'Value ($0.00 available)'
          }`}
          leftClassName="is-flex-2"
          rightClassName="is-flex-3"
          className="is-hidden-touch"
          leftSlot={
            <Select
              isSearchable={false}
              options={amountTypeOptions}
              defaultValue={amountTypeOptions[0]}
              onChange={this.handleAmountType}
            />
          }
          rightSlot={
            <React.Fragment>
              {this.state.amountType === 'qty' ? (
                <Textfield placeholder="Enter a number of units" />
              ) : (
                <Textfield placeholder="Enter a value" />
              )}
            </React.Fragment>
          }
        />

        {selectedOption.exchange === 'ASX' && (
          <MultiField
            required
            label={
              this.state.orderType === 'limit'
                ? 'Limit price in AUD'
                : 'Order at market price'
            }
            leftClassName="is-flex-2"
            rightClassName="is-flex-3"
            className="is-hidden-touch"
            leftSlot={
              <Select
                isSearchable={false}
                options={orderTypeOptions}
                defaultValue={orderTypeOptions[0]}
                onChange={this.handleOrderType}
              />
            }
            rightSlot={
              <Textfield
                isDisabled={this.state.orderType === 'market'}
                placeholder={
                  this.state.orderType === 'market'
                    ? 'Not applicable'
                    : 'Enter a value'
                }
              />
            }
          />
        )}

        {selectedOption.exchange === 'ASX' && (
          <RadioGroup
            name="orderType"
            label="Order type"
            buttons
            fullwidth
            selectedValue={this.state.orderType}
            onChange={this.handleOrderType}
            className="is-hidden-desktop">
            <Radio button value="limit" label="Limit" className="mb-0" />
            <Radio button value="market" label="Market" className="mb-0" />
          </RadioGroup>
        )}

        {(this.state.orderType === 'limit' ||
          selectedOption.exchange !== 'ASX') && (
          <React.Fragment>
            <Textfield
              name="limitPrice"
              label="Limit price in XXX"
              required
              placeholder="Enter a value"
              onChange={this.updateLimitPrice}
              className={cn(
                selectedOption.exchange !== 'ASX' ? '' : 'is-hidden-desktop'
              )}
            />
            <Textfield
              label="Estimated price in AUD"
              placeholder="Enter a value"
              isDisabled
              value={this.state.aud}
              className={cn(
                selectedOption.exchange !== 'ASX' ? '' : 'is-hidden-desktop'
              )}
            />
          </React.Fragment>
        )}

        <Alert
          inverted
          info
          isOpened={selectedOption.exchange !== 'ASX'}
          mb={4}>
          <strong>Only limit orders allowed.</strong>
        </Alert>

        <RadioGroup
          buttons
          fullwidth
          name="amountType"
          label="Amount type"
          selectedValue={this.state.amountType}
          onChange={this.handleAmountType}
          className="is-hidden-desktop">
          <Radio button value="qty" label="Quantity" />
          <Radio button value="value" label="Value" />
        </RadioGroup>

        {this.state.amountType === 'qty' ? (
          <Textfield
            label="Units"
            required
            placeholder="Enter number of units"
            className="is-hidden-desktop"
          />
        ) : (
          <Textfield
            label="Value"
            required
            placeholder="Enter value"
            className="is-hidden-desktop"
          />
        )}

        {selectedOption.type === 'eto' && (
          <Textfield
            label="Contracts"
            placeholder="Enter number of contracts"
          />
        )}

        <RadioGroup
          name="duration"
          label="Duration"
          buttons
          fullwidth
          selectedValue={this.state.duration}
          onChange={this.handleDuration}>
          <Radio
            spreadEvenly
            button
            value="until cancelled"
            label="Until cancelled"
            className="mb-0"
            disabled={selectedOption.exchange !== 'ASX'}
          />
          <Radio
            spreadEvenly
            button
            value="today"
            label="Today"
            className="mb-0"
          />
          <Radio
            spreadEvenly
            button
            value="set date"
            label="Set date"
            className="mb-0"
            disabled={selectedOption.exchange !== 'ASX'}
          />
        </RadioGroup>

        {this.state.duration === 'set date' && (
          // <Textfield label="Date" prependBoxedIcon="date" />
          <DatePicker
            required
            label="Date picker"
            onChange={date => function(date) {}}
            inputProps={{
              placeholder: 'Insert date',
              id: 'id-duration-date'
            }}
          />
        )}

        <Alert inverted info isOpened={selectedOption.exchange !== 'ASX'}>
          <strong>Day only orders permitted for this security.</strong>
        </Alert>
      </Section>
    );
  }
}
