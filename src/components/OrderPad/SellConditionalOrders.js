import React from 'react';
import cn from 'classnames';
import {
  Checkbox,
  CheckboxGroup,
  Collapse,
  Divider,
  Label,
  MultiField,
  Radio,
  RadioGroup,
  Section,
  Select,
  Textfield,
  Title
} from 'shaper-react';

class ConditionalOrders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      conditionalOrders: [],
      coExpand: false,
      condition: 'takeprofit',
      triggerType: 'fixed',
      trigger: { value: '$', label: '$' },
      ifLastPrice: 'smallerEqual',
      trailLastPrice: 'above'
    };

    this.conditionalOrdersChanged = this.conditionalOrdersChanged.bind(this);
    this.handleCondition = this.handleCondition.bind(this);
  }
  conditionalOrdersChanged = value => {
    let coExpand = false;
    if (!this.state.conditionalOrders.includes('co')) {
      coExpand = true;
    }
    this.setState({
      conditionalOrders: value,
      coExpand: coExpand
    });
  };
  handleCondition = value => {
    let newTriggerType = this.state.triggerType;
    if (value === 'stopentry') {
      newTriggerType = 'fixed';
    }
    this.handleTriggerType(newTriggerType);
    this.setState({ condition: value });
  };
  handleTriggerType = value => {
    this.setState({ triggerType: value });
  };
  handleTriggers = value => {
    this.setState({ trigger: value });
  };
  handleIfLastPrice = value => {
    this.setState({ ifLastPrice: value });
  };
  handleTrailLastPrice = value => {
    this.setState({ trailLastPrice: value });
  };
  render() {
    // const { ...props } = this.props;
    return (
      <Section className="pa-0">
        <Divider className="mb-2" />
        <CheckboxGroup
          right
          fullwidth
          isSwitch
          name="conditionalOrders"
          value={this.state.conditionalOrders}
          onChange={this.conditionalOrdersChanged}
          className="mb-0">
          <Checkbox
            value="co"
            label={<span className="nab-label">Conditional order</span>}
            className="mb-0"
          />
        </CheckboxGroup>
        <Collapse isOpened={this.state.coExpand}>
          <RadioGroup
            name="condition"
            label={
              <React.Fragment>
                Condition{' '}
                <a
                  href="/#"
                  className="ml-2 is-underlined is-capitalized has-text-primary">
                  What's this?
                </a>
              </React.Fragment>
            }
            buttons
            fullwidth
            required
            selectedValue={this.state.condition}
            onChange={this.handleCondition}
            className="mt-2">
            <Radio
              spreadEvenly
              button
              value="takeprofit"
              label="Take profit"
              className="mb-0"
            />
            <Radio
              spreadEvenly
              button
              value="stoploss"
              label="Stop loss"
              className="mb-0"
            />
            <Radio
              spreadEvenly
              button
              value="advanced"
              label="Advanced"
              className="mb-0"
            />
          </RadioGroup>

          <RadioGroup
            name="trigger"
            label={
              <React.Fragment>
                Trigger{' '}
                <a
                  href="/#"
                  className="ml-2 is-underlined is-capitalized has-text-primary">
                  What's this?
                </a>
              </React.Fragment>
            }
            required
            buttons
            fullwidth
            selectedValue={
              this.state.condition !== 'advanced'
                ? 'fixed'
                : this.state.triggerType
            }
            onChange={this.handleTriggerType}
            className="mt-2">
            <Radio button value="fixed" label="Fixed" className="mb-0" />
            <Radio
              button
              value="trailing"
              label="Trailing"
              className="mb-0"
              disabled={this.state.condition !== 'advanced' ? true : false}
            />
          </RadioGroup>

          {this.state.condition === 'advanced' &&
            this.state.triggerType === 'fixed' && (
              <RadioGroup
                name="lastprice"
                label="If last price"
                required
                buttons
                fullwidth
                selectedValue={this.state.ifLastPrice}
                onChange={this.handleIfLastPrice}
                className="mt-2">
                <Radio button value="smallerEqual" label="≤" className="mb-0" />
                <Radio button value="smaller" label="<" className="mb-0" />
                <Radio button value="larger" label=">" className="mb-0" />
                <Radio button value="largerEqual" label="≥" className="mb-0" />
              </RadioGroup>
            )}

          {this.state.triggerType === 'fixed' && (
            <MultiField
              required
              label={
                this.state.trigger.value === '$'
                  ? 'Trigger price'
                  : 'Trigger percentage'
              }
              leftClassName="is-flex-2"
              rightClassName="is-flex-3"
              leftSlot={
                <Select
                  isSearchable={false}
                  options={triggers}
                  defaultValue={triggers[0]}
                  onChange={this.handleTriggers}
                />
              }
              rightSlot={
                <React.Fragment>
                  {this.state.trigger === '$' ? (
                    <Textfield placeholder="Enter a trigger price" />
                  ) : (
                    <Textfield placeholder="Enter a trigger percentage" />
                  )}
                </React.Fragment>
              }
            />
          )}

          {this.state.condition === 'advanced' &&
            this.state.triggerType === 'trailing' && (
              <React.Fragment>
                <RadioGroup
                  name="traillastprice"
                  label="Trail last price"
                  required
                  buttons
                  fullwidth
                  selectedValue={this.state.trailLastPrice}
                  onChange={this.handleTrailLastPrice}
                  className="mt-2">
                  <Radio button value="above" label="Above" className="mb-0" />
                  <Radio button value="below" label="Below" className="mb-0" />
                </RadioGroup>

                <MultiField
                  leftClassName="is-flex-2"
                  rightClassName="is-flex-3"
                  leftSlot={
                    <Select
                      isSearchable={false}
                      options={triggers}
                      defaultValue={triggers[0]}
                      onChange={this.handleTriggers}
                    />
                  }
                  rightSlot={
                    <Textfield placeholder="Enter a trailing offset" />
                  }
                />
              </React.Fragment>
            )}

          {this.state.trigger.value === '%' &&
            this.state.triggerType === 'fixed' && (
              <div className="nab-box pa-4">
                <Label className="nab-label mb-0">
                  Converted trigger price
                </Label>
                <Title size="5" className="mb-0">
                  $0
                </Title>
              </div>
            )}
        </Collapse>
        <Divider
          className={cn('mb-2', this.state.coExpand ? 'mt-4' : 'mt-2')}
        />
      </Section>
    );
  }
}

export default ConditionalOrders;

const triggers = [
  {
    label: '$',
    value: '$'
  },
  {
    label: '%',
    value: '%'
  }
];
