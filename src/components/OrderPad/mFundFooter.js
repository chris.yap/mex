import React, { useState } from 'react';
import { getState } from '../../context/StateProvider';
import { Alert, Button, Div, Footer } from 'shaper-react';

const MFundFooter = ({ ...props }) => {
  const [{ orderPad }, dispatch] = getState();
  const [step, setStep] = useState(1);
  const [alert, setAlert] = useState(false);
  const updateStep = value => {
    setStep(value);
  };
  const updateAlert = value => {
    setAlert(value);
  };
  return (
    <Footer
      py={2}
      mt={0}
      fixed
      bg="white"
      borderTop="1px solid rgba(0,0,0,.15)"
      style={{
        zIndex: orderPad.coOverlay ? 201 : 10
      }}>
      <React.Fragment>
        <Alert info inverted isOpened={alert} my={2}>
          <p className="mb-0">
            <strong>Terms and conditions</strong>
          </p>
          <p className="mb-2">
            Please open and read the{' '}
            <span
              onClick={() => updateStep(3)}
              className="has-text-primary has-pointer has-text-weight-semibold">
              Product disclosure statement
            </span>
            , dated 18-12-17.
          </p>
          <p className="mb-0">
            I/We have read and understood the above document and the{' '}
            <span
              onClick={() => updateStep(3)}
              className="has-text-primary has-pointer has-text-weight-semibold">
              ASX mFund Investor Fact Sheet
            </span>
            . Your personal information will be sent to the fund manager to
            complete the order. nabtrade offers mFunds to Australian residents
            only.
          </p>
        </Alert>
        <Div display="flex">
          {step === 1 ? (
            <React.Fragment>
              <Button
                primary
                fullwidth
                onClick={() => {
                  updateStep(2);
                  updateAlert(true);
                }}>
                View PDS
              </Button>
              <Button
                flat
                onClick={() => dispatch({ type: 'orderPadSelectOption' })}>
                Reset
              </Button>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Button
                primary
                fullwidth
                onClick={() => {
                  updateStep(4);
                  updateAlert(false);
                  dispatch({ type: 'toggleOrderSummary' });
                }}
                disabled={step !== 2 ? false : true}>
                I agree
              </Button>
              <Button
                flat
                onClick={() => {
                  updateStep(1);
                  updateAlert(false);
                }}>
                Cancel
              </Button>
            </React.Fragment>
          )}
        </Div>
      </React.Fragment>
    </Footer>
  );
};

export default MFundFooter;
