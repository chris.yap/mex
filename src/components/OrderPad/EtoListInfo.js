import React from 'react';
import {
  Button,
  Card,
  Collapse,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem
} from 'shaper-react';

const EtoListInfo = () => {
  const [expand, setExpand] = React.useState(false);
  return (
    <div className="has-text-centered mb-2">
      <Collapse isOpened={expand}>
        <Card className="has-elevation-0 mt-2">
          <List>
            {Data.map((item, i) => (
              <ListItem className="py-1">
                <Level className="is-flex-1 has-text-size-1">
                  <LevelItem left>{item.name}</LevelItem>
                  <LevelItem right>
                    <strong>{item.value}</strong>
                  </LevelItem>
                </Level>
              </ListItem>
            ))}
          </List>
        </Card>
      </Collapse>
      <Button flat small onClick={() => setExpand(expand ? false : true)}>
        <Icon>{!expand ? 'arrow-down' : 'arrow-up'}</Icon>
        {!expand && <span>Show more info</span>}
      </Button>
    </div>
  );
};

export default EtoListInfo;

const Data = [
  { name: 'Volume', value: '0' },
  { name: 'Delta', value: '1.0000' },
  { name: 'Vega', value: '0' },
  { name: 'Gamma', value: '0' },
  { name: 'Rho', value: '0.0003' },
  { name: 'Theta', value: '0' },
  { name: 'Contract size', value: '104' },
  { name: 'Open interest', value: '250' },
  { name: 'Intrinsic value', value: '19.49%' },
  { name: 'Implied volatility', value: '0.2211' }
];
