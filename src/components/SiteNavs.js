import React from 'react';
import PropTypes from 'prop-types';
import Headroom from 'react-headroom';
import { getState } from '../context/StateProvider';
import { MainNav } from '../constants';
import {
  Breakpoints,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Container,
  Icon,
  Level,
  LevelItem,
  Section,
  Toolbar,
  ToolbarNav,
  ToolbarNavItem
} from 'shaper-react';
import NabtradeLogo from '../components/NabtradeLogo';
import QuickNavs from '../components/QuickNavs';
import '../style/headroom.css';

const SiteNavs = ({ activeBreakpoints, breadcrumb, subnav, ...props }) => {
  const [, dispatch] = getState();
  const toolbarStyle = {
    top: 0,
    left: 0,
    right: 0,
    zIndex: 31
  };
  return (
    <>
      <Headroom
        disableInlineStyles
        className="pos-f"
        wrapperStyle={toolbarStyle}>
        <Toolbar
          className="has-bg-white has-elevation-1 is-hidden-touch"
          href="/"
          image={<NabtradeLogo className="mr-4" width="122" />}
          style={{ zIndex: 31 }}>
          <ToolbarNav left>
            {MainNav.map((item, i) => {
              return (
                <ToolbarNavItem
                  key={i}
                  to={item.to}
                  className="has-text-weight-bold has-text-size1">
                  {item.label}
                </ToolbarNavItem>
              );
            })}
          </ToolbarNav>
          <ToolbarNav right className="">
            <Button
              secondary
              borderRadius={0}
              fontSize="18px"
              mx={0}
              px={activeBreakpoints.when.md ? 4 : 5}
              height="68px"
              onClick={() => {
                dispatch({ type: 'toggleSearchBar' });
              }}>
              <Icon>search</Icon>
              <span>
                {activeBreakpoints.when.md ? 'Quotes' : 'Search quotes'}
              </span>
            </Button>
            <Button
              primary
              borderRadius={0}
              fontSize="18px"
              mx={0}
              px={activeBreakpoints.when.md ? 4 : 5}
              height="68px"
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              <Icon>order</Icon>
              <span>Trade</span>
            </Button>
          </ToolbarNav>
        </Toolbar>

        {subnav ? (
          <Toolbar
            level={2}
            className="is-hidden-touch has-bg-blue-grey lighten-5 has-elevation-1">
            <ToolbarNav left>
              {subnav.map((item, i) => {
                return (
                  <React.Fragment key={i}>
                    {!item.onClick ? (
                      <ToolbarNavItem to={item.to} className="has-text-size0">
                        {item.label}
                      </ToolbarNavItem>
                    ) : (
                      <Button
                        flat
                        secondary
                        fontWeight="400"
                        borderRadius="0"
                        mx={0}
                        height="60px"
                        onClick={() => dispatch({ type: item.onClick })}>
                        {item.label}
                      </Button>
                    )}
                  </React.Fragment>
                );
              })}
            </ToolbarNav>
            <ToolbarNav right>
              <QuickNavs />
            </ToolbarNav>
          </Toolbar>
        ) : (
          <Toolbar
            className="is-hidden-touch has-bg-blue-grey lighten-5 has-elevation-1"
            style={{ minHeight: '60px', height: '60px' }}>
            <ToolbarNav left>
              {breadcrumb.length > 0 && (
                <Breadcrumb className="ml-4 is-align-self-center">
                  {breadcrumb.map((bread, b) => (
                    <BreadcrumbItem
                      key={b}
                      href={bread.link}
                      active={bread.active}>
                      {bread.name}
                    </BreadcrumbItem>
                  ))}
                </Breadcrumb>
              )}
            </ToolbarNav>
            <ToolbarNav right>
              <QuickNavs />
            </ToolbarNav>
          </Toolbar>
        )}
      </Headroom>

      {breadcrumb && subnav && (
        <Section className="py-4 is-hidden-touch">
          <Container>
            <Level>
              <LevelItem left>
                {breadcrumb.length > 0 && (
                  <Breadcrumb className="is-align-self-center">
                    {breadcrumb.map((bread, b) => (
                      <BreadcrumbItem
                        key={b}
                        href={bread.link}
                        active={bread.active}>
                        {bread.name}
                      </BreadcrumbItem>
                    ))}
                  </Breadcrumb>
                )}
              </LevelItem>
              <LevelItem right>
                <Button small flat className="mb-0">
                  <Icon>quicklinks</Icon>
                  <span>Add to shortcuts</span>
                </Button>
              </LevelItem>
            </Level>
          </Container>
        </Section>
      )}
    </>
  );
};

export default Breakpoints(SiteNavs);

SiteNavs.propTypes = {
  breadcrumb: PropTypes.array
};

SiteNavs.defaultProps = {
  breadcrumb: []
};
