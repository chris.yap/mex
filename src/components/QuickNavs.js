import React from 'react';
import cn from 'classnames';
import { getState } from '../context/StateProvider';
import { Link, withRouter } from 'react-router-dom';
import { Dropdown, Icon, List, ListItem, Tag } from 'shaper-react';

const FloatingNav = ({ ...props }) => {
  const [{ floatingNav }, dispatch] = getState();
  return (
    <>
      {TabsItems.map((tab, t) => (
        <Dropdown
          key={t}
          right={t === TabsItems.length - 1}
          activator={
            tab.to ? (
              <Link
                to={tab.to}
                key={t}
                className="nab-toolbar-item has-text-size-2 has-text-weight-normal is-align-content-center"
                style={{
                  flexDirection: 'column',
                  minWidth: '4em',
                  height: '60px'
                }}>
                <Icon>{tab.icon}</Icon>
                <span>
                  {tab.label}{' '}
                  {tab.alert && (
                    <Tag small primary height="18px" py={1} px={2}>
                      {tab.alert}
                    </Tag>
                  )}
                </span>
              </Link>
            ) : (
              <div
                key={t}
                className="nab-toolbar-item has-text-size-2 has-text-weight-normal is-align-content-center has-pointer"
                style={{
                  flexDirection: 'column',
                  minWidth: '4em',
                  height: '60px'
                }}>
                <Icon>{tab.icon}</Icon>
                <span>{tab.label}</span>
              </div>
            )
          }>
          {tab.items && (
            <List className="has-elevation-2">
              {tab.items.map((ttab, tt) => (
                <ListItem
                  key={tt}
                  href={ttab.to}
                  icon={ttab.icon}
                  className={cn('has-text-size-1', ttab.className)}
                  onClick={
                    ttab.label === 'Cash transfer'
                      ? () => {
                          dispatch({
                            type: 'toggleCashTransferModel',
                            payload: true
                          });
                          dispatch({
                            type: 'handleStep',
                            payload: 0
                          });
                        }
                      : null
                  }>
                  {ttab.label}
                </ListItem>
              ))}
              {t === 3 && (
                <ListItem
                  className={cn(
                    'has-bg-blue-grey has-text-size-1',
                    floatingNav.hover ? 'darken-2' : ''
                  )}
                  onMouseOver={() =>
                    dispatch({ type: 'toggleFloatingNavHover' })
                  }
                  onMouseOut={() =>
                    dispatch({ type: 'toggleFloatingNavHover' })
                  }
                  onClick={() => dispatch({ type: 'toggleLogin' })}>
                  <span className="has-text-white is-flex">
                    <Icon small className="mr-2">
                      logout
                    </Icon>
                    <span>Log out</span>
                  </span>
                </ListItem>
              )}
            </List>
          )}
        </Dropdown>
      ))}
    </>
  );
};

export default withRouter(FloatingNav);

const TabsItems = [
  // {
  //   icon: 'nabtrade',
  //   label: 'NAB accounts',
  //   to: '/nab-accounts'
  // },
  {
    icon: 'cash',
    label: 'Cash transfer',
    items: [
      { label: 'Cash transfer', to: '', className: 'has-pointer' },
      { label: 'Payee list', to: '/payee-list' }
      // { label: 'Andre\'s portfolio', to: '' },
      // { label: 'My rainy day play watchlist', to: '' },
      // { label: 'BHP company page', to: '' },
      // { label: 'MY historical transactions', to: '' }
    ]
  },
  {
    icon: 'watchlist',
    label: 'Watchlists',
    to: '/watchlists'
  },
  {
    icon: 'bell-outline',
    label: 'Alerts',
    to: '/alerts',
    alert: 2
  },
  {
    icon: 'user-outline',
    label: 'Account',
    items: [
      { label: 'Change password', to: '/account' },
      { label: 'Communication preferences', to: '' },
      { label: 'Alerts & notifications', to: '' }
    ]
  }
];
