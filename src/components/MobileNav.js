import React from 'react';
import cn from 'classnames';
import { getState } from '../context/StateProvider';
import { NavLink } from 'react-router-dom';
import {
  //ShortcutItems,
  PortfolioItems,
  MarketsInsightsItems,
  InvestmentOptionsItems,
  SupportItems,
  SettingsItems,
  CashItems
} from '../constants';
import {
  Box,
  Button,
  Collapse,
  Container,
  Div,
  Divider,
  Icon,
  List,
  ListItem
} from 'shaper-react';

const MobileNav = ({ className, ...props }) => {
  const [{ mobileNav }, dispatch] = getState();
  let { isOpened, openSub } = mobileNav;
  const onClickEvent = label => {
    dispatch({ type: 'toggleMobileNav' });
    if (label === 'Cash transfer') {
      dispatch({ type: 'toggleCashTransferModel', payload: true });
      dispatch({ type: 'handleStep', payload: 0 });
    }
  };
  return (
    <React.Fragment>
      <Box
        p={0}
        boxShadow="none"
        className={cn('nab-mobile-nav', 'is-hidden-desktop', className)}>
        <Container fluid p={0} className="nab-container">
          <Button
            height="50px"
            m={0}
            borderRadius="0"
            border="none"
            boxShadow="none !important"
            className="nab-button"
            onClick={() => dispatch({ type: 'toggleMobileNav' })}>
            <span
              className={`nab-hamburger-cross ${isOpened ? 'is-active' : ''}`}>
              <span />
              <span />
              <span />
              <span />
            </span>
            {isOpened ? <span>Close</span> : <span>Menu</span>}
          </Button>
          {NavItems.map((item, i) => {
            return (
              <NavLink
                key={i}
                to={item.to}
                className="nab-button"
                activeClassName="is-active">
                <Icon>{item.icon}</Icon>
                <span>{item.label}</span>
              </NavLink>
            );
          })}
          <Button
            height="50px"
            m={0}
            borderRadius="0"
            onClick={() => dispatch({ type: 'toggleSearchBar' })}>
            <Icon>search</Icon>
            <span>Search</span>
          </Button>
        </Container>

        <Collapse isOpened={isOpened}>
          <div className="nab-mobile-more-menu">
            <Divider />
            <List className="mb-0">
              {MoreItems.map((mm, m) => {
                if (mm.items) {
                  return (
                    <React.Fragment key={m}>
                      <ListItem
                        href={mm.to}
                        onClick={() => onClickEvent(mm.label)}
                        className={mm.class ? mm.class : ''}>
                        <span>{mm.label}</span>
                        {mm.icon && (
                          <Icon primary className="ml-1">
                            {mm.icon}
                          </Icon>
                        )}
                        <Div
                          onClick={event => {
                            event.stopPropagation();
                            event.preventDefault();
                            dispatch({ type: 'toggleSubNav', payload: m });
                          }}>
                          {openSub === m && <Icon small>arrow-up</Icon>}
                          {openSub !== m && <Icon small>arrow-down</Icon>}
                        </Div>
                      </ListItem>
                      {mm.items && (
                        <Collapse isOpened={openSub === m}>
                          <List>
                            {mm.items.map((mmm, mi) => {
                              return (
                                <ListItem
                                  key={mi}
                                  className="has-text-size-1"
                                  href={mmm.to}
                                  onClick={() =>
                                    dispatch({ type: 'toggleMobileNav' })
                                  }>
                                  {mmm.label}
                                </ListItem>
                              );
                            })}
                          </List>
                        </Collapse>
                      )}
                    </React.Fragment>
                  );
                } else {
                  return (
                    <ListItem
                      key={m}
                      className={mm.class ? mm.class : ''}
                      onClick={() => onClickEvent(mm.label)}>
                      <span>{mm.label}</span>
                      {mm.icon && (
                        <Icon primary className="ml-1">
                          {mm.icon}
                        </Icon>
                      )}
                    </ListItem>
                  );
                }
              })}
            </List>

            <Button
              primary
              block
              height="3em"
              borderRadius={0}
              onClick={() => {
                dispatch({ type: 'toggleOrderPad' });
                dispatch({ type: 'toggleMobileNav' });
              }}>
              <Icon>order</Icon>
              <span>Trade</span>
            </Button>

            <Button
              flat
              secondary
              block
              borderRadius={0}
              onClick={() => {
                dispatch({ type: 'toggleLogin' });
                dispatch({ type: 'toggleMobileNav' });
              }}>
              <Icon>logout</Icon>
              <span>Logout</span>
            </Button>
          </div>
        </Collapse>
      </Box>
    </React.Fragment>
  );
};

export default MobileNav;

const NavItems = [
  { label: 'Portfolio', icon: 'briefcase', to: '/portfolio' },
  { label: 'Watchlists', icon: 'watchlist', to: '/watchlists' },
  { label: 'Alerts', icon: 'bell-outline', to: '/alerts' }
];

const MoreItems = [
  // {
  //   label: 'Shortcuts',
  //   to: '',
  //   items: ShortcutItems
  // },
  {
    label: 'Portfolio',
    to: '/portfolio',
    items: PortfolioItems
  },
  {
    label: 'Markets & insights',
    to: '/markets-insights',
    items: MarketsInsightsItems
  },
  {
    label: 'Investment options',
    to: '',
    items: InvestmentOptionsItems
  },
  {
    label: 'Support',
    to: '/support',
    items: SupportItems
  },
  {
    label: 'Cash transfer',
    items: CashItems
    //class: 'has-bg-black lighten-5'
    //icon: 'cash'
  },
  {
    label: 'Account',
    to: '/account',
    items: SettingsItems
  }
];
