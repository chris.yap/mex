import React from 'react';
import PropTypes from 'prop-types';

const LoaderAnimation = ({ color, stroke, size, className, ...props }) => (
	<svg
		viewBox="0 0 256 256"
		xmlns="http://www.w3.org/2000/svg"
		className={`star ${className}`}
		style={{ width: size }}
		{...props}
	>
		<path
			clipRule="evenodd"
			d="m118.7 58.9 40.1-49.2c1-1.2 2-1.9 3.6-1.7 1.1.3 1.9 1.3 2.3 3.2.1.5 13.6 60.9 13.5 61.4.3 1.3-.4 2.3-1.6 3.7l-50.4 60.7c-.5.7-.5 1.2-.3 1.3.3.3.8.1 1.3-.5.9-1.2 49.5-52.4 59.2-62.7.7-.7 2.3-2.1 4.4-2.1l21 .1c.7 0 1.2.3 1.5.8.1.5.1.9-.3 1.5-16.8 20-49.9 60.1-51.1 61.6-.5.7-.5 1.2-.3 1.3.3.3.8.1 1.3-.5l59.9-63.4c.8-.8 1.9-1.3 3-1.3l16.2.1c1.9 0 3.1.5 3.6 1.6s.1 2.4-1.1 3.9l-40 49.6 39.4 49.6c1.2 1.5 1.6 2.9 1.1 3.9-.4 1.1-1.7 1.6-3.6 1.6h-63.9l-14.4 61.4c-.5 1.9-1.3 2.9-2.4 3.2s-2.3-.4-3.5-1.9l-39.8-49.6-57.3 27c-1.7.8-3.1.8-4 .1-.8-.7-1.1-2-.7-3.9l14.1-61.7-56.9-27.8c-1.6-.9-2.6-2-2.6-3.1s.9-2.1 2.7-2.9l57.3-27.4-13.4-61.8c-.5-1.7-.3-3.2.7-3.9.9-.8 2.3-.7 4 .1z"
			strokeLinecap="round"
			stroke={color}
			strokeWidth={stroke}
			fill="transparent"
			fillRule="evenodd"
			className="path"
		/>
	</svg>
);

export default LoaderAnimation;

LoaderAnimation.propTypes = {
	color: PropTypes.string,
	stroke: PropTypes.number,
	size: PropTypes.string,
};

LoaderAnimation.defaultProps = {
	color: '#4c626c',
	stroke: 5,
	size: '100px',
};
