import React from 'react';
import cn from 'classnames';
import NabtradeLogo from '../components/NabtradeLogo';
import { Breakpoints, Container, Footer } from 'shaper-react';
import { FooterItems } from '../constants/index';

const SiteFooter = ({ activeBreakpoints, ...props }) => {
  return (
    <Footer bg="bluegreys.1">
      <Container>
        <p className="mb-2">
          {/* <img src="/images/nabtrade-logo.png" alt="nabtrade" width="150" /> */}
          <NabtradeLogo />
        </p>
        {/* <p
          className={cn(
            'mb-0',
            activeBreakpoints.below.sm ? 'is-flex is-flex-wrap' : ''
          )}
          style={{ flexWrap: 'wrap', margin: '0 -8px' }}
        > */}
        <div>
          {FooterItems.map((item, index) => (
            <a href={item.to} key={index} className={cn('mx-2')}>
              <small>{item.label}</small>
            </a>
          ))}
        </div>
        {/* </p> */}
      </Container>
    </Footer>
  );
};

export default Breakpoints(SiteFooter);
