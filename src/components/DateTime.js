import React from "react";
import faker from "faker";
import Moment from "react-moment";

class DateTime extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount = () => {
    let newDate = faker.date.recent();
    this.setState({
      dateTime: newDate
    });
    setTimeout(() => {}, 100);
  };
  render() {
    const { dateTime } = this.state;
    const { time, className } = this.props;
    return (
      <React.Fragment>
        <Moment format="DD-MM-YYYY" className={className}>
          {dateTime}
        </Moment>
        {time && (
          <React.Fragment>
            {" "}
            <Moment format="h:mma">{dateTime}</Moment> AEDT
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default DateTime;
