import React from 'react';
import { getState } from '../../context/StateProvider';
import { Div, Toolbar, ToolbarNav, ToolbarNavItem } from 'shaper-react';

const SubNav = ({ items, ...props }) => {
  const [{}, dispatch] = getState();
  return (
    <Toolbar
      level="2"
      fixed
      className="is-hidden-touch has-bg-blue-grey lighten-5 has-elevation-1">
      {items && (
        <ToolbarNav left>
          {items.map((item, i) => {
            return (
              <React.Fragment>
                {!item.onClick ? (
                  <ToolbarNavItem
                    key={i}
                    to={item.to}
                    className="has-text-size-1">
                    {item.label}
                  </ToolbarNavItem>
                ) : (
                  <Div>{item.label}</Div>
                )}
              </React.Fragment>
            );
          })}
        </ToolbarNav>
      )}
    </Toolbar>
  );
};

export default SubNav;
