import React from 'react';
import { PortfolioItems } from '../../constants';
import { Toolbar, ToolbarNav, ToolbarNavItem } from 'shaper-react';

const PortfolioNav = ({ ...props }) => {
	return (
		<Toolbar level="2" fixed className="is-hidden-touch has-bg-blue-grey lighten-5 has-elevation-1">
			<ToolbarNav left>
				{PortfolioItems.map((item, i) => {
					return (
						<ToolbarNavItem key={i} to={item.to} className="has-text-size-1">
							{item.label}
						</ToolbarNavItem>
					);
				})}
			</ToolbarNav>
		</Toolbar>
	);
};

export default PortfolioNav;
