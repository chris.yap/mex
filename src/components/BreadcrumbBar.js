import React from 'react';
import PropTypes from 'prop-types';
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Container,
  Icon,
  Level,
  LevelItem,
  Section
} from 'shaper-react';

const BreadcrumbBar = ({ breadcrumb, shortcut, ...props }) => (
  <Section
    flat
    className={`${shortcut && 'py-4'} is-hidden-mobile`}
    style={{ padding: !shortcut ? '19px 5rem' : null }}
  >
    <Container>
      <Level>
        <LevelItem left>
          <Breadcrumb>
            {breadcrumb &&
              breadcrumb.map((item, i) => (
                <BreadcrumbItem key={i} href={item.to} active={item.active}>
                  {item.label}
                </BreadcrumbItem>
              ))}
          </Breadcrumb>
        </LevelItem>
        <LevelItem right>
          {shortcut && (
            <Button small flat>
              <Icon>quicklinks</Icon>
              <span>Add to shortcuts</span>
            </Button>
          )}
        </LevelItem>
      </Level>
    </Container>
  </Section>
);

export default BreadcrumbBar;

BreadcrumbBar.propTypes = {
  breadcrumb: PropTypes.array,
  shortcut: PropTypes.bool
};

BreadcrumbBar.defaultProps = {
  shortcut: true
};
