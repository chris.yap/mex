import React from "react";
import DateTime from "../components/DateTime";

const RealtimeQuoteMessage = ({
  dark,
  local,
  international,
  currency,
  className,
  ...props
}) => (
  <p
    className={`has-text-size-2 ${
      dark ? "has-text-white" : "has-text-blue-grey"
    } ${className}`}
    {...props}
  >
    {local && (
      <React.Fragment>
        ASX real-time quotes as of <DateTime time />.
      </React.Fragment>
    )}
    {local && international && <br />}
    {international && (
      <React.Fragment>
        International quotes delayed at least 15 minutes as of <DateTime time />
        .
      </React.Fragment>
    )}
    {currency && (
      <React.Fragment>
        <br />
        All quotes in local currency.
      </React.Fragment>
    )}
  </p>
);

export default RealtimeQuoteMessage;
