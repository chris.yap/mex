import React from 'react';
import PropTypes from 'prop-types';
import { getState } from '../context/StateProvider';
import { Button, Modal, Textfield } from 'shaper-react';

class OTPFields extends React.Component {
  constructor() {
    super();
    this.state = {
      step: 0
    };
    this.inputRef = [];
  }
  componentDidUpdate = prevProps => {
    if (this.props.isOpened && this.props.isOpened !== prevProps.isOpened) {
      this.inputRef[0].focus();
    }
  };
  onKeyDown = (value, index) => {
    if (value.keyCode === 8 && value.target.value.length < 1 && index !== 0) {
      this.inputRef[index - 1].focus();
    }
  };

  handleTextChange = (value, index) => {
    if (value.target.value.length > 0 && index < 5) {
      this.inputRef[index + 1].focus();
    }
  };
  render = () => {
    const inputs = Array(6).fill(0);
    return (
      <>
        {inputs.map((input, index) => (
          <Textfield
            maxLength="1"
            autoFocus
            inputRef={el => (this.inputRef[index] = el)}
            validationIcon={false}
            onChange={e => this.handleTextChange(e, index)}
            onKeyDown={e => this.onKeyDown(e, index)}
          />
        ))}
      </>
    );
  };
}

const OneTimePin = ({ header, footer, isOpened, ...props }) => {
  const [, dispatch] = getState();

  return (
    <Modal
      isOpened={isOpened}
      header={header}
      footer={
        footer || (
          <>
            <Button primary onClick={() => dispatch({ type: 'toggleOTP' })}>
              Submit
            </Button>
            <Button flat onClick={() => dispatch({ type: 'toggleOTP' })}>
              Cancel
            </Button>
          </>
        )
      }
      className="has-bg-secondary lighten-5">
      <p>
        To complete the transfer, please enter the 6-digit code send to XXXX XXX
        XXX
      </p>

      <div className="nab-multifield-input">
        <OTPFields isOpened={isOpened} />
      </div>

      <p className="has-text-centered">
        Didn't receive a code? <br /> <a href="/#">Click here to resend</a> or
        call us on 1300 123 456
      </p>
    </Modal>
  );
};

export default OneTimePin;

OneTimePin.propTypes = {
  header: PropTypes.string,
  footer: PropTypes.node
};

OneTimePin.defaultProps = {
  header: 'One-time PIN'
};
