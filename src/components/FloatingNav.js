import React from 'react';
import cn from 'classnames';
import { getState } from '../context/StateProvider';
import Headroom from 'react-headroom';
import { withRouter } from 'react-router-dom';
import { Icon, List, ListItem, Tabs, Tab } from 'shaper-react';

const FloatingNav = ({ ...props }) => {
  const [{ floatingNav }, dispatch] = getState();
  return (
    <Headroom disableInlineStyles className="nab-floating-widget">
      <Tabs justify stacked closable mandatory={false} className="mb-0">
        {TabsItems.map((tab, t) => (
          <Tab
            key={t}
            label={tab.label}
            icon={tab.icon}
            tabClick={() => props.history.push(tab.to)}
          >
            {tab.items && (
              <List className="has-elevation-2">
                {tab.items.map((ttab, tt) => (
                  <ListItem
                    key={tt}
                    href={ttab.to}
                    icon={ttab.icon}
                    className={cn('has-text-size-1', ttab.className)}
                    onClick={
                      ttab.label === 'Cash transfer'
                        ? () => {
                            dispatch({
                              type: 'toggleCashTransferModel',
                              payload: true
                            });
                            dispatch({
                              type: 'handleStep',
                              payload: 0
                            });
                          }
                        : ''
                    }
                  >
                    {ttab.label}
                  </ListItem>
                ))}
                {t === 3 && (
                  <ListItem
                    className={cn(
                      'has-bg-blue-grey has-text-size-1',
                      floatingNav.hover ? 'darken-2' : ''
                    )}
                    onMouseOver={() =>
                      dispatch({ type: 'toggleFloatingNavHover' })
                    }
                    onMouseOut={() =>
                      dispatch({ type: 'toggleFloatingNavHover' })
                    }
                    onClick={() => dispatch({ type: 'toggleLogin' })}
                  >
                    <span className="has-text-white is-flex">
                      <Icon small className="mr-2">
                        logout
                      </Icon>
                      <span>Log out</span>
                    </span>
                  </ListItem>
                )}
              </List>
            )}
          </Tab>
        ))}
      </Tabs>
    </Headroom>
  );
};

export default withRouter(FloatingNav);

const TabsItems = [
  // {
  //   icon: 'nabtrade',
  //   label: 'NAB accounts',
  //   to: '/nab-accounts'
  // },
  {
    icon: 'cash',
    label: 'Cash',
    items: [
      { label: 'Cash transfer', to: '', className: 'has-pointer' },
      { label: 'Payee list', to: '/payee-list' }
      // { label: 'Andre\'s portfolio', to: '' },
      // { label: 'My rainy day play watchlist', to: '' },
      // { label: 'BHP company page', to: '' },
      // { label: 'MY historical transactions', to: '' }
    ]
  },
  {
    icon: 'watchlist',
    label: 'Watchlists',
    to: '/watchlists'
  },
  {
    icon: 'bell-outline',
    label: 'Alerts',
    to: '/alerts'
  },
  {
    icon: 'user-outline',
    label: 'Account',
    items: [
      { label: 'Account', to: '/account' },
      { label: 'Communication preferences', to: '' },
      { label: 'Alerts & notifications', to: '' }
    ]
  }
];
