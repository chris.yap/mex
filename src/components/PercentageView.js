import React from 'react';
import cn from 'classnames';
import { Icon } from 'shaper-react';

const PercentageView = ({ noArrows, dark, value }) => {
	let cleanValue = ('' + value).replace('%', '');
	return (
		<React.Fragment>
			{cleanValue !== 0 && !noArrows && (
				<Icon small white={dark} danger={cleanValue < 0 && !dark} success={cleanValue > 0 && !dark}>
					{cleanValue > 0 ? 'caret-up' : 'caret-down'}
				</Icon>
			)}
			<span
				className={cn(
					'has-text-weight-semibold',
					noArrows ? '' : cleanValue < 0 && !dark ? 'has-text-danger' : 'has-text-success',
					dark ? 'has-text-white' : ''
				)}
			>
				{cleanValue}
				<sup className="pos-r has-top4">%</sup>
			</span>
		</React.Fragment>
	);
};

export default PercentageView;
