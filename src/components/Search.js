import React from 'react';
import { getState } from '../context/StateProvider';
import { withRouter } from 'react-router';
import {
  Breakpoints,
  Button,
  Column,
  Columns,
  Container,
  Icon,
  Section
} from 'shaper-react';
import GlobalSearchField from './GlobalSearchField';

const SiteSearch = ({ activeBreakpoints, ...props }) => {
  const [{ searchBar }, dispatch] = getState();

  const barStyle = {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '68px',
    zIndex: 1000,
    transition: 'ease-in-out .3s',
    transform: searchBar.isOpened
      ? 'translateY(0)'
      : 'translateY(calc(-100% - 3px))'
  };

  return (
    <Section
      style={barStyle}
      className={`has-bg-white has-elevation-2 ${
        activeBreakpoints.below.sm ? 'py-0 px-2' : 'pa-0'
      }`}
    >
      <Container>
        <Columns mobile gapless={activeBreakpoints.below.sm}>
          <Column narrow className="is-align-self-center pr-0">
            <Icon medium className="has-text-blue-grey">
              search
            </Icon>
          </Column>
          <Column className="px-4">
            <GlobalSearchField {...props} />
            {/* <Select
              placeholder="Search for a security … "
              isClearable
              autoFocus={searchBar.isOpened}
              backspaceRemovesValue={true}
              components={{ Option: SecurityOptionTrade }}
              options={Securities}
              styles={colourStyles}
              value={searchBar.searchSelection}
              onChange={gotoSecurity}
              inputValue={searchBar.searchValue}
              onInputChange={(e, action) =>
                dispatch({
                  type: "handleSearchInput",
                  payload: { action, value: e }
                })
              }
              {...props}
            /> */}
          </Column>
          <Column
            narrow
            className={`is-align-self-center ${
              activeBreakpoints.below.sm ? 'pl-4' : 'pl-0'
            }`}
          >
            <Button
              round
              small
              secondary
              flat
              onClick={() => dispatch({ type: 'toggleSearchBar' })}
            >
              <Icon>close</Icon>
            </Button>
          </Column>
        </Columns>
      </Container>
    </Section>
  );
};

export default withRouter(Breakpoints(SiteSearch));
