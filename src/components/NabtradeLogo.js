import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import './NabtradeLogo.css';

const NabtradeLogo = ({ dark, width, shadow, className, ...props }) => {
	let style = {
		width: width + 'px',
	};
	return (
		<svg
			className={cn('nabtrade-logo', dark ? 'is-dark' : '', shadow ? 'has-shadow' : '', className)}
			viewBox="0 0 800 179.63"
			style={style}
			{...props}
		>
			<defs>
				<linearGradient id="star-gradient" x1="0" y1="0" x2="0" y2="1">
					<stop className="top" offset="0" />
					<stop className="middle" offset="0.5" />
				</linearGradient>
			</defs>
			<path
				id="trade"
				className="trade"
				d="M486,132.6c-13.2,0-22.2-5.7-22.2-22.8V65.3H452.2V50h11.6l4.7-23.4H484V50h17.8V65.3H484v38.4c0,10.7,3.1,12.9,11.3,12.9h6.4v14.1c-3.6,1.2-9.8,1.9-15.7,1.9m71.8-64.7a40.69,40.69,0,0,0-8.3-1.1c-5.2,0-9.9,4.4-17.5,14v50H511.9V50h15.7l1.9,13.5c7.9-10.9,12.7-15.7,21.1-15.7a33.8,33.8,0,0,1,9.6,1.1l-2.4,19M598,47.8A92.87,92.87,0,0,0,567.9,53l3.4,14.6a68.6,68.6,0,0,1,23.8-4.7c13,0,16.9,4.9,16.9,14.9v4.6H602c-25.5,0-40.4,7.7-40.4,26.1,0,16.4,11.2,24.6,27.7,24.6,10.4,0,18.2-3.5,25.5-10.1l1.6,7.5H632V76C632,58.2,622.2,47.8,598,47.8Zm14,62.6c-5,4.7-10.8,8-17.9,8-8.2,0-12.3-4.2-12.3-11,0-8,6.1-11.9,19.9-11.9H612v14.9Zm85-97.9V50.2a73.08,73.08,0,0,0-15.6-1.6c-28.6,0-41.7,20-41.7,45.1,0,21.9,10.2,39.3,33,39.3,11.8,0,20-4.9,27.5-12.9l1.6,10.4H717V12.5H697Zm0,92.2c-7,8.2-12.9,11.9-20.3,11.9-11.8,0-15.7-11-15.7-25.3,0-17.3,7.1-26.9,20.8-26.9,6.1,0,12.3,1.4,15.3,3.3v37H697ZM800,85.2c0-23.1-10.7-37.5-34.7-37.5-25.6,0-39.8,16.7-39.8,43.4,0,28.3,12.9,41.8,40.1,41.8a111.47,111.47,0,0,0,31.8-4.5l-2-14.6a103.11,103.11,0,0,1-26.6,3.3c-16.7,0-22.5-7.7-22.5-22.7h53.5C799.8,91.5,800,87.3,800,85.2Zm-20-3.7H746.2c0.3-9,5.8-19.7,17.9-19.7C775.3,61.8,780.5,69.5,780,81.5Z"
				transform="translate(0 -0.01)"
			/>
			<g id="star" className="star">
				<path
					className="fill"
					d="M110.5,1.4L80.7,38.2,38.1,17.5c-1.3-.6-2.3-0.7-3-0.1s-0.9,1.6-.5,2.9l10,46.2L2,87c-1.3.6-2,1.4-2,2.2s0.7,1.6,1.9,2.3l42.3,20.8L33.7,158.5c-0.3,1.4-.1,2.4.5,2.9s1.7,0.5,3-.1l42.6-20.2,29.6,37.1c0.9,1.1,1.8,1.6,2.6,1.4s1.4-1,1.8-2.4l10.7-45.9H172c1.4,0,2.4-.4,2.7-1.2s0.1-1.8-.8-2.9L144.6,90.1,174.3,53c0.9-1.1,1.2-2.1.8-2.9s-1.3-1.2-2.7-1.2l-12-.1a3.21,3.21,0,0,0-2.2,1L113.7,97.2c-0.4.5-.8,0.6-1,0.4s-0.2-.5.2-1c0.9-1.1,25.5-31.1,38-46.1a1.13,1.13,0,0,0,.2-1.1,1.15,1.15,0,0,0-1.1-.6l-15.6-.1a4.88,4.88,0,0,0-3.3,1.6c-7.2,7.7-43.3,46-44,46.9-0.4.5-.8,0.6-1,0.4s-0.2-.5.2-1l37.4-45.4c0.9-1.1,1.4-1.8,1.2-2.8,0.1-.3-9.9-45.5-10-45.9-0.3-1.4-.9-2.2-1.7-2.4S111.4,0.3,110.5,1.4Z"
					transform="translate(0 -0.01)"
				/>
				<path
					className="stroke"
					d="M112.65,2.07A3.09,3.09,0,0,1,113,3c0.08,0.36,9,40.71,9.9,45.29v0l0.1,0.52a5.91,5.91,0,0,1-.79,1.12L84.74,95.35A3,3,0,0,0,84,97.86a2.25,2.25,0,0,0,.87,1.35,2.33,2.33,0,0,0,1.44.49,3,3,0,0,0,2.31-1.23c0.78-.92,16.83-18,28.56-30.46C124,60.78,130,54.39,132.54,51.69a2.82,2.82,0,0,1,1.85-1l13.67,0.09c-13,15.61-36.49,44.28-36.72,44.56a3,3,0,0,0-.71,2.51,2.25,2.25,0,0,0,.88,1.35,2.33,2.33,0,0,0,1.44.49,3,3,0,0,0,2.26-1.18L159.63,51.2a1.27,1.27,0,0,1,.75-0.4l12,0.1a2.79,2.79,0,0,1,.83.1,3.23,3.23,0,0,1-.49.75L143,88.85l-1,1.24,1,1.25,29.32,37.13a3,3,0,0,1,.47.75,3.31,3.31,0,0,1-.82.09H122.91l-0.36,1.55-10.69,45.85a3.75,3.75,0,0,1-.33.83A3.47,3.47,0,0,1,111,177l-29.6-37.1-1-1.24-1.43.68L36.36,159.48a3.07,3.07,0,0,1-.8.27,3.39,3.39,0,0,1,.09-0.81l10.5-46.2,0.35-1.54-1.42-.7L2.85,89.74a4,4,0,0,1-.7-0.51,3.55,3.55,0,0,1,.72-0.42l42.6-20.5,1.42-.68-0.33-1.54-10-46.2,0-.08,0-.08A2.24,2.24,0,0,1,36.4,19a3.48,3.48,0,0,1,.83.29L79.83,40l1.43,0.69,1-1.23L112,2.67a3.11,3.11,0,0,1,.6-0.59m0-2.06A3.06,3.06,0,0,0,110.5,1.4L80.7,38.2,38.1,17.5a4.58,4.58,0,0,0-1.85-.5,1.7,1.7,0,0,0-1.15.4c-0.7.5-.9,1.6-0.5,2.9l10,46.2L2,87c-1.3.6-2,1.4-2,2.2s0.7,1.6,1.9,2.3l42.3,20.8L33.7,158.5c-0.3,1.4-.1,2.4.5,2.9a2,2,0,0,0,1.21.36,4.36,4.36,0,0,0,1.79-.46l42.6-20.2,29.6,37.1a3.21,3.21,0,0,0,2.25,1.44,1.45,1.45,0,0,0,.35,0q1.2-.3,1.8-2.4l10.7-45.9H172c1.4,0,2.4-.4,2.7-1.2s0.1-1.8-.8-2.9L144.6,90.1,174.3,53c0.9-1.1,1.2-2.1.8-2.9s-1.3-1.2-2.7-1.2l-12-.1a3.21,3.21,0,0,0-2.2,1L113.7,97.2a1.1,1.1,0,0,1-.76.5,0.33,0.33,0,0,1-.24-0.1c-0.2-.1-0.2-0.5.2-1,0.9-1.1,25.5-31.1,38-46.1a1.13,1.13,0,0,0,.2-1.1,1.15,1.15,0,0,0-1.1-.6l-15.6-.1a4.88,4.88,0,0,0-3.3,1.6c-7.2,7.7-43.3,46-44,46.9a1.1,1.1,0,0,1-.76.5,0.33,0.33,0,0,1-.24-0.1c-0.2-.1-0.2-0.5.2-1l37.4-45.4c0.9-1.1,1.4-1.8,1.2-2.8,0.1-.3-9.9-45.5-10-45.9-0.3-1.4-.9-2.2-1.7-2.4A1.63,1.63,0,0,0,112.68,0h0Z"
					transform="translate(0 -0.01)"
				/>
			</g>
			<path
				id="nab"
				className="nab"
				d="M257.6,130.8V84c0-12.1-3.3-19.6-13.2-19.6-8.3,0-13,3.9-20,11.3v55.2H204.3V50h15.4l1.9,10.2c8.7-8.6,15.9-12.4,27.4-12.4,20.4,0,28.8,12.9,28.8,32.1v50.9H257.6m81.9,0-1.6-7.7a35.48,35.48,0,0,1-25.5,9.9c-16.5,0-27.7-8.3-27.7-24.7,0-18.4,14.9-26.3,40.4-26.3h9.8V77.7c0-10.1-3.8-14.9-16.8-14.9a67.15,67.15,0,0,0-23.7,4.7L290.9,53a92.74,92.74,0,0,1,30-5.2c24.2,0,33.8,10.4,33.8,28.1v54.9H339.5m-4.6-35.7H324.8c-13.8,0-20,4.1-20,12.1,0,6.8,4.1,11.2,12.3,11.2,7.1,0,12.6-3.3,17.8-8V95.1m64.3,37.1a117.51,117.51,0,0,1-33.6-5.4V12.8h20.1V58.1c6.4-6.1,13.8-10.4,24.8-10.4,22.3,0,32.4,17.6,32.4,39.3,0,26.4-13.4,45.2-43.7,45.2m7.1-68c-7.4,0-13.7,3.8-20.6,11.9v37.1a39.44,39.44,0,0,0,14.9,3.1c14,0,21.2-9.6,21.2-26.9,0-14.2-3.9-25.2-15.5-25.2"
				transform="translate(0 -0.01)"
			/>
		</svg>
	);
};

export default NabtradeLogo;

NabtradeLogo.propTypes = {
	width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	dark: PropTypes.bool,
	shadow: PropTypes.bool,
	className: PropTypes.string,
};

NabtradeLogo.defaultProps = {
	width: 150,
};
