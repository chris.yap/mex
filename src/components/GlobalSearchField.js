import React, { useState } from 'react';
import { getState } from '../context/StateProvider';
import { Securities } from '../constants/securities';
import { SecurityOptionTrade } from '../helpers/CustomSelect';
import { Select } from 'shaper-react';

const GlobalSearchField = ({ ...props }) => {
  const [value, updateValue] = useState('');
  const [loading, updateLoading] = useState(false);
  const [options, updateOptions] = useState(null);
  const [, dispatch] = getState();

  const SetValue = value => {
    updateValue(value);
  };
  const SetLoading = value => {
    updateLoading(value);
  };
  const SetOptions = value => {
    updateOptions(value);
  };
  let gotoSecurity = (stock, event) => {
    if (event && event.action === 'select-option') {
      if (stock) {
        props.history.push('/security/details');
        dispatch({ type: 'toggleSearchBar' });
        dispatch({ type: 'selectStock', payload: stock });
        updateValue('');
      } else {
        updateValue('');
      }
    }
  };

  let filterSecurities = inputValue => {
    if (inputValue) {
      let results = Securities.filter(i =>
        i.label.toLowerCase().includes(inputValue.toLowerCase())
      );
      SetOptions(results ? results : []);
      SetLoading(false);
    }
  };
  let handleInputChange = (newValue, event) => {
    let newerValue;
    if (newValue) {
      SetLoading(true);
      newerValue = ('' + newValue).replace(/\W/g, '');
    }
    if (
      event &&
      (event.action === 'menu-close' ||
        event.action === 'input-blur' ||
        event.action === 'set-value')
    ) {
      newerValue = value;
    }
    SetValue(newerValue);
    getOptionsAsync(newerValue);
  };

  let getOptionsAsync = newInput => {
    if (newInput) {
      setTimeout(() => {
        filterSecurities(newInput);
      }, 2000);
    }
  };

  const colourStyles = {
    control: (styles, state) => ({
      ...styles,
      height: '68px',
      backgroundColor: '#ffffff',
      color: '#4c626c',
      borderWidth: '0 1px 0 0',
      boxShadow: 'none',
      borderRadius: 0
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default'
      };
    },
    input: (styles, state) => ({
      ...styles,
      color: '#4c626c',
      fontWeight: 'bold'
    }),
    placeholder: (styles, state) => ({
      ...styles,
      color: '#909ea4',
      fontWeight: 'bold',
      border: state.isFocused ? 'none' : 'inherit',
      boxShadow: state.isFocused && 'none'
    }),
    singleValue: (styles, state) => ({
      ...styles,
      color: '#4c626c',
      fontWeight: 'bold'
    })
  };

  return (
    <Select
      isLoading={loading}
      placeholder="Search for a security … "
      components={{ Option: SecurityOptionTrade }}
      options={options || []}
      inputValue={value}
      onChange={gotoSecurity}
      onInputChange={handleInputChange}
      styles={colourStyles}
      isClearable={true}
      noOptionsMessage={e =>
        e.inputValue === '' ? 'Type to start search' : 'No results found'
      }
      {...props}
    />
  );
};

export default GlobalSearchField;
