import React from 'react';
import { Alert, Button, Textfield, Container, Section } from 'shaper-react';

export default class Test extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: true,
      message: false,
      error: false,
      errorMessage: this.generateNumber(1, 1000)
    };
  }
  toggleMessage() {
    this.setState({
      message: !this.state.message,
      error: !this.state.error,
      errorMessage: this.generateNumber(1, 1000)
    });
  }
  generateNumber = (min, max) => {
    const rndNum = Math.floor(Math.random() * (max - min + 1) + min);
    this.setState({
      number: rndNum
    });
    return rndNum;
  };
  render() {
    return (
      <>
        <Section>
          <Container>
            <Alert isOpened={this.state.alert}>
              {this.state.message ? (
                <strong>Message 1</strong>
              ) : (
                <strong>Message 2</strong>
              )}
            </Alert>

            <Button onClick={() => this.toggleMessage()}>Toggle message</Button>

            <Button type="submit" href="./test">
              Link
            </Button>

            <Textfield
              label="test"
              error={this.state.error}
              errorMessage="234"
            />

            {this.state.errorMessage}
          </Container>
        </Section>
      </>
    );
  }
}
