import React from 'react';
//import cn from 'classnames';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import {
  // SocialItems,
  SupportItems
} from '../../constants';
// import { termsConditionsItems } from '../../constants/termsConditions';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import { Breakpoints, Column, Columns, Container, Section } from 'shaper-react';

const FAQ = ({ activeBreakpoints, ...props }) => {
  return (
    <LayoutDefault
      subnav={SupportItems}
      breadcrumb={Breadcrumbs.support.faq.items}>
      <HeroView title="F.A.Q." />

      <Section
        className={`py-4 ${activeBreakpoints.below.sm &&
          'px-0 is-overflow-hidden'}`}>
        <Container>
          <Columns mobile multiline>
            <Column>Faq</Column>
          </Columns>
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(FAQ);
