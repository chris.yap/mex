import React from 'react';
//import cn from 'classnames';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { SocialItems, SupportItems } from '../../constants';
import { termsConditionsItems } from '../../constants/termsConditions';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import {
  Breakpoints,
  Button,
  Card,
  CardContent,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  List,
  ListItem,
  Section,
  Title
} from 'shaper-react';

const Contact = ({ activeBreakpoints, ...props }) => {
  return (
    <LayoutDefault subnav={SupportItems} breadcrumb={Breadcrumbs.support.items}>
      <HeroView title="Support" />

      <Section
        className={`py-4 ${activeBreakpoints.below.sm &&
          'px-0 is-overflow-hidden'}`}>
        <Container>
          <Columns mobile multiline>
            <Column
              mobile={12}
              tablet={6}
              desktop={6}
              className={activeBreakpoints.below.sm && 'px-0'}>
              <Title size="4" className={activeBreakpoints.below.sm && 'px-6'}>
                Call us on{' '}
                {activeBreakpoints.below.md ? (
                  <Button
                    inverted
                    secondary
                    medium
                    href="tel:13 13 80"
                    style={{ verticalAlign: 'baseline' }}>
                    13 13 80
                  </Button>
                ) : (
                  '13 13 80'
                )}
              </Title>
              <Card outline className="mb-4">
                <CardContent>
                  <p>
                    We are available Monday to Friday, between 8am and 7pm AEST.
                  </p>
                  <p className="mb-0">
                    If you're overseas call us on +61 2 8220 5945.
                  </p>
                </CardContent>
              </Card>

              <Title size="4" className={activeBreakpoints.below.sm && 'px-6'}>
                Email us
              </Title>
              <Card outline className="mb-4">
                <CardContent>
                  <p className="mb-0">
                    Contact us on{' '}
                    <a href={'mailto:enquiries@nabtrade.com.au'}>
                      enquiries@nabtrade.com.au
                    </a>{' '}
                    and we'll get back to you within two business days.
                  </p>
                </CardContent>
              </Card>

              <Title size="4" className={activeBreakpoints.below.sm && 'px-6'}>
                Send forms or mail
              </Title>
              <Card outline className="mb-4">
                <CardContent>
                  <p>Write to us at the following address:</p>

                  <p className="mb-0">
                    nabtrade <br />
                    GPO Box 4545 <br />
                    Melbourne, Victoria 3001
                  </p>
                </CardContent>
              </Card>

              {!activeBreakpoints.below.sm && (
                <FollowUs activeBreakpoints={activeBreakpoints} />
              )}
            </Column>
            <Column
              mobile={12}
              tablet={6}
              desktop={6}
              className={activeBreakpoints.below.sm && 'px-0'}>
              <Title size="4" className={activeBreakpoints.below.sm && 'px-6'}>
                Terms and conditions
              </Title>
              <Card outline className="mb-4">
                <List>
                  {termsConditionsItems.map(item => {
                    return (
                      <ListItem
                        key={item.title}
                        href={item.url}
                        linkType="external"
                        target="_blank"
                        className={activeBreakpoints.below.sm && 'mx-2'}>
                        {item.title}
                      </ListItem>
                    );
                  })}
                </List>
              </Card>
              {activeBreakpoints.below.sm && (
                <FollowUs activeBreakpoints={activeBreakpoints} />
              )}
              <Divider className="mb-2" />
              <Button
                flat
                linkType="external"
                href="https://www.nabtrade.com.au"
                className={activeBreakpoints.below.sm ? 'mx-5' : 'mx-0'}>
                <Icon medium>login</Icon>
                <span>View full site</span>
              </Button>
            </Column>
          </Columns>
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(Contact);

const FollowUs = ({ activeBreakpoints }) => (
  <>
    <Title size="4" className={activeBreakpoints.below.sm && 'px-6'}>
      Follow us
    </Title>
    <Card outline className="mb-4">
      <CardContent>
        {SocialItems.map(item => {
          return (
            <a href={item.url} target={'_blank'}>
              <Icon
                large
                style={item.label !== 'linkedin' ? { top: '3px' } : {}}
                className="mr-4">
                {item.label}
              </Icon>
            </a>
          );
        })}
      </CardContent>
    </Card>
  </>
);
