import React from 'react';
import cn from 'classnames';
import ClampLines from 'react-clamp-lines';
import {
  Alert,
  Box,
  Breakpoints,
  Card,
  CardContent,
  CardHeader,
  CardImage,
  Column,
  Columns,
  Divider,
  Level,
  LevelItem,
  List,
  ListItem,
  Title
} from 'shaper-react';
import RealtimeQuoteMsg from '../../../components/RealtimeQuoteMsg';

const Technical = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns>
        <Column>
          <img
            src="/images/trading_central_logo.png"
            width="100"
            alt=""
            className={`${activeBreakpoints.below.sm && 'mx-4'}`}
          />
          <Divider className="mb-4" />

          <Columns multiline gapless={activeBreakpoints.below.sm}>
            <Column>
              <Card className={activeBreakpoints.below.sm ? 'mb-1' : ''}>
                <CardImage
                  aspectRatio={
                    activeBreakpoints.below.sm
                      ? 6
                      : activeBreakpoints.below.md
                      ? 2
                      : activeBreakpoints.above.lg
                      ? 6
                      : 4
                  }
                  className={'has-bg-success'}>
                  <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                    <Title
                      size="7"
                      className="has-text-white mb-0 has-text-weight-bold">
                      <span className="is-capitalized">Short Term View:</span>
                    </Title>
                    <Title
                      size="2"
                      className="has-text-white mb-0 mt-1 is-uppercase font-nabimpact">
                      Limited Rise
                    </Title>
                  </div>
                </CardImage>
                <CardHeader>
                  <div>
                    <Title
                      size="7"
                      className="is-uppercase has-text-blue-grey mb-0">
                      Trading Central
                    </Title>
                    <Title
                      size="5"
                      className={cn(
                        'has-text-blue-grey pr-2 mb-1 is-capitalized pos-r has-top-2'
                      )}>
                      <ClampLines
                        text={'Recent Technical Change'}
                        buttons={false}
                      />
                    </Title>
                    <p>
                      <ClampLines
                        text="Trading Central recently changed their Short Term View from Decline to Limited Rise."
                        buttons={false}
                      />
                    </p>
                  </div>
                </CardHeader>
                <CardContent>
                  <Level mobile>
                    <LevelItem left>
                      <p className="has-text-size-3 mb-0 has-text-black text--lighten-2">
                        <strong className="is-uppercase">
                          Trading Central
                        </strong>{' '}
                        | As of 03.04.19
                      </p>
                    </LevelItem>
                    {/* <LevelItem right>
											<p className="has-text-size-3 has-text-right mb-0 has-text-black text--lighten-2">
												Research report
												<Icon small style={{ top: '2px' }}>
													pdf
												</Icon>
												PDF
											</p>
										</LevelItem> */}
                  </Level>
                </CardContent>
              </Card>
            </Column>
            <Column>
              <Card className={activeBreakpoints.below.sm ? 'mb-1' : ''}>
                <CardImage
                  aspectRatio={
                    activeBreakpoints.below.sm
                      ? 6
                      : activeBreakpoints.below.md
                      ? 2
                      : activeBreakpoints.above.lg
                      ? 6
                      : 4
                  }
                  className={'has-bg-danger'}>
                  <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                    <Title
                      size="7"
                      className="has-text-white has-text-weight-bold mb-0">
                      <span className="is-capitalized">Medium Term View:</span>
                    </Title>
                    <Title
                      size="2"
                      className="has-text-white mb-0 mt-1 is-uppercase font-nabimpact">
                      Bearish
                    </Title>
                  </div>
                </CardImage>
                <CardHeader>
                  <div>
                    <Title
                      size="7"
                      className="is-uppercase has-text-blue-grey mb-0">
                      Trading Central
                    </Title>
                    <Title
                      size="5"
                      className={cn(
                        'has-text-blue-grey pr-2 mb-1 is-capitalized pos-r has-top-2'
                      )}>
                      <ClampLines
                        text={'Recent Technical Change'}
                        buttons={false}
                      />
                    </Title>
                    <p>
                      <ClampLines
                        text="Trading Central recently changed its Medium Term View to Bearish"
                        buttons={false}
                      />
                    </p>
                  </div>
                </CardHeader>
                <CardContent>
                  <Level mobile>
                    <LevelItem left>
                      <p className="has-text-size-3 mb-0 has-text-black text--lighten-2">
                        <strong className="is-uppercase">
                          Trading Central
                        </strong>{' '}
                        | As of 03.04.19
                      </p>
                    </LevelItem>
                    {/* <LevelItem right>
											<p className="has-text-size-3 has-text-right mb-0 has-text-black text--lighten-2">
												Research report
												<Icon small style={{ top: '2px' }}>
													pdf
												</Icon>
												PDF
											</p>
										</LevelItem> */}
                  </Level>
                </CardContent>
              </Card>
            </Column>
          </Columns>
        </Column>
        <Column
          tablet="5"
          desktop="4"
          widescreen="3"
          className="is-hidden-mobile">
          <Alert info isOpened={true} inverted>
            <p className="mb-2">
              <strong>
                Why is technical research important to my investment?
              </strong>
            </p>
            <p className="mb-0">
              Technicals research is gathered by analysing trading patterns over
              a period of time – usually in the short term. Technical
              recommendations are formulated by using trading models and do not
              explicitly consider factors like company valuations or market
              consensus estimates. Technical research should be used as a guide
              for investors who are looking to profit from short term movements
              in the share price.
            </p>
          </Alert>
        </Column>
      </Columns>
      <Title
        size="5"
        className={`mb-2 ${activeBreakpoints.below.sm && 'mx-4'}`}>
        Short term rebound towards 38.32
      </Title>
      <Divider className="mb-4" />
      <Columns>
        <Column>
          <Box className="pb-2">
            <Title size="5">Technical research</Title>
            <Divider />
            <List>
              <ListItem className="px-0 mb-0">
                <Level mobile className="is-flex-1">
                  <LevelItem left>Our preference</LevelItem>
                  <LevelItem right>
                    <strong className="has-text-right">
                      Short term rebound {activeBreakpoints.below.md && <br />}{' '}
                      towards 38.32
                    </strong>
                  </LevelItem>
                </Level>
              </ListItem>
              <ListItem className="px-0 mb-0">
                <Level mobile className="is-flex-1">
                  <LevelItem left>Our pivot point</LevelItem>
                  <LevelItem right>
                    <strong className="has-text-right">37.52</strong>
                  </LevelItem>
                </Level>
              </ListItem>
              <ListItem className="px-0">
                <Level mobile className="is-flex-1">
                  <LevelItem left>
                    Alternative {activeBreakpoints.below.sm && <br />} scenario
                  </LevelItem>
                  <LevelItem right>
                    <strong className="has-text-right">
                      Below 38.32, {activeBreakpoints.below.sm && <br />} expect
                      37.53
                      {activeBreakpoints.below.md && <br />} and 37.20
                    </strong>
                  </LevelItem>
                </Level>
              </ListItem>
            </List>
          </Box>
          <p className={`${activeBreakpoints.below.sm && 'mx-4'}`}>
            The RSI is below 50. The MACD is negative and above its signal line.
            The configuration is mixed. Moreover, the share stands below its 20
            and 50 day MA (respectively at 34.23 and 39.21). BHP Group Limited
            is currently trading near its 52 week low at 32.23 reached on
            05.04.19.
          </p>
        </Column>
        <Column tablet="5" desktop="4" widescreen="3">
          <Box className="pb-2">
            <Title size="5">Supports and resistances</Title>
            <Divider />
            <List>
              {Support.map((sup, sI) => {
                return (
                  <ListItem key={sI} className="px-0 py-1">
                    <strong>
                      {sup.support} {sI === 4 && '(AUD-last)'}
                    </strong>
                  </ListItem>
                );
              })}
            </List>
          </Box>
        </Column>
      </Columns>
      <Card>
        <img src="/images/technical.png" width="100%" alt="" />
      </Card>
      <RealtimeQuoteMsg
        local
        className={`mt-2 ${activeBreakpoints.below.sm && 'mx-4'}`}
      />
    </React.Fragment>
  );
};

export default Breakpoints(Technical);

const Support = [
  {
    support: '37.78'
  },
  {
    support: '36.52'
  },
  {
    support: '36.38'
  },
  {
    support: '35.86'
  },
  {
    support: '35.70'
  },
  {
    support: '34.01'
  },
  {
    support: '33.12'
  },
  {
    support: '32.94'
  },
  {
    support: '32.68'
  }
];
