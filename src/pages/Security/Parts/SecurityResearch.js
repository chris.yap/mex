import React from 'react';
import cn from 'classnames';
import ClampLines from 'react-clamp-lines';
import {
  Alert,
  Box,
  Breakpoints,
  Card,
  CardHeader,
  CardImage,
  CardContent,
  Column,
  Columns,
  Content,
  Icon,
  Level,
  LevelItem,
  Radio,
  RadioGroup,
  Title
} from 'shaper-react';

class SecurityResearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredResearch: [],
      researchFilter: 'all'
    };
    this.researchChange = this.researchChange.bind(this);
  }
  componentDidMount() {
    this.researchChange(this.state.researchFilter);
  }
  researchChange = event => {
    let filteredResearch = [];
    this.setState({ filteredResearch: [] });
    for (let doc of Articles) {
      if (event === 'all') {
        filteredResearch.push(doc);
      } else if (doc.type.toLowerCase().indexOf(event.toLowerCase()) >= 0) {
        filteredResearch.push(doc);
      }
    }
    this.setState({
      researchFilter: event,
      filteredResearch: filteredResearch
    });
  };
  render() {
    const { activeBreakpoints } = this.props;
    const { researchFilter } = this.state;
    return (
      <React.Fragment>
        <RadioGroup
          name="research"
          buttons
          rounded={activeBreakpoints.above.xs}
          selectedValue={this.state.researchFilter}
          onChange={this.researchChange}
          className={cn(
            activeBreakpoints.below.sm
              ? ''
              : 'is-justify-content-flex-start is-inline-flex'
          )}>
          <Radio button value="all" label="All" />
          <Radio button value="technical" label="Technical" />
          <Radio button value="consensus" label="Consensus" />
          <Radio button value="quantitative" label="Quantitative" />
        </RadioGroup>

        <Columns>
          <Column>
            <Columns
              mobile
              multiline
              className={cn(activeBreakpoints.below.sm ? 'mt-4' : '')}>
              {this.state.filteredResearch.map((doc, dIndex) => (
                <Column
                  key={dIndex}
                  mobile="12"
                  tablet="6"
                  desktop={researchFilter === 'all' ? '4' : '6'}
                  className={activeBreakpoints.below.sm ? 'py-0' : ''}>
                  <Card
                    href={doc.link}
                    className={activeBreakpoints.below.sm ? 'mb-1' : ''}>
                    <CardImage
                      aspectRatio={
                        activeBreakpoints.below.md
                          ? '5'
                          : activeBreakpoints.below.xl
                          ? '4'
                          : '5'
                      }
                      className={cn(
                        'lighten-5',
                        (doc.ratings === 'buy' ||
                          doc.ratings === 'positive' ||
                          doc.ratings === 'rise' ||
                          doc.ratings === 'bullish' ||
                          doc.ratings === 'limited rise') &&
                          'has-bg-success',
                        (doc.ratings === 'hold' ||
                          doc.ratings === 'neutral' ||
                          doc.ratings === 'conservative') &&
                          'has-bg-info',
                        (doc.ratings === 'overvalued' ||
                          doc.ratings === 'sell' ||
                          doc.ratings === 'bearish' ||
                          doc.ratings === 'decline' ||
                          doc.ratings === 'limited decline') &&
                          'has-bg-danger'
                      )}>
                      <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                        <Title
                          size="7"
                          className={cn(
                            'mb-0',
                            (doc.ratings === 'buy' ||
                              doc.ratings === 'positive' ||
                              doc.ratings === 'rise' ||
                              doc.ratings === 'bullish' ||
                              doc.ratings === 'limited rise') &&
                              'has-text-success',
                            (doc.ratings === 'hold' ||
                              doc.ratings === 'neutral' ||
                              doc.ratings === 'conservative') &&
                              'has-text-info',
                            (doc.ratings === 'overvalued' ||
                              doc.ratings === 'sell' ||
                              doc.ratings === 'bearish' ||
                              doc.ratings === 'decline' ||
                              doc.ratings === 'limited decline') &&
                              'has-text-danger'
                          )}>
                          {(doc.ratings === 'positive' ||
                            doc.ratings === 'neutral' ||
                            doc.ratings === 'negative') &&
                            'Average score: 5'}
                          {(doc.ratings === 'sell' ||
                            doc.ratings === 'reduce' ||
                            doc.ratings === 'hold' ||
                            doc.ratings === 'buy' ||
                            doc.ratings === 'strong buy') && (
                            <span className="is-capitalized">Rating:</span>
                          )}
                          {(doc.ratings === 'undervalued' ||
                            doc.ratings === 'fairly valued' ||
                            doc.ratings === 'overvalued') && (
                            <span className="is-capitalized">
                              Quantitative Recommendation:{' '}
                            </span>
                          )}
                          {(doc.ratings === 'rise' ||
                            doc.ratings === 'limited rise' ||
                            doc.ratings === 'conservative' ||
                            doc.ratings === 'limited decline' ||
                            doc.ratings === 'decline') && (
                            <span className="is-capitalized">
                              Short Term View:
                            </span>
                          )}
                          {(doc.ratings === 'bullish' ||
                            doc.ratings === 'range' ||
                            doc.ratings === 'bearish') && (
                            <span className="is-capitalized">
                              Medium Term View:
                            </span>
                          )}
                        </Title>
                        <Title
                          size="2"
                          className={cn(
                            'mb-0 mt-1 is-uppercase font-nabimpact',
                            (doc.ratings === 'buy' ||
                              doc.ratings === 'positive' ||
                              doc.ratings === 'rise' ||
                              doc.ratings === 'bullish' ||
                              doc.ratings === 'limited rise') &&
                              'has-text-success',
                            (doc.ratings === 'hold' ||
                              doc.ratings === 'neutral' ||
                              doc.ratings === 'conservative') &&
                              'has-text-info',
                            (doc.ratings === 'overvalued' ||
                              doc.ratings === 'sell' ||
                              doc.ratings === 'bearish' ||
                              doc.ratings === 'decline' ||
                              doc.ratings === 'limited decline') &&
                              'has-text-danger'
                          )}>
                          {doc.ratings}
                        </Title>
                      </div>
                    </CardImage>
                    <CardHeader>
                      <Content>
                        <Title
                          size="5"
                          className={cn('mt-0 mb-1 is-capitalized')}>
                          <ClampLines text={doc.title} buttons={false} />
                        </Title>
                        <ClampLines
                          text={doc.desc}
                          buttons={false}
                          innerElement="p"
                          className="has-line-height-125"
                        />
                      </Content>
                    </CardHeader>
                    <CardContent>
                      <Level mobile className="mb-2">
                        <LevelItem
                          left
                          className="has-text-size-3 has-text-black text--lighten-2">
                          As of {doc.date}
                        </LevelItem>
                        {doc.pdf && (
                          <LevelItem
                            right
                            className="has-text-size-3 has-text-right has-text-black text--lighten-2">
                            Research report
                            <Icon small style={{ top: '2px' }}>
                              pdf
                            </Icon>
                            PDF
                          </LevelItem>
                        )}
                      </Level>
                      <Box
                        p={3}
                        width={
                          doc.source.toLowerCase() === 'morningstar'
                            ? '100px'
                            : doc.source.toLowerCase() === 'trading central'
                            ? '48px'
                            : '110px'
                        }
                        display="inline-flex"
                        boxShadow="none"
                        backgroundPosition="left center"
                        backgroundRepeat="no-repeat"
                        backgroundSize="contain"
                        backgroundImage={`url(${
                          doc.source.toLowerCase() === 'morningstar'
                            ? '/images/Morningstar_Logo.svg'
                            : doc.source.toLowerCase() === 'trading central'
                            ? '/images/trading_central_logo.png'
                            : '/images/rtr_ahz_rgb_pos.png'
                        })`}
                      />
                    </CardContent>
                  </Card>
                </Column>
              ))}
            </Columns>
          </Column>
          {activeBreakpoints.above.sm && researchFilter !== 'all' && (
            <Column size="3">
              {researchFilter === 'consensus' ? (
                <Alert info isOpened={true} inverted>
                  <p className="mb-2">
                    <strong>
                      Why is consensus research important to my investment?
                    </strong>
                  </p>
                  <p className="mb-0">
                    Consensus research considers collectively the
                    recommendations of all analysts covering a security and
                    presents a simple average of analyst recommendations.
                    Consensus research gives investors a view of what analysts
                    are recommending to their clients to give a better insight
                    as to whether the market views a company as a buying or
                    selling opportunity. Consensus research provides a cheap
                    alternative to research subscriptions for mid to small cap
                    companies.
                  </p>
                </Alert>
              ) : researchFilter === 'technical' ? (
                <Alert info isOpened={true} inverted>
                  <p className="mb-2">
                    <strong>
                      Why is technical research important to my investment?
                    </strong>
                  </p>
                  <p className="mb-0">
                    Technicals research is gathered by analysing trading
                    patterns over a period of time – usually in the short term.
                    Technical recommendations are formulated by using trading
                    models and do not explicitly consider factors like company
                    valuations or market consensus estimates. Technical research
                    should be used as a guide for investors who are looking to
                    profit from short term movements in the share price.
                  </p>
                </Alert>
              ) : (
                <Alert info isOpened={true} inverted>
                  <p className="mb-2">
                    <strong>
                      Why is quantitative research important to my investment?
                    </strong>
                  </p>
                  <p className="mb-0">
                    Quantitative analysis provide insight into the valuation or
                    historic performance of a specific security or market. But
                    quantitative analysis is not often used as a standalone
                    method for evaluating long-term investments. Instead,
                    quantitative analysis is used in conjunction with
                    fundamental and technical analysis to determine the
                    potential advantages and risks of investment decisions.
                  </p>
                </Alert>
              )}
            </Column>
          )}
        </Columns>
      </React.Fragment>
    );
  }
}

export default Breakpoints(SecurityResearch);

const Articles = [
  {
    ratings: 'rise',
    source: 'Thomson Reuters',
    pdf: false,
    title: 'Analysts Recommendation',
    desc:
      'Sint eiusmod irure mollit eiusmod Lorem pariatur. Ad anim duis veniam officia esse sint. Consectetur officia fugiat officia do qui consequat commodo voluptate commodo culpa nisi consectetur. Reprehenderit mollit nisi cupidatat reprehenderit magna ullamco in quis eiusmod cupidatat. Excepteur veniam aliqua elit tempor.',
    date: '23.11.2016',
    type: 'consensus'
  },
  {
    ratings: 'limited decline',
    source: 'Trading Central',
    pdf: false,
    title: 'Analysts Recommendation',
    desc:
      'Do exercitation minim est est et consectetur fugiat aute magna sint incididunt nulla. Tempor veniam sit elit mollit eiusmod do fugiat nisi velit. Incididunt elit sint sit sit eu consectetur ipsum ut amet est eu. Eiusmod incididunt sunt minim aute deserunt consectetur ipsum est.',
    date: '11.08.2015',
    type: 'consensus'
  },
  {
    ratings: 'conservative',
    source: 'Morningstar',
    pdf: true,
    title: 'Recent Technical Change',
    desc:
      'Consectetur laborum aliqua officia irure consectetur sint culpa commodo eiusmod occaecat cillum consectetur cillum mollit. Est sint laborum adipisicing in. Laboris cupidatat sint officia proident ea laboris sint deserunt minim quis laborum. Proident amet aliqua Lorem velit cillum aute id labore.',
    date: '26.05.2017',
    type: 'consensus'
  },
  {
    ratings: 'buy',
    source: 'Thomson Reuters',
    pdf: false,
    title: 'Previous Recommendation Changes',
    desc:
      'Sunt ullamco dolore reprehenderit nisi labore culpa Lorem ex Lorem irure est cupidatat. Est dolore nulla et sit laboris exercitation exercitation deserunt commodo voluptate ad magna anim. In elit occaecat sint do ut qui sunt laborum non commodo do esse tempor.',
    date: '22.05.2016',
    type: 'consensus'
  },
  {
    ratings: 'neutral',
    source: 'Thomson Reuters',
    pdf: true,
    title: 'Recent Technical Change',
    desc:
      'Adipisicing ad tempor dolore ipsum irure culpa cillum culpa do minim elit qui anim aliqua. Velit occaecat eiusmod officia culpa labore ipsum duis nisi laborum. Et commodo consectetur consectetur veniam fugiat quis commodo aliquip pariatur sit magna dolor. Aliqua veniam ad minim duis adipisicing consequat ex Lorem incididunt. Mollit duis et mollit Lorem est ad minim voluptate occaecat magna elit est commodo. Dolore ullamco eu laboris magna aliqua sit elit cillum ea.',
    date: '13.04.2014',
    type: 'consensus'
  },
  {
    ratings: 'neutral',
    source: 'Thomson Reuters',
    pdf: true,
    title: 'Analysts Recommendation',
    desc:
      'Consectetur ut pariatur est commodo esse aute aute cupidatat cillum enim qui. Minim sit nisi ullamco eiusmod nulla. Anim do laboris dolore occaecat reprehenderit sunt reprehenderit enim velit anim velit pariatur. Magna voluptate ex veniam sunt consequat ullamco incididunt cupidatat reprehenderit.',
    date: '26.05.2015',
    type: 'quantitative'
  },
  {
    ratings: 'bullish',
    source: 'Thomson Reuters',
    pdf: false,
    title: 'Analysts Recommendation',
    desc:
      'Cillum nostrud sunt sit amet elit deserunt enim ipsum. Consectetur do est ea minim tempor veniam voluptate nulla. Ex ex enim et elit incididunt ut cupidatat non.',
    date: '18.07.2014',
    type: 'quantitative'
  },
  {
    ratings: 'sell',
    source: 'Trading Central',
    pdf: true,
    title: 'Recent Technical Change',
    desc:
      'Duis est aliqua sint enim minim consequat cupidatat. Aliqua dolore eiusmod pariatur ad occaecat eiusmod laboris dolor. Laborum nostrud deserunt tempor id Lorem proident ipsum culpa. Commodo aliquip ut excepteur exercitation cupidatat consequat ea consectetur sunt laboris. Sint veniam aliquip sunt est officia commodo do labore aliqua cupidatat anim enim velit officia. Aliqua et tempor enim occaecat sit exercitation magna aute ad laboris.',
    date: '30.09.2014',
    type: 'technical'
  }
];
