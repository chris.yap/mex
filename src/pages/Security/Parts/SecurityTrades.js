import React from 'react';
import faker from 'faker';
import {
  Breakpoints,
  Card,
  CardHeader,
  CardHeaderTitle,
  Columns,
  Column,
  Label,
  List,
  ListItem,
  Pagination,
  Progress,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

class SecurityTrades extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  componentDidMount = () => {
    faker.locale = 'en_AU';
    this.CreateData();
  };
  CreateData = () => {
    let tableData = [];
    for (let i = 0; i < 25; i++) {
      tableData.push({
        price: faker.finance.amount(39, 40, 2),
        qty: faker.finance.amount(1, 1000, 0)
      });
    }
    this.setState({
      data: tableData
    });
  };
  render() {
    const { data } = this.state;
    const { activeBreakpoints } = this.props;
    return (
      <Columns mobile multiline>
        <Column>
          <Card className="mb-4">
            <Table
              hasStripes
              headers={Headers}
              data={data}
              hasNoScroll={activeBreakpoints.above.xs}
              tableRow={(row, ind) => (
                <TableRow
                  key={ind}
                  activeBreakpoints={activeBreakpoints}
                  {...row}
                />
              )}
              my={0}
            />
          </Card>
          <Pagination
            total="12"
            className={`${activeBreakpoints.below.sm ? 'mx-4' : ''}`}
          />
        </Column>
        <Column
          mobile="12"
          tablet="12"
          desktop="5"
          widescreen="4"
          className="is-hidden-mobile">
          <Card>
            <CardHeader className="px-4">
              <CardHeaderTitle>Trade highlights</CardHeaderTitle>
            </CardHeader>
            <List>
              <ListItem>
                <div className="is-flex-1">
                  <Label className="nab-label">Number of trades</Label>
                  <Columns mobile>
                    <Column size="4" className="has-text-right">
                      Today
                    </Column>
                    <Column className="pt-4 px-0">
                      <Progress
                        small
                        secondary
                        value="2157"
                        className="my-0 py-0"
                      />
                    </Column>
                    <Column size="3">2,157</Column>
                  </Columns>
                </div>
              </ListItem>
              <ListItem>
                <div className="is-flex-1">
                  <Label className="nab-label">Total volume</Label>
                  <Columns mobile className="mb-0">
                    <Column size="4" className="has-text-right pb-1">
                      Today
                    </Column>
                    <Column className="pt-4 px-0 pb-1">
                      <Progress
                        small
                        secondary
                        value="714589"
                        max="4504068"
                        className="my-0 py-0"
                      />
                    </Column>
                    <Column size="3" className="pb-1">
                      714,589
                    </Column>
                  </Columns>
                  <Columns mobile>
                    <Column size="4" className="has-text-right">
                      Previous day
                    </Column>
                    <Column className="pt-4 px-0">
                      <Progress
                        small
                        secondary
                        value="4504068"
                        max="4504068"
                        className="my-0 py-0"
                      />
                    </Column>
                    <Column size="3">4,504,068</Column>
                  </Columns>
                </div>
              </ListItem>
              <ListItem>
                <div className="is-flex-1">
                  <Label className="nab-label">Total value</Label>
                  <Columns mobile className="mb-0">
                    <Column size="4" className="has-text-right">
                      Today
                    </Column>
                    <Column className="pt-4 px-0">
                      <Progress
                        small
                        secondary
                        value="28541656"
                        className="my-0 py-0"
                      />
                    </Column>
                    <Column size="3">$28,541,656</Column>
                  </Columns>
                </div>
              </ListItem>
            </List>
          </Card>
        </Column>
      </Columns>
    );
  }
}

export default Breakpoints(SecurityTrades);

const TableRow = ({ row, activeBreakpoints }) => (
  <Row>
    <Cell>
      10:15am <span className="is-hidden-mobile">AEST</span>
    </Cell>
    <Cell className="has-text-right">{row.price}</Cell>
    <Cell className="has-text-right">{row.qty}</Cell>
    <Cell width={`${activeBreakpoints.below.sm ? '40%' : '20%'}`} className="">
      <Progress
        noBg
        square
        small
        secondary
        value={row.qty}
        max={1000}
        className="mt-1"
      />
    </Cell>
    <Cell className="has-text-right">
      {new Intl.NumberFormat('en-AU').format(`${row.price * row.qty}`)}
    </Cell>
    <Cell>{row.status}</Cell>
  </Row>
);

const Headers = [
  { name: 'Time' },
  { name: 'Price', align: 'right' },
  { name: 'Qty', align: 'right' },
  { name: ' ' },
  { name: 'Value', align: 'right' },
  { name: 'Status' }
];
