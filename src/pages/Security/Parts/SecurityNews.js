import React from 'react';
import cn from 'classnames';
// import ClampLines from 'react-clamp-lines';
import { News } from '../../../constants/news';
import {
  Breakpoints,
  Button,
  Card,
  CardContent,
  CardHeader,
  Column,
  Columns,
  Container,
  Divider,
  Hero,
  Icon,
  Modal,
  Pagination,
  Section,
  Tag,
  Title
} from 'shaper-react';

class SecurityNewsAnnouncements extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newsModal: false
    };
  }
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <React.Fragment>
        <Columns mobile multiline>
          {News.map((doc, index) => (
            <Column
              key={index}
              mobile="12"
              tablet="12"
              desktop="12"
              widescreen="12"
              className={'py-1'}>
              <Card
                onClick={() => this.setState({ newsModal: true })}
                className={cn(
                  'has-link has-pointer',
                  activeBreakpoints.below.sm ? 'mb-1' : ''
                )}>
                <CardHeader>
                  <Title size="5" className={cn('has-text-blue-grey mb-0')}>
                    {/* <ClampLines text={doc.title} buttons={false} lines={doc.sensitive && doc.type !== 'news' ? 2 : 3} /> */}
                    {doc.title} <br />
                    {doc.sensitive && doc.type !== 'news' && (
                      <Tag small secondary className="mt-2">
                        Price sensitive
                      </Tag>
                    )}
                  </Title>
                </CardHeader>
                <CardContent>
                  <p className="has-text-size-3 mb-0 has-text-black text--lighten-2 is-flex is-align-items-center">
                    <img
                      src="/images/rtr_ahz_rgb_pos.png"
                      width="70"
                      alt=""
                      className="pos-r has-top mr-1"
                    />{' '}
                    | {doc.date}
                  </p>
                </CardContent>
              </Card>
            </Column>
          ))}
        </Columns>
        <Pagination
          current={1}
          total={10}
          className={activeBreakpoints.below.sm ? 'mx-4' : ''}
        />

        <Modal
          isOpened={this.state.newsModal}
          fullscreen
          header={
            <div
              className={
                'is-flex is-align-items-center is-justify-content-center'
              }>
              <Button
                square
                secondary
                inverted
                className="mr-4"
                onClick={() => this.setState({ newsModal: false })}>
                <Icon>arrow-left</Icon>
              </Button>
              <Title size={activeBreakpoints.above.xs ? 3 : 4} className="mb-0">
                BHP buys stake in Canadian firm that extracts emissions from the
                air
              </Title>
            </div>
          }
          className="pa-0">
          <Section className="py-8">
            <Container>
              <Columns mobile>
                <Column
                  mobile="12"
                  tablet="10"
                  desktop="8"
                  widescreen="8"
                  tabletOffset="1"
                  desktopOffset="2"
                  widescreenOffset="2">
                  <Hero
                    secondary
                    backgroundImage="url(/images/bhp.jpg)"
                    backgroundSize="cover"
                    backgroundPosition="center"
                    height="300px"
                    className="mb-4"
                  />
                  <p className="has-text-blue-grey">
                    BHP buys stake in Canadian firm that extracts emissions from
                    the air -<small> By Barbara Lewis</small>
                  </p>
                  <Divider className="mb-4" />
                  <p className="is-flex is-align-items-center">
                    <img
                      src="/images/rtr_ahz_rgb_pos.png"
                      width="100"
                      className="mr-4"
                      alt="Reuters"
                    />
                    <span className="has-text-size-2 has-text-blue-grey">
                      05-03-2019 08:01pm AEDT
                    </span>
                  </p>
                  <Divider className="mb-4" />
                  <p>
                    LONDON (Reuters) - The world's biggest coking coal producer
                    BHP has bought a $6 million (£4.6 million) equity stake in a
                    Canadian-based company that sucks carbon dioxide from the
                    atmosphere, as miners' quest to become sustainable and
                    retain ethical investors gathers pace.
                  </p>
                  <p>
                    U.N. scientists warned last year that temperature rises
                    could only be kept under control if much more radical action
                    were taken, including lifestyle changes and technologies
                    that capture and remove CO2 would be needed.
                  </p>
                  <p>
                    BHP, alone among the major miners has a target of net zero
                    emissions by the second half of the century, in line with
                    U.N. carbon-cutting goals.
                  </p>
                  <p>
                    That is a huge challenge, especially if the emissions caused
                    by selling its amounts of coking coal and iron ore for
                    steel-making are included.
                  </p>
                  <p>
                    Canada's Carbon Engineering (CE) has been removing emissions
                    from the atmosphere since 2015 at a pilot plant in British
                    Columbia and converting it into fuel since 2017.
                  </p>
                  <p>
                    BHP said the direct air capture technology had the potential
                    to deliver large-scale negative emissions.
                  </p>
                  <p>
                    "We hope that this investment can accelerate the development
                    and adoption of this technology," BHP Vice President,
                    Sustainability and Climate Change, Fiona Wild said in a
                    statement.
                  </p>
                  <p>
                    CE's CEO Steve Oldham said BHP's global reach and experience
                    of complex projects made it "an ideal partner" as CE sought
                    to deliver affordable, carbon-neutral fuels and emissions
                    cuts.
                  </p>
                  <p>
                    Oil and gas companies are also acknowledging the scale of
                    their challenge, especially as pressure mounts on them over
                    the downstream emissions from burning their products, not
                    just the emissions caused by their own operations.
                  </p>
                  <p>
                    BHP's equity stake in CE follows the investment of an
                    undisclosed sum announced in January from a subsidiary of
                    Occidental Petroleum and Chevron's venture capital arm.
                    Philanthropist Bill Gates is also a backer.
                  </p>
                  <p>
                    Technology to capture emissions adds to costs and has
                    struggled to find corporate investors.
                  </p>
                  <p>
                    Even the relatively established technology of carbon capture
                    and storage, which captures and buries emissions released by
                    power generation, for instance, has struggled for years.
                  </p>
                  <p>
                    Financial analysts are even more sceptical about direct
                    capture from the air, although scientists say it could help
                    to curb global warming, blamed for causing more heatwaves,
                    wildfires, floods and rising sea levels.
                  </p>
                  <p>
                    Mining companies are under particular pressure to prove
                    their ethical and sustainable credentials after a fatal dam
                    burst in Brazil in January has heightened investor scrutiny.
                  </p>
                  <p>
                    Glencore, the world's biggest producer of seaborne coal, has
                    said it will cap coal production and last week named climate
                    change in a revised list of the risks it faces.
                  </p>
                  <p>
                    <small>
                      (Reporting by Barbara Lewis; additional reporting by Nina
                      Chestney; Editing by Susan Fenton)
                    </small>
                  </p>
                </Column>
              </Columns>
            </Container>
          </Section>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Breakpoints(SecurityNewsAnnouncements);
