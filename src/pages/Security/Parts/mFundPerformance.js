import React from 'react';
import faker from 'faker';
import {
  Box,
  Breakpoints,
  Card,
  CardContent,
  Columns,
  Column,
  Level,
  LevelItem,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Subtitle
} from 'shaper-react';
import PercentageView from '../../../components/PercentageView';

const MFundPerformance = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns mobile multiline>
        <Column mobile={12} desktop={6}>
          <Card style={{ height: '100%' }}>
            <CardContent>Chart goes here</CardContent>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Annualised total trailing returns
          </Subtitle>
          <Card>
            <Table
              hasStripes
              headers={TrailingReturnsHeaders}
              data={TrailingReturns}
              tableRow={TrailingReturnsRow}
              my={0}
            />
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Peer group fund ranking
          </Subtitle>
          <Card>
            <List>
              <ListItem>
                <Columns mobile className="is-flex-1">
                  <Column size="3" className=" has-text-weight-semibold">
                    Period
                  </Column>
                  <Column className=" has-text-weight-semibold">
                    Bottom 20%
                  </Column>
                  <Column className="has-text-right has-text-weight-semibold">
                    Top 20%
                  </Column>
                </Columns>
              </ListItem>
              {PeerGroup.map((item, i) => (
                <ListItem key={i}>
                  <Columns mobile className="is-flex-1">
                    <Column size="3">{item.name}</Column>
                    <Column>
                      <Columns mobile gapless>
                        <Column>
                          <Box
                            className={`pt-2 pb-4 has-bg-secondary ${
                              item.value === '1' ? '' : 'lighten-5'
                            }`}
                          />
                        </Column>
                        <Column>
                          <Box
                            className={`pt-2 pb-4 has-bg-secondary ${
                              item.value === '2' ? '' : 'lighten-5'
                            }`}
                          />
                        </Column>
                        <Column>
                          <Box
                            className={`pt-2 pb-4 has-bg-secondary ${
                              item.value === '3' ? '' : 'lighten-5'
                            }`}
                          />
                        </Column>
                        <Column>
                          <Box
                            className={`pt-2 pb-4 has-bg-secondary ${
                              item.value === '4' ? '' : 'lighten-5'
                            }`}
                          />
                        </Column>
                        <Column>
                          <Box
                            className={`pt-2 pb-4 has-bg-secondary ${
                              item.value === '5' ? '' : 'lighten-5'
                            }`}
                          />
                        </Column>
                      </Columns>
                    </Column>
                  </Columns>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Fees
          </Subtitle>
          <Card>
            <List>
              {Fees.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.name}</LevelItem>
                    <LevelItem right>
                      <PercentageView noArrows value={item.value} />
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Fund details
          </Subtitle>
          <Card>
            <List>
              {FundDetails.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.name}</LevelItem>
                    <LevelItem right className="has-text-weight-semibold">
                      {item.value}
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
      </Columns>
      <p
        className={`has-text-centered has-text-size-1 ${
          activeBreakpoints.below.sm ? 'px-4' : ''
        }`}>
        ASX Pricing Data provided by Thomson Reuters© Thomson Reuters Limited.{' '}
        <a href="/#">Click for restrictions.</a>
      </p>
    </React.Fragment>
  );
};

export default Breakpoints(MFundPerformance);

const PeerGroup = [
  { name: '1 Month', value: '4' },
  { name: '3 Months', value: '3' },
  { name: '6 Months', value: '3' },
  { name: '1 Year', value: '4' },
  { name: '3 Years', value: '3' },
  { name: '5 Years', value: '3' },
  { name: '10 Years', value: '0' }
];

const FundDetails = [
  { name: 'Fund Inception', value: '01-07-2010' },
  { name: 'Base Currency', value: 'AUD' },
  { name: 'Net Assets', value: '$4.09B' },
  { name: 'Regular Savings Plan', value: 'No' }
];

const Fees = [
  { name: 'Entry/Contribution', value: '0' },
  { name: 'Exit/Withdrawal', value: '0.2' },
  { name: 'Buy/Sell Spread', value: '0.4' },
  { name: 'Annual Mgmt Fee', value: '0.9' },
  { name: 'Annual ICR', value: '--' },
  { name: 'Admin Fee', value: '0' }
];

const TrailingReturns = [
  { period: '1 Month' },
  { period: '3 Months' },
  { period: '6 Months' },
  { period: 'YTD' },
  { period: '1 Year' },
  { period: '3 Years' },
  { period: '5 Years' },
  { period: '10 Years' }
];
const TrailingReturnsRow = ({ row }) => (
  <Row>
    <Cell>{row.period}</Cell>
    <Cell className="has-text-right">
      <PercentageView value={faker.finance.amount(0, 8)} />
    </Cell>
    <Cell className="has-text-right">
      <PercentageView value={faker.finance.amount(0, 8)} />
    </Cell>
  </Row>
);
const TrailingReturnsHeaders = [
  {
    name: (
      <span>
        Period as of <br />
        30-04-2019
      </span>
    )
  },
  {
    name: (
      <span>
        SCH11 <br />
        NAV Returns
      </span>
    ),
    align: 'right'
  },
  {
    name: (
      <span>
        Category <br />
        NAV Returns
      </span>
    ),
    align: 'right'
  }
];
