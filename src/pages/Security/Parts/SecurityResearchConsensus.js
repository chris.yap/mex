import React from 'react';
import {
  Alert,
  Breakpoints,
  Card,
  CardContent,
  CardImage,
  Column,
  Columns,
  Divider,
  Title
} from 'shaper-react';
import RealtimeQuoteMsg from '../../../components/RealtimeQuoteMsg';

const Consensus = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns mobile multiline gapless={activeBreakpoints.below.sm}>
        <Column>
          <img
            src="/images/thomson-reuters-corporation-logo.svg"
            width="250"
            alt=""
            className={activeBreakpoints.below.sm && 'mx-4'}
          />
          <Title
            size="5"
            className={`mt-2 ${activeBreakpoints.below.sm && 'mx-4'}`}>
            I/B/E/S Mean
          </Title>
          <Divider className="mt-2 mb-4" />
          <p className={activeBreakpoints.below.sm && 'mx-4'}>
            The I/B/E/S Mean is the average recommendation of all analysts
            covering the stock, as provided by Thompson Reuters I/B/E/S
            (Institutional Brokers Estimate System). The recommendations are
            presented on a 5 point standardized scale: strong buy, buy, hold,
            reduce and sell.
          </p>
          <Card>
            <CardImage
              aspectRatio={activeBreakpoints.above.sm ? 7 : 5}
              className={'has-bg-warning'}>
              <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                <Title
                  size="7"
                  className="has-text-white mb-0 has-text-weight-bold">
                  <span className="is-capitalized">13 analysts:</span>
                </Title>
                <Title
                  size="2"
                  className="has-text-white mb-0 mt-1 is-uppercase font-nabimpact">
                  <span
                    className="has-text-size-6 mr-4"
                    style={{ opacity: '.5' }}>
                    Sell
                  </span>
                  <span
                    className="has-text-size-6 mr-4"
                    style={{ opacity: '.5' }}>
                    Reduce
                  </span>
                  <span className="mr-4">Hold</span>
                  <span
                    className="has-text-size-6 mr-4"
                    style={{ opacity: '.5' }}>
                    Buy
                  </span>
                  <span
                    className="has-text-size-6 mr-4"
                    style={{ opacity: '.5' }}>
                    Strong buy
                  </span>
                </Title>
              </div>
            </CardImage>
            <CardContent>
              <p className="mb-0">
                Mean recommendation from all analysts covering the company on a
                standardized 5-point scale.
              </p>
            </CardContent>
          </Card>
        </Column>
        <Column
          tablet="5"
          desktop="4"
          widescreen="3"
          className="is-hidden-mobile">
          <Alert info isOpened={true} inverted>
            <p className="mb-2">
              <strong>
                Why is consensus research important to my investment?
              </strong>
            </p>
            <p className="mb-0">
              Consensus research considers collectively the recommendations of
              all analysts covering a security and presents a simple average of
              analyst recommendations. Consensus research gives investors a view
              of what analysts are recommending to their clients to give a
              better insight as to whether the market views a company as a
              buying or selling opportunity. Consensus research provides a cheap
              alternative to research subscriptions for mid to small cap
              companies.
            </p>
          </Alert>
        </Column>
      </Columns>
      <Title
        size="5"
        className={`mb-2 ${activeBreakpoints.below.sm && 'mx-4'}`}>
        Price target (12 month)
      </Title>
      <Divider className="mb-4" />
      <Columns multiline mobile>
        <Column>
          <Card className="pa-4">
            <img src="/images/consensus-target.png" alt="" width="100%" />
          </Card>
        </Column>
        <Column tablet="6" desktop="4" widescreen="3">
          <p className={`${activeBreakpoints.below.sm && 'mx-4'}`}>
            The mean estimates shows how the consensus has changed over the past
            30 days and 90 days for the upcoming two quarters (when available)
            and two years. The percentage change is calculated for the company
            as a means for comparison by measuring the difference between the
            current consensus estimate and that of 90 days ago.
          </p>
        </Column>
      </Columns>
      <RealtimeQuoteMsg
        local
        className={`mt-2 ${activeBreakpoints.below.sm && 'mx-4'}`}
      />
    </React.Fragment>
  );
};

export default Breakpoints(Consensus);
