import React from 'react';
import cn from 'classnames';
import { Box, Breakpoints, Card, Columns, Column, Level, LevelItem, List, ListItem } from 'shaper-react';

const SecurityChart = ({ activeBreakpoints, ...props }) => (
	<React.Fragment>
		<Columns mobile multiline className="mt-0">
			<Column
				mobile="12"
				tablet="12"
				desktop="8"
				widescreen="9"
				className={cn(activeBreakpoints.below.sm ? 'pt-0' : '')}
			>
				<Box className="">
					<img src="/images/chart.png" alt="" width="100%" />
				</Box>
			</Column>
			<Column mobile="12" tablet="12" desktop="4" widescreen="3">
				<Card>
					<List>
						{ChartData.map((chart, cI) => (
							<ListItem>
								<Level mobile style={{ width: '100%' }}>
									<LevelItem left>{chart.label}</LevelItem>
									<LevelItem right>
										<strong>{chart.value}</strong>
									</LevelItem>
								</Level>
							</ListItem>
						))}
					</List>
				</Card>
			</Column>
		</Columns>
		<p className={cn('has-text-size-2 mb-0', activeBreakpoints.below.sm ? 'px-4' : '')}>
			ASX Pricing Data provided by Thomson Reuters© Thomson Reuters Limited. <a href="/#">Click for restrictions</a>.
		</p>
	</React.Fragment>
);

export default Breakpoints(SecurityChart);

const ChartData = [
	{
		label: 'Open',
		value: '36.46',
	},
	{
		label: 'High',
		value: '36.46',
	},
	{
		label: 'Low',
		value: '36.35',
	},
	{
		label: 'Close',
		value: '36.08',
	},
	{
		label: 'Bid/Units',
		value: '36.38/1,671',
	},
	{
		label: 'Ask/Units',
		value: '36.39/339',
	},
	{
		label: 'VWAP',
		value: '36.44',
	},
	{
		label: 'Volume',
		value: '445,935',
	},
];
