import React from 'react';
import cn from 'classnames';
import { Breakpoints, Card, Column, Columns, Divider, Level, LevelItem, List, ListItem, Title } from 'shaper-react';

const SecurityDetails = ({ activeBreakpoints, ...props }) => (
	<Columns className="mt-0">
		<Column>
			<Title size="4" className={cn(activeBreakpoints.below.sm ? 'px-4' : '')}>
				Performance
			</Title>
			<Card>
				{Performance.map((perf, pI) => (
					<React.Fragment>
						<List key={pI}>
							<ListItem bordered>
								<Level mobile style={{ width: '100%' }}>
									<LevelItem left>{perf.label}</LevelItem>
									<LevelItem right>
										<strong>{perf.value}</strong>
									</LevelItem>
								</Level>
							</ListItem>
						</List>
						<Divider />
					</React.Fragment>
				))}
			</Card>
		</Column>
		<Column>
			<Title size="4" className={cn(activeBreakpoints.below.sm ? 'px-4' : '')}>
				Earnings
			</Title>
			<Card>
				{Earnings.map((earn, eI) => (
					<React.Fragment>
						<List key={eI}>
							<ListItem bordered>
								<Level mobile style={{ width: '100%' }}>
									<LevelItem left>{earn.label}</LevelItem>
									<LevelItem right>
										<strong>{earn.value}</strong>
									</LevelItem>
								</Level>
							</ListItem>
						</List>
						<Divider />
					</React.Fragment>
				))}
			</Card>
		</Column>
	</Columns>
);

export default Breakpoints(SecurityDetails);

const Performance = [
	{
		label: 'Open',
		value: '36.46',
	},
	{
		label: 'Previous close',
		value: '36.08',
	},
	{
		label: '52-week range',
		value: '28.15 - 36.46',
	},
	{
		label: 'Market capitalisation',
		value: '106.39B',
	},
	{
		label: 'Shares outstanding',
		value: '2.95B',
	},
];

const Earnings = [
	{
		label: 'Earnings per share',
		value: '2.496',
	},
	{
		label: 'Price/Earnings',
		value: '14.5',
	},
	{
		label: 'Forward EPS',
		value: '2.496',
	},
	{
		label: 'Forward P/E',
		value: '14.46',
	},
	{
		label: '1-yr EPS growth',
		value: '44.5%',
	},
];
