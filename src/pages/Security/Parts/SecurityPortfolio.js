import React from 'react';
import {
  Box,
  Breakpoints,
  Card,
  Column,
  Columns,
  Level,
  LevelItem,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Subtitle
} from 'shaper-react';
import PercentageView from '../../../components/PercentageView';

const SecurityPortfolio = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns multiline mobile>
        <Column mobile={12} tablet={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Portfolio overview
          </Subtitle>
          <Card>
            <List>
              {Overview.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.label}</LevelItem>
                    <LevelItem right>
                      <strong>{item.value}</strong>
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
        <Column mobile={12} tablet={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Market capitalisation
          </Subtitle>
          <Columns multiline mobile>
            <Column
              narrow={activeBreakpoints.above.xs}
              mobile={12}
              className={`${
                activeBreakpoints.below.sm ? 'has-text-centered' : ''
              }`}>
              <Box
                style={{ width: '200px', height: '200px', margin: '0 auto' }}>
                Donut chart
              </Box>
            </Column>
            <Column>
              <Card>
                <Table
                  hasStripes
                  headers={ChartHeaders}
                  data={Chart}
                  tableRow={ChartRow}
                  my={0}
                />
              </Card>
            </Column>
          </Columns>
        </Column>
      </Columns>
      <Columns multiline mobile>
        <Column mobile={12}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Top 10 holdings
          </Subtitle>
          <Card>
            <Table
              hasStripes
              headers={Top10Headers}
              data={Top10}
              tableRow={Top10Row}
              my={0}
            />
          </Card>
        </Column>
        <Column mobile={12}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Morningstar sector weightings
          </Subtitle>
          <Card>
            <Table
              hasStripes
              headers={MorningstarHeaders}
              data={Morningstar}
              tableRow={MorningstarRow}
              my={0}
            />
          </Card>
        </Column>
      </Columns>
      <Columns>
        <Column>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Regional holdings
          </Subtitle>
          <Card>
            <Table
              hasStripes
              headers={RegionalHeaders}
              data={Regional}
              tableRow={RegionalRow}
              my={0}
            />
          </Card>
        </Column>
        <Column />
      </Columns>
    </React.Fragment>
  );
};

export default Breakpoints(SecurityPortfolio);

const RegionalRow = ({ row }) => (
  <Row>
    <Cell>{row.region}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={row.allocation} />
    </Cell>
  </Row>
);

const RegionalHeaders = [
  { name: 'Region' },
  { name: 'Allocation', align: 'right' }
];

const Regional = [
  { region: 'South Pacific', allocation: '30' },
  { region: 'North America', allocation: '15' },
  { region: 'Asia', allocation: '30' },
  { region: 'Europe', allocation: '25' }
];

const ChartRow = ({ row }) => (
  <Row>
    <Cell>{row.holdings}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={row.allocation} />
    </Cell>
  </Row>
);

const ChartHeaders = [
  { name: 'Holdings detail' },
  { name: 'Allocation', align: 'right' }
];

const Chart = [
  { holdings: 'Giant Cap', allocation: '9.43' },
  { holdings: 'Large Cap', allocation: '32.21' },
  { holdings: 'Medium Cap', allocation: '35.14' },
  { holdings: 'Micro Cap', allocation: '5.92' },
  { holdings: 'Small Cap', allocation: '17.30' }
];

const MorningstarRow = ({ row }) => (
  <Row>
    <Cell>{row.sector}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={row.allocation} />
    </Cell>
  </Row>
);

const MorningstarHeaders = [
  { name: 'Sector' },
  { name: 'Allocation', align: 'right' }
];

const Morningstar = [
  { sector: 'Financial', allocation: '28.14' },
  { sector: 'Consumer Cyclical', allocation: '15.79' },
  { sector: 'Industrial', allocation: '14.45' },
  { sector: 'Basic Materials', allocation: '10.10' },
  { sector: 'Energy', allocation: '9.39' },
  { sector: 'Technology', allocation: '6.66' },
  { sector: 'Utilities', allocation: '4.34' },
  { sector: 'Consumer Defensive', allocation: '3.50' },
  { sector: 'Healthcare', allocation: '3.13' },
  { sector: 'Communication', allocation: '2.92' },
  { sector: 'Real Estate', allocation: '1.58' }
];

const Top10Row = ({ row }) => (
  <Row>
    <Cell>
      <a href="/#" className="has-text-weight-bold">
        {row.code}
      </a>
    </Cell>
    <Cell>{row.company}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={row.allocation} />
    </Cell>
    <Cell className="has-text-right">
      <PercentageView value={row.change} />
    </Cell>
  </Row>
);

const Top10 = [
  {
    code: 'MU',
    company: 'Micron Technology Inc',
    allocation: '0.46%',
    change: '-4.63'
  },
  {
    code: 'LUV',
    company: 'Southwest Airlines Co',
    allocation: '0.41%',
    change: '-0.39'
  },
  { code: 'F', company: 'Ford Motor Co', allocation: '0.41%', change: '0.63' },
  { code: 'T', company: 'AT&T Inc', allocation: '0.40%', change: '-1.58' },
  {
    code: 'WBA',
    company: 'Walgreens Boots Alliance Inc',
    allocation: '0.39%',
    change: '-0.03'
  },
  { code: 'EXC', company: 'Exelon Corp', allocation: '0.39%', change: '0.12' },
  { code: 'FDX', company: 'FedEx Corp', allocation: '0.39%', change: '-18.95' },
  {
    code: 'DAL',
    company: 'Delta Air Lines Inc',
    allocation: '0.39%',
    change: '-1.43'
  },
  {
    code: 'GM',
    company: 'General Motors Co',
    allocation: '0.38%',
    change: '-3.15'
  },
  {
    code: 'CCL',
    company: 'Carnival Corp',
    allocation: '0.36%',
    change: '-0.79'
  }
];

const Top10Headers = [
  { name: 'Code' },
  { name: 'Company' },
  { name: 'Allocation', align: 'right' },
  { name: '1M change', align: 'right' }
];

const Overview = [
  { label: 'Total number of holdings', value: '1,180' },
  { label: 'Top 10 allocation', value: '3.97%' },
  { label: 'Category average', value: '22.02' },
  { label: 'Distinct portfolio', value: 'Yes' },
  { label: 'Portfolio turnover', value: '-' }
];
