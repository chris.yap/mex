import React from 'react';
import faker from 'faker';
import {
  Box,
  Breakpoints,
  Card,
  CardContent,
  Columns,
  Column,
  Level,
  LevelItem,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title,
  Subtitle
} from 'shaper-react';
import PercentageView from '../../../components/PercentageView';

const MFundPortfolio = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns mobile multiline>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Portfolio overview
          </Subtitle>
          <Card>
            <List>
              {Overview.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.name}</LevelItem>
                    <LevelItem right className="has-text-weight-semibold">
                      {item.value}
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Market capitalisation
          </Subtitle>
          <Card>
            <p className="has-text-centered py-10">Donut goes here</p>
            <Table
              hasStripes
              headers={MarketHeaders}
              data={Market}
              tableRow={MarketRow}
              mb={0}
            />
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Morningstar style box
          </Subtitle>
          <Card className="pt-4">
            <CardContent>
              {Morningstar.map((item, i) => (
                <Columns mobile key={i}>
                  <Column size="2">
                    <Title size="6" className="mb-0">
                      {item.name}
                    </Title>
                  </Column>
                  <Column>
                    <Columns mobile gapless multiline>
                      <Column
                        size={3}
                        className="has-text-weight-semibold is-flex is-align-items-center is-justify-content-flex-end">
                        <span className="pr-2">Large</span>
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '1-1' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '1-2' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '1-3' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column
                        size={3}
                        className="has-text-weight-semibold is-flex is-align-items-center is-justify-content-flex-end">
                        <span className="pr-2">Medium</span>
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '2-1' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '2-2' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '2-3' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column
                        size={3}
                        className="has-text-weight-semibold is-flex is-align-items-center is-justify-content-flex-end">
                        <span className="pr-2">Small</span>
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '3-1' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '3-2' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3}>
                        <Box
                          className={`py-4 has-bg-secondary ${
                            item.value === '3-3' ? '' : 'lighten-5'
                          }`}
                        />
                      </Column>
                      <Column size={3} />
                      <Column
                        size={3}
                        className="has-text-centered has-text-weight-semibold pr-2">
                        Value
                      </Column>
                      <Column
                        size={3}
                        className="has-text-centered has-text-weight-semibold">
                        Blend
                      </Column>
                      <Column
                        size={3}
                        className="has-text-centered has-text-weight-semibold">
                        Growth
                      </Column>
                    </Columns>
                  </Column>
                </Columns>
              ))}
            </CardContent>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Top 10 holdings
          </Subtitle>
          <Card>
            <Table
              hasStripes
              headers={Top10Headers}
              data={Top10}
              tableRow={Top10Row}
              my={0}
            />
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Morningstar sector weightings
          </Subtitle>
          <Card>
            <List>
              <ListItem>
                <Level mobile className="is-flex-1">
                  <LevelItem left className="has-text-weight-semibold">
                    Sector
                  </LevelItem>
                  <LevelItem right className="has-text-weight-semibold">
                    Allocation
                  </LevelItem>
                </Level>
              </ListItem>
              {Weightings.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.sector}</LevelItem>
                    <LevelItem right>
                      <PercentageView
                        noArrows
                        value={faker.finance.amount(0, 30)}
                      />
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
        <Column mobile={12} desktop={6}>
          <Subtitle
            size="4"
            className={`mb-2 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
            Regional holdings
          </Subtitle>
          <Card>
            <List>
              <ListItem>
                <Level mobile className="is-flex-1">
                  <LevelItem left className="has-text-weight-semibold">
                    Region
                  </LevelItem>
                  <LevelItem right className="has-text-weight-semibold">
                    Allocation
                  </LevelItem>
                </Level>
              </ListItem>
              {Regional.map((item, i) => (
                <ListItem key={i}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.region}</LevelItem>
                    <LevelItem right>
                      <PercentageView noArrows value={item.allocation} />
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
        </Column>
      </Columns>
    </React.Fragment>
  );
};

export default Breakpoints(MFundPortfolio);

const Regional = [
  { region: 'South Pacific', allocation: '85.97' },
  { region: 'North America', allocation: '2.7' },
  { region: 'Asia', allocation: '2.9' },
  { region: 'Europe', allocation: '1.99' }
];

const Weightings = [
  { sector: 'Financials' },
  { sector: 'Basic materials' },
  { sector: 'Industrial' },
  { sector: 'Consumer cyclical' },
  { sector: 'Real estate' },
  { sector: 'Consumer defensive' },
  { sector: 'Energy' },
  { sector: 'Technology' },
  { sector: 'Communication' },
  { sector: 'Healthcare' },
  { sector: 'Utilities' }
];

const Top10Headers = [
  { name: 'Code' },
  { name: 'Company' },
  { name: 'Allocation', align: 'right' },
  { name: '1M change', align: 'right' }
];

const Top10Row = ({ row }) => (
  <Row>
    <Cell>
      <a href="/#" className="has-text-weight-semibold">
        {row.code}
      </a>
    </Cell>
    <Cell>{row.co}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={faker.finance.amount(3, 15)} />
    </Cell>
    <Cell className="has-text-right">{row.change}</Cell>
  </Row>
);

const Top10 = [
  { code: '-', co: 'Schroder Australian Equity Professional', change: '--' },
  { code: '-', co: 'US 10 Year Note (CBT) Mar19', change: '--' },
  { code: '-', co: 'Schroder Sterling Corporate Bond Z Inc', change: '--' },
  { code: '-', co: 'Schroder Global Blend Ex Tobacco Fund', change: '--' },
  { code: '-', co: 'Schroders Global Bond Fund', change: '--' },
  { code: '-', co: 'Schroder Fixed Income Institutional', change: '--' },
  { code: '-', co: 'BT PPSP-Schroder Hybrid Securities', change: '--' },
  { code: '-', co: 'Schroder Enhanced Cash Management Inst', change: '--' },
  { code: '-', co: 'Schroder ISF EM Dbt Abs Rt I Acc USD', change: '--' },
  {
    code: '-',
    co: 'Australian 10 Year Treasury Bond Future Mar19',
    change: '--'
  }
];

const Morningstar = [
  { name: 'Bond', value: '2-2' },
  { name: 'Equity', value: '1-1' }
];

const MarketHeaders = [
  { name: 'Holdings detail' },
  { name: 'Allocation', align: 'right' }
];

const MarketRow = ({ row }) => (
  <Row>
    <Cell>{row.holdings}</Cell>
    <Cell className="has-text-right">
      <PercentageView noArrows value={row.allocation} />
    </Cell>
  </Row>
);

const Market = [
  { holdings: 'Giant cap', allocation: '41.35' },
  { holdings: 'Large cap', allocation: '26.28' },
  { holdings: 'Medium cap', allocation: '25.19' },
  { holdings: 'Micro cap', allocation: '0.99' },
  { holdings: 'Small cap', allocation: '6.19' }
];

const Overview = [
  { name: 'Total number of holdings', value: '280' },
  { name: 'Top 10 allocation', value: '82.69%' },
  { name: 'Category average', value: '31.21' },
  { name: 'Distinct portfolio', value: 'No' },
  { name: 'Portfolio turnover', value: '--' }
];
