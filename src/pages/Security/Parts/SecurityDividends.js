import React from 'react';
import {
  Breakpoints,
  Card,
  CardContent,
  CardHeader,
  Column,
  Columns,
  Label,
  Radio,
  RadioGroup,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

class SecurityDividends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: 'list'
    };
    this.changeFilter = this.changeFilter.bind(this);
  }
  changeFilter = value => {
    this.setState({
      filter: value
    });
  };
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <React.Fragment>
        {/* // <Card>
			// 	<CardContent className={cn(activeBreakpoints.below.sm ? 'px-0 pt-4' : 'py-4')}> */}
        <RadioGroup
          buttons
          rounded
          selectedValue={this.state.filter}
          onChange={this.changeFilter}
          className={
            activeBreakpoints.below.sm ? '' : 'is-justify-content-flex-end'
          }>
          <Radio button value="list" label="List" />
          <Radio button value="table" label="Table" />
        </RadioGroup>
        {this.state.filter === 'list' ? (
          <React.Fragment>
            {Data.map((d, i) => (
              <Card key={i} className="mb-1">
                <CardHeader>
                  <div>
                    <Label className="nab-label mb-0">Dividend rate</Label>
                    <p
                      className="has-text-size3 mt-0 has-text-secondary mb-0"
                      style={{ lineHeight: '1' }}>
                      <strong>{d.rate}</strong>
                    </p>
                  </div>
                </CardHeader>
                <CardContent>
                  <Columns gapless mobile className="mb-2">
                    <Column>
                      <Label className="nab-label mb-0">Ex-date</Label>
                      <p className="mb-0">
                        <strong>{d.edate}</strong>
                      </p>
                    </Column>
                    <Column>
                      <Label className="nab-label mb-0">Pay date</Label>
                      <p className="mb-0">
                        <strong>{d.pdate}</strong>
                      </p>
                    </Column>
                  </Columns>
                  <Columns gapless mobile className="mb-2">
                    <Column>
                      <Label className="nab-label mb-0">Type</Label>
                      <p className="mb-0">
                        <strong>{d.type}</strong>
                      </p>
                    </Column>
                    <Column>
                      <Label className="nab-label mb-0">Franking</Label>
                      <p className="mb-0">
                        <strong>{d.franking}</strong>
                      </p>
                    </Column>
                  </Columns>
                  <Columns gapless mobile className="mb-0">
                    <Column>
                      <Label className="nab-label mb-0">Comments</Label>
                      <p className="mb-0 is-capitalized">
                        <strong>{d.comment}</strong>
                      </p>
                    </Column>
                  </Columns>
                </CardContent>
              </Card>
            ))}
          </React.Fragment>
        ) : (
          <Table hasStripes headers={Headers} data={Data} tableRow={TableRow} />
        )}
        {/* </CardContent>
			</Card> */}
      </React.Fragment>
    );
  }
}

export default Breakpoints(SecurityDividends);

const Headers = [
  { name: 'Dividend rate' },
  { name: 'Ex-date' },
  { name: 'Pay date' },
  { name: 'Type' },
  { name: 'Franking' },
  { name: 'Comment' }
];

const TableRow = ({ row }) => {
  return (
    <Row>
      <Cell>{row.rate}</Cell>
      <Cell>{row.edate}</Cell>
      <Cell>{row.pdate}</Cell>
      <Cell>{row.type}</Cell>
      <Cell>{row.franking}</Cell>
      <Cell className="is-capitalized">{row.comment}</Cell>
    </Row>
  );
};

const Data = [
  {
    rate: '0.2405',
    edate: '04.11.2015',
    pdate: '03.07.2014',
    type: 'Final',
    franking: '100%',
    comment: 'anim labore sit Lorem laborum'
  },
  {
    rate: '0.7562',
    edate: '26.04.2018',
    pdate: '17.03.2016',
    type: 'Interim',
    franking: '100%',
    comment: 'dolor excepteur sint non tempor'
  },
  {
    rate: '0.3917',
    edate: '24.06.2018',
    pdate: '10.03.2018',
    type: 'Final',
    franking: '100%',
    comment: 'ea nulla fugiat eiusmod pariatur'
  },
  {
    rate: '0.2414',
    edate: '07.05.2017',
    pdate: '23.08.2016',
    type: 'Interim',
    franking: '100%',
    comment: 'nostrud Lorem non irure cupidatat'
  },
  {
    rate: '0.8011',
    edate: '23.02.2014',
    pdate: '01.01.2016',
    type: 'Final',
    franking: '100%',
    comment: 'do est consequat laborum consequat'
  }
];
