import React from 'react';
import cn from 'classnames';
// import ClampLines from 'react-clamp-lines';
import {
	Breakpoints,
	Card,
	CardContent,
	CardHeader,
	Checkbox,
	CheckboxGroup,
	Column,
	Columns,
	Icon,
	Pagination,
	Tag,
	Title,
} from 'shaper-react';
import { Announcements } from '../../../constants/announcements';

class SecurityNewsAnnouncements extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			filteredArticles: [],
			sensitive: ['sensitive'],
		};
		this.filteringArticles = this.filteringArticles.bind(this);
	}
	componentDidMount = () => {
		this.filteringArticles();
	};
	filteringArticles = () => {
		const { sensitive } = this.state;
		let filteredArticles = [];
		this.setState({ filteredArticles: [] });
		for (let doc of Announcements) {
			if (sensitive.length > 0) {
				if (doc.sensitive === true) {
					filteredArticles.push(doc);
				}
			} else {
				filteredArticles.push(doc);
			}
		}
		this.setState({ filteredArticles: filteredArticles });
	};
	sensitiveChange = value => {
		this.setState({
			sensitive: value,
		});
		setTimeout(() => {
			this.filteringArticles();
		}, 100);
	};
	getTags = info => {
		let tags = info.split(',');
		return tags.map(tag => (
			<Tag small className="mt-2 mr-1">
				{tag}
			</Tag>
		));
	};
	render() {
		const { filteredArticles, sensitive } = this.state;
		const { activeBreakpoints } = this.props;
		return (
			<React.Fragment>
				<CheckboxGroup
					name="announcements"
					value={sensitive}
					onChange={this.sensitiveChange}
					className={cn(
						activeBreakpoints.below.sm ? 'has-text-centered' : 'ml-4 is-justify-content-flex-start is-inline-flex'
					)}
				>
					<Checkbox value="sensitive" label="Price sensitive only" />
				</CheckboxGroup>

				<Columns mobile multiline className={cn(activeBreakpoints.below.sm ? 'mt-4' : '')}>
					{filteredArticles.map((doc, index) => {
						return (
							<Column key={index} mobile="12" tablet="12" desktop="12" widescreen="12" className={'py-1'}>
								<Card
									href={'#'}
									target={'_blank'}
									className={cn('has-link has-pointer', activeBreakpoints.below.sm ? 'mb-1' : '')}
								>
									<CardHeader>
										<Title size="5" className={`has-text-blue-grey mb-0`}>
											{/* <ClampLines text={doc.title} buttons={false} lines={doc.sensitive ? 2 : 3} /> */}
											{doc.title} <br />
											{doc.sensitive && (
												<Tag small secondary className="mt-2 mr-1">
													Price sensitive
												</Tag>
											)}
											{this.getTags(doc.type)}
										</Title>
									</CardHeader>
									<CardContent>
										<p className="has-text-size-3 mb-0 has-text-black text--lighten-2">
											{doc.source && doc.type === 'news' && <strong>{doc.source}</strong>}
											{doc.type !== 'news' && (
												<React.Fragment>
													<Icon small style={{ top: '2px' }}>
														pdf
													</Icon>{' '}
													PDF
												</React.Fragment>
											)}{' '}
											| {doc.date}
										</p>
									</CardContent>
								</Card>
							</Column>
						);
					})}
				</Columns>
				<Pagination current={1} total={10} className={activeBreakpoints.below.sm ? 'mx-4' : ''} />
			</React.Fragment>
		);
	}
}

export default Breakpoints(SecurityNewsAnnouncements);
