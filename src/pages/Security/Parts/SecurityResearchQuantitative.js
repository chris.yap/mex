import React from 'react';
import cn from 'classnames';
import ClampLines from 'react-clamp-lines';
import {
  Alert,
  Breakpoints,
  Card,
  CardContent,
  CardHeader,
  CardImage,
  Column,
  Columns,
  Divider,
  Icon,
  Level,
  LevelItem,
  Title
} from 'shaper-react';

const Quantitative = ({ activeBreakpoints, ...props }) => {
  return (
    <React.Fragment>
      <Columns mobile multiline gapless={activeBreakpoints.below.sm}>
        <Column>
          <img
            src="/images/thomson-reuters-corporation-logo.svg"
            width="250"
            alt="Thomson Reuters"
            className={activeBreakpoints.below.sm && 'mx-4'}
          />
          {/* <Title size="5" className="mt-2">
						I/B/E/S Mean
					</Title> */}
          <Divider className="mt-2 mb-4" />

          <Columns mobile multiline gapless={activeBreakpoints.below.sm}>
            <Column mobile="12" tablet="12" desktop="6">
              <Card>
                <CardImage
                  aspectRatio={
                    activeBreakpoints.below.sm
                      ? 6
                      : activeBreakpoints.below.md
                      ? 5
                      : 4
                  }
                  className={'has-bg-success'}>
                  <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                    <Title
                      size="7"
                      className="has-text-white mb-0 has-text-weight-bold">
                      <span className="is-capitalized">Average score: 9</span>
                    </Title>
                    <Title
                      size="2"
                      className="has-text-white mb-0 mt-1 is-uppercase font-nabimpact">
                      <span className="mr-4">Positive</span>
                    </Title>
                  </div>
                </CardImage>
                <CardHeader>
                  <div>
                    <Title
                      size="5"
                      className={cn(
                        'has-text-blue-grey pr-2 mb-1 is-capitalized pos-r has-top-2'
                      )}>
                      <ClampLines
                        text={'Previous recommendation changes'}
                        buttons={false}
                      />
                    </Title>
                    <p className="mb-0">
                      <strong>Downgrade</strong> from <strong>10</strong> with
                      an <strong>Average Score of 9</strong> on 13.04.19
                    </p>
                  </div>
                </CardHeader>
                <CardContent>
                  <Level mobile>
                    <LevelItem left>
                      <p className="has-text-size-3 mb-0 has-text-black text--lighten-2 is-flex is-align-items-center">
                        <img
                          src="/images/rtr_ahz_rgb_pos.png"
                          width="70"
                          alt=""
                          className="pos-r has-top mr-1"
                        />{' '}
                        | As of 03.04.19
                      </p>
                    </LevelItem>
                    <LevelItem right>
                      <p className="has-text-size-3 has-text-right mb-0 has-text-black text--lighten-2">
                        Research report
                        <Icon small style={{ top: '2px' }}>
                          pdf
                        </Icon>
                        PDF
                      </p>
                    </LevelItem>
                  </Level>
                </CardContent>
              </Card>
            </Column>
            <Column mobile="12" tablet="12" desktop="6">
              <Card>
                <CardImage
                  aspectRatio={
                    activeBreakpoints.below.sm
                      ? 6
                      : activeBreakpoints.below.md
                      ? 5
                      : 4
                  }
                  className={'has-bg-secondary'}>
                  <div className="is-flex-1 px-6 pos-r has-top8 has-text-white is-align-self-flex-end">
                    <Title
                      size="7"
                      className="has-text-white mb-0 has-text-weight-bold">
                      <span className="is-capitalized">Rating:</span>
                    </Title>
                    <Title
                      size="2"
                      className="has-text-white mb-0 mt-1 is-uppercase font-nabimpact">
                      <span className="mr-4">Hold</span>
                    </Title>
                  </div>
                </CardImage>
                <CardHeader>
                  <div>
                    <Title
                      size="5"
                      className={cn(
                        'has-text-blue-grey pr-2 mb-1 is-capitalized pos-r has-top-2'
                      )}>
                      <ClampLines
                        text={'I/B/E/S Mean - Analysts recommendation'}
                        buttons={false}
                      />
                    </Title>
                    <p className="mb-0">
                      Mean recommendation from all analysts covering the company
                      on a standardized 5-point scale.{' '}
                      <strong>21 analysts</strong>
                    </p>
                  </div>
                </CardHeader>
                <CardContent>
                  <Level mobile>
                    <LevelItem left>
                      <p className="has-text-size-3 mb-0 has-text-black text--lighten-2 is-flex is-align-items-center">
                        <img
                          src="/images/rtr_ahz_rgb_pos.png"
                          width="70"
                          alt=""
                          className="pos-r has-top mr-1"
                        />{' '}
                        |{' '}
                        {/* <strong className="is-uppercase">Trading Central</strong> */}
                        As of 03.04.19
                      </p>
                    </LevelItem>
                    <LevelItem right>
                      <p className="has-text-size-3 has-text-right mb-0 has-text-black text--lighten-2">
                        Research report
                        <Icon small style={{ top: '2px' }}>
                          pdf
                        </Icon>
                        PDF
                      </p>
                    </LevelItem>
                  </Level>
                </CardContent>
              </Card>
            </Column>
          </Columns>
        </Column>
        <Column
          tablet="5"
          desktop="4"
          widescreen="3"
          className="is-hidden-mobile">
          <Alert info isOpened={true} inverted>
            <p className="mb-2">
              <strong>
                Why is quantitative research important to my investment?
              </strong>
            </p>
            <p className="mb-0">
              Quantitative analysis provide insight into the valuation or
              historic performance of a specific security or market. But
              quantitative analysis is not often used as a standalone method for
              evaluating long-term investments. Instead, quantitative analysis
              is used in conjunction with fundamental and technical analysis to
              determine the potential advantages and risks of investment
              decisions.
            </p>
          </Alert>
        </Column>
      </Columns>

      {/* <RealtimeQuoteMsg local className="mt-4" /> */}
    </React.Fragment>
  );
};

export default Breakpoints(Quantitative);
