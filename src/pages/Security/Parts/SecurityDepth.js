import React from 'react';
import faker from 'faker';
import {
  Breakpoints,
  Button,
  Card,
  CardContent,
  Column,
  Columns,
  Div,
  Divider,
  Icon,
  Label,
  Progress,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Tab,
  Tabs,
  Title
} from 'shaper-react';

const SecurityDepth = ({ activeBreakpoints, ...props }) => {
  React.useEffect(() => {
    GetData();
  });
  return (
    <React.Fragment>
      <Tabs
        toggle
        rounded
        selectedTab={0}
        centered={activeBreakpoints.below.sm ? true : false}>
        <Tab label="Market depth">
          <Columns mobile>
            <Column>
              <Card className="mb-4">
                <Columns gapless={true} mobile className="mb-0">
                  <Column>
                    <Table
                      hasNoScroll
                      stickyHeader={true}
                      headers={DepthBuyersHeaders}
                      headerProps={{
                        success: true,
                        top: activeBreakpoints.below.sm ? '50px' : '128px'
                      }}
                      data={Buyers}
                      tableRow={(props, key) => (
                        <DepthBuyersRow key={key} {...props} />
                      )}
                      my={0}
                      className={`${activeBreakpoints.below.sm &&
                        'has-text-size-1'}`}
                    />
                  </Column>
                  <Column narrow>
                    <Divider vertical />
                  </Column>
                  <Column>
                    <Table
                      hasNoScroll
                      stickyHeaderCount
                      headers={DepthSellersHeaders}
                      headerProps={{
                        danger: true,
                        top: activeBreakpoints.below.sm ? '50px' : '128px'
                      }}
                      data={Sellers}
                      tableRow={(props, index) => (
                        <DepthSellersRow key={index} {...props} />
                      )}
                      my={0}
                      className={`${activeBreakpoints.below.sm &&
                        'has-text-size-1'}`}
                    />
                  </Column>
                </Columns>
              </Card>

              <p
                className={`has-text-size-2 ${activeBreakpoints.below.sm &&
                  'px-4'}`}>
                ASX Pricing Data provided by Thomson Reuters© Thomson Reuters
                Limited. <a href="/#">Click for restrictions</a>.
              </p>
              <p className="has-text-centered has-text-size-1 has-text-blue-grey">
                End of results
              </p>
            </Column>
            <Column desktop="4" className="is-hidden-touch">
              <Card>
                <CardContent className="px-4">
                  <Columns mobile>
                    <Column className="has-text-left">
                      <Title size="4" className="mb-2 has-text-success">
                        Buyers
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} buyers
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-2">
                        1,413
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} shares
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-0">
                        {faker.finance.amount(200, 5000, 0)}
                      </Title>
                    </Column>
                    <Column narrow>
                      <Divider vertical />
                    </Column>
                    <Column className="has-text-right">
                      <Title size="4" className="mb-2 has-text-danger">
                        Sellers
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} sellers
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-2">
                        1,054
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} shares
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-0">
                        {faker.finance.amount(200, 5000, 0)}
                      </Title>
                    </Column>
                  </Columns>
                </CardContent>
              </Card>
            </Column>
          </Columns>

          <p className="has-text-centered">
            <Button flat>
              <Icon>arrow-up</Icon>
              <span>Back to top</span>
            </Button>
          </p>
        </Tab>
        <Tab label="Market detail">
          <Columns mobile>
            <Column>
              <Card className="mb-4">
                <Columns gapless mobile className="mb-0">
                  <Column>
                    <Table
                      hasStripes
                      hasNoScroll
                      stickyHeader
                      headers={DetailBuyersHeaders}
                      headerProps={{
                        success: true,
                        top: activeBreakpoints.below.sm ? '50px' : '128px'
                      }}
                      data={DetailBuyers}
                      tableRow={DetailBuyersRow}
                      my={0}
                      className={`my-0 ${activeBreakpoints.below.sm &&
                        'has-text-size-1'}`}
                    />
                  </Column>
                  <Column narrow>
                    <Divider vertical />
                  </Column>
                  <Column>
                    <Table
                      hasStripes
                      hasNoScroll
                      stickyHeader
                      headers={DetailSellersHeaders}
                      headerProps={{
                        danger: true,
                        top: activeBreakpoints.below.sm ? '50px' : '128px'
                      }}
                      data={DetailSellers}
                      tableRow={DetailSellersRow}
                      my={0}
                      className={`${activeBreakpoints.below.sm &&
                        'has-text-size-1'}`}
                    />
                  </Column>
                </Columns>
              </Card>

              <p
                className={`has-text-size-2 ${activeBreakpoints.below.sm &&
                  'px-4'}`}>
                ASX Pricing Data provided by Thomson Reuters© Thomson Reuters
                Limited. <a href="/#">Click for restrictions</a>.
              </p>
              <p className="has-text-centered has-text-size-1 has-text-blue-grey">
                End of results
              </p>
            </Column>
            <Column desktop="4" className="is-hidden-touch">
              <Card>
                <CardContent className="px-4">
                  <Columns gapless mobile>
                    <Column className="has-text-left">
                      <Title size="4" className="mb-2 has-text-success">
                        Buyers
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} buyers
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-2">
                        1,413
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} shares
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-0">
                        1,107,393
                      </Title>
                    </Column>
                    <Column narrow>
                      <Divider vertical />
                    </Column>
                    <Column className="has-text-right">
                      <Title size="4" className="mb-2 has-text-danger">
                        Sellers
                      </Title>
                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} sellers
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-2">
                        1,054
                      </Title>

                      <Label className="nab-label mb-0">
                        {activeBreakpoints.below.sm ? `` : `Total`} shares
                      </Label>
                      <Title
                        size={activeBreakpoints.below.sm ? '6' : '5'}
                        className="mb-0">
                        1,703,453
                      </Title>
                    </Column>
                  </Columns>
                </CardContent>
              </Card>
            </Column>
          </Columns>

          <p className="has-text-centered">
            <Button flat>
              <Icon>arrow-up</Icon>
              <span>Back to top</span>
            </Button>
          </p>
        </Tab>
      </Tabs>
    </React.Fragment>
  );
};

export default Breakpoints(SecurityDepth);

const Buyers = [];
const Sellers = [];
const DetailBuyers = [];
const DetailSellers = [];

const GetData = () => {
  for (let i = 0; i < 20; i++) {
    Buyers.push(i);
    Sellers.push(i);
    DetailBuyers.push(i);
    DetailSellers.push(i);
  }
};

const DepthBuyersRow = () => {
  let value = parseInt(faker.finance.amount(200, 5000, 0), 10);
  let formatedValue = value.toLocaleString('en-AU');
  return (
    <React.Fragment>
      <Row>
        <Cell>{faker.finance.amount(2, 50, 0)}</Cell>
        <Cell px={0} centered>
          {formatedValue}
        </Cell>
        <Cell right color="success" fontWeight="bold">
          {faker.finance.amount(37, 40, 2)}
        </Cell>
      </Row>
      <Row>
        <Div p={0} position="absolute" width="100%">
          <Progress success right square height="2" value={value} max={15000} />
        </Div>
      </Row>
    </React.Fragment>
  );
};
const DepthSellersRow = () => {
  let value = parseInt(faker.finance.amount(200, 5000, 0), 10);
  let formatedValue = value.toLocaleString('en-AU');
  return (
    <React.Fragment>
      <Row>
        <Cell color="danger" fontWeight="bold">
          {faker.finance.amount(37, 40, 2)}
        </Cell>
        <Cell px={0} centered>
          {formatedValue}
        </Cell>
        <Cell right>{faker.finance.amount(2, 50, 0)}</Cell>
      </Row>
      <Row>
        <Div p={0} position="absolute" width="100%">
          <Progress danger square height="2" value={value} max={15000} />
        </Div>
      </Row>
    </React.Fragment>
  );
};
const DepthBuyersHeaders = [
  { name: 'Buyers' },
  { name: 'Qty', align: 'centered' },
  { name: 'Bid', align: 'right' }
];
const DepthSellersHeaders = [
  { name: 'Ask' },
  { name: 'Qty', align: 'centered' },
  { name: 'Sellers', align: 'right' }
];

const DetailBuyersRow = ({ row }) => (
  <Row>
    <Cell>
      {parseInt(faker.finance.amount(1, 10000, 0), 10).toLocaleString('en-AU')}
    </Cell>
    <Cell right color="success" fontWeight="bold">
      {faker.finance.amount(39.75, 39.95, 2)}
    </Cell>
  </Row>
);
const DetailSellersRow = ({ row }) => (
  <Row>
    <Cell color="danger" fontWeight="bold">
      {faker.finance.amount(39.75, 39.95, 2)}
    </Cell>
    <Cell right>
      {parseInt(faker.finance.amount(1, 10000, 0), 10).toLocaleString('en-AU')}
    </Cell>
  </Row>
);

const DetailBuyersHeaders = [{ name: 'Qty' }, { name: 'Bid', align: 'right' }];
const DetailSellersHeaders = [{ name: 'Ask' }, { name: 'Qty', align: 'right' }];
