import React from 'react';
import cn from 'classnames';
import { getState } from '../../context/StateProvider';
import LayoutDefault from '../../layout/LayoutDefault';
import {
  Breakpoints,
  Button,
  Container,
  Column,
  Columns,
  Divider,
  Hero,
  Icon,
  Label,
  Section,
  Subtitle,
  Tab,
  Tabs,
  Tag,
  Title
} from 'shaper-react';
import PercentageView from '../../components/PercentageView';
import MoneyView from '../../components/MoneyView';
import SecurityDepth from './Parts/SecurityDepth';
import SecurityChart from './Parts/SecurityChart';
import SecurityDetails from './Parts/SecurityDetails';
import SecurityNews from './Parts/SecurityNews';
import SecurityTrades from './Parts/SecurityTrades';
import SecurityAnnouncements from './Parts/SecurityAnnouncements';
import SecurityResearch from './Parts/SecurityResearch';
import SecurityDividends from './Parts/SecurityDividends';
import SecurityPortfolio from './Parts/SecurityPortfolio';
import MFundPerformance from './Parts/mFundPerformance';
import MFundPortfolio from './Parts/mFundPortfolio';
import RealtimeQuoteMsg from '../../components/RealtimeQuoteMsg';

const BHP = ({ activeBreakpoints, ...props }) => {
  const [{ searchBar }, dispatch] = getState();
  const { selectedStock } = searchBar;
  return (
    <LayoutDefault
      breadcrumb={[
        { name: 'Home', link: '/' },
        {
          name: selectedStock.code + ' | ' + selectedStock.label,
          link: '/security/detail',
          active: true
        }
      ]}>
      <Hero bg="black" pt={2} pb={4}>
        <Container>
          <Columns
            mobile
            multiline
            className={cn(
              'mb-0',
              activeBreakpoints.below.sm ? 'has-text-centered' : ''
            )}>
            <Column mobile="12">
              <Title
                className={cn(
                  'font-nabimpact has-text-white',
                  activeBreakpoints.below.sm ? 'has-text-ellipsis' : ''
                )}>
                {selectedStock && selectedStock.label}
                <span
                  className={`is-hidden-touch ${
                    activeBreakpoints.below.md ? 'has-text-right' : ''
                  }`}>
                  <Button secondary className="ml-4 pos-r has-top-6">
                    <Icon>alert</Icon>
                    <span>Alert</span>
                  </Button>
                  <Button secondary className="mx-0 pos-r has-top-6">
                    <Icon>watchlist</Icon>
                    <span>Add to watchlist</span>
                  </Button>
                  <Button
                    success
                    className="mr-0 pos-r has-top-6"
                    onClick={() => {
                      dispatch({
                        type: 'orderPadSelectOption',
                        payload: selectedStock
                      });
                      dispatch({ type: 'toggleOrderPad' });
                    }}>
                    Buy
                  </Button>
                  <Button
                    danger
                    className="mr-0 pos-r has-top-6"
                    onClick={() => {
                      dispatch({
                        type: 'orderPadSelectOption',
                        payload: selectedStock
                      });
                      dispatch({ type: 'toggleOrderPad' });
                    }}>
                    Sell
                  </Button>
                </span>
              </Title>
              <Subtitle
                size={activeBreakpoints.below.sm ? '7' : '6'}
                className="mb-0 has-text-white">
                <span className="is-uppercase">
                  {selectedStock && selectedStock.code + ' | '}
                  {selectedStock && selectedStock.exchange}
                </span>
                {selectedStock && selectedStock.industry ? (
                  <React.Fragment>
                    {' '}
                    | {selectedStock && selectedStock.industry}
                  </React.Fragment>
                ) : (
                  ''
                )}
                {selectedStock && !selectedStock.type && (
                  <React.Fragment>
                    {' '}
                    | Large Cap
                    <Tag
                      small
                      primary
                      className="ml-2 has-text-weight-semibold">
                      XD
                    </Tag>
                  </React.Fragment>
                )}
              </Subtitle>
            </Column>
            <Column
              mobile="12"
              narrow={activeBreakpoints.above.xs ? true : false}
              className={cn(
                'is-align-self-center',
                activeBreakpoints.below.sm ? 'pt-0' : 'is-hidden-desktop',
                activeBreakpoints.when.sm ? 'has-text-right' : ''
              )}>
              <Button
                secondary
                className="ml-0 mb-0"
                style={{ height: '36px', width: '36px' }}>
                <Icon>alert</Icon>
                {/* <span>Alert</span> */}
              </Button>
              <Button secondary className="mx-0 mb-0">
                <Icon>watchlist</Icon>
                <span>Add to watchlist</span>
              </Button>
              {activeBreakpoints.when.sm && <br />}
              <Button
                primary
                className="mr-0"
                onClick={() => dispatch({ type: 'toggleOrderPad' })}>
                Trade{' '}
                {activeBreakpoints.below.sm
                  ? ``
                  : selectedStock && (
                      <span className="is-uppercase ml-1">
                        {' '}
                        {selectedStock.value}
                      </span>
                    )}
              </Button>
            </Column>
          </Columns>
          <Divider dark className="mb-4" />
          <Columns mobile multiline className="mb-0">
            {selectedStock && selectedStock.type === 'mfund' ? (
              <React.Fragment>
                <Column>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Redemption price
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <MoneyView value="$1.1476" />
                  </Title>
                </Column>
                <Column>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Application price
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <MoneyView value="$1.1522" />
                  </Title>
                </Column>
                <Column>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Net assets
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <MoneyView value="$1.91B" />
                  </Title>
                </Column>
                <Column>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Mgmt fees
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    {' '}
                    <PercentageView noArrows value="0.9" />
                  </Title>
                </Column>
                <Column>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Min. investment
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <MoneyView value="$20K" />
                  </Title>
                </Column>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Column
                  mobile="6"
                  tablet="3"
                  className={cn(activeBreakpoints.below.sm ? 'pb-0' : '')}>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Last
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    ${selectedStock && selectedStock.last}
                  </Title>
                </Column>
                <Column
                  mobile="6"
                  tablet="3"
                  className={cn(activeBreakpoints.below.sm ? 'pb-0' : '')}>
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Bid
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    ${selectedStock && selectedStock.bid}
                  </Title>
                </Column>
                <Column mobile="6" tablet="3">
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Ask
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    ${selectedStock && selectedStock.ask}
                  </Title>
                </Column>
                <Column mobile="6" tablet="3">
                  <Label className="nab-label mb-0 has-text-black text--lighten-4">
                    Change
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    -$0.380 <PercentageView dark value={-1.06} />
                  </Title>
                </Column>
              </React.Fragment>
            )}
          </Columns>
          {selectedStock && (
            <RealtimeQuoteMsg
              dark
              local
              international={selectedStock.exchange !== 'ASX' ? true : false}
              currency={selectedStock.exchange !== 'ASX' ? true : false}
              className={selectedStock.type === 'mfund' ? 'mb-4' : 'mb-0'}
            />
          )}
          {selectedStock && selectedStock.type === 'mfund' && (
            <React.Fragment>
              <Button className="ml-0">
                <Icon small>file</Icon>
                <span>Product disclosure statement</span>
              </Button>
              <Button className="ml-0">
                <Icon small>file</Icon>
                <span>Fund profile document</span>
              </Button>
            </React.Fragment>
          )}
        </Container>
      </Hero>

      <Section
        px={activeBreakpoints.below.sm && 0}
        pt={activeBreakpoints.below.sm ? 0 : 2}>
        <Container>
          {selectedStock && selectedStock.type === 'etf' ? (
            <Tabs selectedTab={0}>
              <Tab id="id-tab-depth" label="Depth">
                <SecurityDepth />
              </Tab>
              <Tab id="id-tab-chart" label="Chart">
                <SecurityChart />
              </Tab>
              <Tab id="id-tab-portfolio" label="Portfolio">
                <SecurityPortfolio />
              </Tab>
              <Tab id="id-tab-asx-announcements" label="ASX Announcements">
                <SecurityAnnouncements />
              </Tab>
              <Tab id="id-tab-news" label="News">
                <SecurityNews />
              </Tab>
            </Tabs>
          ) : selectedStock && selectedStock.type === 'mfund' ? (
            <Tabs selectedTab={0}>
              <Tab id="id-tab-mfund-performance" label="Performance">
                <MFundPerformance />
              </Tab>
              <Tab id="id-tab-mfund-portfolio" label="Portfolio">
                <MFundPortfolio />
              </Tab>
            </Tabs>
          ) : (
            <Tabs selectedTab={0}>
              <Tab id="id-tab-depth" label="Depth">
                <SecurityDepth />
              </Tab>
              <Tab id="id-tab-trades" label="Trades">
                <SecurityTrades />
              </Tab>
              <Tab id="id-tab-chart" label="Chart">
                <SecurityChart />
              </Tab>
              <Tab id="id-tab-details" label="Details">
                <SecurityDetails />
              </Tab>
              <Tab id="id-tab-asx-announcements" label="ASX Announcements">
                <SecurityAnnouncements />
              </Tab>
              <Tab id="id-tab-dividends" label="Dividends">
                <SecurityDividends />
              </Tab>
              <Tab id="id-tab-research" label="Research">
                <SecurityResearch />
              </Tab>
              <Tab id="id-tab-news" label="News">
                <SecurityNews />
              </Tab>
            </Tabs>
          )}
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(BHP);

// const Breadcrumbs = [
//   { name: 'Home', link: '/' },
//   { name: 'Portfolio', link: '/portfolio', active: true }
// ];
