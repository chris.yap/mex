import React from 'react';
import cn from 'classnames';
import { Breadcrumbs } from '../constants/breadcrumbs';
import { InvestmentOptionsItems } from '../constants';
import LayoutDefault from '../layout/LayoutDefault';
import HeroView from '../components/HeroView';
import { Breakpoints, Container, Section } from 'shaper-react';

const InvestmentOptions = ({ activeBreakpoints, ...props }) => {
  return (
    <LayoutDefault
      subnav={InvestmentOptionsItems}
      breadcrumb={Breadcrumbs.investmentOptions.items}>
      <HeroView title="Investment options" />

      <Section className={cn('pt-4', activeBreakpoints.below.sm ? 'px-0' : '')}>
        <Container>Content goes here...</Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(InvestmentOptions);
