import React from 'react';
import { Route, Redirect } from 'react-router-dom';
// import Home from './Home'
import Account from './Account/Account';
import Alerts from './Alerts/Alerts';
import Notifications from './Alerts/Notifications';
import Login from './Login/Login';
import Portfolio from './Portfolio/Portfolio';
import OpenOrders from './Portfolio/Orders/OpenOrders';
import OrderDetails from './Portfolio/Orders/OrderDetails';
import OrderHistory from './Portfolio/Orders/OrderHistory';
import PersonalPortfolio from './Portfolio/Personal';
import CashAccount from './Portfolio/CashAccounts/Account';
import MarketsInsights from './Markets/MarketsInsights';
import MarketsNews from './Markets/MarketsNews';
import MarketsAnnouncements from './Markets/MarketsAnnouncements';
import NabAccounts from './NAB/NabAccounts';
import InvestmentOptions from './InvestmentOptions';
import Watchlist from './Watchlists/Watchlist';
import PayeeList from './Portfolio/CashAccounts/PayeeList';

import Security from './Security/Details';

import Support from './Support/Support';
import Forms from './Support/Forms';
import Faq from './Support/Faq';
import HowTo from './Support/HowTo';
import Test from './Test';

const Routes = ({ ...props }) => {
  return (
    <>
      <Route exact path="/" render={() => <Redirect to="/portfolio" />} />
      <Route exact path="/alerts" component={Alerts} />
      <Route exact path="/alerts/notifications" component={Notifications} />
      <Route exact path="/Login" component={Login} />
      <Route exact path="/portfolio" component={Portfolio} />
      <Route exact path="/portfolio/open-orders" component={OpenOrders} />
      <Route exact path="/portfolio/order-details" component={OrderDetails} />
      <Route exact path="/portfolio/order-history" component={OrderHistory} />
      <Route
        exact
        path="/portfolio/personal"
        component={() => (
          <PersonalPortfolio toggleOrderPad={props.toggleOrderPad} />
        )}
      />
      <Route
        exact
        path="/portfolio/personal/cash-account"
        component={CashAccount}
      />
      <Route exact path="/markets-insights" component={MarketsInsights} />
      <Route exact path="/markets-insights/news" component={MarketsNews} />
      <Route
        exact
        path="/markets-insights/announcements"
        component={MarketsAnnouncements}
      />
      <Route exact path="/nab-accounts" component={NabAccounts} />
      <Route exact path="/investment-options" component={InvestmentOptions} />
      <Route exact path="/security/details" component={Security} />
      <Route exact path="/watchlists" component={Watchlist} />
      <Route exact path="/support" component={Support} />
      <Route exact path="/support/forms" component={Forms} />
      <Route exact path="/support/faq" component={Faq} />
      <Route exact path="/support/how-to" component={HowTo} />
      <Route exact path="/test" component={Test} />
      <Route exact path="/account" component={Account} />
      <Route exact path="/payee-list" component={PayeeList} />
    </>
  );
};

export default Routes;
