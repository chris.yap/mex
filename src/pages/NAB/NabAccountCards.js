import React, { useState } from 'react';
import cn from 'classnames';
import faker from 'faker';
import PercentageView from '../../components/PercentageView';
import MoneyView from '../../components/MoneyView';
import {
  Box,
  Breakpoints,
  Button,
  Collapse,
  Column,
  Columns,
  Div,
  Divider,
  Icon,
  Label,
  Level,
  LevelItem,
  List,
  ListItem,
  Pagination,
  Section,
  Subtitle,
  Tab,
  Tabs,
  Table,
  TableRow as Row,
  TableCell as Cell,
  Textfield,
  Title
} from 'shaper-react';

const NabAccountCards = ({ activeBreakpoints, ...props }) => {
  const [expand, setExpand] = useState(false);
  return (
    <React.Fragment>
      <List className="has-bg-blue-grey is-lighten-5">
        {Accounts.map((Acct, a) => (
          <React.Fragment key={a}>
            {Acct.header && (
              <ListItem
                block
                style={{
                  padding: activeBreakpoints.below.sm ? '1rem' : ''
                }}
                className="has-bg-black lighten-5"
                onClick={() => setExpand(expand === a ? null : a)}>
                <Level multiline className="is-flex-1">
                  <LevelItem left>
                    <Title size="5" className="mb-0 ">
                      {Acct.header}
                    </Title>
                  </LevelItem>
                  {Acct.limit && (
                    <LevelItem
                      right
                      style={{ width: activeBreakpoints.above.sm && '50%' }}
                      className={activeBreakpoints.below.sm && 'mt-2'}>
                      <Level mobile className="is-flex-1">
                        <LevelItem
                          className={
                            activeBreakpoints.below.sm &&
                            'is-justify-content-flex-start'
                          }>
                          <Div>
                            <Label className="nab-label mb-0">
                              Portfolio limit
                            </Label>
                            <Title size="5" className="mb-0">
                              {Acct.limit}
                            </Title>
                          </Div>
                        </LevelItem>
                        <LevelItem
                          className={
                            activeBreakpoints.below.sm &&
                            'is-justify-content-flex-start'
                          }>
                          <Div>
                            <Label className="nab-label mb-0">
                              Available Portfolio limit
                            </Label>
                            <Title size="5" className="mb-0">
                              {Acct.availLimit}
                            </Title>
                          </Div>
                        </LevelItem>
                      </Level>
                    </LevelItem>
                  )}
                </Level>
              </ListItem>
            )}
            {Acct.items.map((item, i) => (
              <React.Fragment key={i}>
                <ListItem
                  block
                  className={cn(
                    item.transfer && 'has-pointer',
                    activeBreakpoints.below.sm
                      ? 'pa-4'
                      : item.transfer
                      ? 'py-4 px-5'
                      : 'py-5'
                  )}
                  onClick={() =>
                    item.transfer && setExpand(expand === i ? null : i)
                  }>
                  <Columns mobile multiline className="is-flex-1">
                    <Column
                      mobile="10"
                      tablet="6"
                      desktop="8"
                      widescreen="4"
                      className={activeBreakpoints.above.lg ? 'py-4' : 'pb-4'}>
                      <Title
                        size="4"
                        className="mb-2 is-flex is-align-items-center has-text-secondary has-text-weight-bold">
                        <Icon className="mr-2 has-text-primary">nabtrade</Icon>
                        {item.name}
                      </Title>
                      <Subtitle
                        size="6"
                        className="my-0 has-text-secondary has-line-height-100">
                        {item.bsb && (
                          <>
                            BSB: {item.bsb} <br />
                          </>
                        )}
                        {item.accNum && <>Account #: {item.accNum}</>}
                        {item.card && <>Card number: {item.card}</>}
                        {item.interest && (
                          <>
                            <br />
                            Interest rate:{' '}
                            <PercentageView noArrows value={item.interest} />
                          </>
                        )}
                      </Subtitle>
                    </Column>
                    <Column
                      mobile="2"
                      tablet="6"
                      desktop="4"
                      widescreen="8"
                      className={cn(
                        'is-hidden-widescreen is-flex is-justify-content-flex-end py-4',
                        !activeBreakpoints.below.sm && 'is-align-items-center'
                      )}>
                      {item.transfer && activeBreakpoints.above.sm && (
                        <Button className="ma-0 mr-1">
                          Transfer into nabtrade
                        </Button>
                      )}
                      <Button
                        secondary={i !== expand}
                        small
                        square
                        style={{ opacity: !item.transfer && 0 }}
                        disabled={!item.transfer}>
                        <Icon>{i !== expand ? 'arrow-down' : 'arrow-up'}</Icon>
                      </Button>
                    </Column>
                    <Column
                      narrow
                      className="is-hidden-touch is-hidden-desktop-only">
                      <Divider vertical />
                    </Column>

                    <Column
                      className={cn(
                        'is-flex is-align-items-center',
                        activeBreakpoints.below.sm
                          ? 'py-2 pb-0'
                          : activeBreakpoints.below.lg
                          ? 'pt-0'
                          : 'pb-4'
                      )}>
                      <Columns mobile multiline className="is-flex-1">
                        {item.cols.map((col, c) => (
                          <Column
                            key={c}
                            narrow={!!!col.label}
                            mobile={activeBreakpoints.below.md && '6'}
                            desktop={activeBreakpoints.below.lg && '3'}>
                            <div>
                              <Subtitle
                                size="7"
                                className="has-text-secondary is-uppercase my-0">
                                {col.label}
                              </Subtitle>
                              <Title
                                size="5"
                                className="has-text-secondary my-0">
                                {col.value && `${col.value}`}
                              </Title>
                            </div>
                          </Column>
                        ))}
                        {activeBreakpoints.below.sm && (
                          <>
                            <Column size="12">
                              {item.transfer && (
                                <Button className="ma-0 mr-1">
                                  Transfer into nabtrade
                                </Button>
                              )}
                            </Column>
                          </>
                        )}
                        <Column className="is-flex is-align-items-center is-justify-content-flex-end is-hidden-touch is-hidden-desktop-only">
                          {item.transfer && (
                            <>
                              <Button
                                mega
                                className="ma-0 mr-1"
                                onClick={e => {
                                  e.stopPropagation();
                                  e.preventDefault();
                                }}>
                                <p>
                                  Transfer into {item.cols.length > 3 && <br />}
                                  nabtrade
                                </p>
                              </Button>

                              <Button
                                secondary={i !== expand}
                                small
                                square
                                style={{ opacity: !item.transfer && 0 }}
                                disabled={!item.transfer}>
                                <Icon>
                                  {i !== expand ? 'arrow-down' : 'arrow-up'}
                                </Icon>
                              </Button>
                            </>
                          )}
                        </Column>
                      </Columns>
                    </Column>
                  </Columns>
                </ListItem>
                {item.transfer && (
                  <Collapse isOpened={i === expand}>
                    <Section px={activeBreakpoints.above.xs && 4} py={2}>
                      <Tabs className="mb-2">
                        <Tab label="Future transfers">
                          <Level
                            mobile
                            className={activeBreakpoints.below.sm && 'mx-4'}>
                            <LevelItem left>
                              <Textfield
                                prependIcon="search"
                                placeholder="Search transactions"
                              />
                            </LevelItem>
                            <LevelItem right>
                              <Button flat>
                                <Icon>filter</Icon>
                                {activeBreakpoints.above.sm && (
                                  <span>Filter</span>
                                )}
                              </Button>
                              <Divider vertical />
                              <Button flat>
                                <Icon>download</Icon>
                                {activeBreakpoints.above.sm && (
                                  <span>Download</span>
                                )}
                              </Button>
                            </LevelItem>
                          </Level>
                          <Table
                            hasStripes
                            headers={Headers}
                            data={Data}
                            tableRow={TableRow}
                          />
                          <Pagination
                            total={1}
                            className={activeBreakpoints.below.sm && 'px-4'}
                          />
                        </Tab>
                        <Tab label="" disabled />
                      </Tabs>
                    </Section>
                    <Divider className="mt-4" />
                  </Collapse>
                )}
              </React.Fragment>
            ))}
          </React.Fragment>
        ))}
        <Box boxShadow="none" px={4} py={3} color="secondary" bg="blacks.0">
          <p className="has-text-size-2 mb-0">
            <sup className="pos-r has-top2">*</sup>Account balance listed is the
            total amount of funds available. Transfers are subjected to
            uncleared funds e.g. a deposited cheque.
          </p>
        </Box>
      </List>
    </React.Fragment>
  );
};

export default Breakpoints(NabAccountCards);

const TableRow = ({ row }) => {
  let date = new Date(faker.fake('{{date.future}}'));
  date = date.toLocaleDateString();
  return (
    <Row>
      <Cell>{date}</Cell>
      {/* <Cell>{row.type}</Cell> */}
      <Cell>{faker.lorem.sentence()}</Cell>
      <Cell className="has-text-right">
        <MoneyView top="5" value={faker.finance.amount()} />
      </Cell>
      {/* <Cell>{row.credit}</Cell> */}
    </Row>
  );
};

const Data = [
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' },
  { date: '', desc: '', debit: '' }
];

const Headers = [
  { name: 'Date', sort: true },
  // { name: 'Type', sort: true },
  { name: 'Description' },
  { name: 'Debit', sort: true, align: 'right' }
  // { name: 'Credit', sort: true, align: 'right' }
];

const Accounts = [
  {
    header: null,
    items: [
      {
        name: 'Personal Account',
        bsb: '064-234',
        accNum: '923439234',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          },
          {}
        ],
        transfer: true
      },
      {
        name: 'Savings Account',
        bsb: '064-234',
        accNum: '1233456543',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          },
          {}
        ],
        transfer: true
      }
    ]
  },
  {
    header: 'Deposit accounts',
    items: [
      {
        name: 'Home Loan',
        accNum: '879345923',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          },
          {}
        ]
      }
    ]
  },
  {
    header: 'Cards',
    items: [
      {
        name: 'Credit Card',
        card: 'xxxx.xxxx.xxxx.x323',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          },
          {}
        ]
      },
      {
        name: 'NAB Traveller Card',
        card: 'xxxx.xxxx.xxxx.x723',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          },
          {}
        ]
      }
    ]
  },
  {
    header: 'Portfolio facility - 313184242',
    limit: '50,000.00',
    availLimit: '0',
    items: [
      {
        name: 'Line of Credit',
        bsb: '083-047',
        accNum: '313188219',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          }
        ],
        transfer: true
      },
      {
        name: 'Line of Credit',
        bsb: '083-047',
        accNum: '313188219',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          }
        ],
        transfer: true
      }
    ]
  },
  {
    header: 'Private portfolio facility - 313196024',
    limit: '50,000.00',
    availLimit: '0',
    items: [
      {
        name: 'Line of Credit',
        bsb: '083-047',
        accNum: '313188219',
        cols: [
          {
            label: 'Available balance',
            value: '100,000.00'
          },
          {
            label: 'Total balance',
            value: '90,000.00'
          }
        ],
        transfer: true
      }
    ]
  }
];
