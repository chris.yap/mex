import React from 'react';
import {
  Breakpoints,
  Button,
  Container,
  Hero,
  Icon,
  Section,
  Subtitle,
  Title
} from 'shaper-react';
import BreadcrumbBar from '../../components/BreadcrumbBar';
import NabAccountCards from './NabAccountCards';

const NabAccounts = ({ activeBreakpoints, ...props }) => {
  return (
    <div id={`nab-accounts`}>
      <BreadcrumbBar breadcrumb={BreadcrumbItems} shortcut={false} />
      <Hero image={'/images/bg-red-triangle.gif'} dark>
        <Container>
          <Title className="font-nabimpact my-0 h">
            <Icon className="mr-2 pos-r has-top10">nabtrade</Icon> NAB accounts
          </Title>
        </Container>
      </Hero>
      <Hero className="has-bg-secondary lighten-4 py-4">
        <Container>
          <Subtitle className="my-0 is-flex is-align-items-center is-justify-content-center">
            For full management of your NAB accounts, log in to your NAV
            Internet Banking.
            <Button primary className="ml-4">
              <Icon>login</Icon>
              <span>Login</span>
            </Button>
          </Subtitle>
        </Container>
      </Hero>
      <Section className={`py-4 ${activeBreakpoints.below.sm && 'px-0'}`}>
        <Container>
          <p className="has-text-right">
            <Button flat>
              <Icon>download</Icon>
              <span>Download</span>
            </Button>
          </p>

          <NabAccountCards />
        </Container>
      </Section>
    </div>
  );
};

export default Breakpoints(NabAccounts);

const BreadcrumbItems = [
  {
    label: 'Home',
    to: '/'
  },
  {
    label: 'NAB accounts',
    to: '/nab-accounts',
    active: true
  }
];
