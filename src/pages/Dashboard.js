import React from "react";
import {
  // Button, Card, CardHeader, CardImage, CardHeaderTitle, CardContent, Columns, Column, Divider, Hero, Icon, List, ListItem, Tab, Tabs, Tag, Toolbar, ToolbarNav, ToolbarNavItem, Subtitle
  Container,
  Section,
  Title
} from "shaper-react";

export default class Dashboard extends React.Component {
  render() {
    return (
      <div id="dashboard">
        <Section>
          <Container>
            <Title className={``}>Dashboard</Title>
          </Container>
        </Section>
      </div>
    );
  }
}
