import React, { useState } from 'react';
import { getState } from '../../context/StateProvider';
import cn from 'classnames';
import {
  Breakpoints,
  Button,
  Card,
  Collapse,
  Column,
  Columns,
  DatePicker,
  Div,
  Divider,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Pagination,
  Radio,
  RadioGroup,
  Table,
  TableRow as Row,
  TableCell as Cell,
  Textfield,
  Tooltip
} from 'shaper-react';

const Feed = ({ activeBreakpoints, data, filters, manageAlert, ...props }) => {
  const [, dispatch] = getState();
  // const [filters, setFilters] = useState(false);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [date, setDate] = useState();
  const [type, setType] = useState();
  const [focused, updateFocused] = React.useState(null);
  const handleDateChange = newVal => {
    setDate(newVal);
  };
  const handleTypeChange = newVal => {
    setType(newVal);
  };
  return (
    <React.Fragment>
      <Collapse isOpened={filters}>
        <>
          <Divider />
          {activeBreakpoints.above.lg ? (
            <Columns className="mt-0 is-justify-content-center">
              <Column narrow className={`pb-0`}>
                <RadioGroup
                  buttons
                  name="date"
                  label="Date"
                  selectedValue={date}
                  onChange={handleDateChange}
                  className="mb-0 is-align-self-center">
                  <Radio button value="7" label="7 days" />
                  <Radio button value="30" label="30 days" />
                  <Radio button value="90" label="3 months" />
                  <Radio button value="182" label="6 months" />
                  <Radio button value="365" label="12 months" />
                  <Radio button value="ytd" label="YTD" />
                  <Radio button value="custom" label="Custom" />
                </RadioGroup>
                <Collapse isOpened={date === 'custom'}>
                  <Columns className="mt-0">
                    <Column>
                      <DatePicker
                        range
                        startDate={startDate}
                        endDate={endDate}
                        onDatesChange={({ startDate, endDate }) => {
                          setStartDate(startDate);
                          setEndDate(endDate);
                        }}
                        label="Date range"
                        startDatePlaceholderText="Date from"
                        endDatePlaceholderText="Date to"
                        mb={0}
                        focusedInput={focused}
                        onFocusChange={updateFocused}
                      />
                    </Column>
                  </Columns>
                </Collapse>
              </Column>
              <Column narrow className="pb-0">
                <RadioGroup
                  buttons
                  name="type"
                  label="Type"
                  selectedValue={type}
                  onChange={handleTypeChange}
                  className="mb-0">
                  <Radio button value="admin" label="Admin" />
                  <Radio button value="billing" label="Billing" />
                  <Radio button value="cash" label="Cash" />
                  <Radio button value="order" label="Order/Trade" />
                </RadioGroup>
              </Column>
              <Column className="pb-0">
                <Textfield label="Code" />
              </Column>
            </Columns>
          ) : (
            <>
              <Columns mobile>
                <Column className="pb-0">
                  <RadioGroup
                    buttons
                    name="date"
                    label="Date"
                    selectedValue={date}
                    onChange={handleDateChange}
                    className="mb-0 is-align-self-center">
                    <Radio button value="7" label="7 days" />
                    <Radio button value="30" label="30 days" />
                    <Radio button value="90" label="3 months" />
                    <Radio button value="182" label="6 months" />
                    <Radio button value="365" label="12 months" />
                    <Radio button value="ytd" label="YTD" />
                    <Radio button value="custom" label="Custom" />
                  </RadioGroup>
                  <Collapse isOpened={date === 'custom'}>
                    <Columns className="mt-0">
                      <Column>
                        <DatePicker
                          range
                          startDate={startDate}
                          endDate={endDate}
                          onDatesChange={({ startDate, endDate }) => {
                            setStartDate(startDate);
                            setEndDate(endDate);
                          }}
                          label="Date range"
                          startDatePlaceholderText="Date from"
                          endDatePlaceholderText="Date to"
                          mb={0}
                          focusedInput={focused}
                          onFocusChange={updateFocused}
                        />
                      </Column>
                    </Columns>
                  </Collapse>
                </Column>
              </Columns>
              <Columns mobile>
                <Column narrow className="pb-0">
                  <RadioGroup
                    buttons
                    name="type"
                    label="Type"
                    selectedValue={type}
                    onChange={handleTypeChange}
                    className="mb-0">
                    <Radio button value="admin" label="Admin" />
                    <Radio button value="billing" label="Billing" />
                    <Radio button value="cash" label="Cash" />
                    <Radio button value="order" label="Order/Trade" />
                  </RadioGroup>
                </Column>
                <Column className="pb-0">
                  <Textfield label="Code" />
                </Column>
              </Columns>
            </>
          )}

          <p className="has-text-centered ">
            <Button
              flat
              onClick={() => {
                handleTypeChange('');
                handleDateChange('');
              }}>
              Reset filters
            </Button>
            <Button onClick={props.updateFilters}>
              {!date && !type ? 'Close' : 'Apply'}
            </Button>
          </p>
        </>
      </Collapse>
      <Card>
        {activeBreakpoints.below.sm ? (
          <List>
            {data.map((row, key) => {
              return (
                <>
                  <ListItem className="py-1 has-bg-black lighten-5">
                    <Level mobile className="is-flex-1">
                      <LevelItem left className="has-text-size-2">
                        {row.date}
                      </LevelItem>
                      <LevelItem right />
                    </Level>
                  </ListItem>
                  <ListItem className="py-0 px-0">
                    <div
                      className={cn(
                        'is-flex py-4 has-text-size-1 px-0 pl-1 darken-1',
                        row.unread && 'has-bg-danger'
                      )}
                    />
                    <Columns mobile className="is-flex-1 py-3 px-3">
                      <Column>
                        <p className="my-0 has-text-size-1 has-text-weight-bold">
                          {row.code}:{row.exchange}
                        </p>
                        <p className="my-0 has-text-size-1">{row.desc}</p>
                        <p className="my-0 has-text-size-2 has-text-secondary">
                          Type: {row.type}
                        </p>
                      </Column>
                      <Column narrow className="has-text-right">
                        <div>
                          <AlertOptions
                            dispatch={dispatch}
                            row={row}
                            activeBreakpoints={activeBreakpoints}
                          />
                        </div>
                        {row.delivery.map((item, i) => (
                          <Icon key={i} small className="mr-2">
                            {item}
                          </Icon>
                        ))}
                      </Column>
                    </Columns>
                  </ListItem>
                </>
              );
            })}
          </List>
        ) : (
          <Table
            hasStripes
            headers={Headers}
            data={data}
            tableRow={row => (
              <TableRow
                dispatch={dispatch}
                activeBreakpoints={activeBreakpoints}
                {...row}
              />
            )}
            defaultSortCol="dateUnformatted"
            desc
            mb={0}
          />
        )}
      </Card>
      <Pagination
        current={1}
        total={1}
        className={`mt-2 ${activeBreakpoints.below.sm ? 'mx-4' : ''}`}
      />
    </React.Fragment>
  );
};

export default Breakpoints(Feed);

const Headers = [
  { name: '' },
  { name: 'Date' },
  { name: 'Code' },
  { name: 'Description' },
  { name: 'Type' },
  // { name: 'Delivery' },
  { name: '' }
];

const TableRow = ({ dispatch, row, activeBreakpoints }) => {
  return (
    <Row>
      <Cell width="1" className="has-text-primary has-text-size2 py-0 pr-0">
        {row.unread && '•'}
      </Cell>
      <Cell className="has-text-nowrap">{row.date}</Cell>
      <Cell>
        <strong>
          {row.code}.{row.exchange}
        </strong>
      </Cell>
      <Cell minWidth="200px">{row.desc}</Cell>
      <Cell>{row.type}</Cell>
      {/* <Cell hasNoWrap>
        <Div>
          {row.delivery.map((item, i) => (
            <Icon key={i} small className="mr-2">
              {item.icon}
            </Icon>
          ))}
        </Div>
      </Cell> */}
      <Cell right>
        <AlertOptions
          dispatch={dispatch}
          row={row}
          activeBreakpoints={activeBreakpoints}
        />
      </Cell>
    </Row>
  );
};

const AlertOptions = ({ dispatch, row, activeBreakpoints }) => {
  return (
    <>
      <Tooltip tip="Edit" left>
        <Button
          small
          square
          inverted
          secondary
          className={cn(
            'mr-0',
            activeBreakpoints.above.xs && activeBreakpoints.below.xl && 'mb-2'
          )}>
          <Icon>pencil</Icon>
        </Button>
      </Tooltip>
      <Button
        small
        secondary
        className={cn(
          'mr-0',
          activeBreakpoints.above.xs && activeBreakpoints.below.xl && 'mb-2'
        )}
        onClick={() => {
          dispatch({ type: 'toggleOrderPad' });
          dispatch({ type: 'orderPadSelectOption', payload: row });
        }}>
        <Icon>order</Icon>
        <span>Trade</span>
      </Button>
    </>
  );
};
