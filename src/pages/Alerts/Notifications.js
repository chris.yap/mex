import React from 'react';
import LayoutNoNav from '../../layout/LayoutNoNav';
import { NavLink } from 'react-router-dom';
import {
  Breadcrumb,
  BreadcrumbItem,
  Breakpoints,
  Button,
  Buttons,
  Container,
  Hero,
  Icon,
  Level,
  LevelItem,
  Section,
  Title
} from 'shaper-react';
import NotificationList from './NotificationList';

class Notifications extends React.Component {
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <LayoutNoNav
        breadcrumb={
          <Breadcrumb>
            <BreadcrumbItem href="./">Home</BreadcrumbItem>
            <BreadcrumbItem href="./alerts">Alerts</BreadcrumbItem>
            <BreadcrumbItem href="./alerts/notifications" active>
              Notifications
            </BreadcrumbItem>
          </Breadcrumb>
        }
      >
        <Hero dark className="has-bg-black pt-4 pb-2">
          <Container>
            <Title className="my-0 font-nabimpact">Alerts</Title>
          </Container>
        </Hero>

        <Section className={`py-4 ${activeBreakpoints.below.sm && 'px-0'}`}>
          <Container>
            <Level mobile>
              <LevelItem left>
                <Buttons
                  rounded
                  className={activeBreakpoints.below.sm ? 'ml-4' : ''}
                >
                  <NavLink to="/alerts" className="nab-button">
                    Alerts
                  </NavLink>
                  <NavLink
                    to="/alerts/notifications"
                    className="nab-button is-active"
                  >
                    Notifications
                  </NavLink>
                </Buttons>
              </LevelItem>
              <LevelItem right>
                <NavLink to="/alerts/manage" className="pa-0">
                  <Button
                    secondary
                    inverted
                    className={`my-0 ${
                      activeBreakpoints.below.sm ? 'mr-4 is-square' : 'mr-0'
                    }`}
                  >
                    <Icon
                      small
                      className={`${activeBreakpoints.below.sm ? '' : 'mr-2'}`}
                    >
                      cog
                    </Icon>
                    {activeBreakpoints.above.xs && <span>Manage alerts</span>}
                  </Button>
                </NavLink>
              </LevelItem>
            </Level>
            <NotificationList />
          </Container>
        </Section>
      </LayoutNoNav>
    );
  }
}

export default Breakpoints(Notifications);
