import React, { useState, useEffect } from 'react';
import ManageAlertsFields from './ManageAlertsFields';
import ReactHtmlParser from 'react-html-parser';
import { AlertSettings } from '../../constants/alerts';
import {
  Breakpoints,
  Button,
  Card,
  Collapse,
  Column,
  Columns,
  Div,
  Divider,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Select,
  Table,
  TableRow as Row,
  TableCell as Cell,
  Title,
  Tooltip
} from 'shaper-react';

const ManageAlerts = ({ activeBreakpoints, ...props }) => {
  const [selected, setSelected] = useState(0);
  const [action, setAction] = useState(props.action || null);
  const [selectedAlert, setSelectedAlert] = useState({});
  const [tempAlert, setTempAlert] = useState({});

  const updateSelected = value => {
    setSelected(value);
    cloneAlert(value);
  };
  const updateAction = value => {
    setAction(value);
    if (value === 'confirmDelete') {
      deleteAlert(selected);
      updateAction(null);
    }
  };
  const updateSelectedAlert = value => {
    setSelectedAlert(value);
  };
  const cloneAlert = index => {
    AlertSettings.map((item, i) => {
      if (i === index) {
        updateSelectedAlert(item);
        updateTempAlert(item);
      }
      return null;
    });
  };
  const updateTempAlert = value => {
    setTempAlert(value);
  };

  const handleDelivery = newVal => {
    updateTempAlert({
      ...tempAlert,
      delivery: newVal
    });
  };

  const deleteAlert = index => {
    AlertSettings.map((item, i) => {
      if (i === index) {
        AlertSettings.splice(i, 1);
      }
      return null;
    });
  };

  const updateConditions = value => {
    // console.log(value);
  };

  useEffect(() => {
    updateSelected(selected);
    updateAction(action);
  });

  return (
    <>
      <Columns>
        <Column mobile={12} tablet={4} desktop={3} widescreen={3}>
          <Button
            block
            mb={2}
            onClick={() => {
              updateAction('new');
              updateSelected(null);
              updateTempAlert({});
            }}>
            <Icon small>plus</Icon>
            <span>New alert</span>
          </Button>

          {activeBreakpoints.above.xs ? (
            <React.Fragment>
              {AlertSettings.length > 0 && (
                <Card className="mb-4">
                  <List>
                    {AlertSettings.map((item, a) => (
                      <ListItem
                        key={a}
                        bg={selected === a && 'secondary'}
                        color={selected === a && 'white'}
                        fontWeight={selected === a && 'bold'}
                        className={selected !== a && 'has-pointer'}
                        onClick={() => {
                          updateSelected(a);
                          updateAction(null);
                        }}>
                        <Level mobile className={`is-flex-1`}>
                          <LevelItem left>
                            {/* {selected === a && <Icon>arrow-right</Icon>} */}
                            {item.code}.{item.exchange}
                          </LevelItem>
                          {selected !== a && (
                            <LevelItem right>
                              <Icon>arrow-right</Icon>
                            </LevelItem>
                          )}
                        </Level>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              )}
            </React.Fragment>
          ) : (
            <Select options={AlertsSelect(AlertSettings)} />
          )}
        </Column>
        <Column className={activeBreakpoints.below.sm && 'px-0'}>
          {AlertSettings.length > 0 ? (
            <React.Fragment>
              {action !== 'new' && (
                <React.Fragment>
                  <Level
                    mobile
                    className={`mb-2 ${
                      activeBreakpoints.below.sm ? 'px-4' : ''
                    }`}>
                    <LevelItem left>
                      <div className="mr-4">
                        <p className="nab-label mb-0">Stock</p>
                        <Title size="4" className="mb-0">
                          {selectedAlert.code} - {selectedAlert.name}
                        </Title>
                      </div>
                      {/* <div>
                        <p className="nab-label mb-0">Delivery method</p>
                        <p className="mb-0 is-flex has-text-size-2 pb-1">
                          {selectedAlert.delivery &&
                            selectedAlert.delivery.map((item, i) => (
                              <Icon key={i} className="mr-0">
                                {item}
                              </Icon>
                            ))}
                        </p>
                      </div> */}
                    </LevelItem>
                    <LevelItem right>
                      {!action ? (
                        <React.Fragment>
                          <Tooltip tip="Edit">
                            <Button
                              inverted
                              square
                              secondary
                              onClick={() => updateAction('edit')}>
                              <Icon>pencil</Icon>
                            </Button>
                          </Tooltip>
                          <Tooltip tip="Delete">
                            <Button
                              inverted
                              square
                              danger
                              onClick={() => updateAction('delete')}>
                              <Icon>bin</Icon>
                            </Button>
                          </Tooltip>
                        </React.Fragment>
                      ) : action === 'delete' ? (
                        <div className="mb-0 is-flex is-align-items-center">
                          <span className="has-text-secondary mr-2">
                            Delete this alert?
                          </span>
                          <Button onClick={() => updateAction(null)}>
                            <Icon>close</Icon>
                            <span>No</span>
                          </Button>
                          <Button
                            danger
                            onClick={() => updateAction('confirmDelete')}>
                            <Icon>checked</Icon>
                            <span>Yes</span>
                          </Button>
                        </div>
                      ) : action === 'edit' ? (
                        <React.Fragment>
                          <Button
                            inverted
                            secondary
                            onClick={() => updateAction(null)}>
                            Cancel
                          </Button>
                          <Button success>Save</Button>
                        </React.Fragment>
                      ) : null}
                    </LevelItem>
                  </Level>
                  <Divider className="mb-4" />
                </React.Fragment>
              )}

              <Collapse
                isOpened={action === 'edit' || action === 'new' ? true : false}>
                <ManageAlertsFields
                  action={action}
                  updateAction={updateAction}
                  tempAlert={tempAlert}
                  handleDelivery={handleDelivery}
                  updateConditions={updateConditions}
                />
              </Collapse>

              {tempAlert.conditions && (
                <Card>
                  <Table
                    hasBorders
                    border="none"
                    headers={Headers}
                    data={tempAlert.conditions}
                    tableRow={TableRow}
                    mb={0}
                  />
                </Card>
              )}
            </React.Fragment>
          ) : (
            <div className="has-text-centered">
              <Divider className="mb-8" />
              <Title size="4" className="has-text-secondary text--lighten-2">
                No alerts found
              </Title>
            </div>
          )}
        </Column>
      </Columns>
    </>
  );
};

export default Breakpoints(ManageAlerts);

const Headers = [{ name: 'Condition' }];

const TableRow = ({ row }) => {
  let list = [];
  if (Array.isArray(row.value)) {
    row.value.map(item => {
      list.push(item);
      return null;
    });
  } else {
    list.push('active');
  }
  return (
    <>
      {row.active && (
        <Row>
          <Cell>
            <Div display="flex" alignItems="center">
              <Icon className="mr-2">checked</Icon>
              <span>{ReactHtmlParser(row.label)}</span>
            </Div>
            {row.value && (
              <Div ml="32px">
                {row.value.map((val, v) => (
                  <p key={v} className="has-text-weight-bold mb-0">
                    • {val}
                  </p>
                ))}
              </Div>
            )}
          </Cell>
        </Row>
      )}
    </>
  );
};

const AlertsSelect = props => {
  let List = [];
  props.map(item => {
    List.push({ label: item.code + '.' + item.exchange });
    return null;
  });
  return List;
};
