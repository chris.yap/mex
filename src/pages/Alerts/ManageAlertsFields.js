import React, { useState, useEffect } from 'react';
import {
  Button,
  Breakpoints,
  Card,
  CardContent,
  Checkbox,
  CheckboxGroup,
  Collapse,
  Div,
  Divider,
  Icon,
  Select,
  Textfield,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';
import RealtimeQuoteMsg from '../../components/RealtimeQuoteMsg';

const ManageAlertsFields = ({
  activeBreakpoints,
  action,
  tempAlert,
  ...props
}) => {
  const [triggers, setTriggers] = useState([]);
  const [c52h, set52h] = useState([]);
  const [c52l, set52l] = useState([]);
  const [drops, setDrops] = useState([]);
  const [rises, setRises] = useState([]);
  const [delivery, setDelivery] = useState([]);
  const [asx, setASX] = useState([]);

  const [priceRises, setPriceRises] = useState([]);
  const [priceDrops, setPriceDrops] = useState([]);
  const [priceUpField, setPriceUpField] = useState();
  const [priceDownField, setPriceDownField] = useState();

  let SearchArr = (nameKey, myArray) => {
    if (myArray) {
      for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].condition === nameKey && myArray[i].active) {
          return [myArray[i].condition];
        }
      }
    }
  };

  useEffect(() => {
    if (tempAlert) {
      setTriggers(tempAlert.conditions);
      setDelivery(tempAlert.delivery);
      set52h(SearchArr('high', tempAlert.conditions));
      set52l(SearchArr('low', tempAlert.conditions));
      setDrops(SearchArr('drops', tempAlert.conditions));
      setRises(SearchArr('rises', tempAlert.conditions));
      setASX(SearchArr('asx', tempAlert.conditions));
    }
  }, [tempAlert]);
  return (
    <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
      {action === 'new' && <Select label="Security" />}
      <Card className="mb-2">
        <Table
          hasStripes
          headers={Headers}
          data={Data}
          tableRow={TableRow}
          mb={0}
        />
      </Card>
      <RealtimeQuoteMsg local className="has-text-centered" />

      <Card className="mb-4">
        <CardContent>
          <Title size="6" className="mb-2 mt-2">
            Triggers
          </Title>
          <Divider mb={4} />
          <CheckboxGroup name="high" value={c52h} onChange={set52h}>
            <Checkbox
              value="high"
              label={
                <>
                  Price reaches a new <strong>52-week high</strong>
                </>
              }
            />
          </CheckboxGroup>

          <CheckboxGroup name="low" value={c52l} onChange={set52l}>
            <Checkbox
              value="low"
              label={
                <span>
                  Price reaches a new <strong>52-week low</strong>
                </span>
              }
            />
          </CheckboxGroup>

          <CheckboxGroup
            name="drops"
            value={drops}
            onChange={setDrops}
            mb={drops && drops.length > 0 ? 2 : 4}>
            <Checkbox
              value="drops"
              label={
                <>
                  Price <strong>drops below ...</strong>
                </>
              }
            />
          </CheckboxGroup>

          <Collapse isOpened={drops && drops.length > 0}>
            <Div pl="28px">
              {priceDrops.length > 0 ? (
                <>
                  {priceDrops.map((item, i) => (
                    <Div display="flex">
                      <Textfield flex="1" value={item} mb={1} />
                      <Button
                        square
                        danger
                        inverted
                        mr={0}
                        ml={1}
                        onClick={() => {}}>
                        <Icon>close</Icon>
                      </Button>
                    </Div>
                  ))}
                </>
              ) : (
                <Textfield flex="1" mb={1} />
              )}
              <p className="has-text-size-2 has-text-secondary text--lighten-1 mb-2">
                Value must be below the last price
              </p>
              <Button inverted mx={0} mb={4} onClick={() => {}}>
                <Icon>plus</Icon>
                <span>Add another</span>
              </Button>
            </Div>
          </Collapse>

          <CheckboxGroup name="rises" value={rises} onChange={setRises}>
            <Checkbox
              value="rises"
              label={
                <>
                  Price <strong>rises above ...</strong>
                </>
              }
            />
          </CheckboxGroup>

          <CheckboxGroup name="asx" value={asx} onChange={setASX}>
            <Checkbox
              value="asx"
              label={
                <>
                  ASX <strong>price sensitive</strong> announcements
                </>
              }
            />
          </CheckboxGroup>

          <Divider className="mb-4" />

          <CheckboxGroup
            name="delivery"
            value={delivery}
            label="Delivery method"
            onChange={setDelivery}>
            <Checkbox value="email" label="Email" />
            <Checkbox value="sms" label="SMS" />
          </CheckboxGroup>

          <Divider mb={4} />

          <Button success className="ml-0 px-10 mb-4">
            Save
          </Button>
          <Button flat onClick={() => props.updateAction(null)}>
            Cancel
          </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default Breakpoints(ManageAlertsFields);

const Headers = [
  { name: 'Last', align: 'centered' },
  { name: 'Bid', align: 'centered' },
  { name: 'Ask', align: 'centered' }
];

const TableRow = ({ row }) => (
  <Row>
    <Cell className="has-text-centered">{row.last}</Cell>
    <Cell className="has-text-centered">{row.bid}</Cell>
    <Cell className="has-text-centered">{row.ask}</Cell>
  </Row>
);

const Data = [{ last: '20.24', bid: '20.14', ask: '21.03' }];
