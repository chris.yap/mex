import React from 'react';
import {
  Breakpoints,
  Button,
  Card,
  Collapse,
  Column,
  Columns,
  DatePicker,
  Divider,
  Icon,
  Pagination,
  Radio,
  RadioGroup,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

const NotificationList = ({ activeBreakpoints, ...props }) => {
  const [date] = React.useState();
  const [type] = React.useState();
  const [filters] = React.useState(false);

  const [startDate, setStartDate] = React.useState();
  const [endDate, setEndDate] = React.useState();
  const [focused, updateFocused] = React.useState(null);

  return (
    <React.Fragment>
      <Divider className="mb-2" />
      <div className="has-text-centered is-hidden-touch">
        <Button
          square
          flat
          className={filters ? 'is-active' : ''}
          onClick={() => this.setState({ filters: !filters })}>
          <Icon
            style={{
              transition: '.2s',
              transform: filters ? 'rotate(180deg)' : 'none'
            }}>
            filter
          </Icon>
        </Button>
      </div>
      <Collapse isOpened={filters}>
        <Columns className="mt-0 is-justify-content-center is-hidden-touch">
          <Column narrow className={`pb-0`}>
            <RadioGroup
              buttons
              name="date"
              label="Date"
              selectedValue={date}
              onChange={this.handleDateChange}
              className="mb-0 is-align-self-center">
              <Radio button value="7" label="7 days" />
              <Radio button value="30" label="30 days" />
              <Radio button value="90" label="3 months" />
              <Radio button value="182" label="6 months" />
              <Radio button value="365" label="12 months" />
              <Radio button value="ytd" label="YTD" />
              <Radio button value="custom" label="Custom" />
            </RadioGroup>
            <Collapse isOpened={date === 'custom'}>
              <Columns className="mt-0">
                <Column>
                  <DatePicker
                    range
                    showClearDates
                    startDate={startDate}
                    endDate={endDate}
                    onDatesChange={({ startDate, endDate }) => {
                      setStartDate(startDate);
                      setEndDate(endDate);
                    }}
                    label="Date range"
                    startDatePlaceholderText="Date from"
                    endDatePlaceholderText="Date to"
                    mb={0}
                    focusedInput={focused}
                    onFocusChange={updateFocused}
                  />
                </Column>
              </Columns>
            </Collapse>
          </Column>
          <Column narrow className="pb-0">
            <RadioGroup
              buttons
              name="type"
              label="Type"
              selectedValue={type}
              onChange={this.handleTypeChange}
              className="mb-0">
              <Radio button value="admin" label="Admin" />
              <Radio button value="billing" label="Billing" />
              <Radio button value="cash" label="Cash" />
              <Radio button value="order" label="Order/Trade" />
            </RadioGroup>
          </Column>
        </Columns>
        <p className="has-text-centered is-hidden-touch">
          <Button flat onClick={() => this.setState({ date: '', type: '' })}>
            Reset filters
          </Button>
          <Button onClick={() => this.setState({ filters: !filters })}>
            {!date && !type ? 'Close' : 'Apply'}
          </Button>
        </p>
      </Collapse>
      <Card className="mb-4">
        <Table
          hasStripes
          headers={Headers}
          data={data}
          tableRow={TableRow}
          mb={0}
        />
      </Card>
      <Pagination
        current={1}
        total={1}
        className={activeBreakpoints.below.sm ? 'mx-4' : ''}
      />
    </React.Fragment>
  );
};

export default Breakpoints(NotificationList);

const data = [
  {
    date: '01-05-2019',
    account: 'Mr John Smith',
    desc: 'Cash statement available',
    type: 'Cash',
    delivery: ['email']
  },
  {
    date: '01-04-2019',
    account: 'Mr John Smith',
    desc: 'Cash statement available',
    type: 'Cash',
    delivery: ['sms', 'email']
  }
];

const Headers = [
  { name: 'Date' },
  { name: 'Type' },
  { name: 'Account' },
  { name: 'Description' },
  { name: 'Delivery' },
  { name: '' }
];

const TableRow = ({ row }) => (
  <Row>
    <Cell>{row.date}</Cell>
    <Cell>{row.type}</Cell>
    <Cell>{row.account}</Cell>
    <Cell>{row.desc}</Cell>
    <Cell>
      {row.delivery.map((item, i) => (
        <Icon key={i} small className="mr-1">
          {item}
        </Icon>
      ))}
    </Cell>
    <Cell className="has-text-right">
      <Button small round flat>
        <Icon>pencil</Icon>
      </Button>
      <Button small round flat>
        <Icon>arrow-right</Icon>
      </Button>
    </Cell>
  </Row>
);
