import React from 'react';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import {
  Breakpoints,
  Button,
  Container,
  Divider,
  Icon,
  Section,
  Tab,
  Tabs,
  Tag,
  Title
} from 'shaper-react';
import Feed from './AlertsFeed';
import ManageAlerts from './ManageAlerts';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { AlertItems } from '../../constants/alerts';

const Alerts = ({ activeBreakpoints }) => {
  const [filtersIsOpen, setFiltersIsOpen] = React.useState(false);
  const [triggeredTabSelected, setTriggeredTabSelected] = React.useState();
  const [action, setAction] = React.useState(null);
  const [selected, setSelected] = React.useState(null);
  const [selectedAlert, setSelectedAlert] = React.useState(null);
  const [tempAlert, setTempAlert] = React.useState([]);
  const [tempItems, setTempItems] = React.useState([]);

  let clearTempList = () => {
    setTempAlert([]);
    setTempItems([]);
  };

  return (
    <LayoutDefault breadcrumb={Breadcrumbs.alerts.items}>
      <HeroView title="Alerts" />

      <Section className={`py-4 ${activeBreakpoints.below.sm && 'px-0'}`}>
        <Container
          className={activeBreakpoints.below.sm ? 'is-overflow-hidden' : ''}>
          <Tabs
            toggle
            rounded
            px={activeBreakpoints.below.sm && 4}
            selectedTab={triggeredTabSelected ? 1 : 0}
            additionalContent={
              <>
                {activeBreakpoints.above.sm && triggeredTabSelected && (
                  <Button
                    square={activeBreakpoints.below.sm}
                    flat
                    className={`ma-0 ${filtersIsOpen && 'is-active'}`}
                    onClick={() =>
                      setFiltersIsOpen(filtersIsOpen ? false : true)
                    }>
                    <Icon
                      style={{
                        transition: '.2s',
                        transform: filtersIsOpen ? 'rotate(180deg)' : 'none'
                      }}>
                      filter
                    </Icon>
                    {activeBreakpoints.above.xs && <span>Filter</span>}
                  </Button>
                )}
              </>
            }>
            <Tab
              label="Manage alerts"
              tabClick={() => setTriggeredTabSelected(false)}>
              <ManageAlerts
                action={action}
                selected={selected}
                selectedAlert={selectedAlert}
                tempAlert={tempAlert}
                tempItems={tempItems}
                // handleHigh={this.handleHigh}
                // handleLow={this.handleLow}
                // selectAlert={this.selectAlert}
              />
            </Tab>
            <Tab
              label={
                <span>
                  Triggered alerts
                  <Tag
                    primary={!triggeredTabSelected}
                    white={triggeredTabSelected}
                    small
                    ml={2}
                    py={1}
                    px={2}
                    height="18px">
                    2
                  </Tag>
                </span>
              }
              tabClick={() => setTriggeredTabSelected(true)}>
              {AlertItems.length > 0 ? (
                <Feed
                  data={AlertItems}
                  filters={activeBreakpoints.above.sm ? filtersIsOpen : false}
                  updateFilters={() =>
                    setFiltersIsOpen(filtersIsOpen ? false : true)
                  }
                />
              ) : (
                <div className="has-text-centered">
                  <Divider className="mb-8" />
                  <Title
                    size="4"
                    className="has-text-secondary text--lighten-2">
                    No alerts found
                  </Title>
                  <Button secondary>Create new alert</Button>
                </div>
              )}
            </Tab>
          </Tabs>
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(Alerts);
