import React from 'react';
import cn from 'classnames';
import 'flag-icon-css/css/flag-icon.min.css';
import {
  Card,
  CardHeader,
  CardContent,
  Carousel,
  Column,
  Columns,
  Title,
  Subtitle
} from 'shaper-react';
import PercentageView from '../../components/PercentageView';

const FxRates = ({ className, ...props }) => {
  return (
    <Carousel
      cellSpacing={40}
      slidesToShow={3}
      responsive={[
        { breakpoint: 1024, settings: { slidesToShow: 3 } },
        { breakpoint: 768, settings: { slidesToShow: 2 } },
        { breakpoint: 480, settings: { slidesToShow: 1 } }
      ]}
      className={className}
    >
      {fxrates.map(rate => (
        <Card outline key={rate.name}>
          <CardHeader className="px-4 py-4">
            <div>
              <Title size="6" className="mb-4">
                {rate.buyCurrency}/{rate.sellCurrency}
              </Title>
              <Subtitle size="6" className="has-text-blue-grey text--lighten-1">
                {rate.name}
              </Subtitle>
            </div>
            <span
              style={{ width: '3em' }}
              className={cn(
                'flag-icon',
                'flag-icon-' + rate.sellCurrency.substring(0, 2).toLowerCase()
              )}
            />
          </CardHeader>
          <CardContent className="has-bg-blue-grey lighten-5 px-4 px-4">
            <Columns mobile>
              <Column>
                <Title size="6" className="mb-4">
                  Exchange rate
                </Title>
                <Subtitle size="6" className="mb-0 has-text-secondary">
                  {rate.rate}
                </Subtitle>
              </Column>
              <Column className="has-text-right">
                <Title size="6" className="mb-4">
                  Price change
                </Title>
                <Subtitle size="6" className="mb-0">
                  <span
                    className={cn(
                      rate.priceChange > 0
                        ? 'has-text-success'
                        : rate.priceChange < 0
                        ? 'has-text-danger'
                        : 'has-text-secondary'
                    )}
                  >
                    <PercentageView value={rate.priceChange} />
                  </span>
                </Subtitle>
              </Column>
            </Columns>
          </CardContent>
        </Card>
      ))}
    </Carousel>
  );
};

export default FxRates;

const fxrates = [
  {
    buyCurrency: 'AUD',
    sellCurrency: 'USD',
    name: 'United States Dollars',
    rate: '0.6861',
    priceChange: '0.04'
  },
  {
    buyCurrency: 'AUD',
    sellCurrency: 'EUR',
    name: 'Euro',
    rate: '0.5913',
    priceChange: '-0.04'
  },
  {
    buyCurrency: 'AUD',
    sellCurrency: 'GBP',
    name: 'Great British Pound',
    rate: '0.5338',
    priceChange: '0.15'
  },
  {
    buyCurrency: 'AUD',
    sellCurrency: 'SGP',
    name: 'Singapore Dollars',
    rate: '0.9051',
    priceChange: '-0.23'
  },
  {
    buyCurrency: 'AUD',
    sellCurrency: 'JPN',
    name: 'Japanese Yen',
    rate: '74.0415',
    priceChange: '0.23'
  }
];
