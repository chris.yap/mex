import React from 'react';
import cn from 'classnames';
import {
  Breakpoints,
  Column,
  Icon,
  Subtitle,
  Tooltip,
  Title
} from 'shaper-react';

const CardDataHeaders = ({ activeBreakpoints, error, ...props }) => {
  return (
    <React.Fragment>
      {props.name === 'Cash accounts' && (
        <Column
          mobile="6"
          tablet="5"
          desktop="3"
          className={cn(
            'is-flex is-align-items-center',
            activeBreakpoints.below.sm
              ? 'pt-2 pb-0'
              : activeBreakpoints.below.md
              ? 'pb-4'
              : activeBreakpoints.below.lg
              ? 'pt-0 pb-4'
              : 'py-8'
          )}>
          <div>
            <Subtitle
              size="7"
              className={`has-text-secondary is-uppercase is-spaced mb-0 ${error.includes(
                'retrieve-balances'
              ) && 'has-text-warning'}`}>
              {props.name !== 'Cash accounts' ? (
                <>
                  Funds available <br /> for trade
                </>
              ) : (
                'Available balance'
              )}
            </Subtitle>
            {error.includes('retrieve-balances') ? (
              <Tooltip
                bottom
                minWidth="150"
                tip="Unable to retrieve available balance this time">
                <Icon warning>warning-triangle</Icon>
              </Tooltip>
            ) : (
              <Title size="5" className="has-text-secondary mb-0">
                ${props.cash}
              </Title>
            )}
          </div>
        </Column>
      )}
      {props.change && (
        <Column
          mobile="6"
          className={cn(
            'is-flex is-align-items-center',
            activeBreakpoints.below.sm
              ? 'py-2'
              : activeBreakpoints.below.md
              ? 'pb-4'
              : activeBreakpoints.below.lg
              ? 'pt-0 pb-4'
              : 'py-8'
          )}>
          <div>
            <Subtitle size="7" className="has-text-secondary is-uppercase mb-4">
              <br />
              Today's change
            </Subtitle>
            <Title size="5" className="has-text-secondary mb-0">
              <span
                className={cn(
                  props.change > 0 ? 'has-text-success' : '',
                  props.change < 0 ? 'has-text-danger' : ''
                )}>
                <Icon small>
                  {props.change > 0
                    ? 'caret-up'
                    : props.change < 0
                    ? 'caret-down'
                    : ''}
                </Icon>
                {props.change}
                <sup style={{ position: 'relative', top: '4px' }}>%</sup>
              </span>
            </Title>
          </div>
        </Column>
      )}
      <Column
        mobile="6"
        tablet={props.name === 'Cash accounts' && '4'}
        desktop={props.name === 'Cash accounts' && '3'}
        className={cn(
          'is-flex is-align-items-center',
          activeBreakpoints.below.sm
            ? 'pt-2 pb-0'
            : activeBreakpoints.below.md
            ? 'pb-4'
            : activeBreakpoints.below.lg
            ? 'pt-0 pb-4'
            : 'py-8'
        )}>
        {props.value && (
          <div>
            <Subtitle
              size="7"
              className="has-text-secondary is-uppercase is-spaced mb-0">
              <br /> Market value
            </Subtitle>
            <Title size="5" className="has-text-secondary mb-0">
              ${props.value}
            </Title>
          </div>
        )}
        {props.balance && (
          <div>
            <Subtitle
              size="7"
              className="has-text-secondary is-spaced is-uppercase mb-0">
              Total Balance
            </Subtitle>
            <Title size="5" className="has-text-secondary mb-0">
              ${props.balance}
            </Title>
          </div>
        )}
      </Column>

      <Column
        mobile="6"
        className={cn(
          'is-flex is-align-items-center',
          activeBreakpoints.below.sm
            ? 'py-2'
            : activeBreakpoints.below.md
            ? 'pb-4'
            : activeBreakpoints.below.lg
            ? 'pt-0 pb-4'
            : 'py-8'
        )}>
        {props.gainloss && (
          <div>
            <Subtitle size="7" className="has-text-secondary is-uppercase mb-4">
              <br />
              Today's G/L
            </Subtitle>
            <Title size="5" className="has-text-secondary mb-0">
              <span
                className={cn(
                  props.gainloss > 0 ? 'has-text-success' : '',
                  props.gainloss < 0 ? 'has-text-danger' : ''
                )}>
                <Icon small>
                  {props.gainloss > 0
                    ? 'caret-up'
                    : props.gainloss < 0
                    ? 'caret-down'
                    : ''}
                </Icon>
                {props.gainloss}
                <sup style={{ position: 'relative', top: '4px' }}>%</sup>
              </span>
            </Title>
          </div>
        )}
      </Column>
      {props.name !== 'Cash accounts' && (
        <Column
          mobile="6"
          className={cn(
            'is-flex is-align-items-center',
            activeBreakpoints.below.sm
              ? 'pt-2 pb-0'
              : activeBreakpoints.below.md
              ? 'pb-4'
              : activeBreakpoints.below.lg
              ? 'pt-0 pb-4'
              : 'py-8'
          )}>
          <div>
            <Subtitle
              size="7"
              className={`has-text-secondary is-uppercase is-spaced mb-0 ${error.includes(
                'retrieve-balances'
              ) && 'has-text-warning'}`}>
              {props.name !== 'Cash accounts' ? (
                <>
                  Funds available <br /> for trade
                </>
              ) : (
                'Available balance'
              )}
            </Subtitle>
            {error.includes('retrieve-balances') ? (
              <Tooltip
                bottom
                minWidth="150"
                tip="Unable to retrieve available balance this time">
                <Icon warning>warning-triangle</Icon>
              </Tooltip>
            ) : (
              <Title
                size="5"
                className={`has-text-secondary ${
                  activeBreakpoints.below.sm ? 'mb-4' : 'mb-0'
                }`}>
                ${props.cash}
              </Title>
            )}
          </div>
        </Column>
      )}
    </React.Fragment>
  );
};

export default Breakpoints(CardDataHeaders);
