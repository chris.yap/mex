import React, { useState } from 'react';
import cn from 'classnames';
import { getState } from '../../context/StateProvider';
import { PortfolioItems } from '../../constants/';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { Portfolios, PersonalAccounts } from '../../constants/portfolio';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import {
  Breakpoints,
  Button,
  Card,
  CardHeader,
  CheckboxGroup,
  Checkbox,
  Collapse,
  Column,
  Columns,
  Container,
  Divider,
  Div,
  Dropdown,
  Icon,
  Label,
  Level,
  LevelItem,
  List,
  ListItem,
  Section,
  Subtitle,
  Tab,
  Tabs,
  Title,
  Tooltip
} from 'shaper-react';
import CardDataHeaders from './CardDataHeaders';
import CashAccounts from './CashAccounts/AccountsOverview';
import Investments from './Investments';

const PersonalPortfolio = ({ activeBreakpoints, ...props }) => {
  const [{ orderPad }, dispatch] = getState();
  const [expandedCard, setExpandedCard] = useState(null);
  const [portfolio] = useState(['domestic', 'international']);
  const [error, setError] = useState([]);
  const [errorHandling] = useState(true);

  let toggleAccount = index => {
    setExpandedCard(expandedCard === index ? null : index);
  };

  return (
    <LayoutDefault
      subnav={PortfolioItems}
      breadcrumb={Breadcrumbs.portfolio.personal.items}>
      <HeroView className="is-overflow-visible" zIndex="10">
        <Level>
          <LevelItem left>
            <Dropdown
              activator={
                <Title
                  className="font-nabimpact has-text-white mt-1 mb-0 is-flex"
                  size={activeBreakpoints.below.sm ? 3 : null}>
                  <span className="is-flex-1">Personal portfolio</span>
                  <Button square small fontSize="1.25rem" mx={4} mt={1}>
                    <Icon className="has-text-primary">arrow-down</Icon>
                  </Button>
                </Title>
              }>
              <List style={{ width: '18em' }}>
                <ListItem className="">
                  <div>
                    <Title
                      size="4"
                      className="has-text-primary mb-0 font-nabimpact">
                      All portfolios
                    </Title>
                  </div>
                </ListItem>
                {Portfolios.map(entity => {
                  return (
                    <ListItem key={entity.name} href={entity.link}>
                      <div>
                        <Title
                          size="4"
                          className="is-active has-text-black text--lighten-1 font-nabimpact">
                          {entity.name}
                        </Title>
                        <Subtitle size="6" className="has-text-secondary mb-0">
                          HIN: {entity.hin}
                        </Subtitle>
                      </div>
                    </ListItem>
                  );
                })}
              </List>
            </Dropdown>
          </LevelItem>
          <LevelItem
            right
            className={
              activeBreakpoints.below.sm &&
              `mt-2 is-flex is-justify-content-flex-start`
            }>
            <Div mr={4}>
              <Label
                className={`nab-label mb-0 ${
                  error.includes('retrieve-balances')
                    ? 'has-text-warning'
                    : 'has-text-white'
                }`}>
                Account Value
              </Label>
              {error.includes('retrieve-balances') ? (
                <Tooltip
                  bottom
                  minWidth="150"
                  tip="Unable to retrieve available balance this time">
                  <Icon warning>warning-triangle</Icon>
                </Tooltip>
              ) : (
                <Title
                  size="5"
                  className={`${activeBreakpoints.above.xs &&
                    'has-text-right'} ${
                    error.includes('retrieve-balances')
                      ? 'has-text-warning'
                      : 'has-text-white'
                  } mb-0`}>
                  $100,000
                </Title>
              )}
            </Div>

            <Button
              primary
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              <Icon>order</Icon>
              <span>Place order</span>
            </Button>
          </LevelItem>
        </Level>
      </HeroView>
      {/* <Section bg="black" dark py={6} className="is-overflow-visible">
        <Container>
         <Columns mobile>
            <Column mobile="12" tablet="12" desktop="5" widescreen="4">
              <Dropdown
                activator={
                  <Title
                    className="font-nabimpact has-text-white ma-0 is-flex"
                    size={activeBreakpoints.below.sm ? 3 : null}>
                    <span className="is-flex-1">Personal portfolio</span>
                    <Button square className="mx-4 my-0">
                      <Icon className="has-text-primary has-text-size-1">
                        arrow-down
                      </Icon>
                    </Button>
                  </Title>
                }>
                <List style={{ width: '18em' }}>
                  <ListItem className="">
                    <div>
                      <Title
                        size="4"
                        className="has-text-primary mb-0 font-nabimpact">
                        All portfolios
                      </Title>
                    </div>
                  </ListItem>
                  {Portfolios.map(entity => {
                    return (
                      <ListItem key={entity.name} href={entity.link}>
                        <div>
                          <Title
                            size="4"
                            className="is-active has-text-black text--lighten-1 font-nabimpact">
                            {entity.name}
                          </Title>
                          <Subtitle
                            size="6"
                            className="has-text-secondary mb-0">
                            HIN: {entity.hin}
                          </Subtitle>
                        </div>
                      </ListItem>
                    );
                  })}
                </List>
              </Dropdown>
            </Column>
            <Column desktop="7" widescreen="8" className="is-hidden-touch">
              <Columns>
                <Column className="pt-2">
                  <Label
                    className={`nab-label mb-0 ${
                      error.includes('retrieve-balances')
                        ? 'has-text-warning'
                        : 'has-text-white'
                    }`}>
                    Account Value
                  </Label>
                  {error.includes('retrieve-balances') ? (
                    <Tooltip
                      bottom
                      minWidth="150"
                      tip="Unable to retrieve available balance this time">
                      <Icon warning>warning-triangle</Icon>
                    </Tooltip>
                  ) : (
                    <Title
                      size="5"
                      className={`${
                        error.includes('retrieve-balances')
                          ? 'has-text-warning'
                          : 'has-text-white'
                      } mb-0`}>
                      $100,000
                    </Title>
                  )}
                </Column>
                {/* <Column className="pt-2">
                  <Label className="nab-label has-text-white mb-0">
                    Market value
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    $90,000
                  </Title>
                </Column>
                <Column className="pt-2">
                  <Label className="nab-label has-text-white mb-0">
                    Today's change
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <PercentageView dark value="11.00" />
                  </Title>
                </Column>
                <Column className="pt-2">
                  <Label className="nab-label has-text-white mb-0">
                    Total G/L
                  </Label>
                  <Title size="5" className="has-text-white mb-0">
                    <PercentageView dark value="-0.50" />
                  </Title>
                </Column> 
              </Columns>
            </Column>
          </Columns> 
        </Container>
      </Section>*/}

      <Section className={`py-4 ${activeBreakpoints.below.sm && 'px-0'}`}>
        <Container>
          <Tabs
            toggle
            rounded
            className="px-4"
            additionalContent={
              <Button
                id="button-export"
                small
                secondary
                className="is-hidden-mobile mr-0">
                <Icon>upload</Icon>
                <span>Export</span>
              </Button>
            }>
            <Tab label="Data" icon="data">
              {PersonalAccounts.map((account, accIndex) => {
                return (
                  <React.Fragment key={accIndex}>
                    <Card
                      style={{ position: 'static' }}
                      key={accIndex}
                      className={cn(
                        '',
                        (account.name !== 'Cash accounts' &&
                          portfolio.length === 0) ||
                          expandedCard === accIndex ||
                          activeBreakpoints.below.md
                          ? ''
                          : 'has-link',
                        activeBreakpoints.below.sm ? 'mb-1' : 'mb-4'
                      )}
                      href={
                        account.name !== 'Cash accounts' &&
                        portfolio.length === 0
                          ? account.link
                          : null
                      }>
                      <CardHeader
                        className={cn(
                          account.name !== 'Cash accounts' &&
                            portfolio.length === 0
                            ? ''
                            : 'has-pointer',
                          activeBreakpoints.below.sm ? 'pa-4' : 'py-4'
                        )}
                        onClick={
                          portfolio.length > 0
                            ? () => toggleAccount(accIndex)
                            : null
                        }>
                        <Columns mobile>
                          <Column>
                            <Columns mobile multiline>
                              <Column
                                mobile="12"
                                tablet="12"
                                desktop="12"
                                widescreen="4"
                                className={cn(
                                  'is-flex',
                                  activeBreakpoints.above.xs
                                    ? 'is-align-items-center'
                                    : '',
                                  activeBreakpoints.when.sm ? 'pb-0' : ''
                                )}>
                                <div className="is-flex-1">
                                  <Title
                                    className={cn(
                                      'has-text-secondary has-text-weight-bold has-text-size5 has-line-height-90',
                                      !account.desc
                                        ? 'mb-0'
                                        : activeBreakpoints.below.sm &&
                                          portfolio.length === 0
                                        ? 'mb-4'
                                        : error.includes('alert-history') ||
                                          error.includes('domestic-holdings')
                                        ? 'mb-3'
                                        : 'mb-4'
                                    )}>
                                    {account.name}
                                    {(error.includes('alert-history') ||
                                      error.includes('domestic-holdings')) &&
                                    account.name !== 'Cash accounts' ? (
                                      <span className="has-text-size-6">
                                        <Icon />
                                      </span>
                                    ) : (
                                      <span className="ml-2 has-text-black has-text-size-6">
                                        {account.name !== 'Cash accounts' && (
                                          <Icon danger>
                                            {account.name !== 'Cash accounts' &&
                                            portfolio.length === 0
                                              ? ''
                                              : 'alert'}
                                          </Icon>
                                        )}
                                      </span>
                                    )}
                                  </Title>
                                  {account.desc && (
                                    <Subtitle
                                      size="6"
                                      className="has-text-secondary text--lighten-1 mb-0">
                                      {account.desc}
                                    </Subtitle>
                                  )}
                                </div>
                                {account.name !== 'Cash accounts' &&
                                portfolio.length === 0 ? (
                                  <Button small square flat>
                                    <Icon />
                                  </Button>
                                ) : (
                                  <Button
                                    small
                                    square
                                    secondary={
                                      accIndex === expandedCard ? false : true
                                    }
                                    className="mt-1 is-hidden-tablet">
                                    <Icon>
                                      {accIndex === expandedCard
                                        ? 'arrow-up'
                                        : 'arrow-down'}
                                    </Icon>
                                  </Button>
                                )}
                              </Column>
                              {/* <Column
                    narrow={activeBreakpoints.above.sm ? true : false}
                    tablet={activeBreakpoints.when.sm ? '1' : null}
                    className={cn(
                      'is-flex is-align-items-center is-hidden-mobile',
                      activeBreakpoints.below.md ? '' : 'pr-4'
                    )}
                  >
                    <Icon
                      number={
                        account.name !== 'Cash accounts' && this.state.portfolio.length === 0 ? null : 1
                      }
                    >
                      {account.name !== 'Cash accounts' && this.state.portfolio.length === 0 ? '' : 'alert'}
                    </Icon>
                  </Column> */}
                              <Column
                                narrow
                                className="is-hidden-touch is-hidden-desktop-only">
                                <Divider vertical />
                              </Column>
                              {account.name !== 'Cash accounts' &&
                              portfolio.length === 0 ? (
                                <Column
                                  className={
                                    activeBreakpoints.below.sm ? 'pt-0' : ''
                                  }>
                                  <Title
                                    size={
                                      activeBreakpoints.below.lg ? '6' : '5'
                                    }
                                    className={cn(
                                      'has-text-blue-grey text--lighten-3',
                                      activeBreakpoints.below.xl
                                        ? ''
                                        : 'py-1 my-7'
                                    )}>
                                    No domestic or international holdings found
                                  </Title>
                                </Column>
                              ) : (
                                <CardDataHeaders
                                  {...account}
                                  error={error}
                                  portfolio={portfolio}
                                />
                              )}
                              <Column
                                narrow
                                className="py-10 is-flex is-align-items-center is-hidden-mobile">
                                {account.name !== 'Cash accounts' &&
                                portfolio.length === 0 ? (
                                  <Button flat square small />
                                ) : (
                                  <Button
                                    small
                                    square
                                    secondary={
                                      accIndex === expandedCard ? false : true
                                    }>
                                    <Icon>
                                      {accIndex === expandedCard
                                        ? 'arrow-up'
                                        : 'arrow-down'}
                                    </Icon>
                                  </Button>
                                )}
                              </Column>
                            </Columns>
                          </Column>
                        </Columns>
                      </CardHeader>
                      <Collapse isOpened={expandedCard === accIndex}>
                        {account.name === 'Investments' && (
                          <Investments
                            error={error}
                            toggleOrderPad={props.toggleOrderPad}
                            portfolio={portfolio}
                          />
                        )}
                        {account.name === 'Cash accounts' && (
                          <CashAccounts error={error} />
                        )}
                      </Collapse>
                    </Card>
                  </React.Fragment>
                );
              })}
            </Tab>
            <Tab label="Est dividends" icon="date"></Tab>
          </Tabs>
        </Container>
      </Section>

      {expandedCard !== 1 && !orderPad.isOpened && errorHandling && (
        <div
          className="pos-f px-4 py-2 has-bg-white has-elevation-2 "
          style={{ right: '1em', bottom: '1em', zIndex: 100 }}>
          <Columns mobile multiline>
            <Column>
              <Title size="6" className="mb-1">
                Portfolio error handling
              </Title>
              <CheckboxGroup
                stacked
                className="mb-0"
                name="error"
                value={error}
                onChange={e => setError(e)}>
                <Checkbox value="alert-history" label="Get Alerts History" />
                <Checkbox value="retrieve-holdings" label="Retrieve Holdings" />
                <Checkbox
                  value="retrieve-balances"
                  label="Retrieve Account Balances"
                />
                <Checkbox
                  value="domestic-holdings"
                  label="Get Domestic Holdings"
                />
                <Checkbox
                  value="international-holdings"
                  label="Get International Holdings"
                />
              </CheckboxGroup>
              <Title size="6" className="mb-1">
                Cash error handling
              </Title>
              <CheckboxGroup
                stacked
                className="mb-0"
                name="error"
                value={error}
                onChange={e => setError(e)}>
                <Checkbox
                  value="retrieve-future-transfers"
                  label="Retrieve Future Transfers"
                />
                <Checkbox
                  value="delete-future-transfers"
                  label="Delete future transfers"
                />
              </CheckboxGroup>
            </Column>

            {/* <Column narrow>
            <Title size="6" className="mb-1">
              Holdings
            </Title>
            <CheckboxGroup
              stacked
              className="mb-0"
              name="portfolio"
              value={portfolio}
              onChange={togglePortfolio}
            >
              <Checkbox value="domestic" label="Domestic" info />
              <Checkbox value="international" label="International" info />
            </CheckboxGroup>
          </Column> */}
          </Columns>
        </div>
      )}
    </LayoutDefault>
  );
};

export default Breakpoints(PersonalPortfolio);
