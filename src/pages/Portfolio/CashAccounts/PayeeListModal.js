import React, { useState } from 'react';
import { getState } from '../../../context/StateProvider';
//import cn from 'classnames';
import {
  Alert,
  Breakpoints,
  Button,
  Column,
  Columns,
  Level,
  LevelItem,
  Modal,
  Textfield,
  Title
} from 'shaper-react';

const PayeeListModal = ({
  activeBreakpoints,
  isOpened,
  closeModal,
  payeeListAction,
  selectedPayeeDetails,
  error,
  ...props
}) => {
  const [, dispatch] = getState();
  const [onChange, setOnChange] = useState('');
  const [payeeName, setPayeeName] = useState('');
  const [payeeBsb, setPayeeBsb] = useState('');
  const [payeeAccNum, setPayeeAccNum] = useState('');
  const updatePayeeName = value => {
    setPayeeName(value ? value.target.value : '');
  };
  const updatePayeeBsb = value => {
    setPayeeBsb(value ? value.target.value : '');
  };
  const updatePayeeAccNum = value => {
    setPayeeAccNum(value ? value.target.value : '');
  };
  const updateOnChange = value => {
    setOnChange(value);
  };
  const handleChange = () => {
    dispatch({ type: 'toggleOverlay', payload: true });
    setTimeout(() => {
      dispatch({ type: 'toggleOverlay', payload: false });
      if (error.includes('generic-error')) {
        updateOnChange('error');
      } else {
        updateOnChange('success');
      }
    }, 2000);
  };
  const clearSetValues = () => {
    updatePayeeName();
    updatePayeeBsb();
    updatePayeeAccNum();
    updateOnChange('');
  };
  return (
    <Modal
      closeIcon
      isOpened={isOpened}
      onClose={closeModal}
      className="has-bg-blue-grey lighten-5"
      header={
        <Title size="4" className="mb-0 is-flex is-align-items-center">
          {payeeListAction === 'create'
            ? 'Create a new payee'
            : payeeListAction === 'amend'
            ? 'Edit payee details'
            : 'Remove this payee'}
        </Title>
      }
      footer={
        <div className="is-flex-1">
          <Alert
            isOpened={onChange === 'error'}
            inverted
            warning
            mx={1}
            p={activeBreakpoints.below.md && 5}>
            <p className="pos-r has-top3 mb-1">
              We're currently experiencing some issues. Please try again later.
            </p>
          </Alert>
          <Level
            mobile
            style={{ width: '100%' }}
            className={`${onChange === 'error' ? 'mt-2' : ''}`}>
            <LevelItem left>
              {onChange !== 'success' && (
                <Button
                  primary
                  onClick={() => handleChange()}
                  disabled={
                    payeeListAction !== 'delete'
                      ? error.includes('invalid-data-input')
                      : ''
                  }>
                  {payeeListAction === 'create'
                    ? 'Create'
                    : payeeListAction === 'amend'
                    ? 'Save changes'
                    : 'Confirm'}
                </Button>
              )}
            </LevelItem>
            <LevelItem right>
              <Button
                flat
                onClick={() => {
                  closeModal();
                  clearSetValues();
                }}>
                {onChange === 'success' ? 'Close' : 'Cancel'}
              </Button>
            </LevelItem>
          </Level>
        </div>
      }>
      {onChange === 'success' ? (
        <Alert isOpened={true} inverted success>
          <p className="pos-r has-top3 mb-1">
            {payeeListAction === 'create'
              ? 'Your payee has been created'
              : payeeListAction === 'amend'
              ? 'Your payee details have been updated'
              : 'Your payee has been removed'}
          </p>
        </Alert>
      ) : (
        <Columns multiline mobile className="mb-0">
          {payeeListAction === 'create' ? (
            <>
              <Column size={12}>
                <Textfield
                  required
                  label="Account name"
                  placeholder="Enter an account name"
                  value={payeeName}
                  onChange={updatePayeeName}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter an account name"
                />
              </Column>
              <Column size={activeBreakpoints.below.sm ? 5 : 4}>
                <Textfield
                  required
                  label="BSB"
                  placeholder="Enter a BSB"
                  value={payeeBsb}
                  onChange={updatePayeeBsb}
                  maxLength={6}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter a valid BSB"
                />
              </Column>
              <Column>
                <Textfield
                  required
                  label="Account number"
                  placeholder="Enter an account number"
                  value={payeeAccNum}
                  onChange={updatePayeeAccNum}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter an account number"
                />
              </Column>
            </>
          ) : payeeListAction === 'amend' ? (
            <>
              <Column size={12}>
                <Textfield
                  required
                  label="Account name"
                  placeholder="Enter an account name"
                  value={payeeName ? payeeName : selectedPayeeDetails.name}
                  onChange={updatePayeeName}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter an account name"
                />
              </Column>
              <Column size={activeBreakpoints.below.sm ? 5 : 4}>
                <Textfield
                  required
                  label="BSB"
                  placeholder="Enter a BSB"
                  value={payeeBsb ? payeeBsb : selectedPayeeDetails.bsb}
                  onChange={updatePayeeBsb}
                  maxLength={6}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter a valid BSB"
                />
              </Column>
              <Column>
                <Textfield
                  required
                  label="Account number"
                  placeholder="Enter an account number"
                  value={
                    payeeAccNum ? payeeAccNum : selectedPayeeDetails.accNum
                  }
                  onChange={updatePayeeAccNum}
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter an account number"
                />
              </Column>
            </>
          ) : (
            <Column size={12}>
              <p className="mb-0">
                Are you sure you want to remove this payee from your payee list?
              </p>
            </Column>
          )}
        </Columns>
      )}
    </Modal>
  );
};

export default Breakpoints(PayeeListModal);
