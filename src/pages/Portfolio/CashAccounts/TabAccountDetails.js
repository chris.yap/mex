import React, { useState } from 'react';
import { getState } from '../../../context/StateProvider';
import {
  Breakpoints,
  Button,
  Card,
  CardContent,
  Collapse,
  Columns,
  Column,
  Container,
  Divider,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Subtitle,
  Textfield,
  Title
} from 'shaper-react';
import MoneyView from '../../../components/MoneyView';
import PercentageView from '../../../components/PercentageView';

const TabAccountDetails = ({ activeBreakpoints, ...props }) => {
  const [{ cash }, dispatch] = getState();
  const [changeLimit, setChangeLimit] = useState(false);
  return (
    <Container
      fluid
      className={`mx-0 mb-4 pt-2 ${
        activeBreakpoints.below.sm ? 'is-overflow-hidden' : ''
      }`}
    >
      <Columns mobile multiline>
        {props.type !== 'hia' ? (
          <>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Balance and limits
                </Subtitle>
                <Card outline>
                  <List>
                    {BalanceLimit.map((item, i) => (
                      <ListItem key={i}>
                        {item.name !== 'Daily limit' ? (
                          <Level mobile className="is-flex-1">
                            <LevelItem left>{item.name}</LevelItem>
                            <LevelItem right>
                              <MoneyView top={0} value={item.value} />
                            </LevelItem>
                          </Level>
                        ) : (
                          <div className="is-flex-1">
                            <Level mobile className="mb-0">
                              <LevelItem left>{item.name}</LevelItem>
                              <LevelItem right>
                                <div className="ma-0 is-justify-content-flex-end">
                                  <Button
                                    inverted={!cash.changeLimit}
                                    small
                                    square
                                    secondary={cash.changeLimit}
                                    primary={!cash.changeLimit}
                                    className="my-0 mr-4"
                                    onClick={() =>
                                      setChangeLimit(changeLimit ? false : true)
                                    }
                                  >
                                    <Icon>pencil</Icon>
                                  </Button>

                                  <MoneyView top={5} value={item.value} />
                                </div>
                              </LevelItem>
                            </Level>
                            <Divider dotted className="mt-4 mb-1" />
                            <p className="has-text-secondary mb-0">
                              <small>
                                A maximum daily pay anyone limit of{' '}
                                <strong>$20,000.00</strong> can be applied for
                                online transfers.
                              </small>
                            </p>
                            <Collapse isOpened={changeLimit}>
                              <Divider className={'mt-2 mb-4'} />
                              <div className="mt-2 has-text-right">
                                <Textfield
                                  prefix="$"
                                  label="Change daily limit"
                                  placeholder="Enter preferred limit"
                                  className="mb-2"
                                  value={item.value}
                                />
                                <div className="has-text-right">
                                  <Button
                                    flat
                                    className="mr-0"
                                    onClick={() => {
                                      setChangeLimit(
                                        changeLimit ? false : true
                                      );
                                    }}
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    primary
                                    className="mr-0"
                                    onClick={() =>
                                      dispatch({ type: 'toggleOTP' })
                                    }
                                  >
                                    Update limit
                                  </Button>
                                </div>
                              </div>
                            </Collapse>
                          </div>
                        )}
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </div>
            </Column>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Account info
                </Subtitle>
                <Card outline>
                  <List>
                    {AcctInfo.map((item, i) => (
                      <ListItem key={i}>
                        <Level mobile className="is-flex-1">
                          <LevelItem left>
                            {item.logo && (
                              <img
                                src={item.logo}
                                width="40"
                                alt={item.name}
                                className="mr-2"
                              />
                            )}
                            {item.name}
                          </LevelItem>
                          <LevelItem
                            right
                            className="has-text-weight-semibold has-text-size1"
                          >
                            {item.value}
                          </LevelItem>
                        </Level>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </div>
            </Column>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Funding the following accounts
                </Subtitle>
                <Columns mobile multiline>
                  {Accounts.map((item, i) => (
                    <Column
                      key={i}
                      mobile="12"
                      tablet="6"
                      className={activeBreakpoints.below.sm ? 'pb-0' : ''}
                    >
                      <Card>
                        <CardContent className="pr-4 pl-4">
                          <Title size="5" className="is-flex-1 mb-0">
                            {item.name}
                          </Title>
                          <Title size="6" className="mb-0 has-text-secondary">
                            {item.type}
                          </Title>
                          <p className="mb-0 has-text-secondary has-text-size-1">
                            Account #: {item.number}
                          </p>
                        </CardContent>
                      </Card>
                    </Column>
                  ))}
                </Columns>
              </div>
            </Column>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Interest
                </Subtitle>
                <Card outline>
                  <List>
                    {Interest.map((item, i) => (
                      <ListItem key={i}>
                        <Level mobile className="is-flex-1">
                          <LevelItem left>{item.name}</LevelItem>
                          <LevelItem
                            right
                            className="has-text-weight-semibold has-text-size1"
                          >
                            <MoneyView top="0" value={item.value} />
                          </LevelItem>
                        </Level>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </div>
            </Column>
          </>
        ) : (
          <>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Interest
                </Subtitle>
                <Card outline>
                  <List>
                    {HIAInterest.map((item, i) => (
                      <ListItem key={i}>
                        <Level mobile className="is-flex-1">
                          <LevelItem left>{item.name}</LevelItem>
                          <LevelItem
                            right
                            className="has-text-weight-semibold has-text-size1"
                          >
                            {item.name === 'Interest rate' ? (
                              <PercentageView noArrows value={item.value} />
                            ) : (
                              <MoneyView top="0" value={item.value} />
                            )}
                          </LevelItem>
                        </Level>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </div>
            </Column>
            <Column mobile="12" tablet="6">
              <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                <Subtitle className="has-text-black has-text-size1 mb-2">
                  Account info
                </Subtitle>
                <Card outline>
                  <List>
                    {HIAAcctInfo.map((item, i) => (
                      <ListItem key={i}>
                        <Level mobile className="is-flex-1">
                          <LevelItem left>{item.name}</LevelItem>
                          <LevelItem
                            right
                            className="has-text-weight-semibold has-text-size1"
                          >
                            {item.value}
                          </LevelItem>
                        </Level>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </div>
            </Column>
          </>
        )}
      </Columns>
      {props.type !== 'hia' && (
        <div className={activeBreakpoints.below.sm && 'px-4'}>
          <Divider className="mb-2" />
          <small className="has-text-blue-grey">
            ® Registered to BPAY Pty Ltd ABN 69 079 137 518
          </small>
        </div>
      )}
    </Container>
  );
};

export default Breakpoints(TabAccountDetails);

const Accounts = [
  {
    name: 'Personal Portfolio',
    type: 'Trading account',
    number: 'NT1123456765-001'
  },
  {
    name: 'Personal Portfolio',
    type: 'International Trading account',
    number: 'NT1123456765-002'
  }
];

const Interest = [
  { name: 'This month', value: '93.88' },
  { name: 'Current financial year', value: '238.47' },
  { name: 'Last financial year', value: '1,382.19' }
];

const AcctInfo = [
  { name: 'Product name', value: 'Cash Account' },
  { name: 'Account holder', value: 'John Doe' },
  { name: 'Date created', value: '12-05-2013' },
  {
    name: 'BPay® biller code',
    logo: '/images/BPAY_2012_LAND_BLUE.png',
    value: '102343'
  },
  {
    name: 'BPay® reference number',
    logo: '/images/BPAY_2012_LAND_BLUE.png',
    value: '2345342898743983'
  }
];

const BalanceLimit = [
  { name: 'Total balance', value: '210,000.00' },
  { name: 'Available balance', value: '100,000.00' },
  { name: 'Daily limit', value: '20,000.00' },
  { name: 'Daily limit remaining', value: '0.00' }
];

const HIAInterest = [
  { name: 'Interest rate', value: '2.15' },
  { name: 'This month', value: '93.88' },
  { name: 'Current financial year', value: '238.47' },
  { name: 'Last financial year', value: '1,382.19' }
];

const HIAAcctInfo = [
  { name: 'Product name', value: 'Cash Account' },
  { name: 'Account holder', value: 'John Doe' },
  { name: 'Date created', value: '12-05-2013' }
];
