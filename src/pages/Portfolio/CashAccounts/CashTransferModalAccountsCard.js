import React from 'react';
import cn from 'classnames';
import {
  Breakpoints,
  Card,
  CardContent,
  Icon,
  Subtitle,
  Title
} from 'shaper-react';
import PercentageView from '../../../components/PercentageView';

const CashTransferModalAccountsCard = ({
  activeBreakpoints,
  accountDetails,
  onClick,
  step,
  ...props
}) => {
  return (
    <Card href onClick={onClick} className="nab-tile">
      <CardContent className="is-flex-1">
        <Icon className="has-text-primary is-pulled-right">arrow-right</Icon>

        {accountDetails.portfolio && (
          <Title
            size={step === 3 && activeBreakpoints.below.sm ? 6 : 5}
            className={
              step === 3 && activeBreakpoints.below.sm ? 'mb-0' : 'mb-1'
            }
          >
            {accountDetails.portfolio}
          </Title>
        )}
        <Title
          size={step === 3 && activeBreakpoints.below.sm ? 6 : 5}
          className="mb-1 has-text-secondary"
        >
          {accountDetails.name}
        </Title>
        <Subtitle
          size="7"
          className="my-0 has-text-secondary has-line-height-100"
        >
          {accountDetails.bsb && (
            <>
              BSB: {accountDetails.bsb} <br />
            </>
          )}
          {accountDetails.accNum && <>Account #: {accountDetails.accNum}</>}
          {accountDetails.interest && (
            <>
              <br />
              Interest rate:{' '}
              <PercentageView noArrows value={accountDetails.interest} />
            </>
          )}
        </Subtitle>
        {accountDetails.available && (
          <div
            className={cn(
              'is-flex ',
              step === 3 && activeBreakpoints.below.sm ? '' : 'mt-4'
            )}
          >
            <Subtitle
              size="7"
              className="my-0 has-text-secondary has-line-height-100 has-text-black is-flex-1"
            >
              Available balance:
            </Subtitle>
            <Subtitle
              size="7"
              className="my-0 has-text-secondary has-line-height-100 has-text-black "
            >
              <strong>${accountDetails.available}</strong>
            </Subtitle>
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default Breakpoints(CashTransferModalAccountsCard);
