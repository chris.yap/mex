import React from 'react';
import { getState } from '../../../context/StateProvider';
import {
  Breakpoints,
  Button,
  Dropdown,
  Icon,
  List,
  ListItem
} from 'shaper-react';

const DropdownOrButton = ({ activeBreakpoints, acct, dropdown, ...props }) => {
  const [, dispatch] = getState();
  return (
    <>
      {dropdown ? (
        <Dropdown
          right={activeBreakpoints.above.sm ? true : false}
          activator={
            <Button className="ma-0">
              <span>Transfer</span>
              <Icon>arrow-down</Icon>
            </Button>
          }
          className={props.className || null}>
          <List>
            <ListItem
              className="has-pointer"
              onClick={() => {
                dispatch({ type: 'toggleCashTransferModel', payload: true });
                dispatch({
                  type: 'updateTransferTarget',
                  payload: 'nab accounts'
                });
                dispatch({ type: 'getDefaultFromAccount', payload: acct });
                dispatch({ type: 'handleStep', payload: 2 });
              }}>
              NAB Accounts
            </ListItem>
            <ListItem
              className="has-pointer"
              onClick={() => {
                dispatch({ type: 'toggleCashTransferModel', payload: true });
                dispatch({
                  type: 'updateTransferTarget',
                  payload: 'pay anyone'
                });
                dispatch({ type: 'getDefaultFromAccount', payload: acct });
                dispatch({ type: 'handleStep', payload: 2 });
              }}>
              Pay anyone
            </ListItem>
          </List>
        </Dropdown>
      ) : (
        <Button
          className={`ma-0 ${props.className || null}`}
          onClick={() => {
            dispatch({ type: 'toggleCashTransferModel', payload: true });
            dispatch({
              type: 'updateTransferTarget',
              payload: 'nab accounts'
            });
            dispatch({ type: 'getDefaultFromAccount', payload: acct });
            dispatch({ type: 'handleStep', payload: 2 });
          }}>
          Transfer funds
        </Button>
      )}
    </>
  );
};

export default Breakpoints(DropdownOrButton);
