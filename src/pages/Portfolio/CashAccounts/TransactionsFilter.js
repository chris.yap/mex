import React from 'react';
import {
  Button,
  Columns,
  Column,
  DatePicker,
  Radio,
  RadioGroup,
  Select,
  Textfield
} from 'shaper-react';

const TransactionFilters = ({ ...props }) => {
  const [dateType, setDateType] = React.useState('period');
  const [amountType, setAmountType] = React.useState('value');
  const [transType, setTransType] = React.useState('all');

  const [startDate, setStartDate] = React.useState();
  const [endDate, setEndDate] = React.useState();
  const [focused, updateFocused] = React.useState(null);
  return (
    <>
      <Columns mobile multiline className="mb-0">
        <Column mobile="12" desktop="4">
          <RadioGroup
            name="dateType"
            selectedValue={dateType}
            buttons
            fullwidth
            label="Dates"
            onChange={setDateType}>
            <Radio button label="Time period" value="period" />
            <Radio button label="Date range" value="range" />
          </RadioGroup>
          {dateType === 'range' ? (
            <Columns mobile>
              <Column>
                <DatePicker
                  range
                  showClearDates
                  startDate={startDate}
                  endDate={endDate}
                  onDatesChange={({ startDate, endDate }) => {
                    setStartDate(startDate);
                    setEndDate(endDate);
                  }}
                  label="Date range"
                  startDatePlaceholderText="Date from"
                  endDatePlaceholderText="Date to"
                  mb={0}
                  focusedInput={focused}
                  onFocusChange={updateFocused}
                />
              </Column>
            </Columns>
          ) : (
            <Select value={Range[0]} label="Time period" options={Range} />
          )}
        </Column>
        <Column mobile="12" desktop="4">
          {/* <RadioGroup
            name="amountType"
            selectedValue={amountType}
            buttons
            fullwidth
            label="Amount"
            onChange={setAmountType}>
            <Radio button label="Amount range" value="range" />
            <Radio button label="Value" value="value" />
          </RadioGroup>
          {amountType === 'range' ? (
            <Columns mobile>
              <Column>
                <Textfield
                  prefix="$"
                  placeholder="Enter min"
                  label="Minimum amount"
                />
              </Column>
              <Column>
                <Textfield
                  prefix="$"
                  placeholder="Enter max"
                  label="Maximum amount"
                />
              </Column>
            </Columns>
          ) : ( */}
          {/* <Textfield prefix="$" placeholder="Enter amount" label="Amount" /> */}
          <Textfield placeholder="Enter description" label="Description" />
          {/* )} */}
        </Column>
        <Column mobile="12" desktop="4">
          <RadioGroup
            name="transType"
            buttons
            fullwidth
            label="Transaction type"
            selectedValue={transType}
            onChange={setTransType}>
            <Radio button label="All" value="all" />
            <Radio button label="Debit" value="debit" />
            <Radio button label="Credit" value="credit" />
            <Radio button label="Interest" value="interest" />
          </RadioGroup>
        </Column>
      </Columns>
      <p className="has-text-centered">
        <Button
          flat
          onClick={() => {
            setDateType('period');
            setAmountType('range');
            setTransType('all');
          }}
          disabled={
            dateType === 'period' &&
            amountType === 'range' &&
            transType === 'all'
          }>
          Reset filters
        </Button>

        <Button
          secondary
          onClick={props.toggleFilter}
          disabled={
            dateType === 'period' &&
            amountType === 'range' &&
            transType === 'all'
          }>
          Apply
        </Button>

        <Button onClick={props.toggleFilter}>Close</Button>
      </p>
    </>
  );
};

export default TransactionFilters;

const Range = [
  { label: 'Last 7 days' },
  { label: 'Last 30 days' },
  { label: 'Last 3 months' },
  { label: 'Last 6 months' },
  { label: 'Last 12 months' },
  { label: 'YTD' }
];
