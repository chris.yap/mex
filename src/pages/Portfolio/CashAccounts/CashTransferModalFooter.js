import React from 'react';
import { getState } from '../../../context/StateProvider';
import { Alert, Breakpoints, Button, Level, LevelItem } from 'shaper-react';

const CashTransferModalFooter = ({
  step,
  error,
  transferTarget,
  clearSetValues,
  payeeAccount,
  payeeList,
  closeModal,
  ...props
}) => {
  const [{ cash }, dispatch] = getState();
  const submitOverlay = value => {
    dispatch({ type: 'toggleOverlay', payload: value });
  };

  const submitTransfer = () => {
    if (step === 4) {
      if (error.includes('create-order')) {
        submitOverlay(true);
        setTimeout(() => {
          submitOverlay(false);
          dispatch({ type: 'updateSubmissionError', payload: true });
        }, 2000);
      } else {
        if (transferTarget === 'pay anyone') {
          dispatch({ type: 'handleStep', payload: step + 1 });
          dispatch({ type: 'updateSubmissionError', payload: false });
        } else {
          submitOverlay(true);
          setTimeout(() => {
            submitOverlay(false);
            dispatch({ type: 'handleStep', payload: 6 });
            dispatch({ type: 'updateSubmissionError', payload: false });
          }, 2000);
        }
      }
    } else {
      submitOverlay(true);
      setTimeout(() => {
        submitOverlay(false);
        dispatch({ type: 'handleStep', payload: step + 1 });
        dispatch({ type: 'updateSubmissionError', payload: false });
      }, 2000);
    }
  };
  return (
    <div className="is-flex-1">
      <Alert isOpened={cash.submissionError} inverted warning mx={1}>
        <p className="pos-r has-top3 mb-1">
          We're currently experiencing some issues. Please try again later.
        </p>
      </Alert>
      <Level
        mobile
        style={{ width: '100%' }}
        className={`${cash.submissionError ? 'mt-2' : ''}`}>
        <LevelItem left>
          {step === 2 &&
          transferTarget === 'pay anyone' &&
          payeeAccount === 'new payee' ? (
            <Button
              primary
              onClick={() => {
                dispatch({ type: 'handleStep', payload: step + 1 });
                dispatch({
                  type: 'updateSelectedAccount',
                  payload: payeeList[0]
                });
                dispatch({
                  type: 'updateTransferType',
                  payload: 'to'
                });
              }}
              disabled={error.includes('invalid-data-input')}
              loading={false}>
              Next
            </Button>
          ) : step === 3 ? (
            <Button
              primary
              onClick={() =>
                dispatch({ type: 'handleStep', payload: step + 1 })
              }
              disabled={error.includes('invalid-data-input')}
              loading={false}>
              Review
            </Button>
          ) : step === 4 ? (
            <Button
              primary
              onClick={() => {
                submitTransfer();
              }}
              disabled={error.includes('invalid-data-input')}>
              {cash.modalView === 'amend'
                ? 'Amend transfer'
                : 'Submit transfer'}
            </Button>
          ) : step === 5 && !error.includes('create-order') ? (
            <Button
              primary
              onClick={() => submitTransfer()}
              disabled={error.includes('invalid-data-input')}>
              Complete transfer
            </Button>
          ) : step === 6 &&
            cash.modalView !== 'amend' &&
            !error.includes('create-order') ? (
            <Button
              primary
              loading={false}
              onClick={() => {
                clearSetValues(cash.newTransfer);
              }}
              disabled={error.includes('invalid-data-input')}>
              New transfer
            </Button>
          ) : (
            ''
          )}
        </LevelItem>
        <LevelItem right>
          <Button
            flat
            onClick={() => {
              dispatch({ type: 'toggleCashTransferModel', payload: false });
              clearSetValues();
            }}>
            {step === 6 ? 'Close' : 'Cancel'}
          </Button>
        </LevelItem>
      </Level>
    </div>
  );
};

export default Breakpoints(CashTransferModalFooter);
