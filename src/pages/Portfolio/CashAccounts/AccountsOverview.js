import React, { useState } from 'react';
import {
  Breakpoints,
  Button,
  Collapse,
  Columns,
  Column,
  Div,
  Divider,
  Icon,
  List,
  ListItem,
  Section,
  Subtitle,
  Tab,
  Tabs,
  Title
} from 'shaper-react';
import DropdownOrButton from './DropdownOrButton';
import TabTransactions from './TabTransactions';
import TabFutureTransfers from './TabFutureTransfers';
import TabAccountDetails from './TabAccountDetails';
import { CashAccountDetails } from '../../../constants/portfolio';

const CashAccounts = ({ activeBreakpoints, error, ...props }) => {
  const [expand, setExpand] = useState(null);
  const [selectedTab, setSelectedTab] = useState(0);

  return (
    <React.Fragment>
      <List className="has-bg-blue-grey is-lighten-5">
        {CashAccountDetails.map((Acct, a) => (
          <React.Fragment key={a}>
            <ListItem
              block
              className="has-pointer"
              style={{
                padding: activeBreakpoints.below.sm ? '1rem' : '1rem 1.5rem'
              }}
              onClick={() => setExpand(expand === a ? null : a)}>
              <Div flex={1}>
                <Columns mobile multiline className="is-flex-1">
                  <Column
                    mobile="10"
                    tablet="8"
                    desktop="8"
                    widescreen="4"
                    className={activeBreakpoints.above.lg ? 'py-4' : 'pb-4'}>
                    <Title size="4" className="mb-0 has-text-secondary">
                      {Acct.name}
                    </Title>
                    <Subtitle
                      size="6"
                      className="my-0 has-text-secondary has-line-height-100">
                      {Acct.bsb && (
                        <>
                          BSB: {Acct.bsb} <br />
                        </>
                      )}
                      Account #: {Acct.accNum}
                      {Acct.interest && (
                        <>
                          <br />
                          Interest rate: <strong>{Acct.interest}</strong>%
                        </>
                      )}
                    </Subtitle>
                  </Column>
                  <Column
                    mobile="2"
                    tablet="4"
                    className={`is-hidden-widescreen is-flex is-justify-content-flex-end py-4 ${!activeBreakpoints
                      .below.sm && 'is-align-items-center'}`}>
                    {(Acct.name === 'NT cash' ||
                      Acct.name === 'High interest') && (
                      <DropdownOrButton
                        acct={Acct}
                        dropdown={Acct.name === 'NT cash'}
                        className="is-hidden-mobile"
                      />
                    )}
                    <Button
                      small
                      square
                      secondary={a !== expand}
                      className="mr-0">
                      <Icon>{a !== expand ? 'arrow-down' : 'arrow-up'}</Icon>
                    </Button>
                  </Column>
                  <Column
                    narrow
                    className="is-hidden-touch is-hidden-desktop-only">
                    <Divider vertical />
                  </Column>

                  <Column
                    className={`is-flex is-align-items-center ${
                      activeBreakpoints.below.sm
                        ? 'pt-2 pb-0'
                        : activeBreakpoints.below.lg
                        ? 'pt-0'
                        : 'pb-4'
                    }`}>
                    <Columns mobile multiline className="is-flex-1">
                      <Column
                        mobile={activeBreakpoints.below.md && '6'}
                        desktop={activeBreakpoints.below.lg && '4'}>
                        {Acct.available && (
                          <div>
                            <Subtitle
                              size="7"
                              className="has-text-secondary is-uppercase is-spaced mb-0">
                              Available balance
                            </Subtitle>
                            <Title size="5" className="has-text-secondary mb-0">
                              ${Acct.available}
                            </Title>
                          </div>
                        )}
                      </Column>
                      <Column
                        mobile={activeBreakpoints.below.md && '6'}
                        desktop={activeBreakpoints.below.lg && '4'}>
                        {Acct.balance && (
                          <div>
                            <Subtitle
                              size="7"
                              className="has-text-secondary is-uppercase is-spaced mb-0">
                              Total balance
                            </Subtitle>
                            <Title size="5" className="has-text-secondary mb-0">
                              ${Acct.balance}
                            </Title>
                          </div>
                        )}
                      </Column>

                      <Column
                        mobile={activeBreakpoints.below.md && '6'}
                        desktop={activeBreakpoints.below.lg && '4'}
                        className={activeBreakpoints.when.xs && 'pt-0'}>
                        {Acct.earned && (
                          <div>
                            <Subtitle
                              size="7"
                              className="has-text-secondary is-uppercase is-spaced mb-0">
                              Interest earned (FYTD)
                            </Subtitle>
                            <Title size="5" className="has-text-secondary mb-0">
                              ${Acct.earned}
                            </Title>
                          </div>
                        )}
                      </Column>

                      <Column className="is-flex is-align-items-center is-justify-content-flex-end is-hidden-touch is-hidden-desktop-only">
                        {(Acct.name === 'NT cash' ||
                          Acct.name === 'High interest') && (
                          <DropdownOrButton
                            acct={Acct}
                            dropdown={Acct.name === 'NT cash'}
                          />
                        )}
                        <Button
                          small
                          square
                          secondary={a !== expand}
                          className="mr-0">
                          <Icon>
                            {a !== expand ? 'arrow-down' : 'arrow-up'}
                          </Icon>
                        </Button>
                      </Column>
                    </Columns>
                  </Column>
                </Columns>

                {/* Show CTA for mobile only */}
                {activeBreakpoints.below.sm && (
                  <Columns className="is-flex-1">
                    <Column>
                      {(Acct.name === 'NT cash' ||
                        Acct.name === 'High interest') && (
                        <DropdownOrButton
                          acct={Acct}
                          dropdown={Acct.name === 'NT cash'}
                        />
                      )}
                    </Column>
                  </Columns>
                )}
              </Div>
            </ListItem>

            <Collapse isOpened={expand === a}>
              <Section
                position="relative"
                px={activeBreakpoints.below.sm && 0}
                pb={4}
                pt={4}>
                <Tabs className="mb-2" selectedTab={selectedTab}>
                  <Tab label="Transactions" tabClick={() => setSelectedTab(0)}>
                    <TabTransactions />
                  </Tab>
                  <Tab
                    label="Future transfers"
                    tabClick={() => setSelectedTab(1)}>
                    <TabFutureTransfers error={error} />
                  </Tab>
                  <Tab
                    label="Account details"
                    tabClick={() => setSelectedTab(2)}>
                    <TabAccountDetails type={Acct.type} />
                  </Tab>
                </Tabs>
              </Section>
              <Divider />
            </Collapse>
          </React.Fragment>
        ))}
      </List>
    </React.Fragment>
  );
};

export default Breakpoints(CashAccounts);

// const Accts = [
//   {
//     name: 'NT cash',
//     type: 'cash',
//     portfolio: 'Personal Portfolio',
//     link: '/portfolio/personal/cash-account',
//     bsb: '064 234',
//     accNum: '1000 5678',
//     interest: '',
//     balance: '210,000.00',
//     available: '100,000.00',
//     earned: ''
//   },
//   {
//     name: 'High interest',
//     type: 'hia',
//     portfolio: 'SMSF Portfolio',
//     bsb: '',
//     accNum: 'NT1234556789-003',
//     interest: '2.15',
//     balance: '90,000.00',
//     available: '50,000.00',
//     earned: '3,000.00'
//   },
//   {
//     name: 'Cash managed',
//     type: 'cma',
//     portfolio: 'Personal Portfolio',
//     bsb: '062 345',
//     accNum: '1234 5678',
//     interest: '',
//     balance: '70,000.00',
//     available: '70,000.00'
//   }
// ];
