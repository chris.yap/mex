import React from 'react';
import Moment from 'react-moment';
import cn from 'classnames';
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Container,
  Collapse,
  Column,
  Columns,
  Divider,
  Dropdown,
  Hero,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Section,
  Subtitle,
  Tab,
  Tabs,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Textfield,
  Title
} from 'shaper-react';
import CashTransferModal from './CashTransferModal';
import Filters from './Filters';
import { CashTransactions } from '../../../constants/portfolio';
import LayoutPortfolio from '../../../layout/LayoutPortfolio';

export default class CashAccount extends React.Component {
  constructor(props) {
    super();
    this.state = {
      filter: false,
      transferModal: false,
      transferTarget: null
    };
  }
  toggleFilter() {
    this.setState({
      filter: !this.state.filter
    });
  }
  activateTransferModal(e) {
    this.setState({
      transferModal: true
    });
  }
  render() {
    return (
      <LayoutPortfolio
        breadcrumb={
          <Breadcrumb className="mt-2">
            <BreadcrumbItem href="/">Home</BreadcrumbItem>
            <BreadcrumbItem href="/portfolio">Portfolio</BreadcrumbItem>
            <BreadcrumbItem href="/portfolio/personal">
              Personal portfolio
            </BreadcrumbItem>
            <BreadcrumbItem href="/portfolio/personal/account/" active>
              NT cash
            </BreadcrumbItem>
          </Breadcrumb>
        }>
        <CashTransferModal isOpened={this.state.transferModal} />
        <Hero dark className="py-4 has-bg-black is-overflow-visible">
          <Container>
            <Columns>
              <Column desktop="6">
                <Title size="4" className="mb-4">
                  NT Cash
                </Title>
                <Subtitle size="6" className="mb-0">
                  BSB: 06 4234 <br />
                  Account #: 1000 5678
                </Subtitle>
              </Column>
              <Column>
                <Columns className="mb-0">
                  <Column mobile="6" desktop="4">
                    <Subtitle size="7" className="is-uppercase mb-4">
                      Total balance
                    </Subtitle>
                    <Title size="5" className="mb-0">
                      $5,000.00
                    </Title>
                  </Column>
                  <Column>
                    <Subtitle size="7" className="is-uppercase mb-4">
                      Available balance
                    </Subtitle>
                    <Title size="5" className="mb-0">
                      $5,000.00
                    </Title>
                  </Column>
                </Columns>
                <div>
                  <Dropdown
                    className="mr-2"
                    activator={
                      <Button secondary inverted className="ml-0">
                        <span>Transfer funds</span>
                        <Icon>caret-down</Icon>
                      </Button>
                    }>
                    <List light>
                      <ListItem
                        onClick={() => this.activateTransferModal('linked')}>
                        Linked accounts
                      </ListItem>
                      <ListItem>Pay anyone</ListItem>
                      <ListItem>BPay</ListItem>
                    </List>
                  </Dropdown>
                  <Dropdown
                    activator={
                      <Button dark>
                        <span>Manage</span>
                        <Icon>caret-down</Icon>
                      </Button>
                    }>
                    <List light>
                      <ListItem>...</ListItem>
                    </List>
                  </Dropdown>
                </div>
              </Column>
            </Columns>
          </Container>
        </Hero>
        <Section className="pt-4">
          <Container>
            <Tabs>
              <Tab label="Transactions">
                <Level mobile>
                  <LevelItem left>
                    <Textfield
                      prependIcon="search"
                      placeholder="search"
                      hint={false}
                    />
                  </LevelItem>
                  <LevelItem right>
                    <Button
                      square
                      flat
                      className={cn(
                        'mt-0',
                        this.state.filter ? 'is-active' : ''
                      )}
                      onClick={() => this.toggleFilter()}>
                      <Icon>filter</Icon>
                    </Button>
                    <Divider vertical />
                    <Button flat className="mt-0 mr-0">
                      <Icon>download</Icon>
                      <span className="is-hidden-touch">Download</span>
                    </Button>
                  </LevelItem>
                </Level>
                <Collapse isOpened={this.state.filter}>
                  <Filters close={() => this.toggleFilter()} />
                </Collapse>
                <Table
                  hasStripes
                  headers={headers}
                  data={CashTransactions}
                  tableRow={TableRow}
                  defaultSortCol={'date'}
                />
              </Tab>
              <Tab label="Account details">456</Tab>
            </Tabs>
          </Container>
        </Section>
      </LayoutPortfolio>
    );
  }
}

const headers = [
  {
    name: 'Date',
    value: 'date',
    sort: true
  },
  {
    name: 'Type',
    value: 'type',
    sort: true
  },
  {
    name: 'Description'
  },
  {
    name: 'Debit',
    value: 'debit',
    align: 'right',
    sort: 'number'
  },
  {
    name: 'Credit',
    value: 'credit',
    align: 'right',
    sort: 'number'
  },
  {
    name: 'Balance',
    value: 'balance',
    align: 'right',
    sort: 'number'
  }
];

const TableRow = ({ row }) => (
  <Row>
    <Cell>
      <Moment unix format="DD.MM.YYYY">
        {row.date}
      </Moment>
    </Cell>
    <Cell>{row.type}</Cell>
    <Cell>{row.desc}</Cell>
    <Cell className="has-text-right">${row.debit}</Cell>
    <Cell className="has-text-right">${row.credit}</Cell>
    <Cell className="has-text-right">${row.balance}</Cell>
  </Row>
);
