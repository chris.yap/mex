import React, { useState, useRef } from 'react';
//import { getState } from '../../../context/StateProvider';
import cn from 'classnames';
import {
  Alert,
  Breakpoints,
  Button,
  Card,
  CardHeader,
  CheckboxGroup,
  Checkbox,
  Column,
  Columns,
  Container,
  Hero,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Section,
  Select,
  Textfield,
  Title,
  Subtitle
} from 'shaper-react';
// import {
//   disableBodyScroll,
//   enableBodyScroll
//   // clearAllBodyScrollLocks
// } from 'body-scroll-lock';
import { Breadcrumbs } from '../../../constants/breadcrumbs';
import LayoutDefault from '../../../layout/LayoutDefault';
import PayeeListModal from './PayeeListModal';
import { CashPayeeListDetails } from '../../../constants/portfolio';

const PayeeList = ({ activeBreakpoints, ...props }) => {
  const targetRef = useRef();
  const [error, setError] = useState([]);
  const [errorHandling] = useState(false);
  const sortedPayeeListDetails = CashPayeeListDetails.sort((a, b) => {
    return a.name.localeCompare(b.name);
  }, {});
  const getPayeeListNames = CashPayeeListDetails.map(item => {
    return { label: item.name, value: item.name };
  }, {});
  const getAlphabetIndex = CashPayeeListDetails.map(item => {
    return item.name.charAt(0);
  }, {});
  const newPayeeListArray = CashPayeeListDetails.reduce((array, item) => {
    let alpha = item.name.charAt(0).toLocaleUpperCase();
    array[alpha] ? array[alpha].push(item) : (array[alpha] = [item]);
    return array;
  }, {});

  const alphabet = [...new Set(getAlphabetIndex)];
  // const [selectedPayeeName, setSelectedPayeeName] = useState();
  const [selectedPayeeDetails, setSelectedPayeeDetails] = useState(
    sortedPayeeListDetails[0]
  );
  const [payeeListModal, setPayeeListModal] = useState(false);
  const [payeeListAction, setPayeeListAction] = useState();

  const handleChange = value => {
    // const objName = { label: value.name, value: value.name };
    const obj = CashPayeeListDetails.find(item => item.name === value.label);
    setSelectedPayeeDetails(obj ? obj : value);
    // setSelectedPayeeName(objName ? objName : value);
  };
  const activatePayeeListModal = value => {
    setPayeeListModal(value ? true : false);
  };
  const activatePayeeListAction = value => {
    setPayeeListAction(value);
  };

  // useEffect(() => {
  //   if (payeeListModal) {
  //     disableBodyScroll(targetRef);
  //   } else {
  //     enableBodyScroll(targetRef);
  //   }
  // });
  return (
    <LayoutDefault breadcrumb={Breadcrumbs.payeeList.items}>
      <Hero bg="black" py={4}>
        <Container>
          <Title className="font-nabimpact has-text-white my-0">
            Payee list
          </Title>
        </Container>
      </Hero>

      <Section
        ref={targetRef}
        className={cn('py-4', activeBreakpoints.below.sm && 'px-0')}>
        {error.includes('retrieve-payee-list') ? (
          <Container>
            <Columns>
              <Column>
                <Alert isOpened={true} inverted warning>
                  <p className="pos-r has-top3 mb-1">
                    We're currently experiencing some issues. Please try again
                    later.
                  </p>
                </Alert>
              </Column>
            </Columns>
          </Container>
        ) : (
          <Container>
            <Columns
              mobile
              multiline
              className={activeBreakpoints.below.sm && 'px-4 py-2'}>
              <Column size={12}>
                <p className="mb-0">Search your payee list</p>
              </Column>

              <Column
                mobile={7}
                tablet={4}
                desktop={4}
                widescreen={3}
                className="py-0">
                {activeBreakpoints.above.sm ? (
                  <Textfield
                    //label="Search your payee list"
                    placeholder="Enter payee name or account number"
                    prependIcon="search"
                  />
                ) : (
                  <Select
                    options={getPayeeListNames}
                    // defaultValue={
                    //   selectedPayeeName
                    //     ? selectedPayeeName
                    //     : getPayeeListNames[0]
                    // }
                    placeholder="Enter payee name"
                    onChange={handleChange}
                    maxMenuHeight={300}
                  />
                )}
              </Column>
              <Column
                mobile={5}
                tablet={8}
                desktop={8}
                widescreen={9}
                className="py-0 has-text-right">
                <Button
                  secondary
                  className="my-0 mx-0"
                  onClick={() => {
                    activatePayeeListModal(true);
                    activatePayeeListAction('create');
                  }}>
                  New payee
                </Button>
              </Column>
            </Columns>

            <Columns mobile multiline>
              {activeBreakpoints.above.sm && (
                <Column mobile={12} tablet={4} desktop={4} widescreen={3}>
                  <Card>
                    <List
                      bordered
                      style={{ maxHeight: '450px' }}
                      className="is-overflow-auto">
                      {alphabet.map((letter, key) => (
                        <>
                          <ListItem
                            key={key}
                            className="has-bg-blue-grey lighten-4 py-2">
                            <Title size="8" className="mb-0 has-text-secondary">
                              {letter}
                            </Title>
                          </ListItem>
                          {newPayeeListArray[letter].map((Acct, key) => (
                            <ListItem
                              key={key}
                              className="has-pointer has-bg-blue-grey lighten-5 px-0 py-0"
                              onClick={() => handleChange(Acct)}>
                              <div
                                className={cn(
                                  'is-flex py-4 has-text-size-1 has-text-white is-uppercase px-0 pl-1 darken-1',
                                  selectedPayeeDetails.name === Acct.name &&
                                    'has-bg-danger'
                                )}
                              />
                              <div className="py-4 px-4 is-flex-1">
                                <Level>
                                  <LevelItem left>
                                    <div>
                                      <Title
                                        size="6"
                                        className="mb-0 has-text-secondary">
                                        {Acct.name}
                                      </Title>
                                      <Subtitle
                                        size="8"
                                        className="my-0 has-text-secondary">
                                        ({Acct.bsb}) {Acct.accNum}
                                      </Subtitle>
                                    </div>
                                  </LevelItem>
                                  <LevelItem right>
                                    {selectedPayeeDetails.name !==
                                      Acct.name && (
                                      <Icon className="has-text-red">
                                        arrow-right
                                      </Icon>
                                    )}
                                  </LevelItem>
                                </Level>
                              </div>
                            </ListItem>
                          ))}
                        </>
                      ))}
                    </List>
                  </Card>
                </Column>
              )}
              <Column mobile={12} tablet={8} desktop={8} widescreen={9}>
                <Card>
                  <CardHeader className="has-bg-blue-grey darken-5 has-text-white py-7 px-7">
                    <Level style={{ width: '100%' }}>
                      <LevelItem left>
                        <div>
                          <Title
                            size="4"
                            className="mb-0 has-text-secondary has-text-white">
                            {selectedPayeeDetails.name}
                          </Title>
                          <Subtitle
                            size="6"
                            className="my-0 has-text-secondary has-text-white">
                            BSB: {selectedPayeeDetails.bsb} <br />
                            Account #: {selectedPayeeDetails.accNum}
                          </Subtitle>
                        </div>
                      </LevelItem>
                      <LevelItem right>
                        {/* hide transfer button */}
                        {/* <Button
                        secondary
                        className="mr-1"
                        onClick={() => {
                          dispatch({
                            type: 'toggleCashTransferModel',
                            payload: true
                          });
                          dispatch({
                            type: 'updateTransferTarget',
                            payload: 'pay anyone'
                          });
                          dispatch({
                            type: 'getDefaultToAccount',
                            payload: selectedPayeeDetails
                          });
                          dispatch({ type: 'handleStep', payload: 1 });
                        }}
                      >
                        Transfer funds
                      </Button> */}
                        <Button
                          inverted
                          className="mr-1"
                          onClick={() => {
                            activatePayeeListModal(true);
                            activatePayeeListAction('amend');
                          }}>
                          <Icon>pencil</Icon>
                        </Button>
                        <Button
                          inverted
                          danger
                          onClick={() => {
                            activatePayeeListModal(true);
                            activatePayeeListAction('delete');
                          }}>
                          <Icon>bin</Icon>
                        </Button>
                      </LevelItem>
                    </Level>
                  </CardHeader>

                  {/* <CardContent>...transaction content goes here...</CardContent> */}
                </Card>
              </Column>
            </Columns>
          </Container>
        )}
      </Section>
      <PayeeListModal
        isOpened={payeeListModal}
        closeModal={activatePayeeListModal}
        payeeListAction={payeeListAction}
        selectedPayeeDetails={selectedPayeeDetails}
        error={error}
      />
      {errorHandling && (
        <div
          className="pos-f px-4 py-2 has-bg-white has-elevation-2 "
          style={{ right: '1em', bottom: '1em', zIndex: 100 }}>
          <Columns mobile multiline>
            <Column>
              <Title size="6" className="mb-1">
                Payee list error handling
              </Title>
              <CheckboxGroup
                stacked
                className="mb-0"
                name="error"
                value={error}
                onChange={e => setError(e)}>
                <Checkbox
                  value="retrieve-payee-list"
                  label="Retrieve Payee List"
                />
                <Checkbox
                  value="invalid-data-input"
                  label="Invalid Data Input"
                />
                <Checkbox
                  value="generic-error"
                  label="Create/Save/Delete Payee"
                />
              </CheckboxGroup>
            </Column>
          </Columns>
        </div>
      )}
    </LayoutDefault>
  );
};

export default Breakpoints(PayeeList);
