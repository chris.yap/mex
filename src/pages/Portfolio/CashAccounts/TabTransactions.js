import React, { useState } from 'react';
import _ from 'lodash';
import { Transition } from 'react-transition-group';
import TransactionFilters from './TransactionsFilter';

import {
  Breakpoints,
  Button,
  Card,
  Container,
  Collapse,
  Columns,
  Column,
  Divider,
  Icon,
  List,
  ListItem,
  Pagination,
  Section,
  Table,
  TableRow as Row,
  TableCell as Cell
} from 'shaper-react';
import Overlay from '../../../components/Overlay';

const TabTransactions = ({ activeBreakpoints, ...props }) => {
  const [filter, setFilter] = useState(false);
  const [timer, updateTimer] = useState(5);
  const updateFilter = e => {
    setFilter(filter === true ? false : true);
  };
  const convertDate = date => {
    date = new Date(date.replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3'));
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    // var month = date.toLocaleString('en-GB', { month: 'short' });
    // var day = date.getDate();
    // var year = date.getFullYear();
    // var newDate = month + '. ' + day + ', ' + year;
    var newDate = date.toLocaleDateString('en-GB', options);
    return newDate;
  };

  const defaultStyle = {
    transition: `500ms ease-in-out`,
    opacity: 0
  };
  const transitionStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 }
  };
  let loading = () => {
    setTimeout(() => {
      updateTimer(false);
    }, 3000);
  };
  React.useEffect(() => {
    loading(3000);
  }, [props]);
  return (
    <Section p={0} minHeight={timer > 0 && '300px'} position="relative">
      <Overlay absolute spinner transparent isOpened={timer > 0 && true} />
      <Transition in={!timer && true} timeout={500}>
        {state => (
          <Container
            fluid
            px={0}
            className={`${
              activeBreakpoints.below.sm ? 'is-overflow-hidden' : ''
            }`}
            style={{
              ...defaultStyle,
              ...transitionStyles[state],
              height: timer > 0 && '0'
            }}>
            <Columns
              mobile
              className={`mb-0 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
              <Column narrow>
                {/* <Textfield
                  prependIcon="search"
                  placeholder="Search transactions"
                  className="my-1"
                /> */}
                <Button
                  flat
                  square={activeBreakpoints.below.sm}
                  onClick={() => updateFilter()}
                  className="my-1">
                  <Icon
                    style={{
                      transition: '.2s',
                      transform: filter ? 'rotate(180deg)' : 'none'
                    }}>
                    filter
                  </Icon>
                  {activeBreakpoints.above.xs && <span>Filter</span>}
                </Button>
              </Column>
              <Column className="is-flex is-justify-content-flex-end">
                {/* <Divider vertical /> */}
                <Button
                  flat
                  square={activeBreakpoints.below.sm}
                  className="my-1">
                  <Icon>download</Icon>
                  {activeBreakpoints.above.xs && <span>Download</span>}
                </Button>
              </Column>
            </Columns>

            <Collapse
              isOpened={filter}
              className={activeBreakpoints.below.sm ? 'px-4' : ''}>
              <TransactionFilters toggleFilter={updateFilter} />
            </Collapse>

            {activeBreakpoints.above.sm ? (
              <Table
                hasStripes
                headers={Headers}
                tableRow={TableRow}
                data={Data}
                defaultSortCol="date"
                desc
                hasNoScroll={activeBreakpoints.above.sm}
                mt={0}
              />
            ) : (
              <Card className="mb-4">
                <Divider />
                <List>
                  {DataByDate.map((dbd, d) => (
                    <React.Fragment key={d}>
                      <ListItem className="py-1 has-bg-black lighten-5 has-text-size-2">
                        {convertDate(dbd.date)}
                      </ListItem>
                      {dbd.transact.map((tran, t) => (
                        <ListItem key={t}>
                          <Columns mobile className="is-flex-1">
                            <Column>
                              <p
                                className="has-text-ellipsis my-0 has-text-size-1"
                                style={{ maxWidth: '40vw' }}>
                                {tran.desc}
                              </p>
                              <p className="has-text-secondary has-text-size-2 my-0">
                                Account name
                              </p>
                            </Column>
                            <Column narrow className="has-text-right">
                              <p className="my-0 has-text-size-1">
                                {tran.type === 'debit'
                                  ? tran.debit
                                  : tran.credit}
                              </p>
                              <p className="has-text-secondary has-text-size-2 my-0">
                                Balance: {tran.balance}
                              </p>
                            </Column>
                            {/* <Column narrow className="is-align-self-center">
                        <Icon className="has-text-primary" small>
                          arrow-right
                        </Icon>
                      </Column> */}
                          </Columns>
                        </ListItem>
                      ))}
                    </React.Fragment>
                  ))}
                </List>
              </Card>
            )}

            <Pagination
              current={1}
              total={1}
              className={activeBreakpoints.below.sm && 'px-4'}
            />
          </Container>
        )}
      </Transition>
    </Section>
  );
};

export default Breakpoints(TabTransactions);

const Data = [
  {
    date: '03-01-2019',
    type: 'debit',
    desc:
      'Voluptate nulla sint fugiat velit consequat officia magna deserunt voluptate dolor.',
    debit: '-$488.99',
    credit: '$65.55',
    balance: '$5,693.76'
  },
  {
    date: '03-01-2019',
    type: 'credit',
    desc: 'Ullamco aute qui dolor aliquip laborum incididunt.',
    debit: '-$313.28',
    credit: '$175.71',
    balance: '$6,479.64'
  },
  {
    date: '02-10-2018',
    type: 'credit',
    desc: 'Eu id est eu id duis duis do reprehenderit velit.',
    debit: '-$382.12',
    credit: '$119.86',
    balance: '$6,802.64'
  },
  {
    date: '02-10-2018',
    type: 'credit',
    desc:
      'Voluptate nisi dolor ullamco nulla consectetur laboris fugiat quis voluptate magna in do velit in.',
    debit: '-$112.78',
    credit: '$24.39',
    balance: '$6,061.39'
  },
  {
    date: '02-10-2018',
    type: 'credit',
    desc:
      'Ad esse officia cillum eiusmod deserunt cupidatat officia nostrud enim eu.',
    debit: '-$353.82',
    credit: '$421.45',
    balance: '$5,555.78'
  },
  {
    date: '02-10-2018',
    type: 'credit',
    desc:
      'Sit culpa id quis consectetur voluptate minim esse irure anim eiusmod.',
    debit: '-$13.99',
    credit: '$300.76',
    balance: '$6,510.51'
  },
  {
    date: '02-10-2018',
    type: 'debit',
    desc:
      'Aliqua nisi sit est fugiat exercitation enim eiusmod ad veniam incididunt.',
    debit: '-$452.27',
    credit: '$184.23',
    balance: '$5,828.01'
  },
  {
    date: '26-01-2017',
    type: 'debit',
    desc: 'Ipsum nulla et est enim minim sint do qui magna anim consequat.',
    debit: '-$116.60',
    credit: '$61.32',
    balance: '$6,188.30'
  },
  {
    date: '29-01-2019',
    type: 'credit',
    desc:
      'Sint incididunt velit cillum eu aliqua ipsum aliqua nulla voluptate ullamco.',
    debit: '-$302.35',
    credit: '$127.17',
    balance: '$6,195.48'
  },
  {
    date: '06-12-2017',
    type: 'debit',
    desc: 'Labore quis in do sit nulla ut.',
    debit: '-$153.93',
    credit: '$261.02',
    balance: '$6,086.27'
  },
  {
    date: '22-08-2016',
    type: 'credit',
    desc:
      'Ut proident veniam culpa ullamco anim dolore non et sit cillum dolor velit est occaecat.',
    debit: '-$63.52',
    credit: '$380.31',
    balance: '$5,186.08'
  },
  {
    date: '27-02-2018',
    type: 'debit',
    desc: 'Cupidatat occaecat nisi amet laborum sint do pariatur officia.',
    debit: '-$396.36',
    credit: '$152.52',
    balance: '$5,959.24'
  },
  {
    date: '13-10-2014',
    type: 'credit',
    desc:
      'Tempor mollit officia esse fugiat reprehenderit eiusmod occaecat qui incididunt do aliquip duis.',
    debit: '-$277.58',
    credit: '$195.48',
    balance: '$6,100.17'
  },
  {
    date: '22-01-2017',
    type: 'credit',
    desc: 'Cillum aute est ad minim eiusmod.',
    debit: '-$227.96',
    credit: '$77.93',
    balance: '$6,420.39'
  },
  {
    date: '18-01-2018',
    type: 'debit',
    desc: 'Laborum amet id consequat ipsum.',
    debit: '-$441.63',
    credit: '$358.04',
    balance: '$6,096.73'
  },
  {
    date: '09-04-2017',
    type: 'debit',
    desc:
      'Commodo laboris irure eu est anim qui sint adipisicing proident adipisicing.',
    debit: '-$488.36',
    credit: '$419.64',
    balance: '$5,045.78'
  },
  {
    date: '09-04-2014',
    type: 'credit',
    desc:
      'Labore reprehenderit aliquip adipisicing id est ex ea do Lorem nulla veniam do.',
    debit: '-$351.55',
    credit: '$182.89',
    balance: '$6,297.90'
  },
  {
    date: '09-05-2019',
    type: 'debit',
    desc:
      'Sunt consequat velit qui est dolor eiusmod ad duis fugiat aute tempor.',
    debit: '-$394.98',
    credit: '$258.98',
    balance: '$6,878.76'
  },
  {
    date: '28-10-2018',
    type: 'debit',
    desc:
      'Commodo nulla fugiat labore fugiat magna esse dolor commodo minim ex amet pariatur.',
    debit: '-$64.62',
    credit: '$65.27',
    balance: '$5,119.74'
  },
  {
    date: '07-08-2017',
    type: 'debit',
    desc:
      'Mollit et enim duis adipisicing laboris officia ad culpa aute laborum nostrud irure.',
    debit: '-$434.73',
    credit: '$47.70',
    balance: '$5,064.88'
  }
];

const DataByDate = _.chain(Data)
  .groupBy('date')
  .toPairs()
  .map(function(currentItem) {
    return _.zipObject(['date', 'transact'], currentItem);
  })
  .value();

const TableRow = ({ row }) => (
  <Row>
    <Cell className="has-text-nowrap">{row.date}</Cell>
    <Cell className="is-capitalized">{row.type}</Cell>
    <Cell className="">{row.desc}</Cell>
    <Cell className="has-text-right">
      {row.type === 'debit' ? row.debit : null}
    </Cell>
    <Cell className="has-text-right">
      {row.type === 'credit' ? row.credit : null}
    </Cell>
    <Cell className="has-text-right">{row.balance}</Cell>
  </Row>
);

const Headers = [
  { name: 'Date', value: 'date' },
  { name: 'Type', value: 'type', sort: true },
  { name: 'Description', value: '' },
  { name: 'Debit', value: 'debit', align: 'right' },
  { name: 'Credit', value: 'credit', align: 'right' },
  { name: 'Balance', value: 'balance', align: 'right' }
];
