import React, { useState } from 'react';
import { getState } from '../../../context/StateProvider';
import cn from 'classnames';
import _ from 'lodash';
import {
  Alert,
  Breakpoints,
  Button,
  Card,
  Container,
  Columns,
  Column,
  Divider,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Pagination,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';
import {
  CashAccountDetails,
  CashPayeeListDetails
} from '../../../constants/portfolio';

const TabFutureTransfers = ({ activeBreakpoints, error, ...props }) => {
  const [, dispatch] = getState();
  const [deleteInd, setDeleteInd] = useState(null);
  const [deleteError, setDeleteError] = useState(false);
  const updateDeleteInd = newVal => {
    setDeleteInd(newVal);
  };
  const updateDeleteError = newVal => {
    setDeleteError(newVal);
  };
  const convertDate = date => {
    date = new Date(date.replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3'));
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    var newDate = date.toLocaleDateString('en-GB', options);
    return newDate;
  };
  return (
    <Container
      fluid
      className={`mx-0 ${
        activeBreakpoints.below.sm ? 'is-overflow-hidden' : ''
      }`}>
      {error.includes('retrieve-future-transfers') ? (
        <Alert isOpened={true} inverted warning>
          <p className="pos-r has-top3 mb-1">
            We're currently experiencing some issues. Please try again later.
          </p>
        </Alert>
      ) : (
        <>
          {/* <Columns
            mobile
            className={`mb-0 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}
          >
            <Column narrow>
              <Textfield
                prependIcon="search"
                placeholder="Search transactions"
                className="my-1"
              />
            </Column>
            <Column className="is-flex is-justify-content-flex-end">
              <Button
                flat
                square={activeBreakpoints.below.sm}
                onClick={() => updateFilter()}
                className="my-1"
              >
                <Icon
                  style={{
                    transition: '.2s',
                    transform: filter ? 'rotate(180deg)' : 'none'
                  }}
                >
                  filter
                </Icon>
                {activeBreakpoints.above.xs && <span>Filter</span>}
              </Button>
              <Divider vertical />
              <Button flat square={activeBreakpoints.below.sm} className="my-1">
                <Icon>download</Icon>
                {activeBreakpoints.above.xs && <span>Download</span>}
              </Button>
            </Column>
          </Columns> */}
          {/* <Collapse isOpened={filter}>
            <Title>TBC</Title>
          </Collapse> */}
          {activeBreakpoints.above.sm ? (
            <>
              <Table
                hasStripes
                headers={Headers}
                data={Data}
                hasNoScroll
                tableRow={props => (
                  <TableRow
                    key={props.id}
                    deleteIndex={deleteInd}
                    updateDeleteInd={e => updateDeleteInd(e)}
                    error={error}
                    deleteError={deleteError}
                    updateDeleteError={updateDeleteError}
                    {...props}
                  />
                )}
                mt={0}
              />
              <Pagination
                total={1}
                className={activeBreakpoints.below.sm ? 'px-4' : ''}
              />
            </>
          ) : (
            <Card>
              <Divider />
              <List>
                {DataByDate.map((group, key) => {
                  //let ran = Math.floor(Math.random() * 3 + 1);
                  return (
                    <>
                      <ListItem className="py-1 has-bg-black lighten-5">
                        <Level mobile className="is-flex-1">
                          <LevelItem left className="has-text-size-2">
                            {convertDate(group.date)}
                          </LevelItem>
                          <LevelItem
                            right
                            className="has-text-size-2 has-text-secondary">
                            {key === 0
                              ? 'Tomorrow'
                              : key > 0 && key < 3
                              ? '3 days'
                              : 'Next month'}
                          </LevelItem>
                        </Level>
                      </ListItem>

                      {group.transact.map(tran => (
                        <ListItem>
                          <Columns mobile className="is-flex-1">
                            <Column>
                              <p
                                className="my-0 has-text-size-1 has-text-ellipsis"
                                style={{ maxWidth: '45vw' }}>
                                {tran.desc}
                              </p>
                              <p
                                className="my-0 has-text-size-2 has-text-secondary has-text-ellipsis"
                                style={{ maxWidth: '45vw' }}>
                                Account name / Name of recipient
                              </p>
                            </Column>
                            <Column narrow className="has-text-right px-0">
                              <p className="my-0 has-text-size-1 ">
                                -{tran.amount}
                              </p>
                              <p className="my-0 has-text-size-2 has-text-secondary is-capitalized">
                                {tran.freq}
                              </p>
                            </Column>
                            <Column narrow className="is-align-self-center">
                              <Button
                                square
                                small
                                flat
                                className="ma-0"
                                onClick={() => {
                                  dispatch({
                                    type: 'toggleCashTransferModel',
                                    payload: true
                                  });
                                  dispatch({
                                    type: 'getDefaultFromAccount',
                                    payload: CashAccountDetails[0]
                                  });
                                  dispatch({
                                    type: 'getDefaultToAccount',
                                    payload: CashPayeeListDetails[0]
                                  });
                                  dispatch({ type: 'handleStep', payload: 3 });
                                  dispatch({
                                    type: 'updateTransferType',
                                    payload: 'amend'
                                  });
                                }}>
                                <Icon>pencil</Icon>
                              </Button>
                              <Button
                                primary
                                square
                                small
                                flat
                                className="ma-0"
                                onClick={() => updateDeleteInd(tran.id)}>
                                <Icon>trash</Icon>
                              </Button>
                              {deleteInd === tran.id && (
                                <div
                                  className="pos-a has-text-secondary has-bg-white pl-4 pr-1 has-elevation-2 is-flex"
                                  style={{
                                    top: 0,
                                    right: '2px',
                                    margin: '3px',
                                    paddingTop: '6px',
                                    paddingBottom: '6px',
                                    zIndex: 1,
                                    whiteSpace: activeBreakpoints.below.sm
                                      ? 'normal'
                                      : 'nowrap'
                                  }}>
                                  <div
                                    className="is-flex"
                                    style={{ minWidth: '160px' }}>
                                    {error.includes(
                                      'delete-future-transfers'
                                    ) && deleteError ? (
                                      <>
                                        <Icon warning>warning-triangle</Icon>
                                        <span>
                                          We're currently experiencing some
                                          issues. Please try again later
                                        </span>
                                      </>
                                    ) : (
                                      'Delete this transfer?'
                                    )}
                                  </div>
                                  <div className="is-flex">
                                    <Button
                                      small
                                      square
                                      inverted
                                      secondary
                                      className="ml-2"
                                      onClick={() => {
                                        updateDeleteInd(null);
                                        updateDeleteError(false);
                                      }}>
                                      <Icon>close</Icon>
                                    </Button>
                                    {!deleteError && (
                                      <Button
                                        small
                                        square
                                        inverted
                                        danger
                                        onClick={() =>
                                          error.includes(
                                            'delete-future-transfers'
                                          )
                                            ? updateDeleteError(true)
                                            : {}
                                        }>
                                        <Icon>checked</Icon>
                                      </Button>
                                    )}
                                  </div>
                                </div>
                              )}
                            </Column>
                          </Columns>
                        </ListItem>
                      ))}
                    </>
                  );
                })}
              </List>
            </Card>
          )}
        </>
      )}
    </Container>
  );
};

export default Breakpoints(TabFutureTransfers);

const Data = [
  {
    id: 0,
    date: '04-05-2018',
    desc: 'Personal Portfolio - High Interest Account - NT123456788-003',
    amount: '$322.87',
    freq: 'monthly',
    start: '19-12-2017',
    end: '19-12-2017'
  },
  {
    id: 1,
    date: '30-04-2014',
    desc: 'Bill Cosgrove - (062-227) 74563248',
    amount: '$257.42',
    freq: 'once',
    start: '24-02-2015',
    end: '12-10-2016'
  },
  {
    id: 2,
    date: '19-02-2015',
    desc: 'SMSF Portfolio - IB Account - (063-235) 4593245346',
    amount: '$739.99',
    freq: 'monthly',
    start: '11-11-2018',
    end: '11-02-2014'
  },
  {
    id: 3,
    date: '21-04-2014',
    desc: 'Martin Smith - (062-227) 74563248',
    amount: '$982.76',
    freq: 'monthly',
    start: '29-09-2018',
    end: '11-11-2015'
  },
  {
    id: 4,
    date: '27-12-2017',
    desc: 'Wendy Phillips - (062-227) 74563248',
    amount: '$990.42',
    freq: 'yearly',
    start: '22-07-2017',
    end: '06-01-2014'
  }
];

const DataByDate = _.chain(Data)
  .groupBy('date')
  .toPairs()
  .map(function(currentItem) {
    return _.zipObject(['date', 'transact'], currentItem);
  })
  .value();

const TableRow = ({
  deleteIndex,
  row,
  error,
  deleteError,
  updateDeleteError,
  ...props
}) => {
  const [, dispatch] = getState();
  return (
    <Row>
      <Cell className="has-text-nowrap">{row.date}</Cell>
      <Cell>{row.desc}</Cell>
      <Cell className="has-text-right">{row.amount}</Cell>
      <Cell className="is-capitalized">{row.freq}</Cell>
      <Cell className="has-text-nowrap">
        {row.freq !== 'once' && row.start}
      </Cell>
      <Cell className="has-text-nowrap">{row.freq !== 'once' && row.end}</Cell>
      <Cell className="has-text-nowrap pos-r has-text-right">
        <Button
          square
          small
          secondary
          flat
          className="ma-0"
          onClick={() => {
            dispatch({ type: 'toggleCashTransferModel', payload: true });
            dispatch({
              type: 'getDefaultFromAccount',
              payload: CashAccountDetails[0]
            });
            dispatch({
              type: 'getDefaultToAccount',
              payload: CashPayeeListDetails[0]
            });
            dispatch({ type: 'handleStep', payload: 3 });
            dispatch({
              type: 'updateModalView',
              payload: 'amend'
            });
            dispatch({
              type: 'updateTransferTarget',
              payload: 'pay anyone'
            });
          }}>
          <Icon>pencil</Icon>
        </Button>
        <Button
          square
          small
          danger
          flat
          className="ma-0"
          onClick={() => props.updateDeleteInd(row.id)}>
          <Icon>bin</Icon>
        </Button>

        {deleteIndex === row.id && (
          <div
            className={cn(
              'pos-a has-text-secondary has-bg-white pl-4 pr-1 has-elevation-2 ',
              error.includes('delete-future-transfers') && 'is-flex'
            )}
            style={{
              top: 0,
              right: '2px',
              whiteSpace: 'nowrap',
              margin: '3px',
              paddingTop: '6px',
              paddingBottom: '6px',
              zIndex: 1
            }}>
            {error.includes('delete-future-transfers') && deleteError ? (
              <>
                <Icon warning>warning-triangle</Icon> We're currently
                experiencing some issues. Please try again later
              </>
            ) : (
              'Delete this transfer?'
            )}
            <Button
              small
              square
              inverted
              secondary
              className="mr-0 ml-2"
              onClick={() => {
                props.updateDeleteInd(null);
                updateDeleteError(false);
              }}>
              <Icon>close</Icon>
            </Button>
            {!deleteError && (
              <Button
                small
                square
                inverted
                danger
                className="mr-0"
                onClick={() =>
                  error.includes('delete-future-transfers')
                    ? updateDeleteError(true)
                    : {}
                }>
                <Icon>checked</Icon>
              </Button>
            )}
          </div>
        )}
      </Cell>
    </Row>
  );
};

const Headers = [
  { name: 'Transfer date', sort: true },
  { name: 'To account', sort: true },
  { name: 'Amount', sort: true, align: 'right' },
  { name: 'Frequency', sort: true },
  { name: 'Start date', sort: true },
  { name: 'End date', sort: true },
  { name: '' }
];
