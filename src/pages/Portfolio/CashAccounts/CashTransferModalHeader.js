import React from 'react';
import { getState } from '../../../context/StateProvider';
import {
  Breakpoints,
  Button,
  Dropdown,
  Icon,
  List,
  ListItem,
  Subtitle,
  Title
} from 'shaper-react';

const CashTransferModalHeader = ({
  activeBreakpoints,
  step,
  error,
  transferTarget,
  clearSetValues,
  ...props
}) => {
  const [{ cash }, dispatch] = getState();
  return (
    <>
      {step > 2 ? (
        <Title
          size="4"
          className="mb-0 has-text-black is-flex is-align-items-center"
        >
          {step === 3 && cash.modalView !== 'amend' ? (
            <Button
              square
              secondary
              className="mr-2"
              onClick={() => {
                cash.defaultToAccount
                  ? dispatch({ type: 'handleStep', payload: step - 2 })
                  : dispatch({ type: 'handleStep', payload: step - 1 });
              }}
            >
              <Icon small>arrow-left</Icon>
            </Button>
          ) : step === 4 ? (
            <Button
              square
              secondary
              className="mr-2"
              onClick={() => {
                dispatch({ type: 'handleStep', payload: step - 1 });
                cash.submissionError &&
                  dispatch({ type: 'updateSubmissionError', payload: false });
              }}
            >
              <Icon small>arrow-left</Icon>
            </Button>
          ) : (
            ''
          )}
          {step === 3 ? (
            'Transfer funds'
          ) : step === 4 ? (
            'Review'
          ) : step === 5 && !error.includes('create-order') ? (
            'Verify the transaction'
          ) : step === 6 ? (
            <>
              <Icon success className="mr-2">
                tick-circle-outline
              </Icon>{' '}
              Transfer complete{' '}
            </>
          ) : (
            'Transfer Error'
          )}
        </Title>
      ) : (
        <div>
          <Title
            size="4"
            className="mb-0 is-flex is-align-items-center has-text-secondary has-text-black"
          >
            Transfer funds
            {step === 0 ? (
              ''
            ) : step > 2 ? (
              ''
            ) : step < 3 &&
              step !== 0 &&
              cash.defaultFromAccount &&
              cash.defaultFromAccount.type === 'hia' ? (
              ''
            ) : step < 3 && step !== 0 && cash.defaultToAccount ? (
              ''
            ) : (
              <Dropdown
                right={activeBreakpoints.below.sm ? true : false}
                activator={
                  <Button flat className="mx-1">
                    <Icon>arrow-down</Icon>
                  </Button>
                }
              >
                <List className="has-text-weight-normal has-text-size-4 has-pointer">
                  <ListItem
                    onClick={() => {
                      dispatch({
                        type: 'updateTransferTarget',
                        payload: 'nab accounts'
                      });
                      clearSetValues(cash.newTransfer);
                    }}
                  >
                    NAB Accounts
                  </ListItem>
                  <ListItem
                    onClick={() => {
                      dispatch({
                        type: 'updateTransferTarget',
                        payload: 'pay anyone'
                      });
                      clearSetValues(cash.newTransfer);
                    }}
                  >
                    Pay anyone
                  </ListItem>
                </List>
              </Dropdown>
            )}
          </Title>
          {step !== 0 && (
            <Subtitle
              size="6"
              className="my-0 has-text-secondary has-line-height-100"
            >
              {transferTarget === 'nab accounts'
                ? 'NAB Accounts'
                : 'Pay anyone'}
            </Subtitle>
          )}
        </div>
      )}
    </>
  );
};

export default Breakpoints(CashTransferModalHeader);
