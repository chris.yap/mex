import React from 'react';
// import cn from 'classnames';
import {
  Button,
  Buttons,
  Column,
  Columns,
  Divider,
  Dropdown,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Select,
  Textfield,
  Title
} from 'shaper-react';

const Filters = ({ ...props }) => {
  return (
    <React.Fragment>
      <Divider />
      <Level mobile className="mb-2">
        <LevelItem left>
          <Title size="4" className="mt-4 mb-0">
            Filters
          </Title>
        </LevelItem>
        <LevelItem right>
          <Button small flat round onClick={props.close}>
            <Icon>close</Icon>
          </Button>
        </LevelItem>
      </Level>
      <Columns mobile multiline>
        <Column mobile="12" desktop="6">
          <div className="nab-field">
            <label className="nab-label">Dates</label>
            <Buttons className="is-inline-flex mr-2">
              <Button className="mt-0 ml-0" active>
                Time period
              </Button>
              <Button className="mt-0">Date range</Button>
            </Buttons>
            <Dropdown
              activator={
                <Button>
                  <span>Last 30 days</span>
                  <Icon>caret-down</Icon>
                </Button>
              }
            >
              <List>
                <ListItem>Last 30 days</ListItem>
                <ListItem>Last 60 days</ListItem>
                <ListItem>Last quarter</ListItem>
                <ListItem>Last year</ListItem>
              </List>
            </Dropdown>
          </div>
          <Textfield label="Amount" prefix="$" placeholder="eg. 10,000.00" />
        </Column>
        <Column mobile="12" desktop="6">
          <label className="nab-label">Transaction type</label>
          <Select options={transactionType} />
        </Column>
      </Columns>
      <Button secondary className="ml-0 mb-4">
        Apply filters
      </Button>
      <Button flat onClick={props.close}>
        Cancel
      </Button>
    </React.Fragment>
  );
};

export default Filters;

const transactionType = [
  {
    value: 'all',
    label: 'All'
  },
  {
    value: 'credit',
    label: 'Credit'
  },
  {
    value: 'debit',
    label: 'Debit'
  }
];
