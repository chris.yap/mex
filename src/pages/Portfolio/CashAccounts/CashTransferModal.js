import React, { useState, useEffect } from 'react';
import { getState } from '../../../context/StateProvider';
//import cn from 'classnames';
import {
  //Alert,
  Breakpoints,
  Card,
  CardContent,
  Checkbox,
  CheckboxGroup,
  Column,
  Columns,
  DatePicker,
  //Divider,
  Icon,
  Modal,
  Radio,
  RadioGroup,
  Textfield,
  Select,
  Tile,
  Title
} from 'shaper-react';
import CashTransferModalAccountsCard from './CashTransferModalAccountsCard';
import CashTransferModalHeader from './CashTransferModalHeader';
import CashTransferModalFooter from './CashTransferModalFooter';
import CashTransferModalSummary from './CashTransferModalSummary';
import { CashPayeeListDetails } from '../../../constants/portfolio';

const CashTransferModal = ({
  activeBreakpoints,
  isOpened,
  transferTarget,
  defaultFromAccount,
  defaultToAccount,
  availableAccounts,
  step,
  ...props
}) => {
  const [{ cash }, dispatch] = getState();
  const [error, setError] = useState([]);
  const [selectedFromAccount, setSelectedFromAccount] = useState(null);
  const [selectedToAccount, setSelectedToAccount] = useState(null);
  const [transferWhen, setTransferWhen] = useState(
    transferWhenOptions[0].value
  );
  const [payeeAccount, setPayeeAccount] = useState(
    payeeAccountOptions[0].value
  );
  const [selectedTransferFrequency, setHandleChange] = useState(
    transferFrequency[0]
  );

  const handleChange = value => {
    setHandleChange(value);
  };
  const updateSelectedFromAccount = value => {
    setSelectedFromAccount(value);
  };
  const updateSelectedToAccount = value => {
    setSelectedToAccount(value);
  };
  const updateTransferWhen = value => {
    setTransferWhen(value);
  };
  const updatePayeeAccount = value => {
    setPayeeAccount(value);
  };
  const clearSetValues = value => {
    updateSelectedToAccount(null);
    updateSelectedFromAccount(null);
    updatePayeeAccount(payeeAccountOptions[0].value);
    updateTransferWhen(transferWhenOptions[0].value);
    handleChange(transferFrequency[0]);
    dispatch({ type: 'updateSelectedAccount', payload: null });
    dispatch({ type: 'updateSubmissionError', payload: false });
    dispatch({
      type: 'updateModalView',
      payload: null
    });
    if (value) {
      dispatch({
        type: 'handleStep',
        payload: cash.defaultFromAccount ? 2 : 1
      });
    } else {
      dispatch({
        type: 'handleStep',
        payload: 1
      });
      dispatch({ type: 'getDefaultFromAccount', payload: null });
      dispatch({ type: 'getDefaultToAccount', payload: null });
    }
  };

  useEffect(() => {
    if (cash.transferType === 'to') {
      updateSelectedToAccount(cash.selectedAccount);
    }
    if (cash.transferType === 'from') {
      updateSelectedFromAccount(cash.selectedAccount);
    }
  });

  return (
    <Modal
      closeIcon
      fullscreen={activeBreakpoints.below.sm ? true : false}
      isOpened={isOpened}
      onClose={() =>
        dispatch({
          type: 'toggleCashTransferModel',
          payload: false
        })
      }
      className="has-bg-blue-grey lighten-5"
      modalWidth={
        activeBreakpoints.below.md ? 100 : activeBreakpoints.above.lg ? 50 : 60
      }
      header={
        <CashTransferModalHeader
          activeBreakpoints={activeBreakpoints}
          step={step}
          transferTarget={transferTarget}
          clearSetValues={clearSetValues}
          error={error}
        />
      }
      footer={
        <CashTransferModalFooter
          step={step}
          payeeAccount={payeeAccount}
          transferTarget={transferTarget}
          clearSetValues={clearSetValues}
          payeeList={CashPayeeListDetails}
          error={error}
          closeModal={() =>
            dispatch({
              type: 'toggleCashTransferModel',
              payload: false
            })
          }
        />
      }>
      {step === 0 ? (
        <Columns multiline mobile className="mb-0">
          <Column>
            <p>Where would you like to transfer funds</p>
            <Card
              className={activeBreakpoints.above.sm ? 'mb-4' : 'mb-1'}
              onClick={() => {
                dispatch({
                  type: 'handleStep',
                  payload: 1
                });
                dispatch({
                  type: 'updateTransferTarget',
                  payload: 'nab accounts'
                });
              }}>
              <CardContent className="is-flex py-4">
                <Title size="5" className="is-flex-1 mb-0">
                  Nab Accounts
                </Title>
                <Icon className="has-text-primary">arrow-right</Icon>
              </CardContent>
            </Card>
            <Card
              onClick={() => {
                dispatch({
                  type: 'handleStep',
                  payload: 1
                });
                dispatch({
                  type: 'updateTransferTarget',
                  payload: 'pay anyone'
                });
              }}>
              <CardContent className="is-flex py-4">
                <Title size="5" className="is-flex-1 mb-0">
                  Pay anyone
                </Title>
                <Icon className="has-text-primary">arrow-right</Icon>
              </CardContent>
            </Card>
          </Column>
        </Columns>
      ) : step === 1 ? (
        <Columns multiline mobile className="mb-0">
          <Column size="12" className="pb-0">
            <p className={activeBreakpoints.above.sm ? 'mb-0' : ''}>
              Please choose an account to transfer <strong>from</strong>
            </p>
          </Column>
          <Column>
            <Tile ancestor tileBreak={2}>
              {availableAccounts.map((Acct, a) => (
                <Tile
                  key={a}
                  parent
                  className={activeBreakpoints.below.sm ? 'pt-0 pb-1' : ''}>
                  <CashTransferModalAccountsCard
                    onClick={() => {
                      dispatch({
                        type: 'updateSelectedAccount',
                        payload: Acct
                      });
                      dispatch({
                        type: 'updateTransferType',
                        payload: 'from'
                      });
                      selectedFromAccount === null && defaultToAccount === null
                        ? dispatch({ type: 'handleStep', payload: step + 1 })
                        : dispatch({ type: 'handleStep', payload: step + 2 });
                    }}
                    accountDetails={Acct}
                  />
                </Tile>
              ))}
            </Tile>
          </Column>
        </Columns>
      ) : step === 2 ? (
        <>
          {transferTarget === 'nab accounts' ? (
            <Columns multiline mobile className="mb-0">
              <Column size="12" className="pb-0">
                <p className={activeBreakpoints.above.sm ? 'mb-0' : ''}>
                  Please choose an account to transfer <strong>to</strong>
                </p>
              </Column>
              <Column>
                <Tile ancestor tileBreak={2}>
                  {availableAccounts.map((Acct, a) => (
                    <Tile
                      parent
                      className={activeBreakpoints.below.sm ? 'pt-0 pb-1' : ''}>
                      <CashTransferModalAccountsCard
                        onClick={() => {
                          dispatch({ type: 'handleStep', payload: step + 1 });
                          dispatch({
                            type: 'updateSelectedAccount',
                            payload: Acct
                          });
                          dispatch({
                            type: 'updateTransferType',
                            payload: 'to'
                          });
                        }}
                        accountDetails={Acct}
                      />
                    </Tile>
                  ))}
                </Tile>
              </Column>
            </Columns>
          ) : (
            <>
              <Columns>
                <Column size="12">
                  <p className="mb-2">To account</p>
                  <RadioGroup
                    name="payeeAccount"
                    //label="To account"
                    buttons
                    selectedValue={payeeAccount}
                    onChange={updatePayeeAccount}
                    className="is-block">
                    {payeeAccountOptions.map(item => (
                      <Radio
                        spreadEvenly
                        button
                        value={item.value}
                        label={item.label}
                        className="mb-0"
                      />
                    ))}
                  </RadioGroup>
                </Column>
              </Columns>

              {payeeAccount === 'from my payee list' ? (
                <>
                  <Columns>
                    <Column size="12">
                      <p className="mb-2">Search your payee list</p>
                      <Textfield
                        //label="Search your payee list"
                        placeholder="Enter payee name or account number"
                      />
                    </Column>
                  </Columns>
                  <Columns multiline mobile>
                    <Column>
                      <Tile ancestor tileBreak={2}>
                        {CashPayeeListDetails.map((Acct, a) => (
                          <Tile
                            parent
                            className={
                              activeBreakpoints.below.sm ? 'pt-0 pb-1' : ''
                            }>
                            <CashTransferModalAccountsCard
                              onClick={() => {
                                dispatch({
                                  type: 'handleStep',
                                  payload: step + 1
                                });
                                dispatch({
                                  type: 'updateSelectedAccount',
                                  payload: Acct
                                });
                                dispatch({
                                  type: 'updateTransferType',
                                  payload: 'to'
                                });
                              }}
                              accountDetails={Acct}
                              step={step}
                            />
                          </Tile>
                        ))}
                      </Tile>
                    </Column>
                  </Columns>
                </>
              ) : (
                <>
                  <Columns>
                    <Column size="12" className="pb-0">
                      <p className="mb-0">New payee details</p>
                    </Column>
                  </Columns>
                  <Columns multiline mobile>
                    <Column
                      mobile={12}
                      tablet={4}
                      desktop={4}
                      className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                      <Textfield
                        required
                        label="Account name"
                        placeholder="Enter an account name"
                        error={error.includes('invalid-data-input')}
                        errorMessage="Please enter an account name"
                      />
                    </Column>
                    <Column
                      mobile={12}
                      tablet={4}
                      desktop={4}
                      className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                      <Textfield
                        required
                        label="BSB"
                        placeholder="Enter a BSB"
                        error={error.includes('invalid-data-input')}
                        errorMessage="Please enter a valid BSB"
                      />
                    </Column>
                    <Column
                      mobile={12}
                      tablet={4}
                      desktop={4}
                      className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                      <Textfield
                        required
                        label="Account number"
                        placeholder="Enter an account number"
                        error={error.includes('invalid-data-input')}
                        errorMessage="Please enter an account number"
                      />
                    </Column>
                  </Columns>
                  <Columns>
                    <Column size="12">
                      <CheckboxGroup>
                        <Checkbox label="Save payee details" />
                      </CheckboxGroup>
                    </Column>
                  </Columns>
                </>
              )}
            </>
          )}
        </>
      ) : step === 3 ? (
        <>
          <Columns multiline mobile>
            <Column>
              <Tile ancestor>
                <Tile parent vertical>
                  <p className={activeBreakpoints.below.sm ? 'mb-1' : ''}>
                    From account
                  </p>
                  <CashTransferModalAccountsCard
                    onClick={() => {
                      dispatch({
                        type: 'handleStep',
                        payload: 1
                      });
                      dispatch({
                        type: 'updateTransferType',
                        payload: 'from'
                      });
                    }}
                    accountDetails={
                      selectedFromAccount
                        ? selectedFromAccount
                        : defaultFromAccount && !selectedFromAccount
                        ? defaultFromAccount
                        : cash.selectedAccount
                    }
                    step={step}
                  />
                </Tile>
                <Tile parent vertical>
                  <p className={activeBreakpoints.below.sm ? 'mb-1' : ''}>
                    To account
                  </p>
                  <CashTransferModalAccountsCard
                    onClick={() => {
                      dispatch({
                        type: 'handleStep',
                        payload: 2
                      });
                      dispatch({
                        type: 'updateTransferType',
                        payload: 'to'
                      });
                    }}
                    accountDetails={
                      selectedToAccount
                        ? selectedToAccount
                        : defaultToAccount && !selectedToAccount
                        ? defaultToAccount
                        : cash.selectedAccount
                    }
                    step={step}
                  />
                </Tile>
              </Tile>
            </Column>
          </Columns>

          <Columns mobile multiline>
            <Column
              mobile={12}
              tablet={6}
              desktop={6}
              className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
              <small className="is-pulled-right">
                Avail. balance: $
                {selectedFromAccount
                  ? selectedFromAccount.available
                  : defaultFromAccount && !selectedFromAccount
                  ? defaultFromAccount.available
                  : cash.selectedAccount.available}
              </small>
              <Textfield
                required
                label="Amount"
                placeholder="Enter a value"
                prefix="$"
                error={error.includes('invalid-data-input')}
                errorMessage="Please enter a value"
              />
            </Column>
            <Column
              mobile={12}
              tablet={3}
              desktop={3}
              className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
              <RadioGroup
                name="transferWhen"
                label="Transfer When"
                required
                buttons
                fullwidth
                selectedValue={transferWhen}
                onChange={updateTransferWhen}>
                {transferWhenOptions.map(item => (
                  <Radio
                    spreadEvenly
                    button
                    value={item.value}
                    label={item.label}
                    className="mb-0"
                  />
                ))}
              </RadioGroup>
            </Column>

            {transferWhen === 'later' && (
              <Column
                mobile={12}
                tablet={3}
                desktop={3}
                className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                <Select
                  label="Frequency"
                  name="frequency"
                  required
                  isSearchable={false}
                  maxMenuHeight={140}
                  defaultValue={transferFrequency[0]}
                  options={transferFrequency}
                  value={selectedTransferFrequency}
                  onChange={handleChange}
                />
              </Column>
            )}
          </Columns>

          {transferWhen === 'later' && (
            <Columns moblie multiline>
              <Column size="12" className="py-0">
                <p className="mb-0">
                  Transfer {selectedTransferFrequency.value}
                </p>
              </Column>
              <Column
                mobile={12}
                tablet={6}
                desktop={6}
                className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                <DatePicker label="Start date" />
                {/* <Textfield
                  required
                  label="Start date"
                  prependBoxedIcon="date"
                  error={error.includes('invalid-data-input')}
                  errorMessage="Please enter a start date"
                /> */}
              </Column>
              {selectedTransferFrequency.value !== 'once' &&
              selectedTransferFrequency.value !== 'daily' ? (
                <Column
                  mobile={12}
                  tablet={6}
                  desktop={6}
                  className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
                  <Textfield
                    required
                    label="End date"
                    prependBoxedIcon="date"
                    error={error.includes('invalid-data-input')}
                    errorMessage="Please enter a end date"
                  />
                </Column>
              ) : (
                ''
              )}
            </Columns>
          )}

          <Columns moblie multiline className="mb-0">
            <Column size="12" className="py-0">
              <p className="mb-0">Other details</p>
            </Column>

            <Column
              mobile={12}
              tablet={6}
              desktop={6}
              className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
              <Textfield
                required
                label="Remitter name"
                placeholder="Enter a name"
                error={error.includes('invalid-data-input')}
                errorMessage="Please enter a name"
              />
            </Column>

            <Column
              mobile={12}
              tablet={6}
              desktop={6}
              className={activeBreakpoints.below.sm ? 'pt-0' : ''}>
              <Textfield
                required
                label="Description"
                placeholder="Enter a description"
                error={error.includes('invalid-data-input')}
                errorMessage="Please enter a description"
              />
            </Column>
          </Columns>
        </>
      ) : step === 4 ? (
        <Columns multiline mobile className="mb-0">
          <Column size="12" className="pb-0">
            <CashTransferModalSummary
              step={step}
              summary={Summary}
              error={error}
            />
          </Column>
        </Columns>
      ) : step === 5 ? (
        <>
          <Columns multiline mobile className="mb-0">
            <Column size="12" className="pb-0">
              <p>
                To complete the transfer, please enter the 6-digit code send to
                XXXX XXX XXX
              </p>
              <div className="nab-multifield-input">
                <Textfield maxLength="1" validationIcon={false} />
                <Textfield maxLength="1" validationIcon={false} />
                <Textfield maxLength="1" validationIcon={false} />
                <Textfield maxLength="1" validationIcon={false} />
                <Textfield maxLength="1" validationIcon={false} />
                <Textfield maxLength="1" validationIcon={false} />
              </div>

              <p className="has-text-centered">
                Didn't receive a code? <br />{' '}
                <a href="/#">Click here to resend</a> or call us on 1300 123 456
              </p>
            </Column>
          </Columns>
        </>
      ) : step === 6 ? (
        <Columns multiline mobile className="mb-0">
          <Column size="12" className="pb-0">
            <CashTransferModalSummary step={step} summary={transferComplete} />
          </Column>
        </Columns>
      ) : (
        ''
      )}

      <div
        className="pos-f px-4 py-2 has-bg-white has-elevation-2 "
        style={{ right: '1em', bottom: '1em', zIndex: 100 }}>
        <Columns mobile multiline>
          <Column>
            <Title size="6" className="mb-1">
              Cash error handling
            </Title>
            <CheckboxGroup
              stacked
              className="mb-0"
              name="error"
              value={error}
              onChange={e => setError(e)}>
              <Checkbox value="create-order" label="Create/Amend Order" />
              <Checkbox value="invalid-data-input" label="Invalid Data Input" />
            </CheckboxGroup>
          </Column>
        </Columns>
      </div>
    </Modal>
  );
};

export default Breakpoints(CashTransferModal);

const transferWhenOptions = [
  {
    value: 'now',
    label: 'Now'
  },
  {
    value: 'later',
    label: 'Later'
  }
];

const payeeAccountOptions = [
  {
    value: 'from my payee list',
    label: 'From my payee list'
  },
  {
    value: 'new payee',
    label: 'New Payee'
  }
];

const transferFrequency = [
  {
    value: 'once',
    label: 'Once'
  },
  {
    value: 'daily',
    label: 'Daily'
  },
  {
    value: 'weekly',
    label: 'Weekly'
  },
  {
    value: 'fortnightly',
    label: 'Fortnightly'
  },
  {
    value: 'monthly',
    label: 'Monthly'
  },
  {
    value: 'quarterly',
    label: 'Quarterly'
  },
  {
    value: 'half yearly',
    label: 'Half-yearly'
  }
];

const Summary = [
  { name: 'From account', value: 'Cash Managed' },
  { name: 'To account', value: 'High Interest' },
  { name: 'Amount', value: '$1,000.00' },
  { name: 'Transfer when', value: 'Now' },
  { name: 'Remitter name', value: 'John Doe' },
  { name: 'Description', value: 'Cash transfer' }
];

const transferComplete = [
  { name: 'Receipt', value: '#123123' },
  { name: 'From account', value: 'Cash Managed' },
  { name: 'To account', value: 'High Interest' },
  { name: 'Amount', value: '$1,000.00' },
  { name: 'Transfer when', value: 'Now' }
];
