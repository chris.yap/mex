import React from 'react';
import { getState } from '../../../context/StateProvider';
import {
  Breakpoints,
  Card,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem
} from 'shaper-react';

const CashTransferModalSummary = ({ step, handleStep, summary, ...props }) => {
  const [, dispatch] = getState();
  return (
    <>
      {step === 4 && <p>Please ensure all details are correct</p>}
      <Card flat className={`${step === 4 && 'mb-2'}`}>
        <List>
          {summary.map((summ, sIndex) => (
            <ListItem key={sIndex}>
              <Level mobile style={{ width: '100%' }}>
                <LevelItem left>{summ.name}</LevelItem>
                <LevelItem right>
                  <strong>{summ.value}</strong>
                </LevelItem>
              </Level>
            </ListItem>
          ))}
        </List>
      </Card>
      {step === 4 && (
        <>
          <a
            href
            className="has-text-primary is-flex is-align-items-center mb-4"
            onClick={() => {
              dispatch({ type: 'handleStep', payload: step - 1 });
              dispatch({ type: 'updateSubmissionError', payload: false });
            }}>
            <Icon>arrow-left</Icon> Edit details
          </a>
          <p className="mb-0">
            Please ensure all details are correct. NAB cannot check the account
            name matches the BSB or account number. An incorrect BSB or account
            number will result in your money being paid to the wrong account
            number and may result in loss of your funds.
          </p>
        </>
      )}
    </>
  );
};

export default Breakpoints(CashTransferModalSummary);
