import React from 'react';
import cn from 'classnames';
import 'flag-icon-css/css/flag-icon.min.css';
import { NavLink } from 'react-router-dom';
import LayoutDefault from '../../../layout/LayoutDefault';
import {
  Alert,
  Button,
  Breakpoints,
  Card,
  CardContent,
  Container,
  Column,
  Columns,
  Divider,
  Hero,
  Icon,
  Label,
  Level,
  LevelItem,
  Modal,
  Section,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';
import { Breadcrumbs } from '../../../constants/breadcrumbs';
import { PortfolioItems } from '../../../constants';

class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cancelModal: false,
      cancelOrder: false
    };
  }
  toggleCancelModal = event => {
    this.setState({
      cancelModal: !this.state.cancelModal
    });
  };
  cancelOrder = event => {
    setTimeout(() => {
      this.setState({ cancelOrder: true });
    }, 2000);
  };
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <LayoutDefault
        breadcrumb={Breadcrumbs.portfolio.orderDetail.items}
        subnav={PortfolioItems}>
        <Hero bg="black" py={4}>
          <Container>
            <Level>
              <LevelItem left>
                <Title
                  size={cn(activeBreakpoints.below.sm ? '4' : '3')}
                  className="mb-0 is-flex is-align-items-center has-text-white">
                  <NavLink to="/">
                    <Button square white className="ml-0 mr-4">
                      <Icon className="has-text-size-1">arrow-left</Icon>
                    </Button>
                  </NavLink>
                  Order #12345678
                </Title>
              </LevelItem>
              <LevelItem right className={activeBreakpoints.below.sm && 'mt-2'}>
                <Level mobile>
                  <LevelItem>
                    <Button
                      mr={activeBreakpoints.below.sm && 1}
                      secondary
                      block={activeBreakpoints.below.sm}
                      disabled={this.state.cancelOrder ? true : false}>
                      <Icon>edit</Icon>
                      <span>Amend</span>
                    </Button>
                  </LevelItem>
                  <LevelItem>
                    <Button
                      ml={activeBreakpoints.below.sm && 1}
                      danger
                      block={activeBreakpoints.below.sm}
                      onClick={() => this.toggleCancelModal()}
                      disabled={this.state.cancelOrder ? true : false}>
                      <Icon>trash</Icon>
                      <span>Cancel</span>
                    </Button>
                  </LevelItem>
                </Level>
              </LevelItem>
            </Level>
          </Container>
        </Hero>

        <Section className={cn(activeBreakpoints.below.sm ? 'pa-0' : 'py-4')}>
          <Container>
            <Card className="mb-1">
              <CardContent
                className={cn(activeBreakpoints.below.sm ? 'pa-4' : 'pa-10')}>
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Account</Label>
                    <strong>Mr Andre Botes</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Account ID</Label>
                    <strong>NT-12345678-001</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns multiline mobile>
                  <Column size="6">
                    <Label className="nab-label mb-0">Sub account</Label>
                    <strong>Domestic trading account</strong>
                  </Column>
                </Columns>
              </CardContent>
              <Divider />
            </Card>
            {/* Conditional order */}
            <Card>
              <CardContent
                className={cn(activeBreakpoints.below.sm ? 'pa-4' : 'pa-10')}>
                <Alert
                  info
                  inverted
                  isOpened={true}
                  mb={activeBreakpoints.below.sm ? 4 : 6}>
                  If last price of BHP Group Ltd{' '}
                  <strong>decreases to or below $25.00</strong>, then place an
                  order to <strong>BUY 100 units</strong> of BHP.AS at price{' '}
                  <strong>$100.00</strong>.{' '}
                </Alert>
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">
                      Conditional order status
                    </Label>
                    <strong>Triggered</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">
                      Conditional order type
                    </Label>
                    <strong>Fixed</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Condition</Label>
                    <strong>Less or Equal</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Contingent price</Label>
                    <strong>$25.00</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">
                      Contingent base price
                    </Label>
                    <strong>Last</strong>
                  </Column>
                </Columns>
              </CardContent>
              <Divider />
            </Card>
            <Card>
              <CardContent
                className={cn(activeBreakpoints.below.sm ? 'pa-4' : 'pa-10')}>
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Security</Label>
                    <strong className="has-text-size3">
                      Myer Holdings Ltd
                    </strong>{' '}
                    <small>MYR | ASX</small>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Status</Label>
                    <strong>Inactive</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Order</Label>
                    <strong className="has-text-success is-uppercase">
                      Buy
                    </strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Order type</Label>
                    <strong className="">Limit</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Last action</Label>
                    <strong>Create</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Action status</Label>
                    <strong>Ok</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Order date & time</Label>
                    <strong>20.11.2018 | 9:45 am AEST</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Duration</Label>
                    <strong>Good Til Cancel</strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Price</Label>
                    <strong>$1.00</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">Currency</Label>
                    <strong>
                      <span
                        className={cn('mr-2', 'flag-icon', 'flag-icon-au')}
                      />
                      AUD
                    </strong>
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Quantity</Label>
                    <strong>500</strong>
                  </Column>
                  <Column size="6">
                    <Label className="nab-label mb-0">
                      Quantity filled/remaining
                    </Label>
                    <strong>0</strong> / 500
                  </Column>
                </Columns>
                <Divider
                  className={cn(activeBreakpoints.below.sm ? 'mb-4' : 'mb-6')}
                />
                <Columns
                  multiline
                  mobile
                  className={cn(activeBreakpoints.below.sm ? 'mb-0' : '')}>
                  <Column size="6">
                    <Label className="nab-label mb-0">Consideration</Label>
                    <strong>$0.00</strong>
                  </Column>
                </Columns>
              </CardContent>
              <Divider />
            </Card>

            <Card>
              <CardContent
                className={cn(
                  activeBreakpoints.below.sm ? 'py-4 px-0' : 'pa-10'
                )}>
                <Title size="4" className="px-4">
                  Order history
                </Title>
                <Table
                  hasStripes
                  headers={headers}
                  data={data}
                  tableRow={TRow}
                  mb={0}
                />
              </CardContent>
              <Divider />
            </Card>
            <Card>
              <CardContent
                className={cn(
                  activeBreakpoints.below.sm ? 'px-4 py-2' : 'px-10 py-4'
                )}>
                {this.state.cancelOrder && (
                  <p className="mb-2">
                    <em>Note: This order has been cancelled.</em>
                  </p>
                )}
                <Level>
                  <LevelItem left>
                    <Level mobile>
                      <LevelItem>
                        <Button
                          secondary
                          block={activeBreakpoints.below.sm}
                          mr={activeBreakpoints.below.sm && 1}
                          disabled={this.state.cancelOrder ? true : false}>
                          <Icon>edit</Icon>
                          <span>Amend</span>
                        </Button>
                      </LevelItem>
                      <LevelItem>
                        <Button
                          danger
                          block={activeBreakpoints.below.sm}
                          ml={activeBreakpoints.below.sm && 1}
                          onClick={() => this.toggleCancelModal()}
                          disabled={this.state.cancelOrder ? true : false}>
                          <Icon>trash</Icon>
                          <span>Cancel</span>
                        </Button>
                      </LevelItem>
                    </Level>
                  </LevelItem>
                  <LevelItem right className="is-hidden-mobile" />
                </Level>
              </CardContent>
            </Card>
          </Container>
        </Section>

        <Modal
          closeIcon
          onClose={() => this.toggleCancelModal()}
          isOpened={this.state.cancelModal}
          header={this.state.cancelOrder ? 'Order cancelled' : 'Cancel order'}
          footer={
            <React.Fragment>
              {this.state.cancelOrder ? (
                <Button primary block onClick={() => this.toggleCancelModal()}>
                  OK
                </Button>
              ) : (
                <Columns mobile className="is-flex-1">
                  <Column
                    size={activeBreakpoints.below.sm ? '6' : ''}
                    narrow={activeBreakpoints.below.sm ? false : true}>
                    <Button
                      secondary
                      block={activeBreakpoints.below.sm ? true : false}
                      className="px-8 mx-0"
                      onClick={() => this.toggleCancelModal()}>
                      No
                    </Button>
                  </Column>
                  <Column
                    size={activeBreakpoints.below.sm ? '6' : ''}
                    narrow={activeBreakpoints.below.sm ? false : true}>
                    <Button
                      primary
                      loading
                      block={activeBreakpoints.below.sm ? true : false}
                      className="px-8 mx-0"
                      onClick={() => this.cancelOrder()}>
                      Yes
                    </Button>
                  </Column>
                </Columns>
              )}
            </React.Fragment>
          }>
          {this.state.cancelOrder ? (
            <p className="mb-0">
              Your order has now been cancelled. <br />
              To view details, visit the inactive tab in order history.
            </p>
          ) : (
            <p className="mb-0">Would you like to cancel this order?</p>
          )}
        </Modal>
      </LayoutDefault>
    );
  }
}

export default Breakpoints(OrderDetails);

const headers = [
  { name: 'Date created', value: 'date' },
  { name: 'Order details', value: 'details' },
  { name: 'Action status', value: 'status' }
];

const data = [
  {
    date: '20.11.2018 | 9:45 am AEST',
    details: 'BUY 500 MYR:ASX at $1.00',
    status: 'Authorising'
  },
  {
    date: '20.11.2018 | 9:45 am AEST',
    details: 'BUY 500 MYR:ASX at $1.00',
    status: 'Creating'
  }
];

const TRow = ({ row }) => (
  <Row>
    <Cell>{row.date}</Cell>
    <Cell>{row.details}</Cell>
    <Cell className="is-uppercase">{row.status}</Cell>
  </Row>
);
