import React from 'react';
import cn from 'classnames';
import LayoutDefault from '../../../layout/LayoutDefault';
import HeroView from '../../../components/HeroView';
import {
  AccountSingleValue,
  AccountOption
} from '../../../helpers/CustomSelect';
import { Breadcrumbs } from '../../../constants/breadcrumbs';
import { PortfolioItems } from '../../../constants/';
import { accounts } from '../../../components/OrderPad/constants';
import { Link } from 'react-router-dom';
import { getState } from '../../../context/StateProvider';

import {
  Button,
  Breakpoints,
  Checkbox,
  CheckboxGroup,
  Container,
  Column,
  Columns,
  Level,
  LevelItem,
  Icon,
  Pagination,
  Section,
  Select,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Tag,
  Title
} from 'shaper-react';

const OrderHistory = ({ activeBreakpoints, ...props }) => {
  const [, dispatch] = getState();
  return (
    <LayoutDefault
      breadcrumb={Breadcrumbs.portfolio.orderHistory.items}
      subnav={PortfolioItems}>
      <HeroView>
        <Level>
          <LevelItem left>
            <Title className="font-nabimpact has-text-white mt-1 mb-0">
              Order history
            </Title>
          </LevelItem>
          <LevelItem right className={activeBreakpoints.below.sm && 'mt-2'}>
            <Button
              primary
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              <Icon>order</Icon>
              <span>Place order</span>
            </Button>
          </LevelItem>
        </Level>
      </HeroView>

      <Section className={cn('pt-4', activeBreakpoints.below.sm ? 'px-0' : '')}>
        <Container>
          <Columns>
            <Column
              mobile="12"
              className={cn(activeBreakpoints.below.sm ? 'py-2' : '')}>
              <Select
                height="60"
                options={accounts}
                value={accounts[0]}
                components={{
                  SingleValue: AccountSingleValue,
                  Option: AccountOption
                }}
                mx={activeBreakpoints.below.sm && 4}
              />
            </Column>
            <Column
              mobile="12"
              className={`is-align-self-center ${
                activeBreakpoints.below.sm ? 'py-0 has-text-centered pl-8' : ''
              }`}>
              <CheckboxGroup>
                <Checkbox
                  value="co"
                  label={
                    <span>
                      Show conditional orders <Tag small>CO</Tag> only
                    </span>
                  }
                />
              </CheckboxGroup>
            </Column>
            <Column className="has-text-right is-hidden-mobile is-flex is-align-items-center is-justify-content-flex-end">
              <Button inverted secondary className="mr-0">
                <Icon>download</Icon>
                <span>Download</span>
              </Button>
            </Column>
          </Columns>
          <Table
            hasStripes
            isHoverable
            headers={headers}
            data={InactiveData}
            tableRow={TableRow}
          />
          <Pagination current={1} total={10} />
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(OrderHistory);

const headers = [
  { name: 'Date', value: 'date', sort: true },
  { name: 'Action', value: 'action', sort: true },
  { name: 'Code', value: 'code', sort: true },
  { name: 'Qty', value: 'qty', align: 'right', sort: 'number' },
  { name: 'Price', value: 'price', align: 'right', sort: 'number' },
  { name: 'Duration', value: 'duration', sort: true },
  { name: 'Status', value: 'status', sort: true },
  {}
];

const Td = ({ children, to, className, ...props }) => {
  let content = to ? (
    <Link className="is-block" to={to}>
      {children}
    </Link>
  ) : (
    { children }
  );
  return (
    <Cell className={className} {...props}>
      {content}
    </Cell>
  );
};

const TableRow = ({ row }) => (
  <Row>
    <Td to="/portfolio/order-details" className="has-text-nowrap">
      {row.date}
    </Td>
    <Td to="/portfolio/order-details">
      <span
        className={`is-uppercase has-text-weight-semibold ${
          row.action === 'Buy' ? 'has-text-success' : 'has-text-danger'
        }`}>
        {row.action} {row.conditional && <Tag small>CO</Tag>}
      </span>
    </Td>
    <Td to="/portfolio/order-details" className="is-uppercase">
      <a href="/#" className="has-text-weight-semibold">
        {row.code}
      </a>
    </Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      {row.qty}
    </Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      ${row.price}
    </Td>
    <Td to="/portfolio/order-details">{row.duration}</Td>
    <Td to="/portfolio/order-details">{row.status}</Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      <Button small square secondary>
        <Icon>arrow-right</Icon>
      </Button>
    </Td>
  </Row>
);

const InactiveData = [
  {
    date: '02-06-2014',
    action: 'Buy',
    code: 'PLXS',
    exchange: 'NASDAQ',
    qty: '1,976',
    price: '211.08',
    duration: 'Good til cancel',
    status: 'Inactive'
  },
  {
    date: '27-11-2018',
    action: 'Buy',
    code: 'APA',
    exchange: 'ASX',
    qty: '3,163',
    price: '21.37',
    duration: 'Good til cancel',
    status: 'Filled'
  },
  {
    date: '05-07-2017',
    action: 'Sell',
    conditional: true,
    code: 'SBUX',
    exchange: 'NASDAQ',
    qty: '2,631',
    price: '38.23',
    duration: 'Good til cancel',
    status: 'Filled'
  },
  {
    date: '18-01-2017',
    action: 'Sell',
    code: 'CAP',
    exchange: 'ASX',
    qty: '1,969',
    price: '27.73',
    duration: 'End of day',
    status: 'Filled'
  },
  {
    date: '01-08-2015',
    action: 'Buy',
    code: 'AMC',
    exchange: 'ASX',
    qty: '3,135',
    price: '10.19',
    duration: 'End of day',
    status: 'Filled'
  },
  {
    date: '23-03-2017',
    action: 'Buy',
    code: 'GOOG',
    exchange: 'NASDAQ',
    qty: '2,535',
    price: '125.22',
    duration: 'Good til cancel',
    status: 'Inactive'
  },
  {
    date: '02-06-2014',
    action: 'Buy',
    code: 'BHP',
    exchange: 'ASX',
    qty: '3,466',
    price: '34.28',
    duration: 'Good til cancel',
    status: 'Inactive'
  },
  {
    date: '28-06-2015',
    action: 'Sell',
    conditional: true,
    code: 'AMZN',
    exchange: 'NASDAQ',
    qty: '1,623',
    price: '158.91',
    duration: 'Good til cancel',
    status: 'Inactive'
  },
  {
    date: '25-11-2015',
    action: 'Sell',
    code: 'AAPL',
    exchange: 'NASDAQ',
    qty: '2,955',
    price: '231.67',
    duration: 'Good til cancel',
    status: 'Inactive'
  }
];
