import React from 'react';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import LayoutDefault from '../../../layout/LayoutDefault';
import HeroView from '../../../components/HeroView';
import {
  AccountSingleValue,
  AccountOption
} from '../../../helpers/CustomSelect';
import { Breadcrumbs } from '../../../constants/breadcrumbs';
import { PortfolioItems } from '../../../constants/';
import { accounts } from '../../../components/OrderPad/constants';
import { getState } from '../../../context/StateProvider';

import {
  Button,
  Breakpoints,
  Checkbox,
  CheckboxGroup,
  Container,
  Column,
  Columns,
  Icon,
  Level,
  LevelItem,
  Section,
  Select,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Tag,
  Title
} from 'shaper-react';

const OpenOrders = ({ activeBreakpoints, ...props }) => {
  const [, dispatch] = getState();
  return (
    <LayoutDefault
      breadcrumb={Breadcrumbs.portfolio.openOrders.items}
      subnav={PortfolioItems}>
      <HeroView>
        <Level>
          <LevelItem left>
            <Title className="font-nabimpact has-text-white mt-1 mb-0">
              Open orders
            </Title>
          </LevelItem>
          <LevelItem right className={activeBreakpoints.below.sm && 'mt-2'}>
            <Button
              primary
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              <Icon>order</Icon>
              <span>Place order</span>
            </Button>
          </LevelItem>
        </Level>
      </HeroView>

      <Section className={cn('pt-4', activeBreakpoints.below.sm ? 'px-0' : '')}>
        <Container>
          <Columns>
            <Column mobile="12">
              <Select
                height="60"
                options={accounts}
                value={accounts[0]}
                components={{
                  SingleValue: AccountSingleValue,
                  Option: AccountOption
                }}
                mx={activeBreakpoints.below.sm && 4}
              />
            </Column>
            <Column
              mobile="12"
              className={`is-align-self-center ${
                activeBreakpoints.below.sm ? 'py-0 has-text-centered pl-8' : ''
              }`}>
              <CheckboxGroup>
                <Checkbox
                  value="co"
                  label={
                    <span>
                      Show conditional orders <Tag small>CO</Tag> only
                    </span>
                  }
                />
              </CheckboxGroup>
            </Column>
            <Column className="has-text-right is-hidden-mobile is-flex is-align-items-flex-end is-justify-content-flex-end">
              <Button inverted secondary className="mr-0">
                <Icon>download</Icon>
                <span>Download</span>
              </Button>
            </Column>
          </Columns>
          <Table
            hasStripes
            isHoverable
            headers={headers}
            data={openData}
            tableRow={TableRow}
          />
        </Container>
      </Section>
    </LayoutDefault>
  );
};

export default Breakpoints(OpenOrders);

const headers = [
  { name: 'Date', value: 'date', sort: true },
  { name: 'Action', value: 'action', sort: true },
  { name: 'Code', value: 'code', sort: true },
  { name: 'Qty', value: 'qty', align: 'right', sort: 'number' },
  { name: 'Price', value: 'price', align: 'right', sort: 'number' },
  { name: 'Duration', value: 'duration', sort: true },
  { name: 'Status', value: 'status', sort: true },
  {}
];

const Td = ({ children, to, className, ...props }) => {
  let content = to ? (
    <Link to={to} className="is-block">
      {children}
    </Link>
  ) : (
    { children }
  );
  return (
    <Cell className={className} {...props}>
      {content}
    </Cell>
  );
};

const TableRow = ({ row }) => (
  <Row className="">
    <Td to="/portfolio/order-details">{row.date}</Td>
    <Td to="/portfolio/order-details">
      <span
        className={`is-uppercase has-text-weight-semibold ${
          row.action === 'Buy' ? 'has-text-success' : 'has-text-danger'
        }`}>
        {row.action} {row.conditional && <Tag small>CO</Tag>}
      </span>
    </Td>
    <Td to="/portfolio/order-details" className="is-uppercase">
      <a href="/#" className="has-text-weight-semibold">
        {row.code}
      </a>
    </Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      {row.qty}
    </Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      ${row.price}
    </Td>
    <Td to="/portfolio/order-details">{row.duration}</Td>
    <Td to="/portfolio/order-details">{row.status}</Td>
    <Td to="/portfolio/order-details" className="has-text-right">
      <Button small square inverted secondary className="mr-0">
        <Icon>pencil</Icon>
      </Button>
      <Button small square inverted danger className="mr-0">
        <Icon>close</Icon>
      </Button>
      <Button small square secondary className="mr-0">
        <Icon>arrow-right</Icon>
      </Button>
    </Td>
  </Row>
);

const openData = [
  {
    id: '8867549650',
    date: '26-10-2014',
    action: 'Buy',
    code: 'AMZN',
    exchange: 'NASDAQ',
    qty: '2,373',
    price: '265.71',
    duration: 'End of day',
    status: 'Authorising'
  },
  {
    id: '2994934427',
    date: '15-06-2014',
    action: 'Sell',
    code: 'AAPL',
    exchange: 'NASDAQ',
    qty: '2,428',
    price: '133.54',
    duration: 'Good til cancel',
    status: 'Authorising'
  },
  {
    id: '6899566653',
    date: '10-06-2018',
    action: 'Sell',
    conditional: true,
    code: 'MSFT',
    exchange: 'NASDAQ',
    qty: '3,662',
    price: '312.02',
    duration: 'Good til cancel',
    status: 'Authorising'
  },
  {
    id: '1579319030',
    date: '05-03-2018',
    action: 'Buy',
    conditional: true,
    code: 'TOP',
    exchange: 'ASX',
    qty: '3,182',
    price: '101.29',
    duration: 'Good til cancel',
    status: 'Authorising'
  },
  {
    id: '7224097567',
    date: '04-03-2018',
    action: 'Sell',
    code: 'NFLX',
    exchange: 'NASDAQ',
    qty: '1,677',
    price: '308.41',
    duration: 'End of day',
    status: 'Authorising'
  },
  {
    id: '0449176762',
    date: '26-11-2017',
    action: 'Sell',
    code: 'BHP',
    exchange: 'ASX',
    qty: '3,877',
    price: '33.41',
    duration: 'Good til cancel',
    status: 'Authorising'
  }
];
