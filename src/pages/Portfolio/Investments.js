import React from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';
import {
  Alert,
  Breakpoints,
  Button,
  Card,
  CardContent,
  Checkbox,
  CheckboxGroup,
  Collapse,
  Div,
  Divider,
  Dropdown,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Modal,
  Subtitle,
  Tab,
  Tabs,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';
import FxRates from './FxRates';
import PercentageView from '../../components/PercentageView';
import { DomesticAcc } from '../../constants/investmentsAccounts';

class Investments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
      action: false,
      showHiddenAccts: false,
      hideAction: null,
      hideAccts: [],
      unHideAccts: [],
      index: 100
    };
    this.selectOption = this.selectOption.bind(this);
    this.closeAction = this.closeAction.bind(this);
  }
  componentDidUpdate = prevProps => {
    if (this.props.noPortfolio !== prevProps.noPortfolio) {
      //update
    }
  };
  selectOption(type, row) {
    // console.log(type, row);
    if (type === 'modal' && row) {
      this.setState({
        selectedOption: row,
        action: true
      });
    }
    if (type === 'orderpad' && row) {
      this.props.toggleOrderPad(row);
    }
  }
  closeAction = event => {
    this.setState({
      action: false
    });
  };
  clearOption = event => {
    this.setState({
      selectedOption: null,
      action: false
    });
  };
  hideAccount = event => {
    let hiddenAccts = this.state.hideAccts;
    let index = this.state.hideAccts.indexOf(event);
    if (index < 0) {
      hiddenAccts.push(event);
    } else {
      hiddenAccts.splice(index, 1);
    }
    this.setState({
      hideAccts: hiddenAccts,
      unHideAccts: hiddenAccts,
      hideAction: null
    });
  };
  updateTempHideAccts = event => {
    let tempHiddenAccts = this.state.unHideAccts;
    let tempIndex = this.state.unHideAccts.indexOf(event);
    if (tempIndex < 0) {
      tempHiddenAccts.push(event);
    } else {
      tempHiddenAccts.splice(tempIndex, 1);
    }
  };
  render() {
    const { activeBreakpoints, portfolio, data, error, ...props } = this.props;
    let index = 30;
    return (
      <React.Fragment>
        <Tabs className={cn('mt-1', activeBreakpoints.above.xs ? 'px-6' : '')}>
          <Tab label="Standard" icon="asset" className="pt-0">
            <CardContent
              className={cn(activeBreakpoints.below.sm ? 'px-0 pt-0' : 'pt-0')}>
              {error.includes('retrieve-holdings') ? (
                <Alert isOpened={true} inverted warning>
                  <p className="pos-r has-top3 mb-1">
                    Unable to retrieve your holdings information at this moment.
                    Please try again or call <a href="tel:131380">13 13 80</a>.
                  </p>
                  <Button small warning className="mx-0">
                    Refresh page
                  </Button>
                </Alert>
              ) : (
                <>
                  {portfolio.includes('domestic') ? (
                    <React.Fragment>
                      {DomesticAcc.map((dom, dI) => {
                        index -= 1;
                        return (
                          <React.Fragment key={dI}>
                            <Collapse
                              isOpened={this.state.hideAccts.indexOf(dI) < 0}>
                              <div>
                                <Level
                                  mobile
                                  className={cn(
                                    'mb-1',
                                    activeBreakpoints.below.sm ? 'px-4' : ''
                                  )}>
                                  <LevelItem left>
                                    {(this.state.hideAction !== dI ||
                                      activeBreakpoints.above.xs) && (
                                      <p className="mb-0 has-text-size1">
                                        <strong>{dom.label}</strong>{' '}
                                        <small> - {dom.value}</small>
                                      </p>
                                    )}
                                  </LevelItem>
                                  <LevelItem right>
                                    {this.state.hideAccts.length <
                                      DomesticAcc.length - 1 && (
                                      <React.Fragment>
                                        {this.state.hideAction === dI ? (
                                          <p className="my-1 is-flex is-align-items-center has-text-blue-grey has-text-size-1">
                                            Hide this account
                                            <Button
                                              square
                                              secondary
                                              inverted
                                              className="my-0 ml-4"
                                              onClick={() =>
                                                this.hideAccount(dI)
                                              }>
                                              <Icon>checked</Icon>
                                            </Button>
                                            <Button
                                              square
                                              danger
                                              inverted
                                              className="my-0 mr-0 ml-0"
                                              onClick={() =>
                                                this.setState({
                                                  hideAction: null
                                                })
                                              }>
                                              <Icon>close</Icon>
                                            </Button>
                                          </p>
                                        ) : (
                                          <Button
                                            square
                                            flat
                                            className="mr-0"
                                            onClick={() =>
                                              this.setState({
                                                hideAction: dI
                                              })
                                            }>
                                            <Icon>hide</Icon>
                                          </Button>
                                        )}
                                      </React.Fragment>
                                    )}
                                  </LevelItem>
                                </Level>
                                <Divider dotted />
                                <Table
                                  hasStripes
                                  hasNoScroll={activeBreakpoints.above.xs}
                                  stickyHeaderCount={activeBreakpoints.below.sm}
                                  stickyColumnCount={activeBreakpoints.below.sm}
                                  headers={Headers}
                                  data={AusData}
                                  tableRow={(props, index) => (
                                    <AudRow
                                      key={index}
                                      activeBreakpoints={activeBreakpoints}
                                      error={error}
                                      {...props}
                                    />
                                  )}
                                  defaultSortCol="code"
                                  mt={0}
                                  clickFunc={this.selectOption}
                                  style={{ zIndex: index }}
                                />
                              </div>
                            </Collapse>
                          </React.Fragment>
                        );
                      })}

                      <Title
                        className={cn(
                          activeBreakpoints.below.sm ? 'px-4' : '',
                          'mb-4  has-text-size3'
                        )}>
                        Australian total: $50,000.00
                      </Title>
                    </React.Fragment>
                  ) : (
                    <Title
                      size="5"
                      className="has-text-blue-grey text--lighten-2 py-4 has-text-centered">
                      No domestic holdings found
                    </Title>
                  )}
                </>
              )}

              {this.state.hideAccts.length > 0 && (
                <p className="has-text-centered mb-0">
                  <Button
                    inverted
                    danger
                    onClick={() => this.setState({ showHiddenAccts: true })}>
                    {this.state.hideAccts.length} hidden domestic account
                    {this.state.hideAccts.length > 1 ? 's' : ''}
                  </Button>
                </p>
              )}

              <Divider className="my-6" />
              {error.includes('international-holdings') &&
              !error.includes('retrieve-holdings') ? (
                <Alert isOpened={true} warning inverted>
                  <p className="pos-r has-top3 mb-1">
                    Unable to retrieve your holdings information at this moment.
                    Please try again or call <a href="tel:131380">13 13 80</a>.
                  </p>
                  <Button small warning className="mx-0">
                    Refresh page
                  </Button>
                </Alert>
              ) : error.includes('retrieve-holdings') ? null : (
                <React.Fragment>
                  {portfolio.includes('international') ? (
                    <React.Fragment>
                      <Table
                        hasStripes
                        hasNoScroll={activeBreakpoints.above.xs}
                        stickyHeaderCount={activeBreakpoints.below.sm}
                        stickyColumnCount={activeBreakpoints.below.sm}
                        headers={Headers}
                        data={IntlData}
                        tableRow={(props, index) => (
                          <IntlRow
                            key={index}
                            activeBreakpoints={activeBreakpoints}
                            {...props}
                          />
                        )}
                        defaultSortCol="code"
                        mt={0}
                        clickFunc={this.selectOption}
                        style={{ zIndex: 10 }}
                      />

                      <Title
                        className={cn(
                          'mb-0 has-text-size3',
                          activeBreakpoints.below.sm ? 'px-4' : ''
                        )}>
                        International total: $50,000.00
                      </Title>
                    </React.Fragment>
                  ) : (
                    <Title
                      size="5"
                      className="has-text-blue-grey text--lighten-2 py-4 has-text-centered">
                      No international holdings found
                    </Title>
                  )}
                </React.Fragment>
              )}

              <FxRates
                className={cn(
                  'mt-8 mb-4',
                  activeBreakpoints.below.sm ? 'mx-2' : ''
                )}
              />
            </CardContent>
            <Divider />
            <CardContent className="has-bg-sky lighten-5">
              <p className="has-text-blue-grey has-text-size-2 mb-1 has-line-height-125">
                ASX quotes are provided by IRESS Market Ltd and are as of
                30-01-2019 3:19pm AEDT.
              </p>
              <p className="has-text-blue-grey has-text-size-2 mb-1 has-line-height-125">
                International quotes are provided by IRESS Market Ltd and are
                delayed at least 15 minutes as of 30-01-2019 3:19pm AEDT.
                {activeBreakpoints.above.xs && <br />}
                All quotes in local currency.
              </p>
            </CardContent>
          </Tab>

          <Tab label="Dividend" icon="moneybag">
            <CardContent
              className={cn(activeBreakpoints.below.sm ? 'px-0' : '')}>
              <Title size="4" className="mt-4">
                Dividend
              </Title>
            </CardContent>
          </Tab>
          <Tab label="Today's performance" icon="chart">
            <CardContent
              className={cn(activeBreakpoints.below.sm ? 'px-0' : '')}>
              <Title size="4" className="mt-4">
                Today's performance
              </Title>
            </CardContent>
          </Tab>
        </Tabs>

        <Modal
          isOpened={this.state.action && activeBreakpoints.below.lg}
          onClose={this.closeAction}
          closeIcon>
          <Button
            fullwidth
            primary
            className="mb-4"
            onClick={() => props.toggleOrderPad(this.state.selectedOption)}>
            Trade{' '}
            {this.state.selectedOption && (
              <strong>&nbsp;{this.state.selectedOption.code}</strong>
            )}
          </Button>
          <Button fullwidth>Edit cost base</Button>
          <Button fullwidth>Set up Alert</Button>
          <Button fullwidth className="mb-4">
            Add to Watch list
          </Button>
          <Button fullwidth>Set up Stop Loss Order</Button>
          <Button fullwidth>Set up Take Profit Order</Button>
        </Modal>

        <Modal
          header="Hidden accounts"
          footer={
            <Button
              secondary
              onClick={() => this.setState({ showHiddenAccts: false })}>
              Close
            </Button>
          }
          isOpened={this.state.showHiddenAccts}
          className="has-bg-blue-grey lighten-5">
          <p>Unhide your accounts</p>
          <Card>
            <List>
              {DomesticAcc.map((dom, dI) => (
                <React.Fragment key={dI}>
                  {this.state.hideAccts.indexOf(dI) !== -1 && (
                    <ListItem>
                      <Level mobile className="is-flex-1">
                        <LevelItem left>
                          <div>
                            <Title size="6" className="mb-4">
                              {dom.label}
                            </Title>
                            <Subtitle size="7" className="mb-0">
                              {dom.value}
                            </Subtitle>
                          </div>
                        </LevelItem>
                        <LevelItem right>
                          <CheckboxGroup
                            isSwitch
                            value={this.state.unHideAccts}
                            onChange={() => this.updateTempHideAccts(dI)}>
                            <Checkbox value={dI} />
                          </CheckboxGroup>
                        </LevelItem>
                      </Level>
                    </ListItem>
                  )}
                </React.Fragment>
              ))}
            </List>
          </Card>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Breakpoints(Investments);

const Headers = [
  {
    name: <span>Code</span>,
    value: 'code',
    sort: true
  },
  {
    name: (
      <span>
        Today's
        <br /> change
      </span>
    ),
    value: 'changeValue',
    align: 'right',
    sort: 'number'
  },
  {
    name: <span>Gain/Loss</span>,
    value: 'gainLoss',
    align: 'right',
    sort: 'number'
  },
  {
    name: (
      <span>
        Avail
        <br /> Qty
      </span>
    ),
    value: 'qty',
    align: 'right',
    sort: 'number'
  },
  {
    name: (
      <span>
        Average
        <br /> price
      </span>
    ),
    value: 'avg',
    align: 'right',
    sort: 'number'
  },
  {
    name: (
      <span>
        Market
        <br /> price
      </span>
    ),
    value: 'market',
    align: 'right',
    sort: 'number'
  },
  {
    name: (
      <span>
        Market
        <br /> value
      </span>
    ),
    value: 'value',
    align: 'right',
    sort: 'number'
  },
  {
    value: ''
  }
];

const AudRow = ({ activeBreakpoints, row, ...props }) => (
  <Row>
    <Cell width="1">
      {row.link ? (
        <NavLink to={row.link}>{row.code}</NavLink>
      ) : (
        <a href="/#">{row.code}</a>
      )}
      {props.error.includes('alert-history') ? null : (
        <>
          {row.alert && (
            <Div
              display="inline-block"
              ml={1}
              fontSize="12px"
              position="relative"
              top="1px">
              <Icon small>bell</Icon>
            </Div>
          )}
        </>
      )}
    </Cell>
    <Cell right color="secondary">
      {row.changeValue}
      <span
        className={cn(
          'ml-1',
          row.changePercent > 0
            ? 'has-text-success'
            : row.changePercent < 0
            ? 'has-text-danger'
            : ''
        )}>
        <PercentageView value={row.changePercent} />
      </span>
    </Cell>
    <Cell right color="secondary">
      {row.gainLoss}
      <span
        className={cn(
          'ml-1',
          row.gainLossPercent > 0
            ? 'has-text-success'
            : row.gainLossPercent < 0
            ? 'has-text-danger'
            : ''
        )}>
        <PercentageView value={row.gainLossPercent} />
      </span>
    </Cell>
    <Cell right color="secondary">
      {row.qty}
    </Cell>
    <Cell right color="secondary">
      {row.avg}
    </Cell>
    <Cell right color="secondary">
      {row.market}
    </Cell>
    <Cell right color="secondary">
      {row.value}
    </Cell>
    <Cell right>
      {activeBreakpoints.below.lg ? (
        <Button
          small
          secondary
          square
          onClick={() => props.clickFunc('modal', row)}>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2">Edit cost base</ListItem>
            <ListItem className="py-2">Set up Alert</ListItem>
            <ListItem className="py-2">Add to Watchlist</ListItem>
            <ListItem className="py-2">Set up Stop Loss Order</ListItem>
            <ListItem className="py-2 has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);

const IntlRow = ({ activeBreakpoints, row, ...props }) => (
  <Row>
    <Cell width="1px" hasNoWrap>
      {row.link ? (
        <NavLink to={row.link}>{row.code}</NavLink>
      ) : (
        <a href="/#">{row.code}</a>
      )}
      {row.alert && (
        <Div
          display="inline-block"
          ml={1}
          fontSize="12px"
          position="relative"
          top="1px">
          <Icon small>bell</Icon>
        </Div>
      )}
    </Cell>
    <Cell right color="secondary" lineHeight={1}>
      <Div>
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          AUD
        </sup>
        {row.changeValue}
        <PercentageView value={row.changePercent} />
      </Div>
      <Div pb={1} className="has-text-size-2">
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          {row.currency}
        </sup>
        {row.changeValue2}
        <PercentageView value={row.changePercent2} />
      </Div>
    </Cell>
    <Cell right color="secondary" lineHeight={1}>
      <Div>
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          AUD
        </sup>
        {row.gainLoss}
        <PercentageView value={row.gainLossPercent} />
      </Div>
      <Div pb={1} className="has-text-size-2">
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          {row.currency}
        </sup>
        {row.gainLoss}
        <PercentageView value={row.gainLossPercent2} />
      </Div>
    </Cell>
    <Cell right color="secondary">
      {row.qty}
    </Cell>
    <Cell right color="secondary">
      <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
        {row.currency}
      </sup>
      {row.avg}
    </Cell>
    <Cell right color="secondary">
      <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
        {row.currency}
      </sup>
      {row.market}
    </Cell>
    <Cell right color="secondary" lineHeight={1}>
      <Div>
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          AUD
        </sup>
        {row.value}
      </Div>
      <Div pb={1} className="has-text-size-2">
        <sup style={{ position: 'relative', top: '6px' }} className="mr-1">
          {row.currency}
        </sup>
        {row.value}
      </Div>
    </Cell>
    <Cell right>
      {activeBreakpoints.below.lg ? (
        <Button
          small
          secondary
          square
          onClick={() => props.clickFunc('modal', row)}>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2">Edit cost base</ListItem>
            <ListItem className="py-2">Set up Alert</ListItem>
            <ListItem className="py-2">Add to Watchlist</ListItem>
            <ListItem className="py-2">Set up Stop Loss Order</ListItem>
            <ListItem className="py-2 has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);

const AusData = [
  {
    name: 'BHP Billiton Group Ltd',
    link: '/security/details',
    label: 'BHP Group Ltd',
    exchange: 'ASX',
    industry: 'Metals and Mining, Petroleum',
    code: 'BHP.ASX',
    alert: 2,
    changeValue: '-$50,113.50',
    changePercent: '-1.52',
    gainLoss: '$312,240.75',
    gainLossPercent: '10.66',
    qty: '100,227',
    avg: '$29.23',
    market: '$32.35',
    value: '$324,343.45'
  },
  {
    name: 'Commonwealth Bank of Aust.',
    label: 'Commonwealth Bank of Aust.',
    exchange: 'ASX',
    industry: 'Financials',
    code: 'CBA.ASX',
    changeValue: '-$113.68',
    changePercent: '-1.64',
    gainLoss: '$546.84',
    gainLossPercent: '-7.41',
    qty: '98',
    avg: '$75.32',
    market: '$69.99',
    value: '$6,859.02'
  },
  {
    name: 'Vanguard Aust. Fixed Interest',
    label: 'Vanguard Aust. Fixed Interest',
    exchange: 'ASX',
    industry: 'Financials',
    code: 'VAF.ASX',
    changeValue: '65.10',
    changePercent: '0.14',
    gainLoss: '$3,730.06',
    gainLossPercent: '9.01',
    qty: '930',
    avg: '$47.32',
    market: '$48.60',
    value: '$45,198.03'
  }
];

const IntlData = [
  {
    code: 'GOOG.NAS',
    alert: 2,
    currency: 'USD',
    changeValue: '-$60,113.50',
    changePercent: '-11.52',
    gainLoss: '$1,234.50',
    gainLossPercent: '-2.52',

    changeValue2: '-$40,244.50',
    changePercent2: '-10.66',
    gainLoss2: '$893.25',
    gainLossPercent2: '4.33',
    qty: '10',
    avg: '$29.23',
    market: '$32.35',
    value: '$324,343.45'
  },
  {
    code: '0005.HKD',
    currency: 'HKD',
    changeValue: '-$50.113.50',
    changePercent: '-5.52',
    gainLoss: '-1,234.50',
    gainLossPercent: '-1.45',
    changeValue2: '$283,342.48',
    changePercent2: '25.32',
    gainLoss2: '$6,829.56',
    gainLossPercent2: '4.32',
    qty: '98',
    avg: '$75.42',
    market: '$395.92',
    value: '$38,799.14'
  },
  {
    code: 'GGO.AME',
    currency: 'USD',
    changeValue: '$65.12',
    changePercent: '0.31',
    gainLoss: '$3,446.56',
    gainLossPercent: '9.34',
    changeValue2: '$46.54',
    changePercent2: '0.34',
    gainLoss2: '$2,556.22',
    gainLossPercent2: '2.45',
    qty: '930',
    avg: '$0.93',
    market: '$16.34',
    value: '$45,653.23'
  }
];
