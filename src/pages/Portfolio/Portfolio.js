import React from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import NabAccountCards from '../NAB/NabAccountCards';
import { PortfolioItems } from '../../constants/';
import { Portfolios } from '../../constants/portfolio';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { getState } from '../../context/StateProvider';

import {
  Breakpoints,
  Button,
  Card,
  CardHeader,
  CheckboxGroup,
  Checkbox,
  Collapse,
  Column,
  Columns,
  Container,
  Divider,
  Dropdown,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Modal,
  Section,
  Subtitle,
  Title
} from 'shaper-react';

const Portfolio = ({ activeBreakpoints }) => {
  const [expandedCard, setExpandedCard] = React.useState(false);
  const [modal, setModal] = React.useState(false);
  const [errorHandling] = React.useState(true);
  const [error, setError] = React.useState([]);
  const [, dispatch] = getState();

  let ToggleModal = value => {
    setModal(value ? value : modal);
  };

  return (
    <LayoutDefault
      subnav={PortfolioItems}
      breadcrumb={Breadcrumbs.portfolio.items}>
      <HeroView className="is-overflow-visible" zIndex="10">
        <Level>
          <LevelItem left>
            <Dropdown
              activator={
                <Title className="font-nabimpact has-text-white mt-1 mb-0 is-flex">
                  <span className="is-flex-1">All portfolio</span>

                  <Button square small fontSize="1.25rem" mx={4} mt={1}>
                    <Icon className="has-text-primary">arrow-down</Icon>
                  </Button>
                </Title>
              }>
              <List style={{ width: '18em' }}>
                <ListItem className="">
                  <div>
                    <Title
                      size="4"
                      className="is-active has-text-primary mt-1 mb-0 font-nabimpact">
                      All portfolios
                    </Title>
                  </div>
                </ListItem>
                {Portfolios.map(entity => {
                  return (
                    <ListItem key={entity.name} href={entity.link}>
                      <div>
                        <Title
                          size="4"
                          className="is-active has-text-black text--lighten-1 font-nabimpact">
                          {entity.name}
                        </Title>
                        <Subtitle size="6" className="has-text-secondary mb-0">
                          HIN: {entity.hin}
                        </Subtitle>
                      </div>
                    </ListItem>
                  );
                })}
              </List>
            </Dropdown>
          </LevelItem>
          <LevelItem right className={activeBreakpoints.below.sm && 'mt-2'}>
            <Button
              primary
              onClick={() => dispatch({ type: 'toggleOrderPad' })}>
              <Icon>order</Icon>
              <span>Place order</span>
            </Button>
          </LevelItem>
        </Level>
      </HeroView>

      <Section
        paddingY={activeBreakpoints.above.xs && 3}
        className={cn(activeBreakpoints.below.sm && 'px-0 pt-0 pb-8')}>
        <Container>
          {Portfolios.map(portfolio => {
            let card = (
              <Columns mobile multiline>
                <Column
                  mobile="12"
                  tablet="12"
                  // desktop="5"
                  widescreen="4"
                  className={cn(
                    'is-aligned-items-center',
                    activeBreakpoints.below.lg
                      ? 'py-0 is-expanded-mobile'
                      : 'py-8'
                  )}>
                  <Title
                    size="3"
                    className={`font-nabimpact has-text-secondary mt-4 ${activeBreakpoints
                      .below.lg && 'mb-0'}`}>
                    {portfolio.logo && (
                      <Icon small primary className="pos-r has-top3 mr-4">
                        {portfolio.logo}
                      </Icon>
                    )}
                    {portfolio.name}
                    {portfolio.alert && (
                      <span className="ml-2 has-text-size-6 has-text-black">
                        <Icon number={portfolio.alert}>bell</Icon>
                      </span>
                    )}
                  </Title>
                  {/* <Subtitle size="6" className="has-text-secondary mb-2">
                          HIN: {portfolio.hin}
                        </Subtitle> */}
                </Column>

                {/* <Column
                        narrow
                        className={cn(
                          'is-flex is-align-items-center',
                          activeBreakpoints.below.sm
                            ? 'is-hidden-mobile'
                            : activeBreakpoints.below.md
                            ? ''
                            : 'pr-4'
                        )}
                      >
                        <Icon number={portfolio.alert}>bell</Icon>
                      </Column> */}

                <Column
                  narrow
                  className="is-hidden-touch is-hidden-desktop-only">
                  <Divider vertical />
                </Column>

                <Column mobile="6" className="is-flex is-align-items-center">
                  <div>
                    <Subtitle
                      size="7"
                      className="has-text-secondary is-uppercase is-spaced mb-0">
                      Available balance
                    </Subtitle>
                    <Title size="5" className="has-text-secondary mb-0">
                      ${portfolio.funds}
                    </Title>
                  </div>
                </Column>

                <Column mobile="6" className="is-flex is-align-items-center">
                  <div>
                    <Subtitle
                      size="7"
                      className="has-text-secondary is-uppercase is-spaced mb-0">
                      {portfolio.value ? 'Market value' : 'Total balance'}
                    </Subtitle>
                    <Title size="5" className="has-text-secondary mb-0">
                      {portfolio.value
                        ? `${portfolio.value}`
                        : `$${portfolio.total}`}
                    </Title>
                  </div>
                </Column>

                <Column mobile="6" className="is-flex is-align-items-center">
                  {portfolio.change && (
                    <div>
                      <Subtitle
                        size="7"
                        className="has-text-secondary is-uppercase is-spaced mb-0">
                        Today's change
                      </Subtitle>
                      <Title size="5" className="has-text-secondary mb-0">
                        <span
                          className={cn(
                            portfolio.change > 0 ? 'has-text-success' : '',
                            portfolio.change < 0 ? 'has-text-danger' : ''
                          )}>
                          {portfolio.change > 0 && <Icon small>caret-up</Icon>}
                          {portfolio.change < 0 && (
                            <Icon small>caret-down</Icon>
                          )}
                          {portfolio.change}%
                        </span>
                      </Title>
                    </div>
                  )}
                </Column>

                <Column mobile="6" className="is-flex is-align-items-center">
                  {portfolio.gainloss && (
                    <div>
                      <Subtitle
                        size="7"
                        className="has-text-secondary is-uppercase is-spaced mb-0">
                        Total G/L
                      </Subtitle>
                      <Title size="5" className="has-text-secondary mb-0">
                        <span
                          className={cn(
                            portfolio.gainloss > 0 ? 'has-text-success' : '',
                            portfolio.gainloss < 0 ? 'has-text-danger' : ''
                          )}>
                          {portfolio.gainloss > 0 && (
                            <Icon small>caret-up</Icon>
                          )}
                          {portfolio.gainloss < 0 && (
                            <Icon small>caret-down</Icon>
                          )}
                          {portfolio.gainloss}%
                        </span>
                      </Title>
                    </div>
                  )}
                </Column>

                <Column
                  className={cn(
                    'is-flex is-align-items-center is-justify-content-flex-end is-narrow is-hidden-mobile'
                  )}>
                  {portfolio.link ? (
                    <Icon medium className="has-text-primary">
                      arrow-right
                    </Icon>
                  ) : (
                    <Button square small secondary={!expandedCard}>
                      <Icon>{expandedCard ? 'arrow-up' : 'arrow-down'}</Icon>
                    </Button>
                  )}
                </Column>
              </Columns>
            );
            return (
              <React.Fragment key={portfolio.name}>
                {portfolio.link ? (
                  <NavLink to={portfolio.link || '/'}>
                    <Card
                      className={cn(
                        activeBreakpoints.below.sm ? '' : 'mb-4 has-link'
                      )}>
                      <CardHeader
                        className={
                          activeBreakpoints.below.sm ? 'pa-4' : 'py-4'
                        }>
                        {card}
                      </CardHeader>
                    </Card>
                  </NavLink>
                ) : (
                  <Card
                    className={cn(
                      'has-pointer',
                      activeBreakpoints.below.sm ? '' : 'mb-4',
                      expandedCard ? '' : 'has-link'
                    )}>
                    <CardHeader
                      className={activeBreakpoints.below.sm ? 'pa-4' : 'py-4'}
                      onClick={() =>
                        setExpandedCard(expandedCard ? false : true)
                      }>
                      {card}
                    </CardHeader>
                    <Collapse isOpened={expandedCard}>
                      <NabAccountCards />
                    </Collapse>
                  </Card>
                )}
              </React.Fragment>
            );
          })}
        </Container>
      </Section>

      {error.includes('API-2380') && !error.includes('API-2381') && (
        <Modal
          closeIcon
          className="has-bg-blue-grey lighten-5"
          onClose={() => setModal(false)}
          isOpened={
            (error.includes('API-2380') || error.includes('API-2381')) &&
            modal !== false
          }
          header={'Profile not verified'}
          footer={
            <Button
              secondary
              block
              className="ml-0 px-4"
              onClick={() => setModal(false)}>
              OK
            </Button>
          }>
          <React.Fragment>
            <p>We cannot find your profile or it is inactive.</p>
            <p>
              If you have joined nabtrade and not completed your identity
              verification, please visit the full site at{' '}
              <a
                href="https://www.nabtrade.com.au"
                target="_blank"
                rel="noopener noreferrer">
                nabtrade.com.au
              </a>{' '}
              to do this. You can complete the pending verification action from
              the Outstanding actions panel at the top of any of the Admin menu
              pages.
            </p>
            <p>
              If you are a returning customer, we may need to reactivate your
              profile. Visit{' '}
              <a
                href="https://www.nabtrade.com.au/investor/support"
                target="_blank"
                rel="noopener noreferrer">
                Support
              </a>{' '}
              for more information.{' '}
            </p>
          </React.Fragment>
        </Modal>
      )}
      {error.includes('API-2381') && !error.includes('API-2380') && (
        <Modal
          closeIcon
          className="has-bg-blue-grey lighten-5"
          onClose={() => setModal(false)}
          isOpened={
            (error.includes('API-2380') || error.includes('API-2381')) &&
            modal !== false
          }
          header={'Error'}
          footer={
            <Button
              secondary
              block
              className="ml-0 px-4"
              onClick={() => setModal(false)}>
              OK
            </Button>
          }>
          <p>
            There was an error with your request - please try again or contact
            nabtrade support on 13 13 80 for assistance.
          </p>
        </Modal>
      )}
      {errorHandling && (
        <div
          className="pos-f px-4 py-2 has-bg-white has-elevation-2 "
          style={{ right: '1em', bottom: '1em', zIndex: 100 }}>
          <Columns mobile multiline>
            <Column>
              <Title size="6" className="mb-1">
                Account error handling
              </Title>
              <CheckboxGroup
                stacked
                className="mb-0"
                name="error"
                value={error}
                onChange={e => setError(e)}>
                <Checkbox
                  value="API-2380"
                  label="Non KYC - API-2380"
                  onClick={() => ToggleModal(true)}
                />
                <Checkbox
                  value="API-2381"
                  label="General Error - API-2381"
                  onClick={() => ToggleModal(true)}
                />
              </CheckboxGroup>
            </Column>
          </Columns>
        </div>
      )}
    </LayoutDefault>
  );
};

export default Breakpoints(Portfolio);
