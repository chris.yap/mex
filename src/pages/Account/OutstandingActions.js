import React from 'react';
import cn from 'classnames';
import {
  Breakpoints,
  Card,
  // CardHeader,
  // CardContent,
  // Column,
  // Columns,
  List,
  ListItem,
  Title
} from 'shaper-react';

const OutstandingActions = ({ activeBreakpoints, ...props }) => {
  return (
    <>
      <Title size="4" className={cn(activeBreakpoints.below.sm && 'mx-4')}>
        Outstanding actions
      </Title>
      <Card outline>
        <List>
          <ListItem>Outstanding items here...</ListItem>
        </List>
      </Card>
    </>
  );
};

export default Breakpoints(OutstandingActions);
