import React, { useState } from 'react';
import { getState } from '../../context/StateProvider';
import {
  Alert,
  Breakpoints,
  Button,
  Collapse,
  Divider,
  Level,
  LevelItem,
  Textfield
} from 'shaper-react';

const PersonalDetails = ({ activeBreakpoints, error, ...props }) => {
  const [, dispatch] = getState();
  const [errorMsg, showErrorMsg] = useState(false);
  const [collapseStatus, setCollapseStatus] = useState(true);
  const [changePassword, setChangePassword] = useState(false);
  const [updatePassword, setUpdatePassword] = useState(true);

  const updateCollapseStatus = newVal => {
    setCollapseStatus(newVal);
  };

  const handleChangePassword = newVal => {
    setChangePassword(newVal);
  };

  const handleUpdatePassword = newVal => {
    setUpdatePassword(newVal);
  };

  const submitChangePassword = newVal => {
    dispatch({ type: 'toggleOverlay', payload: true });
    setTimeout(() => {
      handleChangePassword(newVal);
      dispatch({ type: 'toggleOverlay', payload: false });
      if (!newVal) {
        showErrorMsg(true);
      }
    }, 2000);
  };

  return (
    <>
      {/* <Title size="4" className={cn(activeBreakpoints.below.sm && 'mx-4')}>
        Change password
      </Title> */}
      {/* <Card outline> */}
      {/* <List> */}
      {/* <ListItem>
            <Level mobile className="is-flex-1">
              <LevelItem left>Name</LevelItem>
              <LevelItem right>Account name</LevelItem>
            </Level>
          </ListItem>
          <ListItem>
            <Level
              mobile
              className={cn(
                'is-flex-1',
                activeBreakpoints.below.sm && 'is-flex-break-2 has-text-right'
              )}>
              <LevelItem left>Address</LevelItem>
              <LevelItem right>
                105 Miller St North Sydney NSW 2060 Australia
              </LevelItem>
            </Level>
          </ListItem> */}
      {/* <ListItem>
          <div className="is-flex-1"> */}
      <Level mobile className="mb-0">
        <LevelItem left>Password</LevelItem>
        <LevelItem right>
          {/* <Button
                    inverted
                    small
                    danger
                    square
                    mr={4}
                    onClick={() => {
                      updateCollapseStatus(collapseStatus ? false : true);
                      handleChangePassword(false);
                      handleUpdatePassword(false);
                      showErrorMsg(false);
                    }}>
                    <Icon>edit</Icon>
                  </Button> */}
          *********
        </LevelItem>
      </Level>

      <Collapse isOpened={collapseStatus}>
        <Divider dotted my={4} />
        {changePassword ? (
          <Alert success inverted isOpened={true}>
            <strong>Your password has been updated.</strong>
          </Alert>
        ) : (
          <>
            {updatePassword ? (
              <>
                <Textfield
                  required
                  name="currentPassword"
                  label="Current password"
                  placeholder="Current password"
                  type="password"
                  error={error.includes('change-password-invalid-input')}
                  errorMessage="Invalid input error"
                />
                <Textfield
                  required
                  name="newPassword"
                  label="New password"
                  placeholder="New password"
                  type="password"
                  error={error.includes('change-password-invalid-input')}
                  errorMessage="Invalid input error"
                />
                <Textfield
                  required
                  name="confirmNewPassword"
                  label="Confirm new password"
                  placeholder="Confirm new password"
                  type="password"
                  error={error.includes('change-password-invalid-input')}
                  errorMessage="Invalid input error"
                />

                <Alert isOpened={errorMsg} inverted warning>
                  <p className="pos-r has-top3 mb-1">
                    We're currently experiencing some issues. Please try again
                    later.
                  </p>
                </Alert>

                <div className={`has-text-right ${errorMsg && 'mt-2'}`}>
                  <Button
                    flat
                    onClick={() => {
                      updateCollapseStatus(false);
                      handleUpdatePassword(false);
                    }}>
                    Cancel
                  </Button>
                  <Button
                    primary
                    disabled={error.includes('change-password-invalid-input')}
                    onClick={() => {
                      submitChangePassword(
                        error.includes('change-password') ? false : true
                      );
                    }}>
                    Change password
                  </Button>
                </div>
              </>
            ) : (
              <div className="has-text-right">
                <Button
                  flat
                  onClick={() => {
                    updateCollapseStatus(false);
                    handleUpdatePassword(false);
                  }}>
                  Cancel
                </Button>
                <Button
                  primary
                  onClick={() => {
                    handleUpdatePassword(true);
                  }}>
                  Change password
                </Button>
              </div>
            )}
          </>
        )}
      </Collapse>
      {/* </div> 
         </ListItem>*/}
      {/* <ListItem>
            <Level mobile className="is-flex-1">
              <LevelItem left>Trading pin</LevelItem>
              <LevelItem right>
                <Button inverted small danger className="my-0 mr-4">
                  <Icon>edit</Icon>
                </Button>
                Off
              </LevelItem>
            </Level>
          </ListItem> 
      </List>*/}
      {/* </Card> */}
    </>
  );
};

export default Breakpoints(PersonalDetails);
