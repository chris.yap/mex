import React, { useState } from 'react';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import LayoutDefault from '../../layout/LayoutDefault';
import PersonalDetails from './PersonalDetails';
// import OutstandingActions from './OutstandingActions';
import {
  Breakpoints,
  Checkbox,
  CheckboxGroup,
  Column,
  Columns,
  Container,
  Section,
  Title
} from 'shaper-react';
import HeroView from '../../components/HeroView';

const Account = ({ activeBreakpoints, ...props }) => {
  const [error, setError] = useState([]);
  return (
    <LayoutDefault breadcrumb={Breadcrumbs.account.items}>
      <HeroView title="Change password" />

      <Section py={4} className={`${activeBreakpoints.below.sm && 'px-0'}`}>
        <Container
          className={activeBreakpoints.below.sm && 'is-overflow-hidden'}>
          <Columns multiline>
            {/* <Column mobile={12} tablet={12} desktop={6}>
              <PersonalDetails error={error} />
            </Column> */}
            <Column
              mobile={12}
              tablet={8}
              desktop={6}
              tabletOffset={2}
              desktopOffset={3}>
              {/* <OutstandingActions /> */}
              <PersonalDetails error={error} />
            </Column>
          </Columns>
        </Container>
      </Section>

      <div
        className="pos-f px-4 py-2 has-bg-white has-elevation-2 "
        style={{ right: '1em', bottom: '1em', zIndex: 100 }}>
        <Columns mobile multiline>
          <Column>
            <Title size="6" className="mb-1">
              Error handling
            </Title>
            <CheckboxGroup
              stacked
              className="mb-0"
              name="error"
              value={error}
              onChange={e => setError(e)}>
              <Checkbox value="change-password" label="Change password" />
              <Checkbox
                value="change-password-invalid-input"
                label="Change password invalid input"
              />
            </CheckboxGroup>
          </Column>
        </Columns>
      </div>
    </LayoutDefault>
  );
};

export default Breakpoints(Account);
