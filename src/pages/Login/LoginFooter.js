import React from 'react';
import { Box, Breakpoints, Footer, Icon, WhichBrowser } from 'shaper-react';

const LoginFooter = ({ activeBrowser, activeBreakpoints, ...props }) => {
  return (
    <Footer
      fixed
      bg="bluegreys.0"
      p={0}
      borderTop="1px solid rgba(0,0,0,.1)"
      textAlign="center">
      <Box
        bg="transparent"
        display="flex"
        boxShadow="none"
        justifyContent="center"
        p={0}>
        <Box
          py={0}
          px={4}
          height="54px"
          bg="transparent"
          boxShadow="none"
          display="flex"
          alignItems="center">
          <a href="#!" onClick={props.showInfo}>
            Important information
          </a>
        </Box>
        {activeBrowser && (activeBrowser.isIos || activeBrowser.isAndroid) && (
          <Box
            py={0}
            px={4}
            height="54px"
            bg="transparent"
            boxShadow="none"
            display="flex"
            alignItems="center"
            borderLeft="1px solid rgba(0,0,0,.1)">
            <a href="./">Full site</a>
          </Box>
        )}
        {activeBrowser && activeBrowser.isIos && (
          <Box
            py={0}
            px={4}
            height="54px"
            bg="transparent"
            boxShadow="none"
            display="flex"
            alignItems="center"
            borderLeft="1px solid rgba(0,0,0,.1)"
            color="secondary"
            onClick={props.showHelp}>
            <Icon medium className={activeBreakpoints.above.xs && 'mr-1'}>
              add-to-home
            </Icon>
            {activeBreakpoints.above.xs && 'Add to home screen'}
          </Box>
        )}
      </Box>
    </Footer>
  );
};

export default WhichBrowser(Breakpoints(LoginFooter));
