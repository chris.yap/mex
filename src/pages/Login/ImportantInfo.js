import React from 'react';
import {
  Button,
  Card,
  Container,
  Column,
  Columns,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Modal,
  Title
} from 'shaper-react';

const ImportantInfo = ({ isOpened, ...props }) => (
  <Modal
    fullscreen
    isOpened={isOpened}
    bodyClassName="has-bg-secondary lighten-5"
    header={
      <Level mobile className="is-flex-1 mb-0">
        <LevelItem left>
          <Title size="4" className="mb-0">
            About nabtrade
          </Title>
        </LevelItem>
        <LevelItem right className="pa-0">
          <Icon medium danger onClick={props.onClose}>
            close
          </Icon>
        </LevelItem>
      </Level>
    }>
    <Container>
      <Columns mobile>
        <Column tablet="8" desktop="6" tabletOffset="2" desktopOffset="3">
          <Card outline className="mb-4">
            <List>
              {Data.map((item, i) => (
                <ListItem key={i} href={item.link}>
                  <Level mobile className="is-flex-1">
                    <LevelItem left>{item.label}</LevelItem>
                    <LevelItem right>
                      <Icon primary>arrow-right</Icon>
                    </LevelItem>
                  </Level>
                </ListItem>
              ))}
            </List>
          </Card>
          <Level mobile>
            <LevelItem>
              <Button px={6} flat medium color="#1da1f2">
                <Icon className="mx-0">twitter</Icon>
              </Button>
            </LevelItem>
            <LevelItem>
              <Button px={6} flat medium color="#c4302b">
                <Icon className="mx-0">youtube</Icon>
              </Button>
            </LevelItem>
            <LevelItem>
              <Button px={6} flat medium color="#3b5998">
                <Icon className="mx-0">facebook</Icon>
              </Button>
            </LevelItem>
          </Level>
        </Column>
      </Columns>
    </Container>
  </Modal>
);

export default ImportantInfo;

const Data = [
  { label: 'Contact us', link: 'https://www.nabtrade.com.au/investor/support' },
  {
    label: 'Terms of Use',
    link:
      'https://www.nabtrade.com.au/investor/terms-and-conditions/terms-of-use'
  },
  {
    label: 'NAB Privacy Policy',
    link: 'https://www.nab.com.au/common/privacy-policy'
  },
  {
    label: 'Financial Services Guide',
    link:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Financial_Services_Guide.pdf'
  },
  {
    label: 'Important Notice',
    link:
      'https://www.nabtrade.com.au/investor/terms-and-conditions/Important-information'
  },
  {
    label: 'General Advice Warning',
    link:
      'https://www.nabtrade.com.au/investor/terms-and-conditions/terms-of-use'
  },
  {
    label: 'nabtrade Client Agreement',
    link:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Client_Agreement.pdf'
  },
  {
    label: 'Best Execution Policy',
    link:
      'https://www.nabtrade.com.au/content/dam/nabtrade/nabtrade2017/PDFs/Disclosure_Docs/nabtrade_Best_Execution_Policy.pdf'
  },
  {
    label: 'NAB Cookie Statement',
    link: 'https://www.nab.com.au/about-us/using-this-site/cookies'
  },
  {
    label: 'Get a nabtrade account now',
    link: 'https://nabtrade.com.au/onboarding'
  }
];
