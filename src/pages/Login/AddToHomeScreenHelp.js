import React from 'react';
import {
  Box,
  Carousel,
  Icon,
  Level,
  LevelItem,
  Modal,
  Title
} from 'shaper-react';

const Slide = ({ children, ...props }) => (
  <Box bg="transparent" boxShadow="none" pb={0}>
    {children}
  </Box>
);

const Device = ({ children, ...props }) => (
  <Box
    bg="transparent"
    boxShadow="none"
    p={0}
    width="60%"
    position="relative"
    m="0 auto 10px">
    <Box
      bg="transparent"
      boxShadow="none"
      backgroundImage="url(/images/iphone-8-portrait_white.png)"
      backgroundSize="cover"
      p={0}
      position="relative"
      pb="203.10345%">
      <Box
        position="absolute"
        top="11.99774%"
        left="6.2069%"
        width="87.35632%"
        height="76.06112%"
        p={0}
        boxShadow="none"
        border="1px solid"
        borderColor="secondaries.2">
        {children}
      </Box>
    </Box>
  </Box>
);

const AddToHomeScreenHelp = ({ isOpened, ...props }) => {
  return (
    <Modal
      isOpened={isOpened}
      closeIcon
      fullscreen
      bodyClassName="has-bg-secondary lighten-5 pt-0"
      header={
        <Level mobile className="is-flex-1 mb-0">
          <LevelItem left>
            <Title size="4" className="mb-0">
              Add to home screen
            </Title>
          </LevelItem>
          <LevelItem right className="pa-0">
            {/* <Button round flat> */}
            <Icon medium danger onClick={props.onClose}>
              close
            </Icon>
            {/* </Button> */}
          </LevelItem>
        </Level>
      }>
      <Carousel arrows={false} dots infinite={false}>
        <Slide>
          <Device>
            <img src="/images/ios_addhomescreen_1.png" alt="Launch Safari" />
          </Device>
          <p className="has-text-centered mb-0">
            Launch <strong>Safari</strong> browser on your Apple iPhone and
            navigate to{' '}
            <a
              href="m.nabtrade.com.au"
              rel="noopener noreferrer"
              target="_blank">
              <strong>m.nabtrade.com.au</strong>
            </a>
          </p>
        </Slide>
        <Slide>
          <Device>
            <img src="/images/ios_addhomescreen_2.png" alt="Launch Safari" />
          </Device>
          <p className="has-text-centered mb-0">
            Tap the <strong>Share</strong> button on the browser's toolbar —
            that's the rectangle with an arrow pointing upward.
          </p>
        </Slide>
        <Slide>
          <Device>
            <img src="/images/ios_addhomescreen_3.png" alt="Launch Safari" />
          </Device>
          <p className="has-text-centered mb-0">
            Tap the <strong>Add to Home Screen</strong> button in the{' '}
            <strong>Share</strong> menu.
          </p>
        </Slide>
        <Slide>
          <Device>
            <img src="/images/ios_addhomescreen_4.png" alt="Launch Safari" />
          </Device>
          <p className="has-text-centered mb-0">
            You will be prompted to name the shortcut before tapping the{' '}
            <strong>Add</strong> button.
          </p>
        </Slide>
        <Slide>
          <Device>
            <img src="/images/ios_addhomescreen_5.png" alt="Launch Safari" />
          </Device>
          <p className="has-text-centered mb-0">
            The <strong>nabtrade</strong> icon will appear on your homescreen.
            It can be placed anywhere — just like a normal app.
          </p>
        </Slide>
      </Carousel>
    </Modal>
  );
};

export default AddToHomeScreenHelp;
