import React from 'react';
import NabtradeLogo from '../../components/NabtradeLogo';
import { getState } from '../../context/StateProvider';
import {
  Alert,
  Breakpoints,
  Button,
  Container,
  Collapse,
  Columns,
  Column,
  Divider,
  Hero,
  Textfield,
  Title,
  WhichBrowser
} from 'shaper-react';
import LoginFooter from './LoginFooter';
import AddToHomeScreenHelp from './AddToHomeScreenHelp';
import ImportantInfo from './ImportantInfo';

const ScreenLogin = ({ activeBrowser, activeBreakpoints, ...props }) => {
  const [{ login }, dispatch] = getState();
  const [addHomeHelp, updateAddHomeHelp] = React.useState(false);
  const [importantInfo, updateImportantInfo] = React.useState(false);
  let { resetPassword, passwordSequence } = login;
  let handleChangePassword = () => {};
  let validateStatePassword = () => {};
  return (
    <Hero
      fullHeight
      id="id"
      p={0}
      backgroundImage="url(/images/tyler-franta-589346-unsplash.jpg)"
      backgroundSize="cover">
      <Container fluid px={0}>
        <Columns desktop>
          <Column className="is-6-desktop has-bg-blue-grey lighten-5 has-elevation-24">
            <Hero fullHeight pt={2}>
              <Container fluid>
                <Columns>
                  <Column
                    tablet={8}
                    desktop={12}
                    widescreen={10}
                    fullhd={8}
                    tabletOffset={2}
                    desktopOffset="0"
                    widescreenOffset={1}
                    fullhdOffset={2}>
                    <p className="mb-4">
                      <NabtradeLogo width="200" />
                    </p>

                    <Collapse isOpened={!resetPassword}>
                      <Textfield
                        prependIcon="male-outline"
                        id="id-textfield-userid"
                        label="User ID"
                        placeholder="123456789"
                      />
                      {/* <Textfield
												type="password"
												id="id-textfield-password"
												prependIcon="lock"
												label="Password"
												placeholder="••••••••"
											/> */}
                      <Textfield
                        error={validateStatePassword() === 'error'}
                        // success={true}
                        type="password"
                        prependIcon="lock"
                        label="Password"
                        onChange={handleChangePassword}
                        value={login.password}
                        // hint="Please enter your password."
                        errorMessage={login.error}
                        maxLength="500"
                      />

                      <Button
                        id="id-button-login"
                        primary
                        medium
                        block
                        className="mt-4"
                        onClick={() =>
                          dispatch({
                            type: 'toggleLogin'
                          })
                        }>
                        Login
                      </Button>

                      {/* <p className="has-text-centered mt-4 mb-10">
                        <a
                          href="javascript:void(0)"
                          onClick={() => {
                            dispatch({ type: 'toggleResetPassword' });
                            dispatch({
                              type: 'setPasswordSequence',
                              payload: 1
                            });
                          }}>
                          Forgot password?
                        </a>
                      </p> */}
                      <p className="has-text-centered mt-2">
                        <Button
                          flat
                          onClick={() => {
                            dispatch({ type: 'toggleResetPassword' });
                            dispatch({
                              type: 'setPasswordSequence',
                              payload: 1
                            });
                          }}>
                          Forgot password?
                        </Button>
                      </p>

                      <p className="has-text-centered is-flex is-align-items-center is-justify-content-center">
                        Don't have an account?{' '}
                        <Button flat ml={2}>
                          Sign up now
                        </Button>
                        {/* <a href="/#">Sign up now</a> */}
                      </p>
                    </Collapse>

                    <Collapse isOpened={resetPassword}>
                      <Title size="4" className="font-nabimpact">
                        Reset your password
                      </Title>

                      <Columns mobile>
                        <Column />
                        <Column className="has-text-centered">
                          <Button
                            round
                            secondary={passwordSequence === 1}
                            inverted={passwordSequence !== 1}>
                            1
                          </Button>
                        </Column>
                        <Column className="has-text-centered">
                          <Button
                            round
                            secondary={passwordSequence === 2}
                            inverted={passwordSequence !== 2}>
                            2
                          </Button>
                        </Column>
                        <Column className="has-text-centered">
                          <Button
                            round
                            secondary={passwordSequence === 3}
                            inverted={passwordSequence !== 3}>
                            3
                          </Button>
                        </Column>
                        <Column />
                      </Columns>

                      {passwordSequence === 1 && (
                        <React.Fragment>
                          <div className="nab-content">
                            <Title size="6" className="mb-0">
                              To securely reset your password, you'll need:
                            </Title>
                            <ul>
                              <li>Your nabtrade User ID, and</li>
                              <li>Your security question keyword.</li>
                            </ul>
                          </div>

                          <Textfield
                            required
                            prependIcon="male-outline"
                            label="Enter your User ID"
                            placeholder="123456789"
                          />
                        </React.Fragment>
                      )}

                      {passwordSequence === 2 && (
                        <React.Fragment>
                          <Title size="5" className="mb-4">
                            Your security question
                          </Title>
                          <Textfield
                            required
                            label="What is your grandmother's maiden name? (Note: Answers are case sensitive)"
                            placeholder="Please answer question here."
                            errorMessage="Incorrect answer."
                          />
                        </React.Fragment>
                      )}

                      {passwordSequence === 3 && (
                        <React.Fragment>
                          <Alert inverted warning isOpened={true}>
                            A temporary password has been sent to you via SMS or
                            email. You are required to change it prior to login.
                          </Alert>
                          <Textfield required label="Temp password" />
                          <Textfield required label="New password" />
                          <Textfield required label="Confirm new password" />
                        </React.Fragment>
                      )}

                      {passwordSequence !== 3 && (
                        <React.Fragment>
                          <Button
                            primary
                            className="mt-4 ml-0"
                            onClick={() =>
                              dispatch({
                                type: 'setPasswordSequence',
                                payload: (passwordSequence += 1)
                              })
                            }>
                            Next
                          </Button>
                          <Button
                            className="mt-4"
                            onClick={() =>
                              dispatch({ type: 'toggleResetPassword' })
                            }>
                            Cancel
                          </Button>
                        </React.Fragment>
                      )}

                      {passwordSequence === 3 && (
                        <Button
                          primary
                          onClick={() =>
                            dispatch({ type: 'toggleResetPassword' })
                          }
                          className="ml-0">
                          Change password
                        </Button>
                      )}
                    </Collapse>

                    {/* <Divider className="my-4">Or</Divider> */}

                    <Divider className="my-8" />

                    <small>
                      {/* <p>By clicking Login, you agree to the nabtrade adviser <a href="#">terms of use</a>.</p> */}
                      <p className="pt-2 mb-0">
                        <img
                          src="/images/nab-defence.png"
                          alt="NAB Defence"
                          width="100"
                        />
                      </p>
                      <p className="mb-0">
                        NAB Defence, your protection against fraud.
                      </p>
                      <p>
                        <a href="/#">Learn more about NAB Defence</a>
                      </p>
                    </small>
                  </Column>
                </Columns>
              </Container>
            </Hero>
          </Column>
        </Columns>
      </Container>

      <LoginFooter
        showHelp={() => updateAddHomeHelp(true)}
        showInfo={() => updateImportantInfo(true)}
      />

      <AddToHomeScreenHelp
        isOpened={addHomeHelp}
        onClose={() => updateAddHomeHelp(false)}
      />

      <ImportantInfo
        isOpened={importantInfo}
        onClose={() => updateImportantInfo(false)}
      />
    </Hero>
  );
};

export default Breakpoints(WhichBrowser(ScreenLogin));
