import React from 'react';
import faker from 'faker/locale/en_AU';
import 'flag-icon-css/css/flag-icon.min.css';
import {
  Breakpoints,
  Card,
  CardHeader,
  CardContent,
  Collapse,
  Divider,
  Icon,
  Select,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';
import RealtimeQuoteMessage from '../../components/RealtimeQuoteMsg';

const Currency = ({ activeBreakpoints, expand, ...props }) => {
  const [baseCurrency] = React.useState('AUD');
  return (
    <React.Fragment>
      <Card>
        <CardHeader
          onClick={activeBreakpoints.below.sm ? () => props.toggleCC(1) : null}>
          <Title size="4" className="mb-0">
            Currency
          </Title>
          {activeBreakpoints.below.sm && (
            <Icon>{expand ? 'arrow-up' : 'arrow-down'}</Icon>
          )}
        </CardHeader>
      </Card>
      <Collapse isOpened={expand}>
        <Card>
          <CardContent>
            <Select options={Base} defaultValue={Base[0]} />
          </CardContent>
          <Divider />
          <Table
            hasStripes
            headers={Headers}
            data={data}
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            tableRow={(row, Index) => (
              <TableRow key={Index} baseCurrency={baseCurrency} {...row} />
            )}
            defaultSortCol="currency"
            mb={0}
          />
        </Card>
        <RealtimeQuoteMessage
          international
          className={` mt-2 ${activeBreakpoints.below.sm && 'mx-4'}`}
        />
      </Collapse>
    </React.Fragment>
  );
};

export default Breakpoints(Currency);

const data = [
  { currency: 'CAD' },
  { currency: 'EUR' },
  { currency: 'GBP' },
  { currency: 'HKD' },
  { currency: 'JPY' },
  { currency: 'SGD' },
  { currency: 'USD' }
];
const Base = [
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-au mr-2" />
        Australian (AUD)
      </React.Fragment>
    ),
    value: 'AUD'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-ca mr-2" />
        Canadian (CAD)
      </React.Fragment>
    ),
    value: 'CAD'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-eu mr-2" />
        Euros (EUR)
      </React.Fragment>
    ),
    value: 'EUR'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-gb mr-2" />
        Great Britain Pound (GBP)
      </React.Fragment>
    ),
    value: 'GBP'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-hk mr-2" />
        Hong Kong Dollars (HKD)
      </React.Fragment>
    ),
    value: 'HKD'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-jp mr-2" />
        Japanese Yen (JPY)
      </React.Fragment>
    ),
    value: 'JPY'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-sg mr-2" />
        Singapore Dollars (SGD)
      </React.Fragment>
    ),
    value: 'SGD'
  },
  {
    label: (
      <React.Fragment>
        <span className="flag-icon flag-icon-us mr-2" />
        United States Dollars (USD)
      </React.Fragment>
    ),
    value: 'USD'
  }
];
const TableRow = ({ baseCurrency, row }) => {
  const random_boolean = Math.random() >= 0.5;
  return (
    <Row>
      <Cell>
        <span
          className={`flag-icon flag-icon-${row.currency
            .slice(0, -1)
            .toLowerCase()} mr-2`}
        />
        {baseCurrency}/{row.currency}
      </Cell>
      <Cell className="has-text-right">{faker.finance.amount(0, 100, 4)}</Cell>
      <Cell
        className={`has-text-right has-text-weight-semibold has-text-${
          random_boolean ? 'success' : 'danger'
        }`}>
        <Icon small>{random_boolean ? 'caret-up' : 'caret-down'}</Icon>
        {new Intl.NumberFormat('en-AU', { maximumSignificantDigits: 2 }).format(
          faker.finance.amount(0, 1, 2)
        )}
      </Cell>
    </Row>
  );
};
const Headers = [
  { name: 'Cross', align: 'left' },
  {
    name: (
      <span>
        Exchange
        <br />
        rate
      </span>
    ),
    align: 'right'
  },
  {
    name: (
      <span>
        Price
        <br />
        change
      </span>
    ),
    align: 'right'
  }
];
