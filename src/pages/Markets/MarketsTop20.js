import React from 'react';
import faker from 'faker';
import {
  Breakpoints,
  Card,
  CardHeader,
  Select,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';
import RealtimeQuoteMsg from '../../components/RealtimeQuoteMsg';
import PercentageView from '../../components/PercentageView';

class MarketsTop20 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  componentDidMount = () => {
    this.CreateData();
  };
  CreateData = () => {
    let tempData = [];
    for (let i = 0; i < 10; i++) {
      tempData.push({
        code: faker.random.alphaNumeric(3),
        last: faker.finance.amount(1, 30, 2, '$'),
        change: faker.finance.amount(0, 3, 2),
        changePercent: faker.finance.amount(0, 20, 2),
        bid: faker.finance.amount(1, 20, 2),
        ask: faker.finance.amount(1, 20, 2),
        open: faker.finance.amount(1, 20, 2),
        range:
          faker.finance.amount(1, 20, 2) +
          ' - ' +
          faker.finance.amount(1, 20, 2),
        vol: faker.finance.amount(500, 900, 0)
      });
    }
    this.setState({
      data: tempData
    });
  };
  render() {
    const { activeBreakpoints } = this.props;
    const { data } = this.state;
    return (
      <React.Fragment>
        <Card>
          <CardHeader className="is-align-items-center">
            <Select
              options={Top}
              defaultValue={Top[0]}
              flex="1"
              mx={1}
              mb={0}
            />
            <Select
              options={Types}
              defaultValue={Types[0]}
              flex="1"
              mx={1}
              mb={0}
            />
            <span className="mx-1">for</span>
            <Select
              options={Exchange}
              defaultValue={Exchange[0]}
              flex="1"
              mx={1}
              mb={0}
            />
          </CardHeader>
          <Table
            hasStripes
            headers={Headers}
            data={data}
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            tableRow={TableRow}
            my={0}
          />
        </Card>
        <RealtimeQuoteMsg local className="mt-4" />
      </React.Fragment>
    );
  }
}

export default Breakpoints(MarketsTop20);

const Exchange = [
  { label: 'Australia - ASX', value: 0 },
  { label: 'Germany - XETRA', value: 1 },
  { label: 'Hong Kong - HKSE', value: 2 },
  { label: 'United Kingdom - LSE', value: 3 },
  { label: 'United States - NYSE', value: 4 },
  { label: 'United States - NASDAQ', value: 5 },
  { label: 'United States - AMEX', value: 6 }
];

const Top = [
  { label: 'Top % Rises', value: 0 },
  { label: 'Top % Falls', value: 1 },
  { label: 'Top $ Rises', value: 2 },
  { label: 'Top $ Falls', value: 3 },
  { label: 'Top by Trades', value: 4 },
  { label: 'Top by Volume', value: 5 },
  { label: 'Top by Value', value: 6 }
];

const Types = [
  { label: 'Equities', value: 0 },
  { label: 'ETFs', value: 1 },
  { label: 'ETOs', value: 2 },
  { label: 'Warrants', value: 3 }
];

const TableRow = ({ row }) => (
  <Row>
    <Cell className="is-uppercase">
      <a href="/#" className="has-text-weight-semibold">
        {row.code}
      </a>
    </Cell>
    <Cell className="has-text-right">{row.last}</Cell>
    <Cell className="has-text-right">
      {row.change} <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">{row.bid}</Cell>
    <Cell className="has-text-right">{row.ask}</Cell>
    <Cell className="has-text-right">{row.open}</Cell>
    <Cell className="has-text-right">{row.range}</Cell>
    <Cell className="has-text-right">{row.vol}K</Cell>
  </Row>
);

const Headers = [
  { name: 'Code', align: '' },
  { name: 'Last', align: 'right' },
  { name: "Today's change", align: 'right' },
  { name: 'Bid', align: 'right' },
  { name: 'Ask', align: 'right' },
  { name: 'Open', align: 'right' },
  { name: 'Day range', align: 'right' },
  { name: 'Volume', align: 'right' }
];
