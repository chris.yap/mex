import React from 'react';
import faker from 'faker/locale/en_AU';
import {
  Breakpoints,
  Card,
  CardHeader,
  Collapse,
  Icon,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';
import DateTime from '../../components/DateTime';
import PercentageView from '../../components/PercentageView';

const Commodities = ({ activeBreakpoints, expand, ...props }) => {
  return (
    <React.Fragment>
      <Card>
        <CardHeader
          onClick={activeBreakpoints.below.sm ? () => props.toggleCC(0) : null}>
          <Title size="4" className="mb-0">
            Commodities
          </Title>
          {activeBreakpoints.below.sm && (
            <Icon>{expand ? 'arrow-up' : 'arrow-down'}</Icon>
          )}
        </CardHeader>
      </Card>
      <Collapse isOpened={expand}>
        <Card>
          <Table
            hasStripes
            headers={Headers}
            data={data}
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            tableRow={TableRow}
            defaultSortCol="name"
            mb={0}
          />
        </Card>
        <p
          className={`mt-2 has-text-size-2 has-text-blue-grey ${activeBreakpoints
            .below.sm && 'mx-4'}`}>
          Last updated at <DateTime time />
        </p>
      </Collapse>
    </React.Fragment>
  );
};

export default Breakpoints(Commodities);

const data = [
  { name: 'Aluminium' },
  { name: 'Copper' },
  { name: 'Iron Ore' },
  { name: 'Gold' },
  { name: 'Silver' },
  { name: 'Platinum' },
  { name: 'WTI Crude Oil' },
  { name: 'RBOB Gasoline' },
  { name: 'Natural Gas' },
  { name: 'Heating Oil' },
  { name: 'Ethanol' },
  { name: 'Brent Crude' },
  { name: 'Corn' },
  { name: 'Wheat' }
];

const Headers = [
  { name: 'Name', value: 'name' },
  {
    name: (
      <span className="has-line-height-60">
        Price <sup className="pos-r has-top6">(USD)</sup>
      </span>
    ),
    value: 'price',
    align: 'right'
  },
  { name: '1-yr change', value: 'change', align: 'right' }
];

const TableRow = ({ row }) => {
  const random_boolean = Math.random() >= 0.5;
  return (
    <Row>
      <Cell>{row.name}</Cell>
      <Cell className="has-text-right">
        {new Intl.NumberFormat('en-AU').format(faker.finance.amount(0, 10, 2))}
      </Cell>
      <Cell
        className={`has-text-right has-text-${
          random_boolean ? 'success' : 'danger'
        }`}>
        <PercentageView value={faker.finance.amount(-10, 10, 2)} />
      </Cell>
    </Row>
  );
};
