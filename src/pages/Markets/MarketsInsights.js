import React from 'react';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { MarketsInsightsItems } from '../../constants';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import {
  Breakpoints,
  Column,
  Columns,
  Container,
  Section,
  Tab,
  Tabs
} from 'shaper-react';
import Commodities from './Commodities';
import Performance from './Performance';
import Currency from './Currency';
import MarketsTop20 from './MarketsTop20';
import MarketsMovers from './MarketsMovers';

class MarketsInsights extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: 0
    };
  }
  toggleCCTab = e => {
    const { expand } = this.state;
    let ind;
    if (e === expand) {
      ind = null;
    } else {
      ind = e;
    }
    this.setState({
      expand: ind
    });
  };
  render() {
    const { activeBreakpoints } = this.props;
    const { expand } = this.state;
    return (
      <LayoutDefault
        subnav={MarketsInsightsItems}
        breadcrumb={Breadcrumbs.markets.items}>
        <HeroView title="Markets" />

        <Section className={`py-4 ${activeBreakpoints.below.sm && 'px-0'}`}>
          <Container>
            <Tabs
              rounded
              toggle
              centered={activeBreakpoints.below.sm ? true : false}
              className={`${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
              <Tab button label="Performance" id="performance">
                <Performance />
              </Tab>
              <Tab button label="Commodities & Currency">
                <Columns gapless={activeBreakpoints.below.sm}>
                  <Column>
                    <Commodities
                      expand={
                        activeBreakpoints.above.xs
                          ? true
                          : expand === 0
                          ? true
                          : false
                      }
                      toggleCC={e => this.toggleCCTab(e)}
                    />
                  </Column>
                  <Column>
                    <Currency
                      expand={
                        activeBreakpoints.above.xs
                          ? true
                          : expand === 1
                          ? true
                          : false
                      }
                      toggleCC={e => this.toggleCCTab(e)}
                    />
                  </Column>
                </Columns>
              </Tab>
              <Tab button label="Top 20s">
                <MarketsTop20 />
              </Tab>
              <Tab button label="Market movers">
                <MarketsMovers />
              </Tab>
            </Tabs>
          </Container>
        </Section>
      </LayoutDefault>
    );
  }
}

export default Breakpoints(MarketsInsights);
