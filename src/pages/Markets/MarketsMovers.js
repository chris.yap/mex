import React from 'react';
import faker from 'faker';
import RealtimeQuoteMsg from '../../components/RealtimeQuoteMsg';
import PercentageView from '../../components/PercentageView';
import {
  Breakpoints,
  Card,
  CardHeader,
  Column,
  Columns,
  Divider,
  Select,
  Table,
  TableCell as Cell,
  TableRow as Row,
  Title
} from 'shaper-react';

class MarketsMovers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: [],
      gainers: [],
      losers: []
    };
  }
  componentDidMount = () => {
    this.CreateData();
  };
  CreateData = () => {
    let tempActive = [];
    let tempGainers = [];
    let tempLosers = [];
    for (let i = 0; i < 5; i++) {
      tempActive.push({
        code: faker.random.alphaNumeric(3),
        name: faker.lorem.words(),
        last: faker.finance.amount(1, 20, 2, '$'),
        change: faker.finance.amount(1, 20, 2),
        changePercent: faker.finance.amount(0, 5, 2),
        range:
          faker.finance.amount(18, 20, 2) +
          ' - ' +
          faker.finance.amount(20, 25, 2),
        vol: faker.finance.amount(500, 990, 0)
      });
      tempGainers.push({
        code: faker.random.alphaNumeric(3),
        name: faker.lorem.words(),
        last: faker.finance.amount(1, 20, 2, '$'),
        change: faker.finance.amount(1, 20, 2),
        changePercent: faker.finance.amount(0, 5, 2),
        range:
          faker.finance.amount(18, 20, 2) +
          ' - ' +
          faker.finance.amount(20, 25, 2),
        vol: faker.finance.amount(500, 990, 0)
      });
      tempLosers.push({
        code: faker.random.alphaNumeric(3),
        name: faker.lorem.words(),
        last: faker.finance.amount(1, 20, 2, '$'),
        change: faker.finance.amount(-20, -1, 2),
        changePercent: faker.finance.amount(-5, -1, 2),
        range:
          faker.finance.amount(18, 20, 2) +
          ' - ' +
          faker.finance.amount(20, 25, 2),
        vol: faker.finance.amount(500, 990, 0)
      });
    }
    this.setState({
      active: tempActive,
      gainers: tempGainers,
      losers: tempLosers
    });
  };
  render() {
    const { activeBreakpoints } = this.props;
    const { active, gainers, losers } = this.state;
    return (
      <React.Fragment>
        <Card>
          <CardHeader className="is-align-items-center">
            <Columns mobile>
              <Column mobile="12" desktop="6" widescreen="4">
                <Select className="my-0 mr-4" />
              </Column>
            </Columns>
          </CardHeader>

          <Title size="5" className="mx-4 mt-4">
            Most actives
          </Title>
          <Table
            hasStripes
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            data={active}
            headers={Headers}
            tableRow={TableRow}
            mb={0}
          />
          <Divider />

          <Title size="5" className="mx-4 mt-4">
            Top gainers
          </Title>
          <Table
            hasStripes
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            data={gainers}
            headers={Headers}
            tableRow={TableRow}
            mb={0}
          />
          <Divider />

          <Title size="5" className="mx-4 mt-4">
            Top Losers
          </Title>
          <Table
            hasStripes
            hasNoScroll={activeBreakpoints.above.xs}
            stickyHeaderCount={activeBreakpoints.below.sm}
            data={losers}
            headers={Headers}
            tableRow={Row}
            defaultSortCol="changePercent"
            mb={0}
          />
        </Card>
        <RealtimeQuoteMsg local className="mt-4" />
      </React.Fragment>
    );
  }
}

export default Breakpoints(MarketsMovers);

const Headers = [
  { name: 'Code' },
  { name: 'Name' },
  { name: 'Last', align: 'right' },
  { name: "Today's change", align: 'right' },
  { name: 'Day range', align: 'right' },
  { name: 'Volume', align: 'right' }
];

const TableRow = ({ row }) => (
  <Row>
    <Cell width="10%">
      <a href="/#" className="is-uppercase has-text-weight-semibold">
        {row.code}
      </a>
    </Cell>
    <Cell width="25%" className="is-capitalized">
      {row.name}
    </Cell>
    <Cell className="has-text-right">{row.last}</Cell>
    <Cell className="has-text-right">
      {row.change}
      <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">{row.range}</Cell>
    <Cell className="has-text-right">{row.vol}K</Cell>
  </Row>
);
