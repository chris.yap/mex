import React from 'react';
import cn from 'classnames';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { MarketsInsightsItems } from '../../constants';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import {
  Breakpoints,
  Card,
  CardContent,
  CardHeader,
  Column,
  Columns,
  Container,
  Pagination,
  Section,
  Tag,
  Title
} from 'shaper-react';
import { News } from '../../constants/news';

class MarketsNews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newsModal: false
    };
  }
  componentDidMount() {}
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <LayoutDefault
        subnav={MarketsInsightsItems}
        breadcrumb={Breadcrumbs.markets.news.items}>
        <HeroView title="News" />

        <Section
          py={activeBreakpoints.below.sm ? 0 : 4}
          px={activeBreakpoints.below.sm && 0}>
          <Container>
            <Columns
              mobile
              multiline
              className={cn('mb-0', activeBreakpoints.below.sm ? 'mt-4' : '')}>
              {News.map((doc, index) => (
                <Column
                  key={index}
                  mobile="12"
                  tablet="12"
                  desktop="12"
                  widescreen="12"
                  className={'py-1'}>
                  <Card
                    onClick={() => this.setState({ newsModal: true })}
                    className={cn('has-link has-pointer mb-1')}>
                    <CardHeader>
                      <Title
                        size="5"
                        className={cn('has-text-blue-grey', 'mb-0')}>
                        {/* <ClampLines
													text={doc.title}
													buttons={false}
													lines={doc.sensitive && doc.type !== 'news' ? 2 : 3}
												/> */}
                        {doc.title}
                        {doc.sensitive && doc.type !== 'news' && (
                          <Tag small secondary className="mt-2">
                            Price sensitive
                          </Tag>
                        )}
                      </Title>
                    </CardHeader>
                    <CardContent>
                      <p className="has-text-size-3 mb-0 has-text-black text--lighten-2 is-flex is-align-items-center">
                        <img
                          src="/images/rtr_ahz_rgb_pos.png"
                          width="70"
                          alt=""
                          className="pos-r has-top mr-1"
                        />{' '}
                        | {doc.date}
                      </p>
                    </CardContent>
                  </Card>
                </Column>
              ))}
            </Columns>
            <Pagination
              current={1}
              total={10}
              className={activeBreakpoints.below.sm ? 'mx-4' : ''}
            />
          </Container>
        </Section>
      </LayoutDefault>
    );
  }
}

export default Breakpoints(MarketsNews);
