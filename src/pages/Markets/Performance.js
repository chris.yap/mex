import React from 'react';
import faker from 'faker';
import {
  Alert,
  Box,
  Breakpoints,
  Card,
  CardContent,
  Column,
  Columns,
  Divider,
  Icon,
  Label,
  Level,
  LevelItem,
  List,
  ListItem,
  Select,
  Subtitle,
  Tag,
  Title
} from 'shaper-react';
import DateTime from '../../components/DateTime';
import PercentageView from '../../components/PercentageView';
import RealtimeQuoteMessage from '../../components/RealtimeQuoteMsg';

class Performance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: '',
      selectedInfo: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.getDefault = this.getDefault.bind(this);
  }
  componentDidMount = () => {
    faker.locale = 'en_AU';
    this.getDefault();
  };
  handleChange = mkt => {
    this.setState({
      selected: mkt ? mkt.value : 0,
      selectedInfo: mkt ? mkt : null
    });
  };
  getDefault = () => {
    let tempSelected = !this.state.selected ? 0 : this.state.selected;
    Markets.map((mkt, mI) => {
      if (tempSelected === mI) {
        this.setState({
          selectedInfo: mkt
        });
      }
      return null;
    });
  };
  render() {
    const { selected, selectedInfo } = this.state;
    const { activeBreakpoints } = this.props;
    return (
      <Columns mobile multiline gapless={activeBreakpoints.below.sm}>
        <Column mobile={12} tablet={12} desktop={4} widescreen={3}>
          {activeBreakpoints.below.md && selected !== '' ? (
            <React.Fragment>
              <Alert
                info
                inverted
                isOpened={true}
                mx={activeBreakpoints.below.sm && 4}>
                <p className="mb-0">
                  The market is expected to open{' '}
                  <strong className="has-text-success">higher</strong> based on{' '}
                  <strong>ASX SPI 200</strong> futures trading{' '}
                  <strong className="has-text-success">
                    up 40 points (0.67%) to 6029
                  </strong>
                  .
                </p>
              </Alert>
              <Select
                options={Markets}
                defaultValue={Markets[selected]}
                onChange={this.handleChange}
                mx={4}
                mb={4}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Alert
                info
                inverted
                isOpened={true}
                mx={activeBreakpoints.below.sm && 4}>
                <p className="mb-0">
                  The market is expected to open{' '}
                  <strong className="has-text-success">higher</strong> based on{' '}
                  <strong>ASX SPI 200</strong> futures trading{' '}
                  <strong className="has-text-success">
                    up 40 points (0.67%) to 6029
                  </strong>
                  .
                </p>
              </Alert>
              <Card>
                <List bordered>
                  {Markets.map((mkt, mI) => {
                    let tempSelected = selected || 0;
                    let val = faker.finance.amount(-50, 50, 2);
                    let percent =
                      val > 0
                        ? faker.finance.amount(0, 1, 2)
                        : -faker.finance.amount(0, 1, 2);
                    return (
                      <ListItem
                        key={mI}
                        bg={mI === tempSelected && 'secondary'}
                        color={mI === tempSelected && 'white'}
                        fontWeight={mI === tempSelected && 'bold'}
                        py={0}
                        pr={0}
                        className={mI !== tempSelected && 'has-pointer'}
                        onClick={() => this.handleChange(mkt)}>
                        <Level mobile className="is-flex-1">
                          <LevelItem left>{mkt.name}</LevelItem>
                          <LevelItem right>
                            <small>
                              {new Intl.NumberFormat('en-AU').format(val)}
                              <PercentageView
                                dark={mI === tempSelected}
                                value={new Intl.NumberFormat('en-AU').format(
                                  percent
                                )}
                              />
                            </small>
                            <div
                              className={`py-4 ml-4 has-text-size-1 has-text-white is-uppercase has-bg-${
                                mkt.open ? 'success' : 'danger'
                              } px-0 pl-1 lighten-2`}
                              style={{ minHeight: '44px' }}>
                              {/* {mI === selected ? (mkt.open ? 'Open' : 'Close') : ' '} */}
                            </div>
                          </LevelItem>
                        </Level>
                      </ListItem>
                    );
                  })}
                </List>
              </Card>
            </React.Fragment>
          )}
        </Column>
        {activeBreakpoints.below.md && selected === '' ? null : (
          <Column mobile={12} tablet={12} desktop={8} widescreen={9}>
            <Level
              mobile
              className={`mb-0 ${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
              <LevelItem left>
                <div>
                  <Title size="4" className="mb-4">
                    {selectedInfo && selectedInfo.name}{' '}
                    <Tag
                      small
                      className={`pos-r has-top-2 has-text-white is-hidden-mobile ${
                        selectedInfo.open ? 'has-bg-success' : 'has-bg-danger'
                      }`}>{`${selectedInfo.open ? 'Open' : 'Closed'}`}</Tag>
                  </Title>
                  <Subtitle size="8">
                    <DateTime time />
                  </Subtitle>
                </div>
              </LevelItem>
              <LevelItem right>
                <div>
                  <Title size="4" className="mb-4">
                    6,354.23
                  </Title>
                  <Subtitle size="8">
                    17.60{' '}
                    <span className="has-text-success">
                      <Icon small>caret-up</Icon>
                      (0.34<sup className="pos-r has-top3">%</sup>)
                    </span>
                  </Subtitle>
                </div>
              </LevelItem>
            </Level>

            <Card
              style={{ paddingTop: '50%' }}
              className="has-bg-secondary lighten-4"
            />
            <Card>
              <CardContent>
                <Title size="6" className="mb-2">
                  Key stats
                </Title>
                <Columns mobile>
                  <Column>
                    <Label className="nab-label mb-0">Open</Label>
                    <p className="mb-0">
                      {new Intl.NumberFormat('en-AU').format(
                        faker.finance.amount(6000, 6400, 2)
                      )}
                    </p>
                  </Column>
                  <Column>
                    <Label className="nab-label mb-0">High</Label>
                    <p className="mb-0">
                      {new Intl.NumberFormat('en-AU').format(
                        faker.finance.amount(6200, 6400, 2)
                      )}
                    </p>
                  </Column>
                  <Column>
                    <Label className="nab-label mb-0">Low</Label>
                    <p className="mb-0">
                      {new Intl.NumberFormat('en-AU').format(
                        faker.finance.amount(6000, 6200, 2)
                      )}
                    </p>
                  </Column>
                </Columns>
                <Divider className="mb-4" />
                <Title size="6" className="mb-2">
                  Returns
                </Title>
                <Columns mobile multiline>
                  <Column mobile="4">
                    <Label className="nab-label mb-0">5 Day</Label>
                    <PercentageView value={0.34} />
                  </Column>
                  <Column mobile="4">
                    <Label className="nab-label mb-0">1 Month</Label>
                    <PercentageView value={2.74} />
                  </Column>
                  <Column mobile="4">
                    <Label className="nab-label mb-0">3 Month</Label>
                    <PercentageView value={5.87} />
                  </Column>
                  <Column mobile="4">
                    <Label className="nab-label mb-0">YTD</Label>
                    <PercentageView value={8.14} />
                  </Column>
                  <Column mobile="4">
                    <Label className="nab-label mb-0">1 Year</Label>
                    <PercentageView value={10.34} />
                  </Column>
                </Columns>
                <Divider className="mb-4" />
                <Title size="6" className="mb-2">
                  52 week range
                </Title>
                <Columns mobile className="mb-1">
                  <Column>
                    <Box className="has-text-centered">
                      <Label className="nab-label mb-0">High</Label>
                      <p className="has-text-success mb-0">
                        {new Intl.NumberFormat('en-AU').format(
                          faker.finance.amount(6000, 7000, 2)
                        )}
                      </p>
                      <p className="has-text-size-2 mb-0">
                        <DateTime />
                      </p>
                    </Box>
                  </Column>
                  <Column>
                    <Box className="has-text-centered">
                      <Label className="nab-label mb-0">Low</Label>
                      <p className="has-text-danger mb-0">
                        {new Intl.NumberFormat('en-AU').format(
                          faker.finance.amount(5000, 6000, 2)
                        )}
                      </p>
                      <p className="has-text-size-2 mb-0">
                        <DateTime />
                      </p>
                    </Box>
                  </Column>
                </Columns>
              </CardContent>
            </Card>
            <RealtimeQuoteMessage
              local
              international
              className={`mt-4 ${activeBreakpoints.below.sm ? 'mx-4' : ''}`}
            />
          </Column>
        )}
      </Columns>
    );
  }
}

export default Breakpoints(Performance);

const Markets = [
  {
    label: (
      <span>
        All Ordinaries
        <Tag small success className="ml-2 is-uppercase">
          Open
        </Tag>
      </span>
    ),
    name: 'All Ordinaries',
    value: 0,
    open: true
  },
  {
    label: (
      <span>
        S&P/ASX 200
        <Tag small success className="ml-2 is-uppercase">
          Open
        </Tag>
      </span>
    ),
    name: 'S&P/ASX 200',
    value: 1,
    open: true
  },
  {
    label: (
      <span>
        S&P 500
        <Tag small danger className="ml-2 is-uppercase">
          Close
        </Tag>
      </span>
    ),
    name: 'S&P 500',
    value: 2,
    open: false
  },
  {
    label: (
      <span>
        Dow Jones
        <Tag small danger className="ml-2 is-uppercase">
          Close
        </Tag>
      </span>
    ),
    name: 'Dow Jones',
    value: 3,
    open: false
  },
  {
    label: (
      <span>
        NASDAQ
        <Tag small danger className="ml-2 is-uppercase">
          Close
        </Tag>
      </span>
    ),
    name: 'NASDAQ',
    value: 4,
    open: false
  },
  {
    label: (
      <span>
        FTSE 100
        <Tag small danger className="ml-2 is-uppercase">
          Close
        </Tag>
      </span>
    ),
    name: 'FTSE 100',
    value: 5,
    open: false
  },
  {
    label: (
      <span>
        DAX
        <Tag small danger className="ml-2 is-uppercase">
          Close
        </Tag>
      </span>
    ),
    name: 'DAX',
    value: 6,
    open: false
  },
  {
    label: (
      <span>
        Hang Seng
        <Tag small success className="ml-2 is-uppercase">
          Open
        </Tag>
      </span>
    ),
    name: 'Hang Seng',
    value: 7,
    open: true
  }
];
