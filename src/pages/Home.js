import React from 'react';
import {
  Button,
  Card,
  CardHeader,
  CardImage,
  CardHeaderTitle,
  CardContent,
  Collapse,
  Columns,
  Column,
  Container,
  Divider,
  Hero,
  Icon,
  List,
  ListItem,
  Tab,
  Tabs,
  Tag,
  Title,
  Section,
  Subtitle
} from 'shaper-react';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openCard: null
    };
  }
  toggleCard = index => {
    if (this.state.openCard === index) {
      this.setState({ openCard: null });
    } else {
      this.setState({ openCard: index });
    }
  };
  render() {
    return (
      <>
        <Hero
          className="is-dark"
          image="/images/jordan-mcqueen-99269-unsplash.jpg">
          <Container>
            <Subtitle size="4" className="font-nabscript has-text-primary">
              Welcome back,
            </Subtitle>
            <Title className="mb-1">Veronica</Title>
            <p>
              <small>
                You last logged in at 10:15 PM (Sydney time) on Monday 18
                December 2017
              </small>
            </p>
          </Container>
        </Hero>

        <Section className="py-6">
          <Container>
            <Columns mobile multiline className="">
              <Column className="is-12-mobile is-12-tablet is-9-desktop">
                {Cards.map((card, c) => {
                  return (
                    <div key={c} className="mb-4">
                      <Card
                        className="has-pointer has-elevation-18"
                        onClick={() => this.toggleCard(c)}>
                        <CardContent>
                          <Columns mobile multiline className="mt-0 mb-0">
                            <Column className="is-12-mobile is-7-tablet is-6-desktop is-6-widescreen is-align-self-center">
                              {card.alerts && (
                                <p className="mb-0 has-text-primary">
                                  <Icon>alerts-on</Icon>
                                </p>
                              )}
                              <Title size="3" className="my-0">
                                {card.title}
                                <Icon
                                  material
                                  small
                                  className="ml-2 has-text-black text--lighten-4 is-hidden-touch is-hidden-desktop-only">
                                  edit
                                </Icon>
                              </Title>
                              <p className="my-0 is-5 has-text-black text--lighten-3">
                                {card.accNum}
                              </p>
                            </Column>
                            <Column className="is-12-mobile is-5-tablet is-6-desktop is-6-widescreen has-text-right">
                              <Columns mobile>
                                <Column className="is-6-mobile is-5-tablet is-6-desktop is-5-widescreen is-align-self-flex-end">
                                  <Title size="6" className="mt-1 mb-0">
                                    {card.gainLoss > 0 && (
                                      <span className="has-text-success">
                                        <Icon small>priceup</Icon>
                                        {card.gainLoss}%
                                      </span>
                                    )}
                                    {card.gainLoss < 0 && (
                                      <span className="has-text-danger">
                                        <Icon small>pricedown</Icon>
                                        {card.gainLoss}%
                                      </span>
                                    )}
                                  </Title>
                                  <p className="my-0 is-uppercase has-text-black text--lighten-3">
                                    <small>Portfolio G/L</small>
                                  </p>
                                  <Title size="6" className="mt-4 mb-0">
                                    {card.today > 0 && (
                                      <span className="has-text-success">
                                        <Icon small>priceup</Icon>
                                        {card.today}%
                                      </span>
                                    )}
                                    {card.today < 0 && (
                                      <span className="has-text-danger">
                                        <Icon small>pricedown</Icon>
                                        {card.today}%
                                      </span>
                                    )}
                                  </Title>
                                  <p className="my-0 is-uppercase has-text-black text--lighten-3">
                                    <small>Today's G/L</small>
                                  </p>
                                </Column>
                                <Column className="is-6-mobile is-7-tablet is-6-desktop is-5-widescreen">
                                  <Title size="4" className="my-0">
                                    ${card.value}
                                  </Title>
                                  <p className="my-0 is-uppercase has-text-black text--lighten-3">
                                    <small>Current value</small>
                                  </p>
                                  <Title size="6" className="mt-4 mb-0">
                                    ${card.cash}
                                  </Title>
                                  <p className="my-0 is-uppercase has-text-black text--lighten-3">
                                    <small>Cash available</small>
                                  </p>
                                </Column>
                                <Column className="has-text-right is-2-widescreen has-text-primary is-hidden-touch is-hidden-desktop-only">
                                  {this.state.openCard === c && (
                                    <Icon material>remove</Icon>
                                  )}
                                  {this.state.openCard !== c && (
                                    <Icon material>add</Icon>
                                  )}
                                </Column>
                              </Columns>
                            </Column>
                          </Columns>
                        </CardContent>
                        {/* <Divider className="my-0" /> */}
                      </Card>

                      <Collapse isOpened={this.state.openCard === c}>
                        <Card
                          className={'has-elevation-18 mb-4 pb-4 fade fade-'}>
                          <Tabs fullwidth boxed className="mb-0">
                            <Tab label="Holdings">
                              <Tabs>
                                <Tab label="Basic">
                                  {DataTables.map((table, t) => {
                                    return (
                                      <div key={`${table.type}-${t}`}>
                                        <Title
                                          size="6"
                                          className="is-uppercase px-4">
                                          {table.type}
                                        </Title>
                                        <Divider />
                                        {table.data && (
                                          <div className="nab-table-wrapper">
                                            <table className="nab-table is-striped">
                                              <thead>
                                                <tr>
                                                  <th />
                                                  <th width="100">Code</th>
                                                  <th className="has-text-right">
                                                    Qty
                                                  </th>
                                                  <th className="has-text-right">
                                                    Purchase price
                                                  </th>
                                                  <th className="has-text-right">
                                                    Current market price
                                                  </th>
                                                  <th className="has-text-right">
                                                    Market value
                                                  </th>
                                                  <th className="has-text-right">
                                                    Gain/Loss
                                                  </th>
                                                  <th>Alerts</th>
                                                  <th />
                                                  <th className="pl-0" />
                                                </tr>
                                              </thead>
                                              <tbody>
                                                {table.data.map((data, d) => {
                                                  return (
                                                    <tr key={d}>
                                                      <td className="pr-0">
                                                        <Button
                                                          small
                                                          round
                                                          flat>
                                                          <Icon
                                                            material
                                                            small
                                                            className="has-text-black text--lighten-3">
                                                            add
                                                          </Icon>
                                                        </Button>
                                                      </td>
                                                      <td>{data.code}</td>
                                                      <td className="has-text-right">
                                                        {data.qty}
                                                      </td>
                                                      <td className="has-text-right">
                                                        ${data.purchase}
                                                      </td>
                                                      <td className="has-text-right">
                                                        ${data.market}
                                                      </td>
                                                      <td className="has-text-right">
                                                        ${data.value}
                                                      </td>
                                                      <td className="has-text-right">
                                                        {data.gainLoss > 0 && (
                                                          <span className="has-text-success">
                                                            <Icon small>
                                                              priceup
                                                            </Icon>
                                                            {data.gainLoss}%
                                                          </span>
                                                        )}
                                                        {data.gainLoss < 0 && (
                                                          <span className="has-text-danger">
                                                            <Icon small>
                                                              pricedown
                                                            </Icon>
                                                            {data.gainLoss}%
                                                          </span>
                                                        )}
                                                        {data.gainLoss ===
                                                          0 && (
                                                          <span>
                                                            {data.gainLoss}%
                                                          </span>
                                                        )}
                                                      </td>
                                                      <td className="has-text-centered">
                                                        {data.alerts && (
                                                          <Tag small>
                                                            {data.alerts}
                                                          </Tag>
                                                        )}
                                                      </td>
                                                      <td className="pr-0">
                                                        <Button small>
                                                          Trade
                                                        </Button>
                                                      </td>
                                                      <td className="pl-0">
                                                        <Button
                                                          flat
                                                          round
                                                          small>
                                                          <Icon small>
                                                            options
                                                          </Icon>
                                                        </Button>
                                                      </td>
                                                    </tr>
                                                  );
                                                })}
                                              </tbody>
                                            </table>
                                          </div>
                                        )}
                                        <Divider className="mb-4" />
                                      </div>
                                    );
                                  })}
                                </Tab>
                                <Tab label="Income">
                                  <CardContent>Income Content</CardContent>
                                </Tab>
                                <Tab label="Today's Performance">
                                  <CardContent>
                                    Today's Performance Content
                                  </CardContent>
                                </Tab>
                              </Tabs>
                              <CardContent>
                                {/* Holdings content */}
                              </CardContent>
                            </Tab>
                            <Tab label="Portfolio Summary">
                              <CardContent>PS content</CardContent>
                            </Tab>
                          </Tabs>
                        </Card>
                      </Collapse>
                    </div>
                  );
                })}

                <Columns mobile multiline className="mt-4">
                  <Column className="is-12-mobile is-6-tablet is-7-desktop">
                    <Title size="5">Expert tips</Title>
                    <p>
                      There are many different ways investors and traders alike
                      research the companies they invest in.
                    </p>
                    <p>
                      We've explored two approaches — Fundamental Analysis and
                      Technical Analysis, but it's important to note that there
                      isn't one approach that suits all investors.
                    </p>

                    <Columns className="mt-4">
                      <Column className="has-text-centered">
                        <Icon large>info</Icon>
                        <Title size="6" className="mb-2">
                          Understanding Fundamental Analysis
                        </Title>
                        <p>
                          <a href="#">Learn more ></a>
                        </p>
                      </Column>
                      <Column className="has-text-centered">
                        <Icon large>info</Icon>
                        <Title size="6" className="mb-2">
                          Using Technical Analysis
                        </Title>
                        <p>
                          <a href="#">Learn more ></a>
                        </p>
                      </Column>
                    </Columns>
                  </Column>
                  <Column className="is-12-mobile is-6-tablet is-5-desktop">
                    <Title size="5">Recent news</Title>
                    <Card className="has-elevation-18 mb-4">
                      <CardImage image="/images/christian-wiediger-735834-unsplash.jpg" />
                      <CardContent>
                        <Title size="6" className="mt-2">
                          Why European exposure makes sense
                        </Title>
                        <p>
                          <small>19 January 2018</small>{' '}
                          <Tag small className="is-pulled-right">
                            Stocks to watch
                          </Tag>
                        </p>
                      </CardContent>
                    </Card>
                    <Card className="has-elevation-18">
                      <CardContent>
                        <Title size="6" className="mt-2">
                          Enter the bull market
                        </Title>
                        <p>
                          As the new year kicks off with a bull market in full
                          swing, portfolios have prepared for wild days. So
                          should you.
                        </p>
                        <p>
                          <small>19 January 2018</small>{' '}
                          <Tag small black className="is-pulled-right">
                            Market news
                          </Tag>
                        </p>
                      </CardContent>
                    </Card>
                  </Column>
                </Columns>
              </Column>

              <Column className="is-12-mobile is-12-tablet is-3-desktop is-3-widescreen">
                <Columns multiline mobile>
                  <Column className="is-12-mobile is-6-tablet is-12-desktop">
                    <Card className="has-elevation-18">
                      <CardHeader className="px-4">
                        <CardHeaderTitle>Key events</CardHeaderTitle>
                      </CardHeader>
                      <List>
                        <ListItem>
                          <Icon medium className="is-align-self-centered">
                            info
                          </Icon>
                          <div className="ml-2">
                            <Subtitle className="ma-0" size="6">
                              Dividend paid for{' '}
                              <strong className="is-uppercase">
                                Domino Pizza Enterpr: $104.31
                              </strong>
                            </Subtitle>
                            <Subtitle
                              className="mt-1 mb-0 has-text-black text--lighten-3"
                              size="7">
                              Jenkin's family trust <br />
                              23 April 2018, 10:01am
                            </Subtitle>
                          </div>
                        </ListItem>
                        <ListItem>
                          <Icon medium className="is-align-self-centered">
                            info
                          </Icon>
                          <div className="ml-2">
                            <Subtitle className="ma-0" size="6">
                              <strong className="is-uppercase">AMP</strong> has
                              reached a{' '}
                              <strong className="is-uppercase">
                                52 week high
                              </strong>
                            </Subtitle>
                            <Subtitle
                              className="mt-1 mb-0 has-text-black text--lighten-3"
                              size="7">
                              Jenkin's family trust <br />
                              23 April 2018, 10:01am
                            </Subtitle>
                          </div>
                        </ListItem>
                        <ListItem>
                          <Icon medium className="is-align-self-centered">
                            info
                          </Icon>
                          <div className="ml-2">
                            <Subtitle className="ma-0" size="6">
                              You've now held{' '}
                              <strong className="is-uppercase">
                                Perpetual Ltd for 45 days
                              </strong>
                            </Subtitle>
                            <Subtitle
                              className="mt-1 mb-0 has-text-black text--lighten-3"
                              size="7">
                              Jenkin's family trust <br />
                              23 April 2018, 10:01am
                            </Subtitle>
                          </div>
                        </ListItem>
                        <ListItem>
                          <Icon medium className="is-align-self-centered">
                            info
                          </Icon>
                          <div className="ml-2">
                            <Subtitle className="ma-0" size="6">
                              A dividend paid for{' '}
                              <strong className="is-uppercase">AMP</strong> of{' '}
                              <strong>$104.31</strong>
                            </Subtitle>
                            <Subtitle
                              className="mt-1 mb-0 has-text-black text--lighten-3"
                              size="7">
                              Jenkin's family trust <br />
                              23 April 2018, 10:01am
                            </Subtitle>
                          </div>
                        </ListItem>
                      </List>
                      {/* <CardContent>123</CardContent> */}
                    </Card>
                  </Column>

                  <Column className="is-12-mobile is-6-tablet is-12-desktop">
                    <Card
                      className="has-bg-blue-grey darken-4 mb-4 has-elevation-18"
                      style={{ height: '380px' }}>
                      <CardContent>
                        <Title dark>Ad</Title>
                      </CardContent>
                    </Card>
                  </Column>
                </Columns>
              </Column>
            </Columns>
          </Container>
        </Section>
      </>
    );
  }
}

const Cards = [
  {
    alerts: 7,
    title: "Jenkin's family trust",
    accNum: 'NT193920402-001',
    value: '1,643,209',
    cash: '43,209',
    gainLoss: '1.2',
    today: '-0.3'
  },
  {
    title: "Amy's uni trust",
    accNum: 'NT193920402-002',
    value: '11,900',
    cash: '23.15',
    gainLoss: '9.5',
    today: '0.3'
  },
  {
    title: 'Individual account',
    accNum: 'NT193920402-003',
    value: '53,450',
    cash: '20.75',
    gainLoss: '1.7',
    today: '-0.3'
  }
];

const DataTables = [
  {
    type: 'Australian equities',
    data: [
      {
        code: 'COH',
        qty: '5,513',
        purchase: '8.12',
        market: '9.18',
        value: '38,808.54',
        gainLoss: '-4.90'
      },
      {
        code: 'CSL',
        qty: '23,144',
        purchase: '42.80',
        market: '44.20',
        value: '7,010',
        gainLoss: '-2.60'
      },
      {
        code: 'A2M',
        qty: '7,024',
        purchase: '1.98',
        market: '2.48',
        value: '603.54',
        gainLoss: '1.10',
        alerts: 1
      },
      {
        code: 'PPT',
        qty: '10,523',
        purchase: '0.11',
        market: '1.01',
        value: '103.27',
        gainLoss: '5.41',
        alerts: 3
      },
      {
        code: 'DAX',
        qty: '2,139',
        purchase: '19.70',
        market: '18.25',
        value: '981.32',
        gainLoss: '2.40'
      },
      {
        code: 'AMP',
        qty: '5,248',
        purchase: '1.14',
        market: '1.14',
        value: '4,703.57',
        gainLoss: '0',
        alerts: 2
      }
    ]
  },
  {
    type: 'International equities',
    data: [
      {
        code: 'COH',
        qty: '5,514',
        purchase: '8.12',
        market: '9.18',
        value: '38,808.23',
        gainLoss: '-4.90'
      },
      {
        code: 'CSL',
        qty: '23,144',
        purchase: '42.80',
        market: '44.20',
        value: '7,010',
        gainLoss: '-2.60'
      },
      {
        code: 'A2M',
        qty: '7,024',
        purchase: '1.98',
        market: '2.48',
        value: '603.54',
        gainLoss: '1.10',
        alerts: 1
      },
      {
        code: 'PPT',
        qty: '10,523',
        purchase: '0.11',
        market: '1.01',
        value: '103.27',
        gainLoss: '5.41',
        alerts: 1
      }
    ]
  }
];
