import React from 'react';
import { Button, Column, Columns, Container, Section, Title } from 'shaper-react';

const NoWatchList = ({ ...props }) => (
	<Section>
		<Container>
			<Columns>
				<Column className="has-text-centered">
					<Title size="4" className="has-text-blue-grey text--lighten-3">
						No watchlist found
					</Title>
					<Button secondary medium onClick={props.newWatchlist}>
						Create new watchlist
					</Button>
				</Column>
			</Columns>
		</Container>
	</Section>
);

export default NoWatchList;
