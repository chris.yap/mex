import React from 'react';
import enhanceWithClickOutside from 'react-click-outside';
import { Button, Icon } from 'shaper-react';

class WatchListDelete extends React.Component {
  constructor(props) {
    super();
  }
  confirmDelete = () => {
    this.props.confirmed();
  };
  render() {
    const { className, ...props } = this.props;
    return (
      <div className={`is-flex is-align-items-center ${className}`}>
        <span className="has-text-blue-grey">Delete this watchlist?</span>
        <Button
          square
          secondary
          inverted
          className="my-0 ml-2 mr-1"
          onClick={props.cancel}
        >
          <Icon>close</Icon>
        </Button>
        <Button
          square
          inverted
          danger
          className="my-0 mx-0"
          onClick={() => this.confirmDelete()}
        >
          <Icon>checked</Icon>
        </Button>
      </div>
    );
  }
}

export default enhanceWithClickOutside(WatchListDelete);
