import React from 'react';
import {
  Button,
  Dropdown,
  Icon,
  List,
  ListItem,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

const Headers = [
  {
    name: 'Code',
    value: 'code',
    sort: true
  },
  {
    name: 'Last',
    value: 'last',
    sort: true,
    align: 'right'
  },
  {
    name: 'Bid',
    value: 'bid',
    sort: true,
    align: 'right'
  },
  {
    name: 'Ask',
    value: 'ask',
    sort: true,
    align: 'right'
  },
  {
    name: 'Change',
    value: 'change',
    sort: true,
    align: 'right'
  },
  {
    name: 'Open',
    value: 'open',
    sort: true,
    align: 'right'
  },
  {
    name: 'Day range',
    value: 'range',
    align: 'right'
  },
  {
    name: 'Volume',
    value: 'vol',
    sort: true
  },
  {
    name: '',
    value: ''
  }
];

const TableRow = ({ row, overflow, ...props }) => (
  <Row>
    <Cell>
      <a href="/" className="has-text-weight-bold is-underlined">
        {row.code}
      </a>{' '}
      {row.alert && (
        <Icon small number={row.alert} className="pos-r has-top3">
          bell
        </Icon>
      )}
    </Cell>
    <Cell right>${row.last}</Cell>
    <Cell right>${row.bid}</Cell>
    <Cell right>${row.ask}</Cell>
    <Cell right>
      ${row.change}
      <span
        className={`${
          row.changePercent > 0 ? 'has-text-success' : 'has-text-danger'
        }`}>
        <Icon small>{`${
          row.changePercent > 0 ? 'caret-up' : 'caret-down'
        }`}</Icon>
        ({row.changePercent}
        <sup className="pos-r has-top3">%</sup>)
      </span>
    </Cell>
    <Cell right>${row.open}</Cell>
    <Cell right>{row.range}</Cell>
    <Cell>
      <strong>{row.adv}</strong> ADV.
    </Cell>
    <Cell right pr={1} overflow="visible">
      {overflow ? (
        <Button
          small
          square
          secondary
          onClick={() => {
            props.toggleModal(true);
            props.action('more', row.code);
          }}>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2 has-text-left"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2 has-text-left">Edit cost base</ListItem>
            <ListItem
              className="py-2 has-text-left has-line-height-110"
              onClick={() => props.addOther(row.code)}>
              Add to another watchlist
            </ListItem>
            <ListItem className="py-2 has-text-left">Set up Alert</ListItem>
            <ListItem className="py-2 has-text-left">
              Set up Stop Loss Order
            </ListItem>
            <ListItem className="py-2 has-text-left has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);

export { Headers, TableRow };
