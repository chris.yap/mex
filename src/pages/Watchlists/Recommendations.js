import React from 'react';
import {
  Button,
  Card,
  Dropdown,
  Icon,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

const Recommendations = ({ data, className, ...props }) => {
  return (
    <Card>
      <Table
        hasStripes
        isHoverable
        data={data}
        headers={Headers}
        tableRow={TableRow}
        defaultSortCol="code"
        my={0}
        className={`${className}`}
        {...props}
      />
    </Card>
  );
};

export default Recommendations;

const Headers = [
  { name: 'Code', sort: true, value: 'code' },
  { name: 'Thomson Reuters' },
  {
    name: (
      <span>
        Trading Central
        <br />
        Short Term
      </span>
    )
  },
  {
    name: (
      <span>
        Trading Central
        <br />
        Medium Term
      </span>
    )
  },
  { name: 'MorningStar' },
  {}
];

const TableRow = ({ overflow, row, ...props }) => (
  <Row>
    <Cell>
      {row.code}{' '}
      {row.alert && (
        <Icon small number={row.alert} className="pos-r has-top3">
          bell
        </Icon>
      )}
    </Cell>
    <Cell className="has-line-height-100">
      {row.tr ? (
        <React.Fragment>
          <span
            className={`is-uppercase has-text-weight-bold
            ${
              row.tr.recommendation.toLowerCase() === 'positive' ||
              row.tr.recommendation.toLowerCase() === 'limited rise' ||
              row.tr.recommendation.toLowerCase() === 'rise' ||
              row.tr.recommendation.toLowerCase() === 'bullish'
                ? 'has-text-success'
                : 'has-text-danger'
            }`}>
            {row.tr.recommendation}
          </span>
          <br />
          <span className="has-text-size-1">As of {row.tr.date}</span>
        </React.Fragment>
      ) : (
        '-'
      )}
    </Cell>
    <Cell className="has-line-height-100">
      {row.tcst ? (
        <React.Fragment>
          <span
            className={`is-uppercase has-text-weight-bold
            ${
              row.tcst.recommendation.toLowerCase() === 'positive' ||
              row.tcst.recommendation.toLowerCase() === 'limited rise' ||
              row.tcst.recommendation.toLowerCase() === 'rise' ||
              row.tcst.recommendation.toLowerCase() === 'bullish'
                ? 'has-text-success'
                : 'has-text-danger'
            }`}>
            {row.tcst.recommendation}
          </span>
          <br />
          <span className="has-text-size-1">As of {row.tcst.date}</span>
        </React.Fragment>
      ) : (
        '-'
      )}
    </Cell>
    <Cell className="has-line-height-100">
      {row.tcmt ? (
        <React.Fragment>
          <span
            className={`is-uppercase has-text-weight-bold
            ${
              row.tcmt.recommendation.toLowerCase() === 'positive' ||
              row.tcmt.recommendation.toLowerCase() === 'limited rise' ||
              row.tcmt.recommendation.toLowerCase() === 'rise' ||
              row.tcmt.recommendation.toLowerCase() === 'bullish'
                ? 'has-text-success'
                : 'has-text-danger'
            }`}>
            {row.tcmt.recommendation}
          </span>
          <br />
          <span className="has-text-size-1">As of {row.tcmt.date}</span>
        </React.Fragment>
      ) : (
        '-'
      )}
    </Cell>
    <Cell className="has-line-height-100">
      {row.ms ? (
        <React.Fragment>
          <span
            className={`is-uppercase has-text-weight-bold
            ${
              row.ms.recommendation.toLowerCase() === 'positive' ||
              row.ms.recommendation.toLowerCase() === 'limited rise' ||
              row.ms.recommendation.toLowerCase() === 'rise' ||
              row.ms.recommendation.toLowerCase() === 'bullish'
                ? 'has-text-success'
                : 'has-text-danger'
            }`}>
            {row.ms.recommendation}
          </span>
          <br />
          <span className="has-text-size-1">As of {row.ms.date}</span>
        </React.Fragment>
      ) : (
        '-'
      )}
    </Cell>
    <Cell className="has-text-right is-align-items-center">
      {overflow ? (
        <Button small square flat secondary>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square flat secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2">Edit cost base</ListItem>
            <ListItem className="py-2">Set up Alert</ListItem>
            <ListItem className="py-2">Set up Stop Loss Order</ListItem>
            <ListItem className="py-2 has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);
