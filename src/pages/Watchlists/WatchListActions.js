import React from 'react';
import { Breakpoints, Button, Divider, Icon, Tooltip } from 'shaper-react';
import WatchListDelete from './WatchListDelete';

class WatchListActions extends React.Component {
  render() {
    const {
      activeBreakpoints,
      action,
      selected,
      className,
      ...props
    } = this.props;
    let spacing = activeBreakpoints.below.sm ? 'ml-0 mr-1' : 'mr-0 ml-1';
    return (
      <React.Fragment>
        {!action && (
          <div className={`is-flex ${className}`}>
            <Tooltip tip="Duplicate">
              <Button
                secondary
                inverted
                square
                className={spacing}
                onClick={props.duplicate}
              >
                <Icon>copy</Icon>
              </Button>
            </Tooltip>
            <Divider
              vertical
              className={activeBreakpoints.below.sm ? 'ml-2 mr-4' : 'ml-4 mr-2'}
            />
            <Button
              secondary
              inverted
              square
              className={spacing}
              onClick={props.edit}
            >
              <Icon>pencil</Icon>
              {/* <span>Edit</span> */}
            </Button>
            {/* <Button secondary inverted className={spacing} onClick={props.duplicate}>
						<Icon>copy</Icon>
						<span>Duplicate</span>
					</Button> */}
            <Button
              danger
              inverted
              square
              className={spacing}
              onClick={props.delete}
            >
              <Icon>trash</Icon>
            </Button>
          </div>
        )}
        {(action === 'new' || action === 'edit') && (
          <div className={className}>
            <Button
              secondary
              inverted
              className={spacing}
              onClick={props.cancel}
            >
              Cancel
            </Button>
            <Button success inverted className={spacing} onClick={props.save}>
              Save
            </Button>
          </div>
        )}
        {action === 'delete' && (
          <WatchListDelete
            className={className}
            cancel={props.null}
            confirmed={props.deleteConfirmed}
          />
        )}
      </React.Fragment>
    );
  }
}

export default Breakpoints(WatchListActions);
