import React from 'react';
import {
  Alert,
  Breakpoints,
  Button,
  Card,
  CheckboxGroup,
  Checkbox,
  Collapse,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem,
  Modal,
  Pagination,
  Section,
  Select,
  Tab,
  Tabs,
  Table,
  Textfield,
  Title,
  Subtitle
} from 'shaper-react';
import { Headers, TableRow } from './TableStructure';
import { Breadcrumbs } from '../../constants/breadcrumbs';
import { Securities } from '../../constants/securities';
import LayoutDefault from '../../layout/LayoutDefault';
import HeroView from '../../components/HeroView';
import NoWatchList from './NoWatchlist';
import WatchListActions from './WatchListActions';
import Performance from './Performance';
import Recommendations from './Recommendations';
import Income from './Income';
import RealtimeQuoteMessage from '../../components/RealtimeQuoteMsg';

class Watchlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      action: null,
      newStep: 1,
      modal: false,
      selected: 0,
      tempList: {},
      tempItems: [],
      addOtherItem: '',
      watchlists: [
        {
          default: true,
          name: 'Watchlist 1',
          items: [
            {
              ask: '34.86',
              bid: '35.91',
              change: '35.33',
              changePercent: '-1.58',
              code: 'BHP',
              exchange: 'ASX',
              industry: 'Metals and Mining, Petroleum',
              label: 'BHP Group Ltd',
              last: '38.14',
              name: 'BHP Billiton Group Ltd',
              open: '35.43',
              range: '33.96 - 33.08',
              value: 'bhp.asx',
              vol: '5,198,068'
            },
            {
              ask: '37.41',
              bid: '35.79',
              change: '35.55',
              changePercent: '3.41',
              code: 'APPL',
              exchange: 'NASDAQ',
              industry: 'Technology',
              label: 'Apple Inc',
              last: '38.60',
              name: 'Apple Inc',
              open: '35.43',
              range: '33.25 - 33.94',
              value: 'aapl.nasdaq',
              vol: '6,632,608'
            }
          ]
        },
        {
          name: 'Potential for SMSF'
        }
      ]
    };
    this.updateName = this.updateName.bind(this);
    this.updateItems = this.updateItems.bind(this);
    this.selectWatchlist = this.selectWatchlist.bind(this);
    this.isOverflowed = this.isOverflowed.bind(this);
  }

  componentDidMount = () => {
    const { watchlists } = this.state;
    if (watchlists.length > 0) {
      this.selectWatchlist();
    }
    this.cloneWatchlists();
  };

  componentWillReceiveProps = prevProps => {
    if (prevProps !== this.props.changeState) {
      console.log('change');
    }
  };

  cloneWatchlists = () => {
    const { watchlists } = this.state;
    let clone = [];
    if (watchlists.length > 0) {
      watchlists.map((wl, w) => {
        clone.push({ label: wl.name, value: w });
        return null;
      });
    }
    this.setState({
      cloneList: clone
    });
  };

  updateName = e => {
    let newName = e.target.value;
    let error;
    if (!newName) {
      error = 'Name of watchlist is required';
    }
    this.setState(prevState => ({
      tempList: {
        ...prevState.tempList,
        name: newName
      },
      nameError: error
    }));
  };

  updateItems = item => {
    this.setState({
      tempItems: [...this.state.tempItems, item]
    });
  };

  getSecurities = () => {
    const { tempList } = this.state;
    let listItems = [];
    if (tempList && tempList.items) {
      tempList.items.map(wi => {
        let obj = Securities.find(o => o.value === wi.value);
        listItems.push(obj);
        return null;
      });
    }
    this.setState({
      tempItems: listItems
    });
  };

  selectWatchlist = item => {
    const { watchlists } = this.state;
    let tempList = {};
    tempList = item ? watchlists[item] : watchlists[0];
    const tempItems =
      tempList.items && tempList.items.length > 0 ? tempList.items : [];

    this.setState({
      tempList,
      tempItems,
      selected: item ? item : 0
    });
  };

  clearTempList = () => {
    this.setState({
      tempList: {},
      tempItems: []
    });
  };

  toggleDefault = i => {
    const { watchlists, selected } = this.state;
    this.setState(
      {
        watchlists: watchlists.map((el, id) =>
          id === i ? { ...el, default: true } : { ...el, default: false }
        )
      },
      () => {
        watchlists.map(
          (el, id) =>
            id === selected &&
            this.setState(
              {
                tempList: { ...el, default: el.default ? true : false }
              },
              () => console.log(this.state.tempList)
            )
        );
      }
    );
  };

  watchlistAction = action => {
    const { watchlists, selected } = this.state;
    let updatedAction = action;
    if (action === 'deleteConfirmed') {
      watchlists.splice(selected, 1);
      updatedAction = null;
      if (watchlists.length > 0) {
        this.selectWatchlist();
      } else {
        this.setState({
          tempList: {}
        });
      }
    }
    if (action === 'new') {
      this.clearTempList();
    }
    if (action === 'duplicate') {
      let tempList = { ...this.state.tempList };
      tempList.name = tempList.name + ' copy';
      this.setState({
        tempList
      });
    }
    if (action === 'save') {
      const { tempList, tempItems, watchlists, selected } = this.state;
      let updateList = {
        name: tempList.name,
        items: tempItems
      };
      this.setState({
        watchlists: watchlists.map((el, index) =>
          index === selected ? Object.assign({}, el, updateList) : el
        )
      });
      updatedAction = null;
    }
    if (action === 'saveNew') {
      const { tempList, tempItems } = this.state;
      let newWatchlist = {
        name: tempList.name,
        items: tempItems
      };
      this.setState(
        {
          watchlists: [...watchlists, newWatchlist],
          newStep: 1
        },
        () =>
          this.state.watchlists.map((ww, i) => {
            if (newWatchlist.name === ww.name) {
              this.setState(
                {
                  selected: i
                },
                () => this.selectWatchlist(this.state.selected)
              );
            }
            return null;
          })
      );
      updatedAction = null;
    }
    this.setState({
      action: updatedAction
    });
  };

  isOverflowed = event => {
    return event;
  };

  addToOther = value => {
    this.setState({
      action: 'addToOther',
      addOtherItem: value
    });
    this.toggleModal(true);
  };

  toggleModal = value => {
    this.setState({
      modal: value ? value : !this.state.modal
    });
  };

  setAction = (value, code) => {
    this.setState({
      action: value,
      code: code
    });
  };

  render() {
    const { activeBreakpoints } = this.props;
    const {
      action,
      newStep,
      code,
      cloneList,
      nameError,
      watchlists,
      selected,
      tempList,
      tempItems,
      modal
    } = this.state;

    return (
      <LayoutDefault breadcrumb={Breadcrumbs.watchlist.items}>
        <HeroView title="Watchlists" />

        {watchlists.length > 0 || action === 'new' ? (
          <Section
            className={`${
              action === 'new' && watchlists.length === 0 ? 'py-10' : 'py-4'
            } ${activeBreakpoints.below.sm ? 'px-0' : ''}`}>
            <Container className="">
              <Columns mobile multiline>
                {watchlists.length > 0 && (
                  <Column
                    mobile={12}
                    tablet={4}
                    desktop={3}
                    widescreen={3}
                    className={activeBreakpoints.below.sm ? 'pb-0' : ''}>
                    {/* FOR MOBILE & TABLET ONLY START */}
                    <div className="is-hidden-tablet px-4 ">
                      <Columns mobile gapless className="">
                        <Column>
                          <Select
                            className="mb-1 mr-2"
                            options={cloneList}
                            value={this.state.selected}
                            onChange={this.selectWatchlist}
                          />
                        </Column>
                        <Column narrow>
                          <Button
                            square
                            className="ma-0"
                            onClick={() => this.watchlistAction('new')}>
                            <Icon>plus</Icon>
                          </Button>
                        </Column>
                      </Columns>
                    </div>
                    {/* FOR MOBILE & TABLET ONLY START */}

                    {/* FOR DESKTOP & ABOVE ONLY START */}
                    <div className="is-hidden-mobile">
                      <Button
                        block
                        className="mt-0 px-0 mb-2"
                        onClick={() => {
                          this.clearTempList();
                          this.watchlistAction('new');
                        }}>
                        <Icon small>plus</Icon>
                        <span>New watchlist</span>
                      </Button>
                      <Card>
                        <List>
                          {watchlists.map((item, w) => (
                            <ListItem
                              key={w}
                              bg={selected === w && 'secondary'}
                              color={selected === w && 'white'}
                              fontWeight={selected === w && 'bold'}
                              className={selected !== w && 'has-pointer'}
                              onClick={() => this.selectWatchlist(w)}>
                              <Level mobile className={`is-flex-1`}>
                                <LevelItem left>
                                  <Icon
                                    small
                                    className={`ml-0 mr-1 ${selected === w &&
                                      'has-text-white'}`}
                                    onClick={e => {
                                      e.stopPropagation();
                                      this.toggleDefault(w);
                                    }}>
                                    {item.default === true
                                      ? 'star'
                                      : 'star-outline'}
                                  </Icon>
                                  {item.name}
                                </LevelItem>
                                {selected !== w && (
                                  <LevelItem right>
                                    <Icon>arrow-right</Icon>
                                  </LevelItem>
                                )}
                              </Level>
                            </ListItem>
                          ))}
                        </List>
                      </Card>
                    </div>
                    {/* FOR DESKTOP & ABOVE ONLY END */}
                  </Column>
                )}

                <Column
                  mobile={12}
                  tablet={8}
                  desktop={9}
                  widescreen={watchlists.length > 0 ? null : 8}
                  widescreenOffset={watchlists.length > 0 ? null : 2}>
                  <Collapse isOpened={action !== 'duplicate' ? true : false}>
                    <Level
                      mobile
                      className={`mb-4 ${
                        activeBreakpoints.below.sm ? 'px-4' : ''
                      }`}>
                      <LevelItem left>
                        {newStep === 2 && action === 'new' ? (
                          <div>
                            <p className="nab-label mb-0">Watchlist</p>
                            <Title size="4" className="my-0">
                              {tempList.name}
                              {action !== 'new' && (
                                <span className="ml-4 pos-r has-top3">
                                  <Icon
                                    small
                                    className={`has-pointer ${
                                      watchlists[selected].default === true
                                        ? 'has-text-primary'
                                        : 'has-text-secondary'
                                    }`}
                                    onClick={e => {
                                      e.stopPropagation();
                                      this.toggleDefault(selected);
                                    }}>
                                    {tempList.default === true
                                      ? 'star'
                                      : 'star-outline'}
                                  </Icon>
                                </span>
                              )}
                            </Title>
                          </div>
                        ) : (
                          action !== 'new' && (
                            <div>
                              <p className="nab-label mb-0">Watchlist</p>
                              <Title size="4" className="my-0">
                                {tempList.name}
                                {action !== 'new' && (
                                  <span className="ml-4 pos-r has-top3">
                                    <Icon
                                      small
                                      className={`has-pointer ${
                                        tempList.default
                                          ? 'has-text-primary'
                                          : 'has-text-secondary'
                                      }`}
                                      onClick={e =>
                                        this.toggleDefault(selected)
                                      }>
                                      {tempList.default
                                        ? 'star'
                                        : 'star-outline'}
                                    </Icon>
                                  </span>
                                )}
                              </Title>
                            </div>
                          )
                        )}
                      </LevelItem>
                      {action !== 'new' && action !== 'duplicate' && (
                        <LevelItem right className="is-hidden-mobile">
                          <WatchListActions
                            action={action}
                            selected={selected}
                            edit={() => this.watchlistAction('edit')}
                            duplicate={() => this.watchlistAction('duplicate')}
                            delete={() => this.watchlistAction('delete')}
                            cancel={() => {
                              this.watchlistAction(null);
                              this.getSecurities(selected);
                            }}
                            save={() =>
                              this.watchlistAction(
                                action === 'edit' ? 'save' : 'saveNew'
                              )
                            }
                            null={() => this.watchlistAction(null)}
                            deleteConfirmed={() =>
                              this.watchlistAction('deleteConfirmed')
                            }
                          />
                        </LevelItem>
                      )}
                    </Level>
                  </Collapse>

                  <Collapse
                    isOpened={
                      action === 'edit' ||
                      action === 'new' ||
                      action === 'duplicate'
                        ? true
                        : false
                    }>
                    {action !== 'new' && action !== 'duplicate' && (
                      <Divider className="mb-2" />
                    )}
                    <div
                      className={`${activeBreakpoints.below.sm ? 'px-4' : ''}`}>
                      <Alert danger inverted isOpened={false} mb={4}>
                        Error message goes here...
                      </Alert>
                      {action !== 'new' ? (
                        <>
                          <Textfield
                            required
                            label="Name of watchlist"
                            placeholder="Enter name"
                            value={tempList && tempList.name}
                            onChange={this.updateName}
                            error={nameError}
                            errorMessage={nameError}
                          />
                          <Select
                            label={'Securities'}
                            options={Securities}
                            placeholder="Search..."
                            value={this.state.selected}
                            onChange={this.updateItems}
                            className="mb-4"
                          />
                        </>
                      ) : newStep === 1 ? (
                        <Textfield
                          required
                          label="Name of watchlist"
                          placeholder="Enter name"
                          value={tempList && tempList.name}
                          onChange={this.updateName}
                          error={nameError}
                          errorMessage={nameError}
                          className="mb-4"
                        />
                      ) : (
                        <Select
                          label={'Securities'}
                          options={Securities}
                          placeholder="Search..."
                          value={this.state.selected}
                          onChange={this.updateItems}
                          className="mb-4"
                        />
                      )}
                    </div>
                    {(action === 'new' || action === 'duplicate') && (
                      <div className={activeBreakpoints.below.sm ? 'px-4' : ''}>
                        {action === 'new' && newStep === 1 ? (
                          <Button
                            secondary
                            className="mt-0 ml-0 px-8"
                            onClick={() => {
                              this.setState({ newStep: 2 });
                            }}>
                            Next
                          </Button>
                        ) : (
                          <Button
                            secondary
                            className="mt-0 ml-0 px-8"
                            onClick={() => this.watchlistAction('saveNew')}>
                            Save
                          </Button>
                        )}
                        <Button
                          flat
                          className="mt-0 ml-0"
                          onClick={() => {
                            this.watchlistAction();
                            this.selectWatchlist();
                          }}>
                          Cancel
                        </Button>
                      </div>
                    )}
                  </Collapse>

                  <div className="is-hidden-tablet is-flex is-align-items-center px-4">
                    <WatchListActions
                      action={action}
                      selected={selected}
                      edit={() => this.watchlistAction('edit')}
                      duplicate={() => this.watchlistAction('duplicate')}
                      delete={() => this.watchlistAction('delete')}
                      cancel={() => {
                        this.watchlistAction(null);
                        this.getSecurities(selected);
                      }}
                      save={() =>
                        this.watchlistAction(
                          action === 'edit' ? 'save' : 'saveNew'
                        )
                      }
                      null={() => this.watchlistAction(null)}
                      deleteConfirmed={() =>
                        this.watchlistAction('deleteConfirmed')
                      }
                      className="mb-4"
                    />
                  </div>

                  <Divider
                    className={`${activeBreakpoints.below.sm ? '' : 'mt-4'}`}
                  />

                  {tempItems.length > 0 ? (
                    <Tabs className="mb-0">
                      <Tab label="Standard" icon="asset">
                        <Card>
                          <Table
                            hasStripes
                            isHoverable
                            headers={Headers}
                            data={tempItems}
                            tableRow={(row, i) => (
                              <TableRow
                                key={i}
                                {...row}
                                addOther={this.addToOther}
                                toggleModal={this.toggleModal}
                                action={this.setAction}
                              />
                            )}
                            defaultSortCol="code"
                            my={0}
                          />
                        </Card>
                        <div
                          className={`${
                            activeBreakpoints.below.sm ? 'px-4' : ''
                          }`}>
                          {tempItems.length > 4 && (
                            <Pagination value={1} total={10} className="mb-4" />
                          )}
                        </div>
                      </Tab>
                      <Tab label="Income" icon="money-bag">
                        <Income data={tempItems} />
                      </Tab>
                      <Tab label="Performance" icon="chart">
                        <Performance data={tempItems} />
                      </Tab>
                      <Tab label="Recommendations" icon="recommendation">
                        <Recommendations data={tempItems} />
                      </Tab>
                    </Tabs>
                  ) : (
                    <div className="has-text-centered py-8">
                      <Title
                        size="5"
                        className="mb-0 has-text-blue-grey text--lighten-2">
                        No securities found in this watchlist
                      </Title>
                      {action !== 'new' && (
                        <Subtitle
                          size="7"
                          className="mt-0 has-text-blue-grey text--lighten-1">
                          Click edit to add security/ies
                        </Subtitle>
                      )}
                    </div>
                  )}

                  <Collapse isOpened={!tempList ? true : false}>
                    <Title
                      size="5"
                      className="has-text-centered has-text-blue-grey text--lighten-3 my-8">
                      No securities found
                    </Title>
                  </Collapse>

                  {tempItems.length > 0 && (
                    <RealtimeQuoteMessage
                      local
                      international
                      currency
                      className={
                        activeBreakpoints.below.sm ? 'mx-4 mt-4' : 'mt-4'
                      }
                    />
                  )}
                </Column>
              </Columns>
            </Container>
          </Section>
        ) : (
          <NoWatchList newWatchlist={() => this.watchlistAction('new')} />
        )}
        <Modal
          closeIcon
          onClose={() => this.setState({ modal: false })}
          isOpened={modal}
          header={
            action === 'addToOther'
              ? `Add ${this.state.addOtherItem} to watchlists`
              : null
          }
          footer={
            action === 'addToOther' ? (
              <React.Fragment>
                <Button
                  secondary
                  className="ml-0 px-4"
                  onClick={() => this.setState({ modal: false })}>
                  Apply
                </Button>
                <Button
                  flat
                  className="ml-0"
                  onClick={() => this.setState({ modal: false })}>
                  Cancel
                </Button>
              </React.Fragment>
            ) : null
          }>
          {action === 'addToOther' ? (
            <AddToOtherModalContent
              watchlists={watchlists}
              tempList={tempList}
            />
          ) : action === 'more' ? (
            <MoreModalContent code={code} addOther={this.addToOther} />
          ) : null}
        </Modal>
      </LayoutDefault>
    );
  }
}

export default Breakpoints(Watchlist);

const AddToOtherModalContent = ({ watchlists, tempList }) => (
  <CheckboxGroup stacked label={`Select watchlists:`} value={[tempList.name]}>
    {watchlists.map((item, i) => (
      <Checkbox
        key={i}
        value={item.name}
        label={`${item.name} ${
          tempList.name === item.name ? '(Current list)' : ''
        }`}
        disabled={tempList.name === item.name}
      />
    ))}
  </CheckboxGroup>
);

const MoreModalContent = ({ ...props }) => (
  <React.Fragment>
    <Button fullwidth primary className="mb-4">
      Trade {props.code}
    </Button>
    <Button fullwidth>Edit cost base</Button>
    <Button
      fullwidth
      className="mb-4"
      onClick={() => props.addOther(props.code)}>
      Add to other watchlists
    </Button>
    <Button fullwidth>Set up Alert</Button>
    <Button fullwidth>Set up Stop Loss Order</Button>
    <Button fullwidth>Set up Take Profit Order</Button>
  </React.Fragment>
);
