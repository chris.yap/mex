import React from 'react';
import {
  Button,
  Card,
  Dropdown,
  Icon,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';
import PercentageView from '../../components/PercentageView';

const Performance = ({ data, className, ...props }) => {
  return (
    <Card>
      <Table
        hasStripes
        isHoverable
        headers={Headers}
        data={data}
        tableRow={TableRow}
        defaultSortCol="code"
        my={0}
        className={`${className}`}
        {...props}
      />
    </Card>
  );
};

export default Performance;

const Headers = [
  { name: 'Code', sort: true },
  {
    name: 'Last',
    align: 'right'
  },
  {
    name: 'Today',
    align: 'right'
  },
  {
    name: '1 month',
    align: 'right'
  },
  {
    name: '12 months',
    align: 'right'
  },
  {
    name: 'Since added',
    align: 'right'
  },
  {
    name: (
      <span>
        52-week
        <br />
        low/high
      </span>
    ),
    align: 'right'
  },
  {}
];
const TableRow = ({ overflow, row, ...props }) => (
  <Row>
    <Cell>
      {row.code}{' '}
      {row.alert && (
        <Icon small number={row.alert} className="pos-r has-top3">
          bell
        </Icon>
      )}
    </Cell>
    <Cell className="has-text-right">${row.last}</Cell>
    <Cell className="has-text-right">
      ${row.change} <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">
      ${row.change} <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">
      ${row.change} <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">
      ${row.change} <PercentageView value={row.changePercent} />
    </Cell>
    <Cell className="has-text-right">
      {row.change}/{row.change}
    </Cell>
    <Cell className="has-text-right">
      {overflow ? (
        <Button small square flat secondary>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square flat secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2">Edit cost base</ListItem>
            <ListItem className="py-2">Set up Alert</ListItem>
            <ListItem className="py-2">Set up Stop Loss Order</ListItem>
            <ListItem className="py-2 has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);
