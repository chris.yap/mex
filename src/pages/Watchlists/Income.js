import React from 'react';
import {
  Button,
  Card,
  Dropdown,
  Icon,
  List,
  ListItem,
  Table,
  TableCell as Cell,
  TableRow as Row
} from 'shaper-react';

class Income extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      income: []
    };
  }
  componentDidMount = () => {
    const { data } = this.props;
    let arr = [];
    if (data) {
      data.map(d => {
        let obj;
        Divs.map(div => {
          if (d.code.toLowerCase() === div.code.toLowerCase()) {
            obj = div;
            obj.alert = d.alert;
            arr.push(obj);
          }
          return null;
        });
        return null;
      });
    }
    this.setState({
      income: arr
    });
  };
  render() {
    const { income } = this.state;
    const { data, className, ...props } = this.props;
    return (
      <Card>
        <Table
          hasStripes
          isHoverable
          headers={Headers}
          data={income}
          tableRow={TableRow}
          defaultSortCol="code"
          my={0}
          className={`${className}`}
          {...props}
        />
      </Card>
    );
  }
}

export default Income;

const Divs = [
  {
    code: 'bhp',
    yield: '0.45',
    date: '23.11.2018',
    amount: '12.12',
    credit: '28.21'
  },
  {
    code: 'nab',
    yield: '2.24',
    date: '03.03.2018',
    amount: '13.67',
    credit: '31.15'
  },
  {
    code: 'appl',
    yield: '3.14',
    date: '17.09.2017',
    amount: '15.25',
    credit: '3.19'
  },
  {
    code: 'tsla',
    yield: '1.05',
    date: '15.02.2014',
    amount: '19.60',
    credit: '11.96'
  },
  {
    code: 'googl',
    yield: '1.06',
    date: '02.03.2017',
    amount: '6.78',
    credit: '12.93'
  },
  {
    code: 'cba',
    yield: '1.43',
    date: '24.04.2015',
    amount: '11.62',
    credit: '18.94'
  }
];

const Headers = [
  { name: 'code', value: 'code', sort: true },
  {
    name: 'Div yield',
    sort: true,
    align: 'right'
  },
  {
    name: 'Next div date',
    sort: true,
    align: 'centered'
  },
  {
    name: 'Div amount',
    sort: true,
    align: 'right'
  },
  {
    name: 'Franking credit',
    sort: true,
    align: 'right'
  },
  {}
];
const TableRow = ({ overflow, row, ...props }) => (
  <Row>
    <Cell>
      <span className="is-uppercase">{row.code}</span>{' '}
      {row.alert && (
        <Icon small number={row.alert} className="pos-r has-top3">
          bell
        </Icon>
      )}
    </Cell>
    <Cell className="has-text-right">
      {row.yield}
      <sup className="pos-r has-top4">%</sup>
    </Cell>
    <Cell className="has-text-centered">{row.date}</Cell>
    <Cell className="has-text-right">${row.amount}</Cell>
    <Cell className="has-text-right">
      {row.credit}
      <sup className="pos-r has-top4">%</sup>
    </Cell>
    <Cell className="has-text-right">
      {overflow ? (
        <Button small square flat secondary>
          <Icon>more-vertical</Icon>
        </Button>
      ) : (
        <Dropdown
          right
          activator={
            <Button small square flat secondary>
              <Icon>more-vertical</Icon>
            </Button>
          }>
          <List>
            <ListItem
              className="py-2"
              onClick={() => props.clickFunc('orderpad', row)}>
              Trade
            </ListItem>
            <ListItem className="py-2">Edit cost base</ListItem>
            <ListItem className="py-2">Set up Alert</ListItem>
            <ListItem className="py-2">Set up Stop Loss Order</ListItem>
            <ListItem className="py-2 has-text-nowrap">
              Set up Take Profit Order
            </ListItem>
          </List>
        </Dropdown>
      )}
    </Cell>
  </Row>
);
