import React from 'react';
import { Breakpoints, Container, Hero, Title } from 'shaper-react';

const WatchListHero = ({ activeBreakpoints, ...props }) => (
  <Hero bg="black" py={4}>
    <Container>
      <Title className="font-nabimpact has-text-white my-0">Watchlists</Title>
    </Container>
  </Hero>
);

export default Breakpoints(WatchListHero);
